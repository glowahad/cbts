﻿###########################
# Greek Events
###########################

add_namespace = gre_internal 

country_event = { #Greek Coup of 1935(March 1, 1935)
	id = gre_internal.2
	title = gre_internal.2.t
	desc = gre_internal.2.d
	picture = GFX_Event_USSR_Battle_of_Stalingrad 
	is_triggered_only = yes

	option = {
		name = gre_internal.2.a
		ai_chance = {
			base = 100
		}
		#
	}
}

country_event = { #Greek Monarchist Coup (October 10, 1935)
	id = gre_internal.3
	title = gre_internal.3.t
	desc = gre_internal.3.d
	picture = GFX_Event_USSR_Battle_of_Stalingrad 
	is_triggered_only = yes

	option = {##Establish Regency Council
		name = gre_internal.3.a
		ai_chance = {
			base = 100
		}
		set_politics = {
			ruling_party = monarchism
		}
		add_popularity = {
   			ideology = monarchism
    		popularity = 0.15
		}
		create_country_leader = {
			name = "Regency Council"
			desc = "POLITICS_GREEK_REGENCY_DESC"
			picture = "Portrait_Greek_Regency_Council.tga"
			expire = "1965.1.1"
			ideology = national_conservatism
			traits = {
				#
			}
		}
		create_country_leader = {
			name = "Regency Council"
			desc = "POLITICS_GREEK_REGENCY_DESC"
			picture = "Portrait_Greek_Regency_Council.tga"
			expire = "1965.1.1"
			ideology = oligarchism
			traits = {
				#
			}
		}
		create_country_leader = {
			name = "Regency Council"
			desc = "POLITICS_GREEK_REGENCY_DESC"
			picture = "Portrait_Greek_Regency_Council.tga"
			expire = "1965.1.1"
			ideology = fascism_ideology
			traits = {
				#
			}
		}
		create_country_leader = {
			name = "Regency Council"
			desc = "POLITICS_GREEK_REGENCY_DESC"
			picture = "Portrait_Greek_Regency_Council.tga"
			expire = "1965.1.1"
			ideology = metaxism
			traits = {
				#
			}
		}

		set_cosmetic_tag = GRE_kingdom

		country_event = {
			id = gre_internal.4
			days = 1
		}

		hidden_effect = {
			load_focus_tree = {
				tree = GEN_rightist_EU
				keep_completed = yes
			}
			COALITION_add_aut = yes	
			COALITION_add_nat = yes	
			set_country_flag = gre_mon_coup_yes
		}	
	}
}

country_event = { #Referendum for Monarchy (November 3, 1935)
	id = gre_internal.4
	title = gre_internal.4.t
	desc = gre_internal.4.d
	picture = GFX_Event_USSR_Battle_of_Stalingrad 
	is_triggered_only = yes

	option = {#Referendum
		name = gre_internal.4.a
		ai_chance = {
			base = 100
		}
		add_popularity = {
   			ideology = monarchism
    		popularity = 0.05
		}
		country_event = {
			id = gre_internal.5
			days = 1
		}
		hidden_effect = {
			#
		}	
	}
}

country_event = { #George II Return (November 25, 1935)
	id = gre_internal.5
	title = gre_internal.5.t
	desc = gre_internal.5.d
	picture = GFX_Event_USSR_Battle_of_Stalingrad 
	is_triggered_only = yes

	option = {#Coronation of George II
		name = gre_internal.5.a
		ai_chance = {
			base = 100
		}
		add_popularity = {
   			ideology = monarchism
    		popularity = 0.10
		}
		create_country_leader = {
			name = "George II"
			desc = "POLITICS_GEORGE_II_DESC"
			picture = "Portrait_George_II_of_Greece.tga"
			expire = "1965.1.1"
			ideology = social_conservatism
			traits = {
				#
			}
		}
		create_country_leader = {
			name = "George II"
			desc = "POLITICS_GEORGE_II_DESC"
			picture = "Portrait_George_II_of_Greece.tga"
			expire = "1965.1.1"
			ideology = oligarchism
			traits = {
				#
			}
		}
		create_country_leader = {
			name = "George II"
			desc = "POLITICS_GEORGE_II_DESC"
			picture = "Portrait_George_II_of_Greece.tga"
			expire = "1965.1.1"
			ideology = metaxism
			traits = {
				#
			}
		}
		create_country_leader = {
			name = "George II"
			desc = "POLITICS_GEORGE_II_DESC"
			picture = "Portrait_George_II_of_Greece.tga"
			expire = "1965.1.1"
			ideology = fascism_ideology
			traits = {
				#
			}
		}
		country_event = {
			id = gre_internal.6
			days = 1
		}
		hidden_effect = {
			#
		}	
	}
}

country_event = { #Konstantinos Demertzis as PM (November 30, 1935)
	id = gre_internal.6
	title = gre_internal.6.t
	desc = gre_internal.6.d
	picture = GFX_Event_USSR_Battle_of_Stalingrad 
	is_triggered_only = yes

	option = {
		name = gre_internal.6.a
		ai_chance = {
			base = 100
		}
		hidden_effect = {
			remove_ideas_with_trait = hog
			remove_ideas_with_trait = frnmin
			remove_ideas_with_trait = ecomin
			remove_ideas_with_trait = secmin
			remove_ideas_with_trait = intmin
		}	
		add_ideas = {
			GRE_hog_Konstantinos_Demertzis
			GRE_frn_Konstantinos_Demertzis
			GRE_eco_George_Mantzavinos
		}
		country_event = {
			id = gre_internal.7
			days = 1
		}		
	}
}

country_event = { #Greek Election of 1936 (January 26, 1936)
	id = gre_internal.7
	title = gre_internal.7.t
	desc = gre_internal.7.d
	picture = GFX_Event_USSR_Battle_of_Stalingrad 
	is_triggered_only = yes

	option = {
		name = gre_internal.7.a
		ai_chance = {
			base = 100
		}

		set_politics = {
			ruling_party = monarchism
		}

		set_popularities = {
			communism = 2
			authoritarian_socialism = 2
			socialism = 2
			social_democracy = 1
			social_liberalism = 2
			market_liberalism = 2
			jacobin = 42 
			democratic = 24
			neutrality = 10
			monarchism = 10
			nationalism = 3
			fascism = 0 
		}

		country_event = {
			id = gre_internal.8
			days = 1
		}	

		hidden_effect = {
			#
		}	
	}
}

country_event = { #George II appoints Metaxas the Minister of Defence (March 5, 1936)
	id = gre_internal.8
	title = gre_internal.8.t
	desc = gre_internal.8.d
	picture = GFX_Event_USSR_Battle_of_Stalingrad 
	is_triggered_only = yes

	option = {
		name = gre_internal.8.a
		ai_chance = {
			base = 100
		}

		hidden_effect = {
			remove_ideas_with_trait = coa
		}	
		add_ideas = {
			GRE_coa_Ioannis_Metaxas
		}
		country_event = {
			id = gre_internal.9
			days = 1
		}	
	}
}

country_event = { #Goverment of Demertzis (March 14, 1936)
	id = gre_internal.9
	title = gre_internal.9.t
	desc = gre_internal.9.d
	picture = GFX_Event_USSR_Battle_of_Stalingrad 
	is_triggered_only = yes

	option = {
		name = gre_internal.9.a
		ai_chance = {
			base = 100
		}

		hidden_effect = {
			remove_ideas_with_trait = hog
			remove_ideas_with_trait = frnmin
			remove_ideas_with_trait = ecomin
			remove_ideas_with_trait = secmin
			remove_ideas_with_trait = intmin
		}	
		add_ideas = {
			GRE_hog_Konstantinos_Demertzis
			GRE_frn_Konstantinos_Demertzis
			GRE_eco_George_Mantzavinos
			GRE_sec_George_Logothetis
		}

		country_event = {
			id = gre_internal.10
			days = 1
		}	

		hidden_effect = {
			#
		}	
	}
}

country_event = { #Demertzis Death, Metaxas as PM (April 13, 1936)
	id = gre_internal.10
	title = gre_internal.10.t
	desc = gre_internal.10.d
	picture = GFX_Event_USSR_Battle_of_Stalingrad 
	is_triggered_only = yes

	option = {
		name = gre_internal.10.a
		ai_chance = {
			base = 100
		}

		hidden_effect = {
			remove_ideas_with_trait = hog
			remove_ideas_with_trait = frnmin
			remove_ideas_with_trait = ecomin
			remove_ideas_with_trait = secmin
			remove_ideas_with_trait = intmin
		}	
		add_ideas = {
			GRE_hog_Ioannis_Metaxas
			GRE_frn_Ioannis_Metaxas
			GRE_eco_Nikolaos_Kanellopoulos
			GRE_sec_George_Logothetis
		}

		country_event = {
			id = gre_internal.11
			days = 1
		}	

		hidden_effect = {
			#
		}	
	}
}

country_event = { #Vote of Confidence for Metaxas (April 27, 1936)
	id = gre_internal.11
	title = gre_internal.11.t
	desc = gre_internal.11.d
	picture = GFX_Event_USSR_Battle_of_Stalingrad 
	is_triggered_only = yes

	option = {
		name = gre_internal.11.a
		ai_chance = {
			base = 100
		}

		#głosowanie

		country_event = {
			id = gre_internal.12
			days = 1
		}	

		hidden_effect = {
			#
		}	
	}
}

country_event = { #Macedonian Strikes (April 29, 1936)
	id = gre_internal.12
	title = gre_internal.12.t
	desc = gre_internal.12.d
	picture = GFX_Event_USSR_Battle_of_Stalingrad 
	is_triggered_only = yes

	option = {
		name = gre_internal.12.a
		ai_chance = {
			base = 100
		}

		#info

		country_event = {
			id = gre_internal.13
			days = 1
		}

		hidden_effect = {
			#
		}	
	}
}

country_event = { #State of Emergency, Creation of 4 August Regime (August 4, 1936)
	id = gre_internal.13
	title = gre_internal.13.t
	desc = gre_internal.13.d
	picture = GFX_Event_USSR_Battle_of_Stalingrad 
	is_triggered_only = yes

	option = {
		name = gre_internal.13.a
		ai_chance = {
			base = 100
		}

		set_politics = {
			ruling_party = nationalism
		}

		hidden_effect = {
			#
		}	
	}
}