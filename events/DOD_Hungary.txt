﻿###########################
# Hungarian Events
###########################

add_namespace = DOD_hungary

#Hungary demands transylvania - agree?
country_event = {
	id = DOD_hungary.50
	title = DOD_hungary.50.t
	desc = DOD_hungary.50.desc
	picture = GFX_report_event_vienna_award_negotiations
	
	is_triggered_only = yes

	option = {#yes 
		name = DOD_hungary.50.a
		ai_chance = {
			factor = 10
			modifier = {
				is_historical_focus_on = yes 
				factor = 0
			}
			modifier = {
				strength_ratio = { tag = HUN ratio < 0.8 }
				factor = 4
			}
		}
		HUN = { country_event = { id = DOD_hungary.51 } }
		add_stability = -0.25
		add_war_support = -0.1

	}

	option = {#no 
		name = DOD_hungary.50.b
		ai_chance = {
			factor = 90
		}
		HUN = { country_event = { id = DOD_hungary.52 } }
	}
}

#Romania hands over Transylvania
country_event = {
	id = DOD_hungary.51
	title = DOD_hungary.51.t
	desc = DOD_hungary.51.desc
	picture = GFX_report_event_vienna_award_hungary
	
	is_triggered_only = yes

	trigger = {
		NOT = {
			has_global_flag = Balkan_crisis_mediation_requested
		}
	}

	option = {# 
		name = DOD_hungary.51.a
		hidden_effect = {
			ROM = {
				set_country_flag = second_vienna_accepted_all #needed for event bulgaria.4
			}
		}
		HUN = {
			if = {
				limit = {
					ROM = {
						controls_state = 76
					}
				}
				transfer_state = 76
			}

			if = {
				limit = {
					ROM = {
						controls_state = 83
					}
				}
				transfer_state = 83
			}
			if = {
				limit = {
					ROM = {
						controls_state = 1048
					}
				}
				transfer_state = 1048
			}	
			if = {
				limit = {
					ROM = {
						controls_state = 1054
					}
				}
				transfer_state = 1054
			}		
			if = {
				limit = {
					ROM = {
						controls_state = 1046
					}
				}
				transfer_state = 1046
			}
			if = {
				limit = {
					ROM = {
						controls_state = 1047
					}
				}
				transfer_state = 1047
			}
			if = {
				limit = {
					ROM = {
						controls_state = 84
					}
				}
				transfer_state = 84
			}
			
			if = {
				limit = {
					ROM = {
						controls_state = 82
					}
				}
				transfer_state = 82
			}
		}
		add_threat = -1
		if = {
			limit = {
				has_idea = HUN_war_preparation
			}
			remove_ideas = HUN_war_preparation
		}
		hidden_effect = {
			news_event = { id = HUN_news.301 days = 1 }
		}
	}
}

#Romania refuses to hand over Transylvania
country_event = {
	id = DOD_hungary.52
	title = DOD_hungary.52.t
	desc = DOD_hungary.52.desc
	picture = GFX_report_event_generic_read_write
	
	is_triggered_only = yes

	option = {# Let it go
		name = DOD_hungary.52.a
		ai_chance = {
			factor = 20
			modifier = {
				strength_ratio = { tag = ROM ratio < 1 } #weaker than Romania
				factor = 4
			}
			modifier = {
				is_historical_focus_on = yes
				factor = 0
			}
		}
		add_stability = -0.2
		add_war_support = -0.1
		remove_state_claim = 76
		remove_state_claim = 83
		remove_state_claim = 84
		add_threat = -1
	}

	option = {# Prepare for war!
		name = DOD_hungary.52.b
		ai_chance = {
			factor = 80
			modifier = {
				strength_ratio = { tag = ROM ratio > 1.2 }
				factor = 3
			}
		}
		add_ideas = HUN_war_preparation
		add_war_support = 0.1
		add_threat = 2
		hidden_effect = {
			news_event = { id = HUN_news.302 days = 1 }
		}
	}
}

#Demand Southern Slovakia event chain - initial event for CZE or SLO, depending on time frame
country_event = {
	id = DOD_hungary.80
	title = DOD_hungary.80.t
	desc = DOD_hungary.80.desc 
	picture = GFX_report_event_generic_rally2
	
	is_triggered_only = yes
	immediate = {
		hidden_effect = {
			news_event = { id = HUN_news.303 }
		}
	}
	option = {# fold
		name = DOD_hungary.80.a
		ai_chance = {
			factor = 75
		}
		HUN = { country_event = DOD_hungary.81 } 
		hidden_effect = {
			news_event = { id = HUN_news.304 days = 1 }
		}
		
	}
	option = {# Refuse outright
		name = DOD_hungary.80.d
		HUN = { country_event = DOD_hungary.84 }
	}

}

#Demand Southern Slovakia event chain - CZE/SLO folds
country_event = {
	id = DOD_hungary.81
	title = DOD_hungary.81.t
	desc = DOD_hungary.81.desc 
	picture = GFX_report_event_german_polish_border
	
	is_triggered_only = yes

	option = {# great
		name = DOD_hungary.81.a
		transfer_state = 664
		
	}
}

#Demand Southern Slovakia event chain - CZE/SLO flatly refuses
country_event = {
	id = DOD_hungary.84
	title = DOD_hungary.84.t
	desc = DOD_hungary.84.desc 
	picture = GFX_report_event_hungary_refused
	
	is_triggered_only = yes

	option = {# fold
		name = DOD_hungary.84.a
		ai_chance = {
			factor = 10
			modifier = {
				strength_ratio = { tag = CZE ratio < 1 }
				factor = 4
			}
			modifier = {
				strength_ratio = { tag = CZE ratio < 0.5 }
				factor = 10
			}
		}
		add_stability = -0.05
		
	}
	option = {# war!
		name = DOD_hungary.84.b
		ai_chance = {
			factor = 90
			modifier = {
				country_exists = SLO
				SLO = { is_puppet = no }
				SLO = { is_in_faction = no }
				factor = 10
			}
		}
		add_war_support = 0.05
		if = {
			limit = { 
				CZE = {
					owns_state = 664
				}
			}

			create_wargoal = {
				type = take_state_focus
				target = CZE
				generator = { 664 }
			}
		}
		if = {
			limit = { 
				SLO = {
					owns_state = 664
				}
			}

			create_wargoal = {
				type = take_state_focus
				target = SLO
				generator = { 664 }
			}
		}
		hidden_effect = {
			news_event = { id = HUN_news.305 days = 1 }
		}
	}
}

#Demand Vojvodina event chain - intial event for Yugoslavia
country_event = {
	id = DOD_hungary.90
	title = DOD_hungary.90.t
	desc = DOD_hungary.90.desc 
	picture = GFX_report_event_hungary_parliament
	
	is_triggered_only = yes

	option = {# fold
		name = DOD_hungary.90.a
		ai_chance = {
			factor = 30
			modifier = {
				is_in_faction = no
				HUN = { is_in_faction = yes }
				factor = 2
			}
			modifier = {
				strength_ratio = { tag = HUN ratio < 1 }
				factor = 2
			}
		}
		HUN = { country_event = DOD_hungary.91 }
		news_event = { id = HUN_news.307 days = 1 }
	}
	option = {# refuse
		name = DOD_hungary.90.b
		ai_chance = {
			factor = 70
			modifier = {
				has_stability < 0.5
				factor = 0.5
			}
		}
		HUN = { country_event = DOD_hungary.92 }
		
	}
}

#Demand Vojvodina event chain - Yugoslavia folds
country_event = {
	id = DOD_hungary.91
	title = DOD_hungary.91.t
	desc = DOD_hungary.91.desc 
	picture = GFX_report_event_generic_peaceful_annexation
	
	is_triggered_only = yes

	option = {# great
		name = DOD_hungary.91.a
	  	transfer_state = 45
	  	transfer_state = 840
	  	transfer_state = 1031
	  	45 = { add_core_of = HUN }
	  	840 = { add_core_of = HUN}
	}
}

#Demand Vojvodina event chain - Yugoslavia refuses
country_event = {
	id = DOD_hungary.92
	title = DOD_hungary.92.t
	desc = DOD_hungary.92.desc 
	picture = GFX_report_event_hungary_refused
	
	is_triggered_only = yes

	option = {# back down
		name = DOD_hungary.92.a
		ai_chance = {
			factor = 10
			modifier = {
				has_war = yes
				factor = 2
			}
		}
		add_stability = -0.05

	}
	option = {# Go alone
		name = DOD_hungary.92.e
		ai_chance = {
			factor = 0
			modifier = {
				add = 10
				AND = {
					NOT = { is_in_faction_with = GER }
					NOT = { is_in_faction_with = ITA }
				}
			}
		}
		if = {
			limit = {
				NOT = { has_wargoal_against = YUG }
			}
			create_wargoal = {
				type = take_state_focus
				target = YUG
				generator = { 45 }
			}
		}
	}
}

#Demand Vojvodina event chain - ultimatum
country_event = {
	id = DOD_hungary.96
	title = DOD_hungary.96.t
	desc = {
		text = DOD_hungary.96.desc_HUN_GER
		trigger = { 
			102 = { is_claimed_by = GER } 
			103 = { NOT = { is_claimed_by = ITA } }
		}
	}
	desc = {
		text = DOD_hungary.96.desc_HUN_ITA
		trigger = { 
			102 = { NOT = { is_claimed_by = GER } }
			103 = { is_claimed_by = ITA } 
		}
	}
	desc = {
		text = DOD_hungary.96.desc_HUN_both
		trigger = { 
			102 = { is_claimed_by = GER } 
			103 = { is_claimed_by = ITA } 
		}
	} 
	picture = GFX_report_event_hitler_parade
	
	is_triggered_only = yes

	option = {# fold
		name = DOD_hungary.96.a
		ai_chance = {
			factor = 75
		}
		if = {
			limit = {
				102 = { is_claimed_by = GER }
			}
			GER = { transfer_state = 102 }
		}
		if = {	
			limit = {
				103 = { is_claimed_by = ITA }
			}
			ITA = { transfer_state = 103 }
		}
		HUN = { transfer_state = 45 }
		45 = { add_core_of = HUN }
	}
	option = {# defiance
		name = DOD_hungary.96.b
		ai_chance = {
			factor = 25
			modifier = {
				is_in_faction = yes
				factor = 4
			}
		}
		HUN = { country_event = DOD_hungary.97 }
		if = {	
			limit = {
				103 = { is_claimed_by = ITA }
			}
			ITA = { country_event = DOD_hungary.97 }
		}
		if = {	
			limit = {
				102 = { is_claimed_by = GER }
			}
			GER = { country_event = DOD_hungary.97 }
		}
		
	}
}

#Demand Vojvodina event chain - Yugoslavia being stubborn
country_event = {
	id = DOD_hungary.97
	title = DOD_hungary.97.t
	desc = DOD_hungary.97.desc 
	picture = GFX_report_event_polish_army
	
	is_triggered_only = yes

	option = {# war
		name = DOD_hungary.97.a
		if = {
			limit = { 
				tag = GER
				102 = { is_claimed_by = GER } }
			GER = { 
				create_wargoal = {
					type = take_state_focus
					target = YUG
					generator = { 102 }
				}
			}
		}
		if = {
			limit = { 
				tag = ITA
				103 = { is_claimed_by = ITA } 
			}
			ITA = { 
				create_wargoal = {
					type = take_state_focus
					target = YUG
					generator = { 163 }
				}
			}
		}
		if = {
			limit = {
				tag = HUN
			}	
			HUN = {
				create_wargoal = {
					type = take_state_focus
					target = YUG
					generator = { 45 840 }
				}
				45 = { add_core_of = HUN }
				840 = { add_core_of = HUN }
			}
		}
		hidden_effect = {
			news_event = { id = HUN_news.308 days = 1 }
		}
	}
}

#Demand Overlordship over Slovakia - CZE holds Slovakia
country_event = {
	id = DOD_hungary.100
	title = DOD_hungary.100.t
	desc = DOD_hungary.100.desc 
	picture = GFX_report_event_hungary_parliament
	
	is_triggered_only = yes

	option = {# hand over Slovakia
		name = DOD_hungary.100.a
		ai_chance = {
			factor = 30
			modifier = {
				strength_ratio = { tag = HUN ratio < 1 }
				factor = 2
			}
		}
		HUN = { country_event = DOD_hungary.101 }
		hidden_effect = {
			news_event = { id = HUN_news.310 days = 1 }
		}
	}

	option = {# refuse
		name = DOD_hungary.100.b
		ai_chance = {
			factor = 70
			modifier = {
				has_war = yes
				factor = 0.5
			}
			modifier = {
				is_in_faction = yes
				factor = 5
			}
		}
		HUN = { country_event = DOD_hungary.102 }
	}
}

#Demand Overlordship over Slovakia - CZE agrees, Hungary gets options on whether to annex or puppet
country_event = {
	id = DOD_hungary.101
	title = DOD_hungary.101.t
	desc = DOD_hungary.101.desc 
	picture = GFX_report_event_military_planning
	
	is_triggered_only = yes

	option = {# annex
		name = DOD_hungary.101.a
		ai_chance = {
			factor = 70
			modifier = {
				country_exists = SLO
				factor = 0
			}
		}
		HUN = {
			transfer_state = 70
			transfer_state = 71
			transfer_state = 72
			transfer_state = 73
		}
		if = {
			limit = {
				country_exists = CZE
			}
			CZE = {
				set_cosmetic_tag = CZE_ONLY
			}
		}
	}

	option = {# puppet
		name = DOD_hungary.101.b
		ai_chance = {
			factor = 30
		}
		if = {
			limit = {
				70 = {
					is_owned_by = CZE
				}
			}
			SLO = { transfer_state = 70 }
		}
		if = {
			limit = {
				71 = {
					is_owned_by = CZE	
				}
			}
			SLO = { transfer_state = 71 }
		}
		if = {
			limit = {
				72 = {
					is_owned_by = CZE	
				}
			}
			SLO = { transfer_state = 72 }
		}
		if = {
			limit = {
				154 = { is_owned_by = HUN }
				73 = {
					is_owned_by = CZE
				}
			}
			HUN = { transfer_state = 73 }
		}
		if = {
			limit = {
				664 = {
					is_owned_by = CZE
				}
			}
			HUN = { transfer_state = 664 }
		}
		every_state = {
				limit = {
					is_owned_by = SLO
				}
				add_core_of = SLO
			}
		SLO = {
				load_oob = SLO_1939
				add_manpower = 9876
				add_state_core = 664
				add_state_core = 73
			}
		puppet = SLO
		if = {
			limit = {
				country_exists = CZE
			}
			CZE = {
				set_cosmetic_tag = CZE_ONLY
			}
		}
	}
}

#Demand Overlordship over Slovakia - CZE refuses to hand over Slovakia
country_event = {
	id = DOD_hungary.102
	title = DOD_hungary.102.t
	desc = DOD_hungary.102.desc 
	picture = GFX_report_event_czech_soldiers_01
	
	is_triggered_only = yes

	option = {# let it go
		name = DOD_hungary.102.a
		ai_chance = {
			factor = 10
			modifier = {
				is_in_faction = no
				CZE = {
					is_in_faction = yes
				}
				factor = 10
			}
		}
	}

	option = {# ask Germany to participate in the split
		name = DOD_hungary.102.b
		ai_chance = {
			factor = 90
			modifier = {
				is_in_faction_with = GER
				factor = 10
			}
		}
		GER = { country_event = DOD_hungary.103 }
	}
}

#Demand Overlordship over Slovakia - GER asked to split CZE with Hungary
country_event = {
	id = DOD_hungary.103
	title = DOD_hungary.103.t
	desc = DOD_hungary.103.desc 
	picture = GFX_report_event_hitler_croatia_handshake
	
	is_triggered_only = yes

	option = {# yes
		name = DOD_hungary.103.a
		ai_chance = {
			factor = 75
			modifier = {
				has_government = democratic
				factor = 0
			}
		}
		set_global_flag = GER_HUN_CZE_split
		HUN = { 
			country_event = DOD_hungary.104 
			add_opinion_modifier = { target = GER modifier = HUN_split_CZE }
			add_ai_strategy = {
					type = alliance
					id = "GER"
					value = 200
				}
		}

	}

	option = {# refuse
		name = DOD_hungary.103.b
		ai_chance = {
			factor = 25
		}
		HUN = { country_event = DOD_hungary.105 }
	}
}

#Demand Overlordship over Slovakia - GER agrees to split CZE with Hungary
country_event = {
	id = DOD_hungary.104
	title = DOD_hungary.104.t
	desc = DOD_hungary.104.desc 
	picture = GFX_report_event_generic_sign_treaty3
	
	is_triggered_only = yes

	option = {# great
		name = DOD_hungary.104.a
	}	
}
#Demand Overlordship over Slovakia - GER refuses to split CZE with Hungary
country_event = {
	id = DOD_hungary.105
	title = DOD_hungary.105.t
	desc = DOD_hungary.105.desc 
	picture = GFX_report_event_german_inspect_troops
	
	is_triggered_only = yes

	option = {# let it go
		name = DOD_hungary.105.a
		ai_chance = {
			factor = 10
		}
	}

	option = {# war goal
		name = DOD_hungary.105.b
		ai_chance = {
			factor = 90
		}
		create_wargoal = {
				type = take_state_focus
				target = CZE
				generator = { 70 71 72 73 }
			}
		hidden_effect = {
			news_event = { id = HUN_news.311 days = 1 }
		}
	}
}
#Demand Overlordship over Slovakia - GER betrays promise
country_event = {
	id = DOD_hungary.106
	title = DOD_hungary.106.t
	desc = DOD_hungary.106.desc 
	picture = GFX_report_event_german_troops
	
	is_triggered_only = yes

	option = {# fume
		name = DOD_hungary.106.a
		ai_chance = {
			factor = 5
		}
		remove_opinion_modifier = { target = GER modifier = HUN_split_CZE }
		add_opinion_modifier = { target = GER modifier = HUN_split_CZE_betrayed }
	}

	option = {# take what is ours
		name = DOD_hungary.106.b
		ai_chance = {
			factor = 95
		}
		remove_opinion_modifier = { target = GER modifier = HUN_split_CZE }
		add_opinion_modifier = { target = GER modifier = HUN_split_CZE_betrayed }
		create_wargoal = {
				type = take_state_focus
				target = CZE
				generator = { 70 71 72 73 }
		}
		hidden_effect = {
			news_event = { id = HUN_news.311 days = 1 }
		}
	}	
}

#Demand Overlordship over Slovakia - Slovakia independent
country_event = {
	id = DOD_hungary.107
	title = DOD_hungary.107.t
	desc = {
		text = DOD_hungary.107.desc_SLO_independent
		trigger = { 
			SLO = { is_subject = no }
		}
	}
	desc = {
		text = DOD_hungary.107.desc_SLO_puppet
		trigger = { 
			SLO = { is_subject = yes }
		}
	}
	picture = GFX_report_event_lithuania_army
	
	is_triggered_only = yes

	option = {# agree to being a puppet
		name = DOD_hungary.107.a
		ai_chance = {
			factor = 50
		}
		HUN = { country_event = DOD_hungary.108 }
		hidden_effect = {
			news_event = { id = HUN_news.310 days = 1 }
		}
	}

	option = {# refuse
		name = DOD_hungary.107.b
		ai_chance = {
			factor = 50
			modifier = {
				is_in_faction = yes
				HUN = { is_in_faction = no }
				factor = 5
			}
		}
		HUN = { country_event = DOD_hungary.109 }
		
	}	
}

#Demand Overlordship over Slovakia - Slovakia agrees to become a puppet
country_event = {
	id = DOD_hungary.108
	title = DOD_hungary.108.t
	desc = DOD_hungary.108.desc 
	picture = GFX_report_event_generic_read_write
	
	is_triggered_only = yes

	option = {# good
		name = DOD_hungary.108.a
		puppet = SLO
	}

	
}

#Demand Overlordship over Slovakia - Slovakia refuses to become a puppet
country_event = {
	id = DOD_hungary.109
	title = DOD_hungary.109.t
	desc = DOD_hungary.109.desc 
	picture = GFX_report_event_military_planning
	
	is_triggered_only = yes

	option = {# war!
		name = DOD_hungary.109.a
		create_wargoal = {
				type = puppet_wargoal_focus
				target = SLO
		}
		hidden_effect = {
			news_event = { id = HUN_news.311 days = 1 }
		}
	}	
}

#Bled Agreement - Hungary asks for rearmament - France or Romania asked first
country_event = {
	id = DOD_hungary.140
	title = DOD_hungary.140.t
	desc = DOD_hungary.140.desc 
	picture = GFX_report_event_hungary_parliament
	
	is_triggered_only = yes

	option = {# allow
		name = DOD_hungary.140.a
		ai_chance = {
			factor = 75
			modifier = {
				threat < 0.5
				factor = 2 #what's the worst that could happen
			}
			modifier = {
				has_war = yes
				factor = 0.25
			}
			modifier = {
				HUN = {
					has_government = democratic
				}
				factor = 3
			}
		}
		if = { 
			limit = { tag = FRA }
			set_global_flag = HUN_FRA_engaged
			ROM = { country_event = { id = DOD_hungary.141 days = 4 } }
			YUG = { country_event = { id = DOD_hungary.141 days = 4 } }
			if = {
				limit = {
					CZE = {
						is_subject = no
					}
				}
				CZE = { country_event = { id = DOD_hungary.141 days = 4 } }
			}
		}
		if = { 
			limit = { tag = ROM }
			YUG = { country_event = { id = DOD_hungary.141 days = 4 } }
			if = {
				limit = {
					CZE = {
						is_subject = no
					}
				}
				CZE = { country_event = { id = DOD_hungary.141 days = 4 } }
			}
		}
		HUN = {
			country_event = { id = DOD_hungary.142 days = 3 }
		}
	}

	option = {# refuse
		name = DOD_hungary.140.b
		ai_chance = {
			factor = 25
			modifier = {
				is_historical_focus_on = yes
				factor = 0
			}
		}
		HUN = { country_event = { id = DOD_hungary.144 days = 3 } }
	}
}

#Bled Agreement - ROM/FRA has agreed, minors get to issue a response
country_event = {
	id = DOD_hungary.141
	title = DOD_hungary.141.t
	desc = DOD_hungary.141.desc 
	picture = GFX_report_event_journalists_speech
	
	is_triggered_only = yes

	option = {# intervene
		name = DOD_hungary.141.a
		trigger = {
			HUN = {
				OR = {
					has_government = fascism
					has_added_tension_amount > 0.05 #hungary has been bad
				}
			}
		}
		ai_chance = {
			factor = 10
			modifier = {
				is_historical_focus_on = yes
				factor = 0
			}
		}
		create_wargoal = {
			type = puppet_wargoal_focus
			target = HUN
		}
		add_political_power = -200
		add_stability = -0.15
		add_war_support = -0.1
		add_threat = 5
	}
	option = {# issue a protest
		name = DOD_hungary.141.b
		ai_chance = {
			factor = 45
		}
		add_opinion_modifier = { target = HUN modifier = protest_action }
		HUN = { country_event = DOD_hungary.143 }
	}

	option = {# accept it
		name = DOD_hungary.141.c
		ai_chance = {
			factor = 45
		}
	}
}

#Bled Agreement - HUN permitted to rearm
country_event = {
	id = DOD_hungary.142
	title = DOD_hungary.142.t
	desc = DOD_hungary.142.desc 
	picture = GFX_report_event_generic_military_parade
	
	is_triggered_only = yes

	option = {# 
		name = DOD_hungary.142.a
		if = {
			limit = { has_idea = HUN_treaty_of_trianon }
				remove_ideas = HUN_treaty_of_trianon
		}
	}	
}

#Bled Agreement - Neighbours issue a protest
country_event = {
	id = DOD_hungary.143
	title = DOD_hungary.143.t
	desc = DOD_hungary.143.desc 
	picture = GFX_report_event_generic_read_write
	
	is_triggered_only = yes

	option = {# good for them
		name = DOD_hungary.143.a
	}
	
}

#Bled Agreement - Hungary denied rearmament
country_event = {
	id = DOD_hungary.144
	title = DOD_hungary.144.t
	desc = DOD_hungary.144.desc 
	picture = GFX_report_event_generic_read_write
	
	is_triggered_only = yes

	option = {# accept
		name = DOD_hungary.144.a
		ai_chance = {
			factor = 10
		}
		add_stability = -0.1
	}
}

#Bled Agreement - Hungary denied rearmament
country_event = {
	id = DOD_hungary.148
	title = DOD_hungary.148.t
	desc = DOD_hungary.148.desc 
	picture = GFX_report_event_hungary_refused
	
	is_triggered_only = yes

	option = {# yield
		name = DOD_hungary.148.a
		ai_chance = {
			factor = 20
			modifier = {
				has_government = fascism
				factor = 0
			}
		}
	}

	option = {# rearm anyway
		name = DOD_hungary.148.b
		ai_chance = {
			factor = 80
		}
		add_threat = 5
		remove_ideas = HUN_treaty_of_trianon
	}
}

#Hungary rearms since there are no one left to oppose
country_event = {
	id = DOD_hungary.170
	title = DOD_hungary.170.t
	desc = DOD_hungary.170.desc 
	picture = GFX_report_event_generic_military_parade
	
	mean_time_to_happen = { days = 2 }

	trigger = {
		#Trigger matches bypass trigger for normal rearm process
		tag = HUN
			has_idea = HUN_treaty_of_trianon
		AND = {
			ROM = {
				OR = {
					exists = no
					is_puppet = yes
				}
			}
			FRA = {
				OR = {
					exists = no
					is_puppet = yes
				}
			}
		}
	}

	option = {# Great
		name = DOD_hungary.170.a
		if = {
			limit = { has_idea = HUN_treaty_of_trianon }
			remove_ideas = HUN_treaty_of_trianon
		}
	}
}

#Gyula Gömbös Dies
country_event = {
	id = DOD_hungary.171
	title = DOD_hungary.171.t
	desc = DOD_hungary.171.desc 
	picture = GFX_report_event_generic_military_parade

	mean_time_to_happen = { days = 6 }

	fire_only_once = yes

	trigger = {
		TAG = HUN
		HUN = {
			exists = yes
			is_puppet = no
		}
		date > 1936.10.1
	}

	option = {
		name = DOD_hungary.171.a
		set_country_flag = purged_gyula_gombos_de_jakfa
		remove_ideas = HUN_gyula_gombos_de_jakfa

	}
}

#King Zog of Albania married Geraldine Apponyi of Hungary.
country_event = {
	id = DOD_hungary.172
	title = DOD_hungary.172.t
	desc = DOD_hungary.172.desc 
	picture = GFX_report_event_generic_military_parade

	mean_time_to_happen = { days = 27 }

	fire_only_once = yes

	trigger = {
		NOT = { has_war_with = ALB }
		ALB = {
			exists = yes
		}
		tag = HUN
		HUN = {
			exists = yes
			is_puppet = no
		}
		date > 1938.4.1
	}

	option = {
		name = DOD_hungary.172.a
	}
}

#Startup_event
country_event = {
	id = DOD_hungary.173
	title = DOD_hungary.173.t
	desc = DOD_hungary.173.desc 
	picture = GFX_report_event_hungary_southern_slovakia

	is_triggered_only = yes

	fire_only_once = yes

	option = {
		name = DOD_hungary.173.a
		ai_chance = {
			base = 100
		}
	}

	option = {
		name = DOD_hungary.173.b
		add_stability = -0.01
		ai_chance = {
			base = 100
		}
	}
}

#Treaty of Eternal Friendship with Yugoslavia
country_event = {
	id = DOD_hungary.174
	title = DOD_hungary.174.t
	desc = DOD_hungary.174.desc 
	picture = GFX_report_event_por_salazar_franco2

	mean_time_to_happen = { days = 12 }

	fire_only_once = yes

	trigger = {
		NOT = { has_war_with = YUG }
		YUG = {
			exists = yes
		}
		tag = HUN
		HUN = {
			exists = yes
			is_puppet = no
			OR = {                    #Random Yugoslavian states near the Hungarian border. Only way I know to ensure we haven't had a war against them.
				NOT = { 
					controls_state = 45
					controls_state = 109
					controls_state = 909
				}
			}
		}
		date > 1940.12.1
	}

	option = {
		name = DOD_hungary.174.a
			set_country_flag = HUN_YUG_non_aggression
			diplomatic_relation = {
    			country = YUG
    			relation = non_aggression_pact
    			active = yes
			}
		ai_chance = {
			base = 100
		}
	}

	option = {
		name = DOD_hungary.174.b
	}
}

#Treaty of Eternal Friendship broken
country_event = {
	id = DOD_hungary.175
	title = DOD_hungary.175.t
	desc = DOD_hungary.175.desc
	picture = GFX_report_event_generic_military_parade

	fire_only_once = yes

	trigger = {
		tag = HUN
		has_war_with = YUG
		has_country_flag = HUN_YUG_non_aggression
	}

	option = {
		name = DOD_hungary.175.a
		add_stability = -0.03
		ai_chance = {
			base = 100
		}
		set_country_flag = purged_pal_teleki
		remove_ideas = HUN_pal_teleki
	}
}

#Anti fascist Marches 
country_event = {
	id = DOD_hungary.176
	title = DOD_hungary.176.t
	desc = DOD_hungary.176.desc
	picture = GFX_report_event_hitler_parade

	fire_only_once = yes

	trigger = {
		NOT = { has_government = fascism }
		original_tag = HUN
		fascism > 0.15
		controls_state = 834
	}

	option = {					#People have the right to their own opinion	
		name = DOD_hungary.176.a
		add_popularity = {
    		ideology = fascism
    		popularity = 0.02
			}
		ai_chance = {
			base = 100
		}
	}

	option = {					#Fascism is the road to destruction					
		name = DOD_hungary.176.b
		add_popularity = {
    		ideology = fascism
    		popularity = -0.02
			}
	}
}

#Anti fascist Marches 2
country_event = {
	id = DOD_hungary.177
	title = DOD_hungary.177.t
	desc = DOD_hungary.177.desc
	picture = GFX_report_event_hitler_parade

	fire_only_once = yes

	trigger = {
		NOT = { has_government = fascism }
		original_tag = HUN
		fascism > 0.22
		controls_state = 834
	}

	option = {					#People have the right to their own opinion	
		name = DOD_hungary.177.a
		add_popularity = {
    		ideology = fascism
    		popularity = 0.02
			}
		ai_chance = {
			base = 100
		}
	}

	option = {					#Fascism is the road to destruction					
		name = DOD_hungary.177.b
		add_popularity = {
    		ideology = fascism
    		popularity = -0.02
			}
	}
}

#Anti fascist Marches 3
country_event = {
	id = DOD_hungary.178
	title = DOD_hungary.178.t
	desc = DOD_hungary.178.desc
	picture = GFX_report_event_hitler_parade

	fire_only_once = yes

	trigger = {
		NOT = { has_government = fascism }
		original_tag = HUN
		fascism > 0.28
		controls_state = 834
	}

	option = {					#People have the right to their own opinion	
		name = DOD_hungary.178.a
		add_popularity = {
    		ideology = fascism
    		popularity = 0.02
			}
		ai_chance = {
			base = 100
		}
	}

	option = {					#Fascism is the road to destruction					
		name = DOD_hungary.178.b
		add_popularity = {
    		ideology = fascism
    		popularity = -0.02
			}
	}
}

#Anti fascist Marches 4
country_event = {
	id = DOD_hungary.179
	title = DOD_hungary.179.t
	desc = DOD_hungary.179.desc
	picture = GFX_report_event_hitler_parade

	fire_only_once = yes

	trigger = {
		NOT = { has_government = fascism }
		original_tag = HUN
		fascism > 0.34
		controls_state = 834
	}

	option = {					#People have the right to their own opinion	
		name = DOD_hungary.179.a
		add_popularity = {
    		ideology = fascism
    		popularity = 0.02
			}
		ai_chance = {
			base = 100
		}
	}

	option = {					#Fascism is the road to destruction					
		name = DOD_hungary.179.b
		add_popularity = {
    		ideology = fascism
    		popularity = -0.02
			}
	}
}

#Anti fascist Marches 5
country_event = {
	id = DOD_hungary.180
	title = DOD_hungary.180.t
	desc = DOD_hungary.180.desc
	picture = GFX_report_event_hitler_parade

	fire_only_once = yes

	trigger = {
		NOT = { has_government = fascism }
		original_tag = HUN
		fascism > 0.39
		controls_state = 834
	}

	option = {					#People have the right to their own opinion	
		name = DOD_hungary.180.a
		add_popularity = {
    		ideology = fascism
    		popularity = 0.02
			}
		ai_chance = {
			base = 100
		}
	}

	option = {					#Fascism is the road to destruction					
		name = DOD_hungary.180.b
		add_popularity = {
    		ideology = fascism
    		popularity = -0.02
			}
	}
}

#Germany demands to place troops in Hungary (Fall Weiss)
country_event = {
	id = DOD_hungary.181
	title = DOD_hungary.181.t
	desc = DOD_hungary.181.desc
	picture = GFX_report_event_hungary_refused

	fire_only_once = yes	

	trigger = {
		tag = HUN
		controls_state = 73
		has_war = no
		NOT = {
			is_in_faction_with = GER
		}
		GER = {
			has_completed_focus = GER_annex_cze
			OR = {
				has_completed_focus = GER_war_with_poland
				has_war_with = POL
			}
		}
	}

	option = {					#Refuse
		name = DOD_hungary.181.a
		set_country_flag = HUN_refused
		add_stability = 0.02
		add_opinion_modifier = {
			target = GER
    		modifier = HUN_refuses
		}
		ai_chance = {
			base = 100
		}		
	}
	option = {					#Accept
		name = DOD_hungary.181.b
		add_stability = -0.02
		GER = { add_to_faction = HUN }
		if = {
			limit = {
				GER = {
				has_war_with = POL
				}
			}
		HUN = {
			add_to_war = {
				targeted_alliance = GER
				enemy = POL
				}
			}
		}
	}
}

#Germany demands to place troops in Hungary (Operation 25)
country_event = {
	id = DOD_hungary.182
	title = DOD_hungary.182.t
	desc = DOD_hungary.182.desc
	picture = GFX_report_event_hungary_refused

	fire_only_once = yes	

	trigger = {
		tag = HUN
		has_country_flag = HUN_refused
		has_war = no
		NOT = {
			is_in_faction_with = GER
		}
		GER = {
			OR = {
				has_war_with = YUG
				has_wargoal_against	= YUG
			}
		}
	}

	option = {					#Refuse
		name = DOD_hungary.182.a
		add_stability = 0.02
		add_opinion_modifier = {
			target = GER
    		modifier = HUN_refuses
		}	
		GER = {
			create_wargoal = {
    			type = puppet_wargoal_focus
    			target = HUN
			}			
		}	
	}
	option = {					#Accept
		name = DOD_hungary.182.b
		add_stability = -0.02
		GER = { add_to_faction = HUN }
		if = {
			limit = {
				GER = {
					has_war_with = POL
				}
			}
		HUN = {
			add_to_war = {
				targeted_alliance = GER
				enemy = YUG
				}
			}
		}
		ai_chance = {
			base = 100
		}
	}
}

country_event = {
	id = DOD_hungary.183
	title = DOD_hungary.183.t
	desc = DOD_hungary.183.desc
	picture = GFX_report_event_hungary_refused

	fire_only_once = yes	

	trigger = {
		tag = HUN
		has_war = no
		has_completed_focus = HUN_secret_rearmament
		has_stability < 0.45
	}	

	option = {					#Let them worry
		name = DOD_hungary.183.a
		add_stability = 0.05
		ai_chance = {
			base = 100
		}		
	}
	option = {					#Perhaps they are right
		name = DOD_hungary.183.b
		add_stability = -0.05
		if = {
			limit = {
				has_idea = HUN_secret_rearmament_idea
			}
			remove_ideas = HUN_secret_rearmament_idea
		}
		ai_chance = {
			base = 0
		}		
	}
}