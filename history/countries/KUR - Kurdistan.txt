﻿capital = 350

add_state_core = 350
add_state_core = 352
add_state_core = 807
add_state_core = 808
oob = "KUR_1933"

set_research_slots = 3

set_technology = {

	infantry_weapons1 = 1
}

set_politics = {
	ruling_party = jacobin
	last_election = "1933.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 13
	authoritarian_socialism = 0
	socialism = 21
	social_democracy = 10
	social_liberalism = 7
	market_liberalism = 8
	jacobin = 26
	democratic = 0
	neutrality = 0
	monarchism = 2
	nationalism = 13
	fascism = 0
}


