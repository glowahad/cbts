﻿capital = 300

oob = "URG_1936"

# Starting tech
set_technology = {
	infantry_weapons1 = 1
	gw_artillery = 1
	gw_fighter = 1
	plane_engine1 = 1
	early_destroyer = 1
	early_light_cruiser = 1
}
set_country_flag = monroe_doctrine

set_convoys = 5

set_politics = {

	ruling_party = social_liberalism
	last_election = "1930.11.30"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 2
	authoritarian_socialism = 0
	socialism = 2
	social_democracy = 1
	social_liberalism = 30
	market_liberalism = 0
	jacobin = 3
	democratic = 27
	neutrality = 25
	monarchism = 5
	nationalism = 5
	fascism = 0
}

COALITION_add_CENTRIST = yes
COALITION_add_aut = yes
COALITION_clr_socdem = yes

create_country_leader = {
	name = "Consejo Nacional de Administración"
	desc = "POLITICS_GABRIEL_TERRA_DESC"
	picture = "Portrait_Uruguay_National_Council.tga"
	expire = "1965.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Gabriel Terra"
	desc = "POLITICS_GABRIEL_TERRA_DESC"
	picture = "Portrait_Uruguay_Gabriel_Terra.tga"
	expire = "1965.1.1"
	ideology = authoritarian
	traits = {
		#
	}
}

create_country_leader = {
	name = "Emilio Frugoni"
	desc = "POLITICS_GABRIEL_TERRA_DESC"
	picture = "Portrait_Uruguay_Emilio_Frugoni.dds"
	expire = "1965.1.1"
	ideology = real_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Emilio Frugoni"
	desc = "POLITICS_GABRIEL_TERRA_DESC"
	picture = "Portrait_Uruguay_Emilio_Frugoni.dds"
	expire = "1965.1.1"
	ideology = dem_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Consejo Nacional de Administración"
	desc = "POLITICS_GABRIEL_TERRA_DESC"
	picture = "Portrait_Uruguay_National_Council.tga"
	expire = "1965.1.1"
	ideology = conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Consejo Nacional de Administración"
	desc = "POLITICS_GABRIEL_TERRA_DESC"
	picture = "Portrait_Uruguay_National_Council.tga"
	expire = "1965.1.1"
	ideology = centrism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Juan Amezaga"
	desc = "POLITICS_GABRIEL_TERRA_DESC"
	picture = "Portrait_Uruguay_Juan_Amezaga.tga"
	expire = "1965.1.1"
	ideology = market_liberal
	traits = {
		#
	}
}
