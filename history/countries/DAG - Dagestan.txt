capital = 232

add_state_core = 232
add_state_core = 831
oob = "DAG_1933"

set_research_slots = 3

set_technology = {

	infantry_weapons1 = 1
}

set_politics = {
	ruling_party = neutrality
	last_election = "1933.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 20
	authoritarian_socialism = 6
	socialism = 3
	social_democracy = 7
	social_liberalism = 2
	market_liberalism = 1
	jacobin = 11
	democratic = 8
	neutrality = 35
	monarchism = 1
	nationalism = 6
	fascism = 0
}

create_country_leader = {
	name = "Gaidar Bammatov"
	desc = "POLITICS__DESC"
	picture = "Gaidar_Bammatov.tga"
	expire = "1965.1.1"
	ideology = social_nationalism
	traits = { }
}
create_country_leader = {
	name = "Gaidar Bammatov"
	desc = "POLITICS__DESC"
	picture = "Gaidar_Bammatov.tga"
	expire = "1965.1.1"
	ideology = real_socialism
	traits = { }
}
create_country_leader = {
	name = "Gaidar Bammatov"
	desc = "POLITICS__DESC"
	picture = "Gaidar_Bammatov.tga"
	expire = "1965.1.1"
	ideology = dem_socialism
	traits = { }
}
create_country_leader = {
	name = "Gaidar Bammatov"
	desc = "POLITICS__DESC"
	picture = "Gaidar_Bammatov.tga"
	expire = "1965.1.1"
	ideology = jadidism
	traits = { }
}
create_country_leader = {
	name = "Gaidar Bammatov"
	desc = "POLITICS__DESC"
	picture = "Gaidar_Bammatov.tga"
	expire = "1965.1.1"
	ideology = market_liberal
	traits = { }
}
create_country_leader = {
	name = "Gaidar Bammatov"
	desc = "POLITICS__DESC"
	picture = "Gaidar_Bammatov.tga"
	expire = "1965.1.1"
	ideology = agrarianism
	traits = { }
}
create_country_leader = {
	name = "Gaidar Bammatov"
	desc = "POLITICS__DESC"
	picture = "Gaidar_Bammatov.tga"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = { }
}
create_country_leader = {
	name = "Gaidar Bammatov"
	desc = "POLITICS__DESC"
	picture = "Gaidar_Bammatov.tga"
	expire = "1965.1.1"
	ideology = big_tent
	traits = { }
}
create_country_leader = {
	name = "Gaidar Bammatov"
	desc = "POLITICS__DESC"
	picture = "Gaidar_Bammatov.tga"
	expire = "1965.1.1"
	ideology = despotism
	traits = { }
}
create_country_leader = {
	name = "Gaidar Bammatov"
	desc = "POLITICS__DESC"
	picture = "Gaidar_Bammatov.tga"
	expire = "1965.1.1"
	ideology = nationalist
	traits = { }
}
create_country_leader = {
	name = "Gaidar Bammatov"
	desc = "POLITICS__DESC"
	picture = "Gaidar_Bammatov.tga"
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = { }
}
create_field_marshal = {
	name = "Nukh-Bek Tarkovsky"
	picture = "Nukh_Bek_Tarkovsky.tga"
	traits = {
		cavalry_officer
		cavalry_leader
		war_hero
		old_guard
	}
	skill = 3
	attack_skill = 4
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 3
}
