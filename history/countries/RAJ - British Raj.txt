﻿capital = 439

oob = "RAJ_1936"

# Starting tech
set_technology = {
	infantry_weapons1 = 1
	tech_support = 1		
	tech_engineers = 1
	tech_recon = 1
	transport = 1
	
}

set_stability = 0.70
set_war_support = 0.50

if = {
	limit = { has_dlc = "Together for Victory" }


	add_to_tech_sharing_group = commonwealth_research
}

COALITION_POP_NUMBER = yes
COALITION_add_nat = yes

add_ideas = {
	RAJ_Hindu_Muslim_tension
	RAJ_British_India_Army
	RAJ_Indian_Independance_Movement
	RAJ_Princely_States
	illiterate_population
	underdeveloped_industry
}
set_convoys = 20

set_cosmetic_tag = RAJ_UK # British Raj

set_politics = {

	ruling_party = neutrality
	last_election = "1930.9.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 1
	authoritarian_socialism = 5
	socialism = 5
	social_democracy = 30
	social_liberalism = 1
	market_liberalism = 1
	jacobin = 1
	democratic = 20
	neutrality = 31
	monarchism = 2
	nationalism = 3
	fascism = 0
}


create_country_leader = {
	name = "Freeman Freeman-Thomas"
	desc = "POLITICS_LORD_LINLITHGOW_DESC"
	picture = "Portrait_British_Raj_Lord_Willingdon.tga"
	expire = "1965.1.1"
	ideology = colonial
	traits = {
		
	}
}

create_country_leader = {
	name = "Nellie Sengupta"
	desc = "POLITICS_INDIA_DESC"
	#picture = "Portrait_British_Raj.tga"
	expire = "1934.10.1"
	ideology = dem_socialism
	traits = {
		
	}
}

create_country_leader = {
	name = "Rajendra Prasad"
	desc = "POLITICS_INDIA_DESC"
	#picture = "Portrait_British_Raj.tga"
	expire = "1936.10.1"
	ideology = dem_socialism
	traits = {
		
	}
}
create_country_leader = {
	name = "Jawaharlal Nehru"
	desc = "POLITICS_INDIA_DESC"
	#picture = "Portrait_British_Raj.tga"
	expire = "1938.10.1"
	ideology = dem_socialism
	traits = {
		
	}
}
create_country_leader = {
	name = "Subhas Chandra Bose"
	desc = "POLITICS_INDIA_DESC"
	#picture = "Portrait_British_Raj.tga"
	expire = "1939.10.1"
	ideology = dem_socialism
	traits = {
		
	}
}
create_country_leader = {
	name = "Rajendra Prasad"
	desc = "POLITICS_INDIA_DESC"
	#picture = "Portrait_British_Raj.tga"
	expire = "1940.10.1"
	ideology = dem_socialism
	traits = {
		
	}
}
create_country_leader = {
	name = "Abul Kalam Azad	"
	desc = "POLITICS_INDIA_DESC"
	##picture = "Portrait_British_Raj.tga"
	expire = "1945.10.1"
	ideology = dem_socialism
	traits = {
		
	}
}
create_country_leader = {
	name = "Keshav Baliram Hedgewar"
	desc = "POLITICS__DESC"
	#picture = "GFX_RAJ_Keshav_Hedgewar"
	expire = "1965.1.1"
	ideology = clerical
	traits = {
		
	}
}

create_country_leader = {
	name = "Vinayak Damodar Savarkar"
	desc = "POLITICS_VD_SAVARKAR_DESC"
	#picture = "GFX_RAJ_veer_savarkar"
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = {
		
	}
}
create_country_leader = {
	name = "Acharya Narendra Deva"
	desc = "POLITICS_INDIA_DESC"
	#picture = "Portrait_British_Raj.tga"
	expire = "1940.10.1"
	ideology = real_socialism
	traits = {
		
	}
}
create_country_leader = {
	name = "Jayaprakash Narayan"
	desc = "POLITICS_INDIA_DESC"
	#picture = "Portrait_British_Raj.tga"
	expire = "1940.10.1"
	ideology = social_nationalism 
	traits = {
		
	}
}
create_country_leader = {
	name = "Anugrah Narayan Sinha"
	desc = "POLITICS_INDIA_DESC"
	#picture = "Portrait_British_Raj.tga"
	expire = "1965.1.1"
	ideology = liberalism
	traits = {
		
	}
}
create_country_leader = {
	name = "Muhammad Ali Jinnah"
	desc = "POLITICS_INDIA_DESC"
	picture = "Portrait_Muhammad_Ali_Jinnah.tga"
	expire = "1950.1.1"
	ideology = liberal_nationalism
	traits = {
		
	}
}
create_country_leader = {
	name = "Madhav Shrihari Aney"
	desc = "POLITICS_INDIA_DESC"
	#picture = "Portrait_British_Raj.tga"
	expire = "1946.1.1"
	ideology = national_conservatism
	traits = {
		
	}
}
create_country_leader = {
	name = "Mohan Singh"
	desc = "POLITICS_INDIA_DESC"
	picture = "Portrait_British_Raj_Mohan_Singh.tga"
	expire = "1946.1.1"
	ideology = nationalist
	traits = {
		
	}
}


create_country_leader = {
	name = "Puran Chand Joshi"
	desc = "POLITICS_INDIA_DESC"
	#picture = "Portrait_British_Raj.tga"
	expire = "1965.1.1"
	ideology = leninism
	traits = {
		
	}
}

create_country_leader = {
	name = "B. P. Sitaramayya"
	desc = "POLITICS_B_P_SITARAMAYYA_DESC"
	picture = "GFX_RAJ_pattabhi_sitaramayya"
	expire = "1965.1.1"
	ideology = social_nationalism
	traits = {
		
	}
}

create_corps_commander = {
	name = "Noel Beresford-Peirse"
	gfx = "GFX_RAJ_noel_beresford_peirse"
	traits = { panzer_leader }
	skill = 3
}

create_corps_commander = {
	name = "Frank Messervy"
	gfx = "GFX_RAJ_frank_messervy"
	traits = { hill_fighter }
	skill = 3
}

create_corps_commander = {
	name = "Douglas Gracey"
	gfx = "GFX_RAJ_douglas_gracey"
	traits = { desert_fox }
	skill = 3
}

create_navy_leader = {
	name = "Herbert Fitzherbert"
	gfx = "GFX_RAJ_herbert_fitzherbert"
	traits = { }
	skill = 3
}
