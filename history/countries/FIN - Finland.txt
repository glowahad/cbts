﻿capital = 111

oob = "FIN_1936"
if = {
	limit = { has_dlc = "Man the Guns" }
		set_naval_oob = "FIN_1936_naval_mtg"
	else = {
		set_naval_oob = "FIN_1936_naval_legacy"
	}
}

set_research_slots = 3

add_ideas = {
	limited_conscription
}

set_technology = {
	#infantry
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	motorised_infantry = 1
	support_weapons1 = 1
	infantry_grenade = 1
	armoured_car1 = 1
	bike_infantry = 1
	motorcycle_infantry = 1
	
	#Support	
	tech_recon = 1
	tech_support = 1		
	tech_engineers = 1
	tech_military_police = 1
	
	#Artillery
	gw_artillery = 1
	interwar_artillery = 1
	
	#Airplane
	gw_fighter = 1
	gw_bomber = 1
	
	#Electronic mechanical engineering
	electronic_mechanical_engineering = 1
	radio = 1
	tank_engine1 = 1
	plane_engine1 = 1
	diesel_engine = 1
	
	#Industry	
	great_war_assembly_line = 1
	factory_electrification = 1
	industry1 = 1
	basic_construction_methods = 1
}
if = {
	limit = { not = { has_dlc = "Man the Guns" } }
	set_technology = {
		early_submarine = 1
		early_heavy_cruiser = 1
	}
}
if = {
	limit = { has_dlc = "Man the Guns" }
	set_technology = {
		basic_naval_mines = 1
		submarine_mine_laying = 1
		early_ship_hull_light = 1
		early_ship_hull_submarine = 1
		early_ship_hull_cruiser = 1
		basic_battery = 1
		basic_secondary_battery = 1
		coastal_defense_ships = 1
	}
}
set_convoys = 5

add_ideas = {
	limited_conscription
	popularity_30_35
	FIN_Agrarian_Reforms
	FIN_Great_Depression
	FIN_Radical_Agitation
	FIN_Language_Issues
}
set_politics = {
	ruling_party = market_liberalism
	last_election = "1930.10.1"
	election_frequency = 36
	elections_allowed = no
}
set_popularities = {
	communism = 3
	authoritarian_socialism = 2
	socialism = 11
	social_democracy = 20
	social_liberalism = 2
	market_liberalism = 6
	jacobin = 24
	democratic = 27
	neutrality = 0
	monarchism = 0
	nationalism = 1
	fascism = 4
}

set_stability = 0.75
set_war_support = 0.30

COALITION_add_marlib = yes #adds Market Liberalism
COALITION_add_cen = yes #adds Liberal Conservatism
COALITION_POP_NUMBER = yes #sets the starting popularity
set_country_flag = FIN_Parliament
set_variable = {
	FIN_com_seats = 0
}
set_variable = {
	FIN_revsoc_seats = 0
}
set_variable = {
	FIN_demsoc_seats = 23
}
set_variable = {
	FIN_socdem_seats = 43
}
set_variable = {
	FIN_soclib_seats = 1
}
set_variable = {
	FIN_marlib_seats = 11
}
set_variable = {
	FIN_libcon_seats = 59
}
set_variable = {
	FIN_soccon_seats = 62
}
set_variable = {
	FIN_aut_seats = 1
}
set_variable = {
	FIN_autdes_seats = 0
}
set_variable = {
	FIN_faraut_seats = 0
}
set_variable = {
	FIN_fas_seats = 0
}
set_variable = {
	FIN_total_seats = 200
}
set_variable = {
	FIN_seats_needed = 100
}
FIN_get_coalition_seats = yes

create_country_leader = {
	name = "Risto Heikki Ryti"
	desc = "POLITICS_RISTO_HEIKKI_RYT_DESC"
	picture = "Portrait_Finland_Risto_Heikki_Ryti.dds"
	expire = "1965.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Aimo Aaltonen"
	desc = "POLITICS_AIMO_AALTONEN_DESC"
	picture = "Portrait_Finland_Aimo_Aaltonen.tga"
	expire = "1965.1.1"
	ideology = stalinism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Vihtori Kosola"
	desc = "POLITICS_VILHO_ANNALA_DESC"
	picture = "Portrait_Finland_Vihtori_Kosola.tga"
	expire = "1965.1.1"
	ideology = clerical
	traits = {
		#
	}
}
create_country_leader = {
	name = "Aarne Valle"
	desc = "POLITICS_VILHO_ANNALA_DESC"
	picture = "Portrait_Finland_Aarne_Valle.dds"
	expire = "1965.1.1"
	ideology = nationalist
	traits = {
		#
	}
}

create_country_leader = {
	name = "Kyösti Kallio"
	desc = "POLITICS_KYÖSTI_KALLIO_DESC"
	picture = "Portrait_Finland_Kyosti_Kallio.dds"
	expire = "1965.1.1"
	ideology = agrarianism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Pehr Evind Svinhufvud"
	desc = "POLITICS_KYÖSTI_KALLIO_DESC"
	picture = "Portrait_Finland_Pehr_Evind_Svinhufvud.dds"
	expire = "1965.1.1"
	ideology = conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Ernst von Born"
	desc = "POLITICS_KYÖSTI_KALLIO_DESC"
	picture = "Portrait_Finland_Ernst_von_Born.dds"
	expire = "1965.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Väinö Salovaara"
	desc = "POLITICS_KYÖSTI_KALLIO_DESC"
	picture = "Portrait_Finland_Umlaut_Man.dds"
	expire = "1965.1.1"
	ideology = dem_socialism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Pehr Evind Svinhufvud"
	desc = "POLITICS_SVINHUFVUD_DESC"
	picture = "Portrait_Finland_Pehr_Evind_Svinhufvud.tga"
	expire = "1965.1.1"
	ideology = stalinism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Pehr Evind Svinhufvud"
	desc = "POLITICS_SVINHUFVUD_DESC"
	picture = "Portrait_Finland_Pehr_Evind_Svinhufvud.tga"
	expire = "1965.1.1"
	ideology = rev_socialism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Pehr Evind Svinhufvud"
	desc = "POLITICS_SVINHUFVUD_DESC"
	picture = "Portrait_Finland_Pehr_Evind_Svinhufvud.tga"
	expire = "1965.1.1"
	ideology = real_socialism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Pehr Evind Svinhufvud"
	desc = "POLITICS_SVINHUFVUD_DESC"
	picture = "Portrait_Finland_Pehr_Evind_Svinhufvud.tga"
	expire = "1965.1.1"
	ideology = dem_socialism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Pehr Evind Svinhufvud"
	desc = "POLITICS_SVINHUFVUD_DESC"
	picture = "Portrait_Finland_Pehr_Evind_Svinhufvud.tga"
	expire = "1965.1.1"
	ideology = liberalism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Pehr Evind Svinhufvud"
	desc = "POLITICS_SVINHUFVUD_DESC"
	picture = "Portrait_Finland_Pehr_Evind_Svinhufvud.tga"
	expire = "1965.1.1"
	ideology = market_liberal
	traits = {
		#
	}
}
create_country_leader = {
	name = "Pehr Evind Svinhufvud"
	desc = "POLITICS_SVINHUFVUD_DESC"
	picture = "Portrait_Finland_Pehr_Evind_Svinhufvud.tga"
	expire = "1965.1.1"
	ideology = agrarianism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Pehr Evind Svinhufvud"
	desc = "POLITICS_SVINHUFVUD_DESC"
	picture = "Portrait_Finland_Pehr_Evind_Svinhufvud.tga"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Pehr Evind Svinhufvud"
	desc = "POLITICS_SVINHUFVUD_DESC"
	picture = "Portrait_Finland_Pehr_Evind_Svinhufvud.tga"
	expire = "1965.1.1"
	ideology = military
	traits = {
		#
	}
}
create_country_leader = {
	name = "Pehr Evind Svinhufvud"
	desc = "POLITICS_SVINHUFVUD_DESC"
	picture = "Portrait_Finland_Pehr_Evind_Svinhufvud.tga"
	expire = "1965.1.1"
	ideology = despotism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Pehr Evind Svinhufvud"
	desc = "POLITICS_SVINHUFVUD_DESC"
	picture = "Portrait_Finland_Pehr_Evind_Svinhufvud.tga"
	expire = "1965.1.1"
	ideology = nationalist
	traits = {
		#
	}
}
create_country_leader = {
	name = "Pehr Evind Svinhufvud"
	desc = "POLITICS_SVINHUFVUD_DESC"
	picture = "Portrait_Finland_Pehr_Evind_Svinhufvud.tga"
	expire = "1965.1.1"
	ideology = clerical
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Carl Gustaf Emil Mannerheim"
	picture = "Portrait_Finland_Karl_Gustav_Emil_Mannerheim.tga"
	traits = {
		inflexible_strategist
		war_hero
		trait_cautious
		defensive_doctrine
		trickster
		winter_specialist
		politically_connected
		infantry_officer
		infantry_leader
		infantry_expert
	}
	skill = 5
	id = 58
	attack_skill = 3
	defense_skill = 5
	planning_skill = 5
	logistics_skill = 3
}
create_corps_commander = {
	name = "Erik Heinrichs"
	picture = "Portrait_Finland_Erik_Heinrichs.tga"
	traits = {
		winter_specialist
		ranger
		infantry_officer
		infantry_leader
	}
	skill = 3
	attack_skill = 3
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 3
}
create_corps_commander = {
	name = "Vilho Nenonen"
	picture = "Portrait_Finland_Vilho_Nenonen.tga"
	traits = {
		winter_specialist
		commando
		bearer_of_artillery
		artillerist
		organizer
	}
	skill = 3
	attack_skill = 3
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 4
}
create_corps_commander = {
	name = "Kurt Wallenius"
	picture = "Portrait_Finland_Kurt_Wallenius.tga"
	traits = {
		unpopular
		media_personality
		trickster
		winter_specialist
	}
	skill = 4
	attack_skill = 4
	defense_skill = 4
	planning_skill = 3
	logistics_skill = 3
}
create_navy_leader = {
	name = "Eero Rahola"
	picture = "Portrait_Finland_Eero_Rahola.tga"
	traits = {
		fleet_protector
		green_water_expert
	}
	skill = 3
	attack_skill = 3
	defense_skill = 3
	maneuvering_skill = 3
	coordination_skill = 3
}
if = {
	limit = { not = { has_dlc = "Man the Guns" } }
	### Ship Variants ###
}
if = {
	limit = { has_dlc = "Man the Guns" }
	# Submarines #
	create_equipment_variant = {
		name = "Vetehinen Class"
		type = ship_hull_submarine_1
		name_group = FIN_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = ship_mine_layer_sub
		}
	}
	create_equipment_variant = {
		name = "Vesikko Class"
		type = ship_hull_submarine_1
		name_group = FIN_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = empty
		}
	}
	# Heavy Cruisers #
	create_equipment_variant = {
		name = "Väinämöinen Class"
		type = ship_hull_cruiser_1
		name_group = FIN_CA_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_heavy_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_0
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = cruiser_ship_engine_1
			fixed_ship_armor_slot = ship_armor_cruiser_1
			fixed_ship_secondaries_slot = ship_secondaries_1
			mid_1_custom_slot = empty
			mid_2_custom_slot = empty
			rear_1_custom_slot = empty
		}
	}
}