capital = 1019

OOB = "ALB_1936"

set_technology = {
	infantry_weapons1 = 1
	tech_support = 1
	tech_recon = 1
}

set_politics = {

	ruling_party = monarchism
	last_election = "1928.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 0
	authoritarian_socialism = 10
	socialism = 0
	social_democracy = 0
	social_liberalism = 0
	market_liberalism = 0
	jacobin = 7
	democratic = 5
	neutrality = 5
	monarchism = 65
	nationalism = 5
	fascism = 3
}

COALITION_add_RIGHTIST = yes
COALITION_clr_fas = yes


set_country_flag = ALB_shefqet_shkupi_unavailable
add_ideas = {
	great_depression
	ALB_tribal_tensions
	ALB_albanian_irrendentism
}

set_convoys = 5

#Victor Emmanuel III of Italy after 39
create_country_leader = {
	name = "Enver Hoxha"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "Portrait_Albania_Enver_Hoxha.dds"
	expire = "1965.1.1"
	ideology = stalinism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Fan Noli"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
	expire = "1965.1.1"
	ideology = social_nationalism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Fan Noli"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_3.dds"
	expire = "1965.1.1"
	ideology = real_socialism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Ali Këlcyra"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
	expire = "1965.1.1"
	ideology = national_conservatism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Mid’hat Frashëri"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_1.dds"
	expire = "1965.1.1"
	ideology = nationalist
	traits = {
		#
	}
}
create_country_leader = {
	name = "Tefik Mborja"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "Portrait_Albania_Tefik_Mborja.dds"
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = {
		#
	}
}

create_country_leader = {
	name = "Zog I, Skanderbeg III"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "Portrait_Albania_Zog_I.tga"
	expire = "1965.1.1"
	ideology = dem_socialism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Zog I, Skanderbeg III"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "Portrait_Albania_Zog_I.tga"
	expire = "1965.1.1"
	ideology = liberalism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Zog I, Skanderbeg III"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "Portrait_Albania_Zog_I.tga"
	expire = "1965.1.1"
	ideology = market_liberal
	traits = {
		#
	}
}
create_country_leader = {
	name = "Zog I, Skanderbeg III"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "Portrait_Albania_Zog_I.tga"
	expire = "1965.1.1"
	ideology = agrarianism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Zog I, Skanderbeg III"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "Portrait_Albania_Zog_I.tga"
	expire = "1965.1.1"
	ideology = national_conservatism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Zog I, Skanderbeg III"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "Portrait_Albania_Zog_I.tga"
	expire = "1965.1.1"
	ideology = authoritarian
	traits = {
		#
	}
}
create_country_leader = {
	name = "Zog I, Skanderbeg III"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "Portrait_Albania_Zog_I.tga"
	expire = "1965.1.1"
	ideology = despotism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Zog I, Skanderbeg III"
	desc = "POLITICS_KING_ZOG_DESC"
	picture = "Portrait_Albania_Zog_I.tga"
	expire = "1965.1.1"
	ideology = nationalist
	traits = {
		#
	}
}
create_field_marshal = {
	name = "Xhemal Aranitasi"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_3.dds"
	traits = {
	}
	skill = 2
	attack_skill = 1
	defense_skill = 2
	planning_skill = 1
	logistics_skill = 1
}


