﻿capital = 6

oob = "FLA_1933"

set_research_slots = 3

set_technology = {

	infantry_weapons1 = 1
}

set_politics = {
	ruling_party = democratic
	last_election = "1933.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 1
	authoritarian_socialism = 2
	socialism = 13
	social_democracy = 17
	social_liberalism = 12
	market_liberalism = 0
	jacobin = 0
	democratic = 40
	neutrality = 0
	monarchism = 0
	nationalism = 11
	fascism = 4
}

create_country_leader = {
	name = "Gustav Charles Sap"
	desc = "POLITICS_HUBERT_PIERLOT_DESC"
	picture = "Portrait_Gustaav_Sap.tga"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {
		#
	}
}


