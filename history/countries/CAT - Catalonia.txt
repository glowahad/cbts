﻿capital = 165

oob = "CAT_1933"

set_research_slots = 3

set_technology = {

	infantry_weapons1 = 1
	infantry_weapons1 = 1
	tech_recon = 1
	tech_support = 1		
	tech_engineers = 1
	tech_mountaineers = 1
	gw_artillery = 1
	early_fighter = 1
	early_bomber = 1
	naval_bomber1 = 1
	early_submarine = 1
	early_destroyer = 1
	basic_destroyer = 1
	early_light_cruiser = 1
	early_heavy_cruiser = 1
	basic_heavy_cruiser = 1
	early_battleship = 1
	transport = 1
}

set_politics = {
	ruling_party = social_democracy
	last_election = "1933.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 10
	authoritarian_socialism = 15
	socialism = 0
	social_democracy = 38
	social_liberalism = 5
	market_liberalism = 2
	jacobin = 5
	democratic = 15
	neutrality = 0
	monarchism = 3
	nationalism = 7
	fascism = 0
}

add_ideas = { 
    CAT_divided_society
	CAT_independence_war
	popularity_35_40
}

create_country_leader = {
	name = "Lluís Companys"
	desc = "POLITICS_MACKENZIE_KING_DESC"
	picture = "Portrait_Catalonia_Lluis_Companys.tga"
	expire = "1965.1.1"
	ideology = dem_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Francesc Cambó"
	desc = "POLITICS_MANUEL_AZANA_DESC"
	picture = "Portrait_Catalonia_Cambo.tga"
	expire = "1965.1.1"
	ideology = national_conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Joaquín Maurín/Joan Comorera"
	desc = "POLITICS_MANUEL_AZANA_DESC"
	picture = "Portrait_Catalonia_NoPortrait.tga"
	expire = "1965.1.1"
	ideology = stalinism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Comité Central Obrero de Cataluña"
	desc = "POLITICS_MANUEL_AZANA_DESC"
	picture = "Portrait_Catalonia_comite.tga"
	expire = "1965.1.1"
	ideology = anarcho_syndicalism 
	traits = {
		#
	}
}

create_corps_commander = {
	name = "Federico Escofet i Alsina"
	picture = "Portrait_Catalonia_Federico_Escofet.tga"
	traits = { trait_reckless infantry_leader }
	skill = 2
	attack_skill = 2
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}

create_corps_commander = {
	name = "Enric Pérez i Farràs"
	picture = "Portrait_Catalonia_Enric_Perez.tga"
	traits = { trickster  infantry_leader }
	skill = 1
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}


