﻿capital = 375

oob = "TEX_1933"

set_research_slots = 3

set_technology = {

	infantry_weapons1 = 1
}

set_politics = {
	ruling_party = social_liberalism
	last_election = "1933.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 0
	authoritarian_socialism = 0
	socialism = 1
	social_democracy = 0
	social_liberalism = 88
	market_liberalism = 11
	jacobin = 0
	democratic = 0
	neutrality = 0
	monarchism = 0
	nationalism = 0
	fascism = 0
}

create_country_leader = {
	name = "Miriam Ferguson"
	desc = "POLITICS_ADOLF_HITLER_DESC"
	picture = "Portrait_Texas_Miriam Ferguson.dds"
	expire = "1965.1.1"
	ideology = liberalism
	traits = { }
}


