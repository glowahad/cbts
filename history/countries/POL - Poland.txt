﻿capital = 10

oob = "POL_1936"

set_research_slots = 3
add_manpower = 200000
# Starting tech
set_technology = {
	infantry_weapons1 = 1
	infantry_weapons2 = 1	
	motorised_infantry = 1
	armoured_car1 = 1
	bike_infantry = 1
	support_weapons1 = 1
	infantry_grenade = 1
	mountain_infantry = 1
	
	radio = 1	
	tech_support = 1		
	tech_engineers = 1
	tech_recon = 1
	
	gw_artillery = 1
	interwar_artillery = 1
	
	tank_engine1 = 1	
	diesel_engine = 1	
	gwtank = 1
	tank_engine1 = 1
	
	plane_engine1 = 1
	gw_fighter = 1
	early_fighter = 1
	plane_engine1 = 1
	gw_bomber = 1
	
	human_wave_doctrine = 1
	static_defense = 1
	infiltration_tactics = 1
	defence_in_depth2 = 1
	total_war = 1
	trench_warfare = 1
	
	electronic_mechanical_engineering = 1
	electronic_mechanical_computing  = 1
	
	great_war_assembly_line = 1
	factory_electrification = 1	
	interwar_machine_tools = 1
	industry1 = 1
	industry2 = 1
	basic_construction_methods = 1
	improved_construction_methods = 1
	fuel_silos = 1
	fuel_refining = 1
}
if = {
	limit = {
		not = { has_dlc = "Man the Guns" }
	}
	set_technology = {
		early_submarine = 1
		early_destroyer = 1
		early_light_cruiser = 1
		transport = 1
	}
	set_naval_oob = "POL_1936_naval_legacy"
}
if = {
	limit = {
		has_dlc = "Man the Guns"
	}
	set_technology = {
		#shipbuild_basics = 1
		elemental_fire_control_system  = 1
		basic_naval_mines = 1
		submarine_mine_laying = 1
		early_ship_hull_light = 1
		early_ship_hull_submarine = 1
		mtg_transport = 1
		basic_torpedo = 1
		improved_ship_torpedo_launcher = 1
		basic_battery = 1
		basic_light_battery = 1
		basic_medium_battery = 1
		basic_heavy_battery = 1
		basic_secondary_battery = 1
		improved_secondary_battery = 1
		basic_depth_charges = 1
		sonar = 1
		smoke_generator = 1
		damage_control_1 = 1
		fire_control_methods_1 = 1
	}
	set_naval_oob = "POL_1936_naval_mtg"
}

set_convoys = 10

if = {
	limit = {
		has_dlc = "Together for Victory"
	}
	set_autonomy = {
		target = DZG
		autonomous_state = autonomy_integrated_puppet
	}
	else = {
		puppet = DZG
	}
}

country_event = { id = pol.1 days = 1 }

set_country_flag = BBWR_infight
set_country_flag = pol_moscicki
set_country_flag = sanation_gov
set_country_flag = POL_locked_army_chief

set_country_flag = bbwr_in_sejm
set_country_flag = sn_in_sejm
set_country_flag = sl_in_sejm
set_country_flag = pps_in_sejm
set_country_flag = ukrainians_in_sejm
set_country_flag = pschd_in_sejm
set_country_flag = npr_in_sejm
set_country_flag = jews_in_sejm
set_country_flag = germans_in_sejm
set_country_flag = kpp_in_sejm
set_country_flag = ukrainian_left_in_sejm
set_country_flag = samopomoc_in_sejm
set_country_flag = agudas_izrael_in_sejm

set_country_flag = kazimierz_switalski_speaker
set_country_flag = POL_prystor_failing

set_variable = {
    var = POL_ANO_influence
    value = 25
}
set_variable = {
    var = POL_AUTH_influence
    value = 15
}
set_variable = {
    var = POL_NAT_influence
    value = 60
}
clamp_variable = {
    var = POL_ANO_influence
    min = 0
    max = 100
}
clamp_variable = {
    var = POL_AUTH_influence
    min = 0
    max = 100
}
clamp_variable = {
    var = POL_NAT_influence
    min = 0
    max = 100
}
add_ideas = {
	aleksanderprystor1
	jozefbeck1
	wladyslawzawadzki1
	bronislawpieracki1
	teodorfurgalski
	jozefpilsudski
	ludomilrayski
	jerzyswirski
	POL_poland_aib
	POL_total_opposition
	POL_great_crisis
	POL_ukrainian_nationalism
	POL_underfunded_military
	POL_communist_threat
	POL_nationalist_universities
	POL_march_constitution
}
	
### SEJM in 1933 ###

set_variable = { var = BBWR_deputies value = 247 }
set_variable = { var = SN_deputies value = 62 }
set_variable = { var = SL_deputies value = 48 }
set_variable = { var = PPS_deputies value = 24 }
set_variable = { var = Ukrainian_deputies value = 18 }
set_variable = { var = PSChD_deputies value = 15 }
set_variable = { var = NPR_deputies value = 10 }
set_variable = { var = Jewish_deputies value = 6 }
set_variable = { var = German_deputies value = 5 }
set_variable = { var = KPP_deputies value = 4 }
set_variable = { var = Ukrainian_Left_deputies value = 3 }
set_variable = { var = Agudas_Izrael_deputies value = 1 }
set_variable = { var = Samopomoc_deputies value = 1 }


set_politics = {
	ruling_party = neutrality
	last_election = "1930.11.23"
	election_frequency = 60
	elections_allowed = no
}

set_popularities = {
	communism = 1
	authoritarian_socialism = 2
	socialism = 8
	social_democracy = 9
	social_liberalism = 12
	market_liberalism = 3
	jacobin = 3
	democratic = 8
	neutrality = 15
	monarchism = 28
	nationalism = 7
	fascism = 4
}

COALITION_add_aut = yes
COALITION_POP_NUMBER = yes
COALITION_add_revsoc = yes
COALITION_POP_NUMBER = yes
COALITION_add_mon = yes
COALITION_POP_NUMBER = yes

complete_national_focus = POL_may_coup
complete_national_focus = POL_puppet_president
complete_national_focus = POL_august_novelization
complete_national_focus = POL_castle_deal
complete_national_focus = POL_fight_opposition
complete_national_focus = POL_brzesc_elections
complete_national_focus = POL_colonels
complete_national_focus = POL_1926_elections
complete_national_focus = POL_bartel
complete_national_focus = POL_support_left
complete_national_focus = POL_support_minorities
complete_national_focus = POL_marshal_government
complete_national_focus = POL_dzikow_deal
complete_national_focus = POL_bbwr
complete_national_focus = POL_1928_elections
complete_national_focus = POL_open_war
complete_national_focus = POL_daszynski_proposal
complete_national_focus = POL_centrolew
complete_national_focus = POL_sejm_crisis
complete_national_focus = POL_bartel_back
complete_national_focus = POL_fight_sejmocracy
complete_national_focus = POL_czechowicz_crisis
complete_national_focus = POL_switalski_government
complete_national_focus = POL_need_of_new_constitution
complete_national_focus = POL_create_constitutional_commitee
complete_national_focus = POL_use_march_against_itself
complete_national_focus = POL_tenants_assembly
complete_national_focus = POL_bartel_again
complete_national_focus = POL_slawek
complete_national_focus = POL_mass_centrolew_protest
complete_national_focus = POL_marshal_back
complete_national_focus = POL_brzesc_process
complete_national_focus = POL_slawek_back
complete_national_focus = POL_pacificate_uwo
complete_national_focus = POL_ignore_opposition
complete_national_focus = POL_lecture_overusing_sanationist
complete_national_focus = POL_close_czechowicz_case
complete_national_focus = POL_prystor
complete_national_focus = POL_discredit_christ_dems
complete_national_focus = POL_assimilate_agrarians
complete_national_focus = POL_fight_endecy

create_country_leader = {
	name = "Ignacy Mościcki"
	desc = "POLITICS_IGNACY_MOSCICKI_DESC"
	picture = "Portrait_Poland_Ignacy_Moscicki1.tga"
	expire = "1965.1.1"
	ideology = sanation
	traits = {
		chemist
	}
}

create_country_leader = {
	name = "Julian Leszczyński"
	desc = "POLITICS_JULIAN_LESZCZYŃSKI_DESC"
	picture = "Portrait_Poland_Julian_Leszczynski.tga"
	expire = "1965.1.1"
	ideology = stalinism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Jędrzej Moraczewski"
	desc = "POLITICS_JEDRZEJ_MORACZEWSKI_DESC"
	picture = "Portrait_Poland_Jedrzej_Moraczewski.dds"
	expire = "1965.1.1"
	ideology = syndicalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Ignacy Mościcki"
	desc = "POLITICS_IGNACY_MOSCICKI_DESC"
	picture = "Portrait_Poland_Ignacy_Moscicki1.tga"
	expire = "1965.1.1"
	ideology = sanationist_despotism
	traits = {
		chemist
	}
}

create_country_leader = {
	name = "Roman Dmowski"
	desc = "POLITICS_ROMAND_DMOWSKI_DESC"
	picture = "Portrait_Poland_Roman_Dmowski.dds"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Roman Dmowski"
	desc = "POLITICS_ROMAN_DMOWSKI_DESC"
	picture = "Portrait_Poland_Roman_Dmowski.dds"
	expire = "1965.1.1"
	ideology = national_radicalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Ignacy Daszyński"
	desc = "POLITICS_IGNACY_DASZYNSKI_DESC"
	picture = "Portrait_Poland_Ignacy_Daszynski.dds"
	expire = "1965.1.1"
	ideology = real_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Ignacy Daszyński"
	desc = "POLITICS_IGNACY_DASZYNSKI_DESC"
	picture = "Portrait_Poland_Ignacy_Daszynski.dds"
	expire = "1965.1.1"
	ideology = dem_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Wincenty Witos"
	desc = "POLITICS_WINCENTY_WITOS_DESC"
	picture = "Portrait_Poland_Wincenty_Witos.dds"
	expire = "1965.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

#create_country_leader = {
#	name = "Antoni Ciszak"
#	desc = "POLITICS_ANTONI_CISZAK_DESC"
#	picture = "Portrait_Poland_Antoni_Ciszak.dds"
#	expire = "1965.1.1"
#	ideology = social_nationalism
#	traits = {
#		#
#	}
#}

create_country_leader = {
	name = "Wojciech Korfanty"
	desc = "POLITICS_WOJCIECH_KORFANTY_DESC"
	picture = "POL_Wojciech_Korfanty.tga"
	expire = "1965.1.1"
	ideology = national_liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Roman Dmowski"
	desc = "POLITICS_ROMAN_DMOWSKI_DESC"
	picture = "Portrait_Poland_Roman_Dmowski.dds"
	expire = "1965.1.1"
	ideology = endecja
	traits = {
	}
}

create_country_leader = {
	name = "Roman Dmowski"
	desc = "POLITICS_ROMAN_DMOWSKI_DESC"
	picture = "Portrait_Poland_Roman_Dmowski.dds"
	expire = "1965.1.1"
	ideology = market_liberal
	traits = {
		#
	}
}

#marszałek

create_field_marshal = {
	name = "Józef Piłsudski"
	picture = "Portrait_Poland_Ziuk.tga"
	traits = { politically_connected war_hero media_personality inspirational_leader }
	skill = 5
	attack_skill = 3
	defense_skill = 3
	planning_skill = 4
	logistics_skill = 3
	id = 1867
}

###generałowie broni#####

#create_corps_commander = { #AVAILABLE LATER, THIS IS JUST A REMINDER
#	name = "Józef Haller"
#	picture = "Portrait_Poland_Haller_general.tga"
#	traits = { war_hero }
#	skill = 3
#}
create_corps_commander = { #dies 15.08.1935
	name = "Karol Durski-Trzaska"
	picture = "Portrait_Poland_Durski.tga"
	traits = { old_guard artillerist }
	skill = 3
	attack_skill = 3
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 3
}
#create_corps_commander = { #AVAILABLE LATER, THIS IS JUST A REMINDER
#	name = "Lucjan Żeligowski"
#	picture = ".tga"
#	traits = {  }
#	skill = 3
#}
#create_corps_commander = { #AVAILABLE LATER, THIS IS JUST A REMINDER
#	name = "Kazimierz Raszewski"
#	picture = ".tga"
#	traits = {  }
#	skill = 3
#}
#create_corps_commander = { #AVAILABLE LATER, THIS IS JUST A REMINDER #dies 26.10.1937
#	name = "Józef Dowbór-Muśnicki" 
#	picture = "Portrait_Poland_Musnicki.tga"
#	traits = {  }
#	skill = 3
#}
#create_corps_commander = { #AVAILABLE LATER, THIS IS JUST A REMINDER
#	name = "Stanisław Szeptycki"
#	picture = ".tga"
#	traits = {  }
#	skill = 3
#}

#generałowie dywizji######

create_corps_commander = {
	name = "Kazimierz Sosnkowski"
	picture = "Portrait_Poland_Sosnkowski.tga"
	traits = { brilliant_strategist media_personality trickster }
	skill = 2
	attack_skill = 4
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 2
}
#create_corps_commander = {    #AVAILABLE LATER, THIS IS JUST A REMINDER
#	name = "Władysław Sikorski"
#	picture = "Portrait_Poland_Sikorski.tga"
#	traits = { politically_connected }
#	skill = 2
#}
create_corps_commander = {
	name = "Edward Rydz-Śmigły"
	picture = "Portrait_Poland_Smigly.tga"
	traits = { homeland_connoisseur politically_connected }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 1
	logistics_skill = 2
	id = 2137
}
create_corps_commander = {
	name = "Aleksander Osiński"
	picture = "Portrait_Poland_Osinski.tga"
	traits = { decided_planner meticulous }
	skill = 2
	attack_skill = 1
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 2
}
create_corps_commander = {
	name = "Leon Berbecki"
	picture = "Portrait_Poland_Berbecki.tga"
	traits = { infantry_leader }
	skill = 2
	attack_skill = 2
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 2
}
create_corps_commander = { #dies 3.04.1935, heart attack
	name = "Daniel Konarzewski"
	picture = "Portrait_Poland_Konarzewski.tga"
	traits = { infantry_leader skilled_staffer }
	id = 0304
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 3
}
create_corps_commander = {
	name = "Henryk Minkiewicz"
	picture = "Portrait_Poland_Minkiewicz.tga"
	traits = { organizer }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 3
}
create_corps_commander = {
	name = "Mieczysław Norwid-Neugebauer"
	picture = "Portrait_Poland_Norwid.tga"
	traits = { politically_connected }
	skill = 2
	attack_skill = 1
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 1
}
create_corps_commander = {
	name = "Juliusz Rómmel"
	picture = "Portrait_Poland_Rommel.tga"
	traits = { trait_reckless }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 1
	logistics_skill = 1
}
create_corps_commander = {
	name = "Rudolf Prich"
	picture = "Portrait_Poland_Prich.tga"
	traits = { infantry_leader }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 2
}
create_corps_commander = {
	name = "Tadeusz Piskor"
	picture = "Portrait_Poland_Piskor.tga"
	traits = { organizer }
	skill = 2
	attack_skill = 2
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 3
}
create_corps_commander = {
	name = "Stefan Dąb-Biernacki"
	picture = "Portrait_Poland_Dab.tga"
	traits = { inflexible_strategist old_guard bad_staffer unpopular harsh_leader }
	skill = 2
	attack_skill = 2
	defense_skill = 3
	planning_skill = 1
	logistics_skill = 1
}
create_corps_commander = {  #dies 16.07.1936, plane crash
	name = "Gustaw Orlicz-Dreszer"
	picture = "Portrait_Poland_Orlicz.tga"
	traits = { politically_connected organizer }
	id = 1607
	skill = 2
	attack_skill = 1
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 2
}
create_corps_commander = {
	name = "Kazimierz Fabrycy"
	picture = "Portrait_Poland_Fabrycy.tga"
	traits = { unpopular harsh_leader }
	skill = 2
	attack_skill = 3
	defense_skill = 3
	planning_skill = 1
	logistics_skill = 1
}


#####
create_navy_leader = {
	name = "Józef Unrug"
	gfx = "Portrait_Poland_Jozef_Unrug.tga"
	traits = { 
		seawolf 
	}
	skill = 3
}

create_equipment_variant = {
	name = "PZL P.24"
	type = early_fighter_equipment
	upgrades = {
		plane_gun_upgrade = 2
		plane_range_upgrade = 0  
		plane_engine_upgrade = 3
		plane_reliability_upgrade = 2
	}
}
if = {
	limit = {
		has_dlc = "Man the Guns"
	}
	create_equipment_variant = {
		name = "Wicher Class"
		type = ship_hull_light_1
		name_group = POL_DD_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_battery_slot = ship_light_battery_1
			fixed_ship_anti_air_slot = ship_anti_air_1
			fixed_ship_fire_control_system_slot = ship_fire_control_system_1
			fixed_ship_radar_slot = empty
			fixed_ship_engine_slot = light_ship_engine_1
			fixed_ship_torpedo_slot = ship_torpedo_1
			rear_1_custom_slot = ship_depth_charge_1
		}
		#obsolete = no
	}
	create_equipment_variant = {
		name = "Wilk Class"
		type = ship_hull_submarine_1
		name_group = POL_SS_HISTORICAL
		parent_version = 0
		modules = {
			fixed_ship_torpedo_slot = ship_torpedo_sub_1
			fixed_ship_engine_slot = sub_ship_engine_1
			rear_1_custom_slot = empty
		}
		#obsolete = no
	}
}
if = {
	limit = { NOT = { has_dlc = "Man the Guns" } }

	create_equipment_variant = {
		name = "Wilk Class"
		type = submarine_1
		parent_version = 1
		upgrades = {
			ship_reliability_upgrade = 3
			sub_engine_upgrade = 1
			sub_stealth_upgrade = 2
			sub_torpedo_upgrade = 3
		}
	}

	create_equipment_variant = {
		name = "Wicher Class"
		type = destroyer_1
		parent_version = 0
		upgrades = {
			ship_torpedo_upgrade = 1
			destroyer_engine_upgrade = 3
			ship_ASW_upgrade = 2
			ship_anti_air_upgrade = 2
		}
		#obsolete = no
	}
}

