﻿capital = 327

oob = "PHI_1936"

set_research_slots = 2

# Starting tech
set_technology = {
	infantry_weapons1 = 1
	tech_support = 1		
	tech_engineers = 1
	gw_artillery = 1
	gw_fighter = 1
	plane_engine1 = 1
	gw_bomber = 1
}

set_cosmetic_tag = PHI_USA
set_stability = 0.45
set_war_support = 0.15

set_convoys = 20

add_equipment_to_stockpile = {
	type = early_fighter_equipment
	producer = "USA" 
	amount = 3
}

set_politics = {
	ruling_party = neutrality
	last_election = "1897.3.2"
	election_frequency = 72
	elections_allowed = no
}
set_popularities = {
	communism = 5
	authoritarian_socialism = 0
	socialism = 0
	social_democracy = 3
	social_liberalism = 10
	market_liberalism = 0
	jacobin = 17
	democratic = 12
	neutrality = 42
	monarchism = 0
	nationalism = 11
	fascism = 0
}
create_country_leader = {
	name = "Theodore Roosevelt Jr."
	desc = "POLITICS_TEDDY_ROOSEVELT_JR_DESC"
	picture = "Portrait_Theodore_Roosevelt_Jr.tga"
	expire = "1944.1.1"
	ideology = military
	traits = {
		#
	}
}
create_country_leader = {
	name = "Manuel L. Quezón"
	desc = "POLITICS_MANUEL_QUEZON_DESC"
	picture = "Portrait_Manuel_Quezon.tga"
	expire = "1944.8.1"
	ideology = liberal_nationalism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Emilio Aguinaldo"
	desc = "POLITICS_EMILIO_AGUINALDO_DESC"
	picture = "Portrait_Emilio_Aguinaldo.tga"
	expire = "1950.1.1"
	ideology = dem_socialism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Juan Sumulong"
	desc = "POLITICS_JUAN_SUMUNGLONG_DESC"
	picture = "Portrait_Juan_Sumulong.tga"
	expire = "1960.1.1"
	ideology = real_socialism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Crisanto Evangelista"
	desc = "POLITICS_CRISANTO_EVANGELISTA_DESC"
	picture = "Portrait_Crisanto_Evangelista.tga"
	expire = "1950.1.1"
	ideology = social_nationalism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Benigno Ramos"
	desc = "POLITICS_CRISANTO_EVANGELISTA_DESC"
	picture = "Portrait_Benigno_Ramos.tga"
	expire = "1950.1.1"
	ideology = agrarian_socialism_rev
	traits = {
		#
	}
}
