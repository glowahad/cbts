﻿capital = 554
oob = "SYR_1933"
# Starting tech
set_technology = {
	infantry_weapons1 = 1
	gwtank = 1
	tank_engine1 = 1
}
add_ideas = {
	SYR_hatay_question
	SYR_conquered
	SYR_maronite_concessions
	SYR_great_depression
}
set_convoys = 5
set_cosmetic_tag = SYR_FRA
set_politics = {

	ruling_party = nationalism
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 3
	authoritarian_socialism = 3
	socialism = 3
	social_democracy = 6
	social_liberalism = 6
	market_liberalism = 5
	jacobin = 5
	democratic = 6
	neutrality = 0
	monarchism = 3
	nationalism = 50
	fascism = 10
}

COALITION_add_con = yes
COALITION_add_aut = yes

create_country_leader = {
	name = "Hashim al-Atassi"
	desc = ""
	picture = "Portrait_Syria_Hashim_Atassi.tga"
	ideology = liberal_nationalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Antun Saadeh"
	desc = ""
	picture = "gfx/leaders/SYR/Portrait_Arabia_Generic_land_3.tga"
	ideology = fascism_ideology
	traits = {
		#
	}
}

create_country_leader = {
	name = "Ghazi bin Faisal"
	desc = ""
	picture = "gfx/leaders/SYR/Portrait_Arabia_Generic_2.tga"
	ideology = despotism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Khalid Bakdash"
	desc = ""
	picture = "gfx/leaders/SYR/Portrait_Arabia_Generic_communism1.tga"
	ideology = leninism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Mohammed Ali Bey al-Abed"
	desc = ""
	picture = "Portrait_Syria_Mohammed_al_Abed.tga"
	ideology = stalinism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Mohammed Ali Bey al-Abed"
	desc = ""
	picture = "Portrait_Syria_Mohammed_al_Abed.tga"
	ideology = social_nationalism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Mohammed Ali Bey al-Abed"
	desc = ""
	picture = "Portrait_Syria_Mohammed_al_Abed.tga"
	ideology = real_socialism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Mohammed Ali Bey al-Abed"
	desc = ""
	picture = "Portrait_Syria_Mohammed_al_Abed.tga"
	ideology = dem_socialism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Mohammed Ali Bey al-Abed"
	desc = ""
	picture = "Portrait_Syria_Mohammed_al_Abed.tga"
	ideology = liberalism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Mohammed Ali Bey al-Abed"
	desc = ""
	picture = "Portrait_Syria_Mohammed_al_Abed.tga"
	ideology = market_liberal
	traits = {
		#
	}
}
create_country_leader = {
	name = "Mohammed Ali Bey al-Abed"
	desc = ""
	picture = "Portrait_Syria_Mohammed_al_Abed.tga"
	ideology = liberal_nationalism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Mohammed Ali Bey al-Abed"
	desc = ""
	picture = "Portrait_Syria_Mohammed_al_Abed.tga"
	ideology = social_conservatism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Mohammed Ali Bey al-Abed"
	desc = ""
	picture = "Portrait_Syria_Mohammed_al_Abed.tga"
	ideology = colonial
	traits = {
		#
	}
}
create_country_leader = {
	name = "Mohammed Ali Bey al-Abed"
	desc = ""
	picture = "Portrait_Syria_Mohammed_al_Abed.tga"
	ideology = despotism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Mohammed Ali Bey al-Abed"
	desc = ""
	picture = "Portrait_Syria_Mohammed_al_Abed.tga"
	ideology = nationalist
	traits = {
		#
	}
}
create_country_leader = {
	name = "Mohammed Ali Bey al-Abed"
	desc = ""
	picture = "Portrait_Syria_Mohammed_al_Abed.tga"
	ideology = fascism_ideology
	traits = {
		#
	}
}
