﻿capital = 76

oob = "TRN_1933"

set_research_slots = 3

set_technology = {

	infantry_weapons1 = 1
}

set_politics = {
	ruling_party = socialism
	last_election = "1933.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 5
	authoritarian_socialism = 0
	socialism = 39
	social_democracy = 0
	social_liberalism = 0
	market_liberalism = 0
	jacobin = 10
	democratic = 8
	neutrality = 0
	monarchism = 30
	nationalism = 5
	fascism = 3
}


