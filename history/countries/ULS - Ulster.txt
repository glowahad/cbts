﻿capital = 119

oob = "SCO_1936"

set_research_slots = 4


# Starting tech
set_technology = {
	infantry_weapons1 = 1
	tech_support = 1		
	tech_engineers = 1
	tech_recon = 1
	motorised_infantry = 1
	diesel_engine = 1
	gw_artillery = 1
	gwtank = 1
	tank_engine1 = 1
	gw_fighter = 1
	plane_engine1 = 1
	gw_bomber = 1
	early_submarine = 1
	early_destroyer = 1
	early_light_cruiser = 1
	early_heavy_cruiser = 1
	early_battleship = 1
	early_battlecruiser = 1
	early_carrier = 1
	transport = 1
	electronic_mechanical_engineering = 1
}

set_convoys = 50

set_politics = {
	ruling_party = democratic
	last_election = "1931.10.27"
	election_frequency = 48
	elections_allowed = no ##suspended through duration of war, which is handled via event
}
set_popularities = {
	communism = 2
	authoritarian_socialism = 0
	socialism = 3
	social_democracy = 10
	social_liberalism = 0
	market_liberalism = 1
	jacobin = 7
	democratic = 69
	neutrality = 4
	monarchism = 1
	nationalism = 3
	fascism = 0
}

create_country_leader = { #
	name = "William McCullough"
	desc = "POLITICS_ULS_PLACEHOLDER_DESC"
	picture = "gfx/generic_leaders/western_europe/Western_European_Generic_2.tga"
	expire = "1965.1.1"
	ideology = stalinism
	traits = {
	}
}
create_country_leader = { #Should be Margaret Buckley, we just don't have female generic portraits yet
	name = "Cathal Ó Murchadha"
	desc = "POLITICS_ULS_PLACEHOLDER_DESC"
	picture = "gfx/generic_leaders/western_europe/Western_European_Generic_2.tga"
	expire = "1965.1.1"
	ideology = social_nationalism
	traits = {
	}
}
create_country_leader = {
	name = "Paddy Agnew"
	desc = "POLITICS_ULS_PLACEHOLDER_DESC"
	picture = "gfx/generic_leaders/western_europe/Western_European_Generic_4.tga"
	expire = "1965.1.1"
	ideology = real_socialism
	traits = {
	}
}
create_country_leader = {
	name = "Paddy Agnew"
	desc = "POLITICS_ULS_PLACEHOLDER_DESC"
	picture = "gfx/generic_leaders/western_europe/Western_European_Generic_4.tga"
	expire = "1965.1.1"
	ideology = dem_socialism
	traits = {
	}
}
create_country_leader = {
	name = "Timothy Mulrooney"
	desc = "POLITICS_ULS_PLACEHOLDER_DESC"
	picture = "gfx/generic_leaders/western_europe/Western_European_Generic_2.tga"
	expire = "1965.1.1"
	ideology = liberalism
	traits = {
	}
}
create_country_leader = {
	name = "Timothy Mulrooney"
	desc = "POLITICS_ULS_PLACEHOLDER_DESC"
	picture = "gfx/generic_leaders/western_europe/Western_European_Generic_2.tga"
	expire = "1965.1.1"
	ideology = market_liberal
	traits = {
	}
}
create_country_leader = {
	name = "Thomas Joseph Campbell"
	desc = "POLITICS_ULS_PLACEHOLDER_DESC"
	picture = "Portrait_Ulster_Thomas_Joseph_Campbell.dds"
	expire = "1965.1.1"
	ideology = liberal_nationalism
	traits = {
	}
}
create_country_leader = {
	name = "James Craig"
	desc = "POLITICS_ULS_PLACEHOLDER_DESC"
	picture = "Portrait_Ulster_James_Craig.dds"
	expire = "1965.1.1"
	ideology = british_conservatism
	traits = {	
	}
}
create_country_leader = {
	name = "James Craig"
	desc = "POLITICS_ULS_PLACEHOLDER_DESC"
	picture = "Portrait_Ulster_James_Craig.dds"
	expire = "1965.1.1"
	ideology = authoritarian
	traits = {	
	}
}
create_country_leader = {
	name = "James Craig"
	desc = "POLITICS_ULS_PLACEHOLDER_DESC"
	picture = "Portrait_Ulster_James_Craig.dds"
	expire = "1965.1.1"
	ideology = semi_constitutional
	traits = {	
	}
}
create_country_leader = {
	name = "Norman Porter"
	desc = "POLITICS_ULS_PLACEHOLDER_DESC"
	picture = "gfx/generic_leaders/western_europe/Portrait_Generic_8.tga"
	expire = "1965.1.1"
	ideology = religious_nationalism
	traits = {	
	}
}
create_country_leader = {
	name = "Norman Porter"
	desc = "POLITICS_ULS_PLACEHOLDER_DESC"
	picture = "gfx/generic_leaders/western_europe/Portrait_Generic_8.tga"
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = {	
	}
}
