﻿capital = 303

oob = "PRU_1936"

# Starting tech
set_technology = {
	infantry_weapons1 = 1
	gw_artillery = 1
	gw_fighter = 1
	plane_engine1 = 1
	gw_bomber = 1
	early_submarine = 1
	early_destroyer = 1
	early_light_cruiser = 1
}


set_politics = {

	ruling_party = nationalism
	last_election = "1931.10.11"
	election_frequency = 96
	elections_allowed = no
}
set_popularities = {
	communism = 0
	authoritarian_socialism = 2
	socialism = 3
	social_democracy = 7
	social_liberalism = 25
	market_liberalism = 0
	jacobin = 0
	democratic = 25
	neutrality = 0
	monarchism = 15
	nationalism = 20
	fascism = 3
}

COALITION_add_RIGHTIST = yes
COALITION_clr_con = yes

create_country_leader = {
	name = "Luis Miguel Sánchez Cerro"
	desc = "POLITICS_OSCAR_BENAVIDES_DESC"
	picture = "Portrait_Luis_Miguel_Sanchez_Cerro.dds"
	expire = "1965.1.1"
	ideology = nationalist
	traits = {
		#
	}
}

create_country_leader = {
	name = "Oscar Benavides Larrea"
	desc = "POLITICS_OSCAR_BENAVIDEZ_DESC"
	picture = "Portrait_Oscar_Benavides.dds"
	expire = "1944.8.1"
	ideology = military
}

create_country_leader = {
	name = "Alfonso López Pumarejo"
	desc = "POLITICS_ALFONSO_LOPEZ_DESC"
	picture = "Portrait_PRU_Manuel_Prado_Ugarteche.tga"
	expire = "1965.1.1"
	ideology = social_conservatism
}

### Field Marshal
create_field_marshal = {
	name = "Oscar Benavides Larrea"
	#picture = ""
	traits = {
		war_hero
		inflexible_strategist
		defensive_doctrine
	}
	skill = 3
	attack_skill = 2
	defense_skill = 3
	planning_skill = 3
	logistics_skill = 2
	id = 1569
}

### Corps Commander
create_corps_commander = {
	name = "Manuel A. Odría"
	#picture = ""
	traits = { infantry_officer media_personality }
	skill = 3
	attack_skill = 3
	defense_skill = 1
	planning_skill = 3
	logistics_skill = 3
}

create_corps_commander = {
	name = "Luis M. S. Cerro"
	picture = "Luis_Miguel_Sanchez_Cerro.dds"
	traits = { harsh_leader trait_mountaineer }
	skill = 2
	attack_skill = 2
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 3
}

create_corps_commander = {
	name = "Eloy G. Ureta"
	#picture = ""
	traits = {
		#artillery_officer
		career_officer
	}
	skill = 2
	attack_skill = 1
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Carlos Miro Quesada"
	#picture = ""
	traits = {
		career_officer
	}
	skill = 1
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}

create_corps_commander = {
	name = "Luis Flores"
	#picture = ""
	traits = {
		infantry_officer
		career_officer
	}
	skill = 1
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}