﻿capital = 455
oob = "JOR_1933"
# Starting tech
set_technology = {
	infantry_weapons1 = 1
	gwtank = 1
	tank_engine1 = 1
}
set_research_slots = 1
set_convoys = 5
add_ideas = {
	illiterate_population
	underdeveloped_industry
}
add_ideas = {
	medium_popularity
}

set_politics = {
	ruling_party = monarchism
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 0
	authoritarian_socialism = 0
	socialism = 0
	social_democracy = 1
	social_liberalism = 2
	market_liberalism = 0
	jacobin = 2
	democratic = 9
	neutrality = 31
	monarchism = 43
	nationalism = 12
	fascism = 0
}
set_cosmetic_tag = JOR_ENG

create_country_leader = {
	name = "Yaasir al-Rad"
	desc = ""
	picture = "gfx/generic_leaders/middle_east/Portrait_Middle_East_Generic_3.tga"
	ideology = right_communism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdul Hameed al-Ahmadi"
	desc = ""
	picture = "gfx/generic_leaders/middle_east/Portrait_Middle_East_Generic_3.tga"
	ideology = syndicalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Salaah el-Kazi"
	desc = ""
	picture = "gfx/generic_leaders/middle_east/Portrait_Middle_East_Generic_2.tga"
	ideology = real_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Misfar el-Salaam"
	desc = ""
	picture = "gfx/generic_leaders/middle_east/Portrait_Middle_East_Generic_2.tga"
	ideology = dem_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdullah bin al-Hussein"
	desc = ""
	picture = "Portrait_Jordan_Abdullah.tga"
	ideology = liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdullah bin al-Hussein"
	desc = ""
	picture = "Portrait_Jordan_Abdullah.tga"
	ideology = market_liberal
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdullah bin al-Hussein"
	desc = ""
	picture = "Portrait_Jordan_Abdullah.tga"
	ideology = national_liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdullah bin al-Hussein"
	desc = ""
	picture = "Portrait_Jordan_Abdullah.tga"
	ideology = conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdullah bin al-Hussein"
	desc = ""
	picture = "Portrait_Jordan_Abdullah.tga"
	ideology = military
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdullah bin al-Hussein"
	desc = ""
	picture = "Portrait_Jordan_Abdullah.tga"
	ideology = despotism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdullah bin al-Hussein"
	desc = ""
	picture = "Portrait_Jordan_Abdullah.tga"
	ideology = nationalist
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdullah bin al-Hussein"
	desc = ""
	picture = "Portrait_Jordan_Abdullah.tga"
	ideology = clerical
	traits = {
		#
	}
}