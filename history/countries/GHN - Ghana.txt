﻿capital = 274

oob = "GHN_1933"

set_research_slots = 3

set_technology = {

	infantry_weapons1 = 1
}

set_politics = {
	ruling_party = authoritarian_socialism
	last_election = "1933.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 0
	authoritarian_socialism = 89
	socialism = 0
	social_democracy = 0
	social_liberalism = 0
	market_liberalism = 0
	jacobin = 0
	democratic = 11
	neutrality = 0
	monarchism = 0
	nationalism = 0
	fascism = 0
}

create_country_leader = {
	name = "Kwame Nkrumah"
	desc = "POLITICS__DESC"
	picture = "Portrait_Ghana_Kwame Nkrumah.dds"
	expire = "1965.1.1"
	ideology = social_nationalism
	traits = { }
}

create_country_leader = {
	name = "J. B. Danaquah"
	desc = "POLITICS__DESC"
	picture = "Portrait_Ghana_J. B. Danaquah.dds"
	expire = "1965.1.1"
	ideology = conservatism
	traits = { }
}


