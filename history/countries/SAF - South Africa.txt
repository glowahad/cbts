﻿capital = 1156

oob = "SAF_1936"

# Starting tech
set_technology = {

	#infantry
	support_weapons1 = 1
	infantry_weapons1 = 1
	infantry_weapons2 = 1
	improved_infantry_weapons1 = 1
	infantry_grenade = 1
	bike_infantry = 1
	motorcycle_infantry = 1
	motorised_infantry = 1
	armoured_car1 = 1
	
	#special forces
	mountain_infantry = 1
	naval_infantry = 1
	
	#support
	tech_support = 1		
	tech_engineers = 1
	tech_recon = 1
	tech_military_police = 1
	tech_maintenance_company = 1
	tech_field_hospital = 1
	tech_logistics_company = 1
	
	#Armor
	gwtank = 1
	light_tank_1 = 1
	gw_heavytank = 1
	
	#Artillery
	gw_artillery = 1
	interwar_artillery = 1
	interwar_antiair = 1
	
	#land doctrine
	human_wave_doctrine = 1
	static_defense = 1
	infiltration_tactics = 1
	#artillery_barrage = 1
	defence_in_depth2 = 1
	total_war = 1
	trench_warfare = 1
	
	#naval doctrine
	fleet_in_being = 1
	
	#Airplanes
	gw_fighter = 1
	early_fighter = 1
	cv_early_fighter = 1
	CAS1 = 1
	cv_CAS1 = 1
	naval_bomber1 = 1
	cv_naval_bomber1 = 1
	gw_bomber = 1
	early_bomber = 1
	transport_plane_technology = 1
	
	#Air Doctrine
	air_superiority = 1
	
	#Engineering
	electronic_mechanical_engineering = 1
	electronic_mechanical_computing = 1
	radio = 1
	diesel_engine = 1
	plane_engine1 = 1
	plane_engine2 = 1
	tank_engine1 = 1
	elemental_fire_control_system = 1
	early_radar = 1
	
	#Industry
	great_war_assembly_line = 1
	factory_electrification = 1
	interwar_machine_tools = 1
	industry1 = 1
	industry2 = 1
	industry3 = 1
	basic_construction_methods = 1
	improved_construction_methods = 1
	advanced_construction_methods = 1
	fuel_silos = 1
	improved_fuel_silos = 1
	fuel_refining = 1
}
if = {
	limit = {
		NOT = {
			has_dlc = "Man the Guns"
		}
	}
	set_technology = {
		#regular navy
		early_destroyer = 1
		basic_destroyer = 1
		early_light_cruiser = 1
		basic_light_cruiser = 1
		early_heavy_cruiser = 1
		basic_heavy_cruiser = 1
		early_battlecruiser = 1
		basic_battlecruiser = 1
		early_battleship = 1
		basic_battleship = 1
		transport = 1
		early_carrier = 1
		basic_carrier = 1
		early_submarine = 1
		basic_submarine = 1
	}
}
if = {
	limit = {
		has_dlc = "Man the Guns"
	}
	set_technology = {
		#Naval mtg
		early_ship_hull_light = 1
		basic_ship_hull_light = 1
		smoke_generator = 1
		basic_depth_charges = 1
		sonar = 1
		early_ship_hull_cruiser = 1
		basic_ship_hull_cruiser = 1
		early_ship_hull_heavy = 1
		basic_ship_hull_heavy = 1
		early_ship_hull_carrier = 1
		basic_ship_hull_carrier = 1
		basic_torpedo = 1
		improved_ship_torpedo_launcher = 1
		early_ship_hull_submarine = 1
		basic_ship_hull_submarine = 1
		basic_battery = 1
		basic_light_battery = 1
		basic_medium_battery = 1
		basic_heavy_battery = 1
		basic_secondary_battery = 1
		improved_secondary_battery = 1
		damage_control_1 = 1
		fire_control_methods_1 = 1
		basic_naval_mines = 1
		submarine_mine_laying = 1
		mtg_transport = 1
	}
}

if = {
	limit = { has_dlc = "Together for Victory" }
	#add_ideas = SAF_ossewabrandwag
	#add_ideas = SAF_history_of_segregation

	add_to_tech_sharing_group = commonwealth_research
	add_opinion_modifier = { target = ENG modifier = SAF_anti_british_sentiment }
}

add_ideas = {	
	SAF_Racial_Segregation
	SAF_depression_1
	SAF_Afrikaner_Dominance
	SAF_Growing_Unionism
	SAF_Kommando_Legacy
	SAF_Industrializing_Nation
	SAF_hog_JBM_Hertzog
	SAF_frn_JBM_Hertzog
	SAF_eco_Nicolaas_Havenga
	SAF_sec_DF_Malan
	SAF_int_Oswald_Pirow
}


set_convoys = 25
set_politics = {

	ruling_party = democratic
	last_election = "1929.6.12"
	election_frequency = 60
	elections_allowed = no
}
set_popularities = {
	communism = 0
	authoritarian_socialism = 0
	socialism = 2
	social_democracy = 4
	social_liberalism = 8
	market_liberalism = 0
	jacobin = 32
	democratic = 32
	neutrality = 2
	monarchism = 0
	nationalism = 16
	fascism = 4
}

#Parliament Stuff
set_country_flag = SAF_Parliament
COALITION_add_con = yes
COALITION_add_socdem = yes
COALITION_add_nat = yes
COALITION_add_fas = yes
set_variable = {
	SAF_com_seats = 0
}
set_variable = {
	SAF_revsoc_seats = 0
}
set_variable = {
	SAF_demsoc_seats = 1
}
set_variable = {
	SAF_socdem_seats = 8
}
set_variable = {
	SAF_soclib_seats = 8
}
set_variable = {
	SAF_marlib_seats = 0
}
set_variable = {
	SAF_libcon_seats = 53
}
set_variable = {
	SAF_soccon_seats = 58
}
set_variable = {
	SAF_aut_seats = 0
}
set_variable = {
	SAF_mon_seats = 0
}
set_variable = {
	SAF_faraut_seats = 20
}
set_variable = {
	SAF_fas_seats = 0
}
set_variable = {
	SAF_total_seats = 150
}
set_variable = {
	SAF_seats_needed = 76
}
SAF_get_coalition_seats = yes

#MarkLib
create_country_leader = {
	name = "George Villiers"
	desc = "POLITICS_GEORGE_VILLIERS_DESC"
	picture = "Portrait_South_Africa_George_Villiers.tga"
	expire = "1965.1.1"
	ideology = market_liberal
	traits = {
		#
	}
}

#SocLib
create_country_leader = {
	name = "George Villiers"
	desc = "POLITICS_GEORGE_VILLIERS_DESC"
	picture = "Portrait_South_Africa_George_Villiers.tga"
	expire = "1965.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

#LibCon
create_country_leader = {
	name = "George Villiers"
	desc = "POLITICS_GEORGE_VILLIERS_DESC"
	picture = "Portrait_South_Africa_George_Villiers.tga"
	expire = "1965.1.1"
	ideology = national_liberalism
	traits = {
		#
	}
}

#SocCon
create_country_leader = {
	name = "George Villiers"
	desc = "POLITICS_GEORGE_VILLIERS_DESC"
	picture = "Portrait_South_Africa_George_Villiers.tga"
	expire = "1965.1.1"
	ideology = national_conservatism
	traits = {
		#
	}
}

#Aut
create_country_leader = {
	name = "George Villiers"
	desc = "POLITICS_GEORGE_VILLIERS_DESC"
	picture = "Portrait_South_Africa_George_Villiers.tga"
	expire = "1965.1.1"
	ideology = colonial
	traits = {
		#
	}
}

#Mon
create_country_leader = {
	name = "George Villiers"
	desc = "POLITICS_GEORGE_VILLIERS_DESC"
	picture = "Portrait_South_Africa_George_Villiers.tga"
	expire = "1965.1.1"
	ideology = semi_constitutional
	traits = {
		#
	}
}


#Right-Nat
create_country_leader = {
	name = "George Villiers"
	desc = "POLITICS_GEORGE_VILLIERS_DESC"
	picture = "Portrait_South_Africa_George_Villiers.tga"
	expire = "1965.1.1"
	ideology = afrikaner_nationalism
	traits = {
		#
	}
}

#Fash
create_country_leader = {
	name = "George Villiers"
	desc = "POLITICS_GEORGE_VILLIERS_DESC"
	picture = "Portrait_South_Africa_George_Villiers.tga"
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = {
		#
	}
}

#Leninism
create_country_leader = {
	name = "Moses Kotane"
	desc = "POLITICS_GEORGE_VILLIERS_DESC"
	picture = "GFX_SAF_moses_kotane"
	expire = "1965.1.1"
	ideology = leninism
	traits = {
		#
	}
}

#SocDem
create_country_leader = {
	name = "George Villiers"
	desc = "POLITICS_GEORGE_VILLIERS_DESC"
	picture = "Portrait_South_Africa_George_Villiers.tga"
	expire = "1965.1.1"
	ideology = british_labour
	traits = {
		#
	}
}

#RevSoc
create_country_leader = {
	name = "George Villiers"
	desc = "POLITICS_GEORGE_VILLIERS_DESC"
	picture = "Portrait_South_Africa_George_Villiers.tga"
	expire = "1965.1.1"
	ideology = syndicalism
	traits = {
		#
	}
}

#DemSoc
create_country_leader = {
	name = "George Villiers"
	desc = "POLITICS_GEORGE_VILLIERS_DESC"
	picture = "Portrait_South_Africa_George_Villiers.tga"
	expire = "1965.1.1"
	ideology = dem_socialism
	traits = {
		#
	}
}

create_corps_commander = {
	name = "George Edwin Brink"
	gfx = "GFX_SAF_george_edwin_brink"
	traits = { desert_fox  }
	skill = 4
}

create_navy_leader = {
	name = "Guy Hallifax"
	gfx = "GFX_SAF_guy_halifax"
	traits = { }
	skill = 3
}
