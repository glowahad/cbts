﻿capital = 741

oob = "CAM_1936"

set_research_slots = 3

# Starting tech
# clone of France
set_technology = {
	infantry_weapons1 = 1
	infantry_weapons1 = 1
	tech_support = 1
	tech_recon = 1
	tech_mountaineers = 1
	motorised_infantry = 1
	diesel_engine = 1
	gw_artillery = 1
	interwar_antiair = 1
	gwtank = 1
	tank_engine1 = 1
	light_tank_1 = 1
	light_tank_2 = 1
	early_fighter = 1
	naval_bomber1 = 1
	cv_early_fighter = 1
	cv_naval_bomber1 = 1
	early_bomber = 1
	tactical_bomber1 = 1
	CAS1 = 1
	early_submarine = 1
	basic_submarine = 1
	early_destroyer = 1
	early_light_cruiser = 1
	basic_light_cruiser = 1
	early_heavy_cruiser = 1
	basic_heavy_cruiser = 1
	early_battleship = 1
	early_battlecruiser = 1
	early_carrier = 1
	transport = 1
	trench_warfare = 1
	fleet_in_being = 1
}

set_politics = {

	ruling_party = neutrality
	last_election = "1933.2.16"
	election_frequency = 36
	elections_allowed = no
}
set_popularities = {
	communism = 25
	authoritarian_socialism = 0
	socialism = 0
	social_democracy = 0
	social_liberalism = 0
	market_liberalism = 0
	jacobin = 0
	democratic = 40
	neutrality = 35
	monarchism = 0
	nationalism = 0
	fascism = 0
}

create_country_leader = {
	name = "Pol Pot" 
	desc = ""
	picture = "gfx/leaders/Asia/Portrait_Asia_Generic_warlord1.dds"
	expire = "1953.3.1"
	ideology = stalinism #no reason for pol pot to be here
	traits = {
		
	}
}

#non standard english alphabet character
create_country_leader = {
	name = "Sisowath Youtévong"
	desc = ""
	picture = "gfx/leaders/Asia/Portrait_Asia_Generic_land_4.dds"
	expire = "1953.3.1"
	ideology = liberalism
	traits = {
		
	}
}

create_country_leader = {
	name = "Norodom Sihanouk" 
	desc = ""
	picture = "gfx/leaders/Asia/Portrait_Asia_Generic_2.dds"
	expire = "1953.3.1"
	ideology = conservatism
	traits = {
		
	}
}

#couldnt find anything historical on fascism


