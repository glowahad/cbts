﻿capital = 3

oob = "SWI_1936"
# FLAGS

set_research_slots = 3

set_technology = {
	infantry_weapons1 = 1
	mountain_infantry = 1
	tech_support = 1		
	tech_engineers = 1
	tech_recon = 1
	gw_fighter = 1
	plane_engine1 = 1
}

set_politics = {

	ruling_party = market_liberalism
	last_election = "1931.10.25"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 2
	authoritarian_socialism = 1
	socialism = 4
	social_democracy = 23
	social_liberalism = 19
	market_liberalism = 27
	jacobin = 3
	democratic = 16
	neutrality = 1
	monarchism = 0
	nationalism = 4
	fascism = 0
}

add_ideas = {
	neutrality_idea	
	limited_conscription
}

set_stability = 1
set_war_support = 0.05

create_country_leader = {
	name = "Fritz Platten"
	desc = "POLITICS_FRITZ_PLATTEN_DESC"
	picture = "Portrait_Switzerland_Fritz_Platten.dds"
	expire = "1965.1.1"
	ideology = leninism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Fritz Platten"
	desc = "POLITICS_FRITZ_PLATTEN_DESC"
	picture = "Portrait_Switzerland_Fritz_Platten.dds"
	expire = "1965.1.1"
	ideology = rev_socialism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Federal Council"
	desc = "POLITICS_FEDERAL_COUNCIL_DESC"
	picture = "Portrait_Switzerland_Federal_Council.tga"
	expire = "1965.1.1"
	ideology = real_socialism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Federal Council"
	desc = "POLITICS_FEDERAL_COUNCIL_DESC"
	picture = "Portrait_Switzerland_Federal_Council.tga"
	expire = "1965.1.1"
	ideology = dem_socialism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Federal Council"
	desc = "POLITICS_FEDERAL_COUNCIL_DESC"
	picture = "Portrait_Switzerland_Federal_Council.tga"
	expire = "1965.1.1"
	ideology = liberalism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Federal Council"
	desc = "POLITICS_FEDERAL_COUNCIL_DESC"
	picture = "Portrait_Switzerland_Federal_Council.tga"
	expire = "1965.1.1"
	ideology = market_liberal
	traits = {
		#
	}
}
create_country_leader = {
	name = "Federal Council"
	desc = "POLITICS_FEDERAL_COUNCIL_DESC"
	picture = "Portrait_Switzerland_Federal_Council.tga"
	expire = "1965.1.1"
	ideology = liberal_nationalism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Federal Council"
	desc = "POLITICS_FEDERAL_COUNCIL_DESC"
	picture = "Portrait_Switzerland_Federal_Council.tga"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Rudolf Minger"
	desc = "POLITICS_FRITZ_PLATTEN_DESC"
	picture = "Portrait_Switzerland_Rudolf_Minger.dds"
	expire = "1965.1.1"
	ideology = despotism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Rudolf Minger"
	desc = "POLITICS_FRITZ_PLATTEN_DESC"
	picture = "Portrait_Switzerland_Rudolf_Minger.dds"
	expire = "1965.1.1"
	ideology = nationalist
	traits = {
		#
	}
}
create_country_leader = {
	name = "Robert Tobler"
	desc = "POLITICS_MAX_LEO_KELLER_DESC"
	picture = "Portrait_Switzerland_Robert_Tobler.dds" 
	expire = "1965.1.1"
	ideology = nazism
	traits = {
		#
	}
}

create_corps_commander = {
	name = "Henri Guisan"
	portrait_path = "gfx/leaders/Europe/Portrait_Europe_Generic_land_3.dds"
	traits = {  winter_specialist }
	skill = 3
}
