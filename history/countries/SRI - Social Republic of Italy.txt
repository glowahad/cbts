﻿capital = 2

oob = "ITA_1936"

set_research_slots = 4
set_cosmetic_tag = SRI_nationalstate

add_ideas = {
	#vittoria_mutilata
	limited_exports
	limited_conscription
	partial_economic_mobilisation
}


# Starting tech
set_technology = {
	infantry_weapons1 = 1
	tech_support = 1		
	tech_engineers = 1
	mountain_infantry = 1
	motorised_infantry = 1
	diesel_engine = 1
	gw_artillery = 1
	gwtank = 1
	tank_engine1 = 1
	gw_fighter = 1
	plane_engine1 = 1
	gw_bomber = 1
	early_submarine = 1
	early_destroyer = 1
	early_light_cruiser = 1
	early_heavy_cruiser = 1
	early_battleship = 1
	early_battlecruiser = 1
	transport = 1
}



set_convoys = 200

set_politics = {

	ruling_party = fascism
	last_election = "1929.3.24"
	election_frequency = 60
	elections_allowed = no
}
set_popularities = {
	communism = 0
	authoritarian_socialism = 1
	socialism = 0
	social_democracy = 0
	social_liberalism = 0
	market_liberalism = 0
	jacobin = 0
	democratic = 0
	neutrality = 0
	monarchism = 0
	nationalism = 0
	fascism = 99
}


create_country_leader = {
	name = "Ruggero Grieco"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	picture = "gfx/leaders/ITA/Portrait_Italy_Ruggero_Grieco.dds"
	expire = "1965.1.1"
	ideology = leninism
	traits = {
		
	}
}

create_country_leader = {
	name = "Alcide De Gasperi"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	picture = "gfx/leaders/ITA/Portrait_Italy_Alcide_Gasperi.dds"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {
		
	}
}

create_country_leader = {
	name = "Arturo Labriola"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	picture = "gfx/leaders/ITA/Portrait_Italy_Arturo_Labriola.dds"
	expire = "1965.1.1"
	ideology = liberalism
	traits = {
		
	}
}

create_country_leader = {
	name = "Luigi Einaudi"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	picture = "gfx/leaders/ITA/Portrait_Italy_Luigi_Einaudi.dds"
	expire = "1965.1.1"
	ideology = centrism
	traits = {
		
	}
}

create_country_leader = {
	name = "Ugo La Malfa"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	picture = "gfx/leaders/ITA/Portrait_Italy_Malfa.dds"
	expire = "1965.1.1"
	ideology = liberalism
	traits = {
		
	}
}

create_country_leader = {
	name = "Pietro Badoglio"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	picture = "gfx/leaders/ITA/Portrait_Italy_Pietro_Badoglio.dds"
	expire = "1965.1.1"
	ideology = military
	traits = {
		
	}
}
#create_country_leader = {
#	name = "Benito Mussolini"
#	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
#	picture = "gfx/leaders/ITA/portrait_benito_mussolini.tga"
#	expire = "1965.1.1"
#	ideology = fascism_ideology
#	traits = {
#		
#	}
#}
create_country_leader = {
	name = "Giuseppe Tassinari"
	desc = "POLITICS_BENITO_MUSSOLINI_DESC"
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = {
		
	}
}

create_field_marshal = {
	name = "Emilio De Bono"
	picture = "portrait_italy_emilio_de_bono.dds"
	traits = { defensive_doctrine old_guard }
	skill = 1
}

create_corps_commander = {
	name = "Ugo Cavallero"
	picture = "gfx/leaders/ITA/Portrait_Italy_Ugo_Cavallero.dds"
	traits = {  old_guard }
	skill = 1
	rank = 1
}

create_corps_commander = {
	name = "Giovanni Messe"
	picture = "gfx/leaders/ITA/Portrait_Italy_Giovanni_Messe.dds"
	traits = {  panzer_leader }
	skill = 4
	rank = 1
}

create_corps_commander = {
	name = "Sebastiano Visconti Prasca"
	picture = "gfx/leaders/ITA/Portrait_Italy_Sebastiano_Visconti_Prasca.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Ubaldo Soddu"
	picture = "gfx/leaders/ITA/Portrait_Italy_Ubaldo_Soddu.dds"
	traits = {  }
	skill = 1
}

create_corps_commander = {
	name = "Italo Balbo"
	picture = "portrait_italy_italo_balbo.dds"
	traits = {  }
	skill = 1
}

create_field_marshal = {
	name = "Rodolfo Graziani"
	picture = "portrait_italy_radolfo_graziani.dds"
	traits = { offensive_doctrine }
	skill = 2
}

create_navy_leader = {
	name = "Inigo Campioni"
		picture = "gfx/leaders/ITA/Portrait_Italy_Inigo_Campioni.dds"
	traits = { superior_tactician spotter }
	skill = 2
}

create_navy_leader = {
	name = "Alberto Da Zara"
		picture = "gfx/leaders/ITA/Portrait_Italy_Alberto_Da_Zara.dds"
	traits = { superior_tactician }
	skill = 3
}

create_navy_leader = {
	name = "Giuseppe Fioravanzo"
		picture = "gfx/leaders/ITA/Portrait_Italy_Giuseppe_Fioravanzo.dds"
	traits = { superior_tactician }
	skill = 3
}

create_navy_leader = {
	name = "Carlo Bergamini"
		picture = "gfx/leaders/ITA/Portrait_Italy_Carlo_Bergamini.dds"
	traits = { superior_tactician }
	skill = 2
}

create_navy_leader = {
	name = "Angelo Iachino"
		picture = "gfx/leaders/ITA/Portrait_Italy_Angelo_Iachino.dds"
	traits = { superior_tactician spotter }
	skill = 2
}

1939.1.1 = {
	create_equipment_variant = {
		name = "Marcello Class"
		type = submarine_2
		upgrades = {
			ship_reliability_upgrade = 1
			sub_engine_upgrade = 1
			sub_stealth_upgrade = 1
			sub_torpedo_upgrade = 1
		}
	}
}
