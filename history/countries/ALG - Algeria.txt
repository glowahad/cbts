﻿capital = 459

oob = "empty"

set_research_slots = 3

set_technology = {

	infantry_weapons1 = 1
}

set_politics = {
	ruling_party = democratic
	last_election = "1933.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 8
	authoritarian_socialism = 3
	socialism = 4
	social_democracy = 8
	social_liberalism = 6
	market_liberalism = 9
	jacobin = 4
	democratic = 35
	neutrality = 17
	monarchism = 0
	nationalism = 6
	fascism = 0
}

create_country_leader = {
	name = "Ferhet Abbas"
	desc = "POLITICS__DESC"
	picture = "Portrait_Algeria_Ferhet Abbas.dds"
	expire = "1965.1.1"
	ideology = social_nationalism
	traits = { }
}


