﻿capital = 671 #Hanoi

oob = "VIN_1936"

set_research_slots = 3

# Starting tech
# clone of France
set_technology = {
	infantry_weapons1 = 1
	tech_support = 1
	tech_recon = 1
	mountain_infantry = 1
	motorised_infantry = 1
	diesel_engine = 1
	gw_artillery = 1
	gwtank = 1
	tank_engine1 = 1
	gw_fighter = 1
	plane_engine1 = 1
	gw_bomber = 1
	early_submarine = 1
	early_destroyer = 1
	early_light_cruiser = 1
	early_heavy_cruiser = 1
	early_battleship = 1
	early_battlecruiser = 1
	early_carrier = 1
	transport = 1
}

set_politics = {

	ruling_party = neutrality
	last_election = "1933.2.16"
	election_frequency = 36
	elections_allowed = no
}
set_popularities = {
	communism = 15
	authoritarian_socialism = 1
	socialism = 1
	social_democracy = 3
	social_liberalism = 1
	market_liberalism = 1
	jacobin = 1
	democratic = 1
	neutrality = 64
	monarchism = 8
	nationalism = 4
	fascism = 0
}
set_cosmetic_tag = Indochina_FRA
#NOTICE# please watch out for spelling, im using Vietnamese names as they spell them, game might not support these characters!

create_country_leader = {
	name = "Phan Boi Chau" 
	desc = ""
	picture = "gfx/leaders/Asia/Portrait_Asia_Generic_warlord2.dds"
	expire = "1953.3.1"
	ideology = fascism_ideology 
	traits = {
		
	}
}

create_country_leader = {
	name = "Ho Chi Minh" 
	desc = ""
	picture = "gfx/leaders/Asia/Portrait_Asia_Generic_communism.dds"
	expire = "1953.3.1"
	ideology = stalinism
	traits = {
		
	}
}

create_country_leader = {
	name = "Ngo Dình Diêm"
	desc = ""
	picture = "gfx/leaders/Asia/Portrait_Asia_Generic_2.dds"
	expire = "1953.3.1"
	ideology = conservatism
	traits = {
		
	}
}

create_country_leader = {
	name = "Nguyen Tuong Tam"
	desc = ""
	picture = "gfx/leaders/Asia/Portrait_Asia_Generic_1.dds"
	expire = "1953.3.1"
	ideology = centrism
	traits = {
		
	}
}
create_country_leader = {
	name = "Bao Dai"
	desc = ""
	picture = "Portrait_Vietnam_Bao_Dai.dds"
	expire = "1953.3.1"
	ideology = fascism_ideology
	traits = {
		
	}
}

create_country_leader = {
	name = "Bao Dai"
	desc = ""
	picture = "Portrait_Vietnam_Bao_Dai.dds"
	expire = "1953.3.1"
	ideology = nationalist
	traits = {
		
	}
}

create_country_leader = {
	name = "Pierre Pasquier"
	desc = ""
	picture = "Portrait_Pierre_Pasquier.tga"
	expire = "1953.3.1"
	ideology = colonial
	traits = {
		
	}
}


create_country_leader = {
	name = "Pierre Pasquier"
	desc = ""
	picture = "Portrait_Pierre_Pasquier.tga"
	expire = "1953.3.1"
	ideology = despotism
	traits = {
		
	}
}

create_country_leader = {
	name = "Bao Dai"
	desc = ""
	picture = "Portrait_Vietnam_Bao_Dai.dds"
	expire = "1953.3.1"
	ideology = social_conservatism
	traits = {
		
	}
}

create_country_leader = {
	name = "Bao Dai"
	desc = ""
	picture = "Portrait_Vietnam_Bao_Dai.dds"
	expire = "1953.3.1"
	ideology = liberalism
	traits = {
		
	}
}

create_country_leader = {
	name = "Bao Dai"
	desc = ""
	picture = "Portrait_Vietnam_Bao_Dai.dds"
	expire = "1953.3.1"
	ideology = centrism
	traits = {
		
	}
}

create_country_leader = {
	name = "Bao Dai"
	desc = ""
	picture = "Portrait_Vietnam_Bao_Dai.dds"
	expire = "1953.3.1"
	ideology = market_liberal
	traits = {
		
	}
}

create_country_leader = {
	name = "Bao Dai"
	desc = ""
	picture = "Portrait_Vietnam_Bao_Dai.dds"
	expire = "1953.3.1"
	ideology = dem_socialism
	traits = {
		
	}
}

create_country_leader = {
	name = "Bao Dai"
	desc = ""
	picture = "Portrait_Vietnam_Bao_Dai.dds"
	expire = "1953.3.1"
	ideology = real_socialism
	traits = {
		
	}
}

create_country_leader = {
	name = "Bao Dai"
	desc = ""
	picture = "Portrait_Vietnam_Bao_Dai.dds"
	expire = "1953.3.1"
	ideology = stalinism
	traits = {
		
	}
}
