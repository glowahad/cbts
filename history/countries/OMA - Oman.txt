﻿capital = 294

oob = "OMA_1936"
set_research_slots = 1
# Starting tech
set_technology = {
	infantry_weapons1 = 1
}

set_convoys = 10
add_ideas = {
	illiterate_population
	underdeveloped_industry
}
set_politics = {

	ruling_party = monarchism
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 0
	authoritarian_socialism = 0
	socialism = 0
	social_democracy = 0
	social_liberalism = 0
	market_liberalism = 0
	jacobin = 0
	democratic = 5
	neutrality = 10
	monarchism = 50
	nationalism = 35
	fascism = 0
}
set_cosmetic_tag = OMA_Kingdom

create_country_leader = {
	name = "Said bin Taimur"
	desc = "POLITICS_SAID_BIN_TAIMUR_DESC"
	picture = "Oman_Said_Bin_Taimur.dds"
	expire = "1965.1.1"
	ideology = clerical
	traits = {
		#
	}
}

create_country_leader = {
	name = "Said bin Taimur"
	desc = "POLITICS_SAID_BIN_TAIMUR_DESC"
	picture = "Oman_Said_Bin_Taimur.dds"
	expire = "1965.1.1"
	ideology = religious_nationalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Said bin Taimur"
	desc = "POLITICS_SAID_BIN_TAIMUR_DESC"
	picture = "Oman_Said_Bin_Taimur.dds"
	expire = "1965.1.1"
	ideology = despotism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Imam Alkhalili"
	desc = "POLITICS_SAID_BIN_TAIMUR_DESC"
	picture = "gfx/generic_leaders/middle_east/Portrait_Arab_King_1.tga"
	expire = "1965.1.1"
	ideology = theocratic
	traits = {
		#
	}
}

create_country_leader = {
	name = "Said bin Taimur"
	desc = "POLITICS_SAID_BIN_TAIMUR_DESC"
	picture = "Oman_Said_Bin_Taimur.dds"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Said bin Taimur"
	desc = "POLITICS_SAID_BIN_TAIMUR_DESC"
	picture = "Oman_Said_Bin_Taimur.dds"
	expire = "1965.1.1"
	ideology = liberal_nationalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Said bin Taimur"
	desc = "POLITICS_SAID_BIN_TAIMUR_DESC"
	picture = "Oman_Said_Bin_Taimur.dds"
	expire = "1965.1.1"
	ideology = market_liberal
	traits = {
		#
	}
}

create_country_leader = {
	name = "Said bin Taimur"
	desc = "POLITICS_SAID_BIN_TAIMUR_DESC"
	picture = "Oman_Said_Bin_Taimur.dds"
	expire = "1965.1.1"
	ideology = jadidism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdul Khaliq al-Ismael"
	desc = "POLITICS_SAID_BIN_TAIMUR_DESC"
	picture = "gfx/generic_leaders/middle_east/Portrait_Middle_East_Generic_2.tga"
	expire = "1965.1.1"
	ideology = dem_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdul Baari el-Azizi"
	desc = "POLITICS_SAID_BIN_TAIMUR_DESC"
	picture = "gfx/generic_leaders/middle_east/Portrait_Middle_East_Generic_1.tga"
	expire = "1965.1.1"
	ideology = real_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Abdul Baari el-Azizi"
	desc = "POLITICS_SAID_BIN_TAIMUR_DESC"
	picture = "gfx/generic_leaders/middle_east/Portrait_Middle_East_Generic_1.tga"
	expire = "1965.1.1"
	ideology = centrist_marxism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Umar al-Pirani"
	desc = "POLITICS_SAID_BIN_TAIMUR_DESC"
	picture = "gfx/generic_leaders/middle_east/Portrait_Middle_East_Generic_2.tga"
	expire = "1965.1.1"
	ideology = leninism
	traits = {
		#
	}
}
