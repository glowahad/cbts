﻿capital = 302

oob = "BOL_1936"
add_stability = 0.5
set_political_power = 40

# Starting tech
set_technology = {
	infantry_weapons1 = 1
	tech_support = 1		
	tech_engineers = 1
	gw_artillery = 1
	gw_fighter = 1
	early_fighter = 1
	gw_bomber = 1
	early_bomber = 1
	plane_engine1 = 1
	diesel_engine = 1
	motorised_infantry = 1
}

if = {
	limit = {
		has_start_date < 1933.01.02
	}

	declare_war_on = {
		target = PAR
		type = take_core_state
	}
	add_ideas = SA_chaco_war
	set_global_flag = SA_chaco_war_flag
}

set_politics = {

	ruling_party = jacobin
	last_election = "1930.11.10"
	election_frequency = 72
	elections_allowed = yes
}
set_popularities = {
	communism = 3
	authoritarian_socialism = 5
	socialism = 3
	social_democracy = 15
	social_liberalism = 3
	market_liberalism = 32
	jacobin = 26
	democratic = 4
	neutrality = 6
	monarchism = 0
	nationalism = 3
	fascism = 0
}

add_ideas = {
	BOL_tin_crisis
	BOL_bolivian_army
	BOL_fragmented_society
	BOL_landlocked_country
	limited_exports
	limited_conscription
	low_economic_mobilisation
}

create_country_leader = {
	name = "Rigoberto Rivera Argandoña"
	desc = "POLITICS_BOL_PLACEHOLDER_DESC"
	picture = "gfx/generic_leaders/south_america/South_America_Generic_1.tga"
	expire = "1965.1.1"
	ideology = leninism 
	traits = {
		#
	}
}
create_country_leader = {
	name = "Arturo Borda"
	desc = "POLITICS_BOL_PLACEHOLDER_DESC"
	picture = "gfx/generic_leaders/south_america/South_America_Generic_1.tga"
	expire = "1965.1.1"
	ideology = anarcho_syndicalism  
	traits = {
		#
	}
}
create_country_leader = {
	name = "José Antonio Arze"
	desc = "POLITICS_BOL_PLACEHOLDER_DESC"
	picture = "gfx/generic_leaders/south_america/South_America_Generic_5.tga" #because that's him
	expire = "1965.1.1"
	ideology = real_socialism 
	traits = {
		#
	}
}
create_country_leader = {
	name = "Bautista Saavedra"
	desc = "POLITICS_BOL_PLACEHOLDER_DESC"
	picture = "gfx/generic_leaders/south_america/South_America_Generic_5.tga" #because that's him
	expire = "1965.1.1"
	ideology = dem_socialism 
	traits = {
		#
	}
}
create_country_leader = {
	name = "Claudio Zuazo"
	desc = "POLITICS_JOSE_LUIS_TEJADA_SORZANO_DESC"
	picture = "gfx/generic_leaders/south_america/South_America_Generic_3.tga"
	expire = "1965.1.1"
	ideology = sa_radicalism 
	traits = {
		#
	}
}
create_country_leader = {
	name = "José Luis Tejada Sorzano"
	desc = "POLITICS_JOSE_LUIS_TEJADA_SORZANO_DESC"
	picture = "Portrait_Bolivia_Jose_Luis_Tejada_Sorzano.dds"
	expire = "1965.1.1"
	ideology = market_liberal 
	traits = {
		#
	}
}
create_country_leader = {
	name = "Daniel Salamanca Urey"
	desc = "POLITICS_DANIEL_SALAMANCA_DESC"
	picture = "Portrait_Bolivia_Daniel_Salamanca_Urey.tga"
	expire = "1965.1.1"
	ideology = national_liberalism 
	traits = {
		hombre_simbolo
	}
}

create_country_leader = {
	name = "-"
	desc = "POLITICS_JOSE_LUIS_TEJADA_SORZANO_DESC"
	picture = "Portrait_Bolivia_Daniel_Salamanca_Urey.tga"
	expire = "1965.1.1"
	ideology = christian_democracy_soccon
	traits = {
		#
	}
}

create_country_leader = {
	name = "Enrique Baldivieso"
	desc = "POLITICS_OSCAR_UNZAGA_DESC"
	picture = "gfx/BOL/leaders/Portrait_Bolivia_Carlos_Quintanilla_Quiroga.tga"
	expire = "1965.1.1"
	ideology = authoritarian

	traits = {
		#
	}
}

create_country_leader = {
	name = "Víctor Paz Estenssoro"
	desc = "POLITICS_OSCAR_UNZAGA_DESC"
	picture = "gfx/BOL/leaders/Portrait_Bolivia_Carlos_Quintanilla_Quiroga.tga"
	expire = "1965.1.1"
	ideology = despotism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Óscar Únzaga de la Vega"
	desc = "POLITICS_OSCAR_UNZAGA_DESC"
	picture = "Portrait_Bolivia_Óscar_Únzaga_de_la_Vega.dds"
	expire = "1965.1.1"
	ideology = falangism
	traits = {
		#
	}
}

create_corps_commander = {
	name = "Hans Kundt"
	picture = "Portrait_Bolivia_Hans_Kundt.tga"
	traits = { bad_staffer harsh_leader old_guard }
	skill = 3
	id = 43
	attack_skill = 4
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 3
}

#6 out of 9 Vickers Vespa
add_equipment_to_stockpile = {
	type = early_fighter_equipment
	producer = ENG
	amount = 6
}
#Curtis-Wright "Hawk II" model 35A and "Sea Hawk" Model 65A
#2 more in 1933 and 3 in 1934
add_equipment_to_stockpile = {
	type = early_fighter_equipment
	producer = USA
	amount = 4
}
#Jukers JU 52
#2 more will come in 1933
add_equipment_to_stockpile = {
	type = transport_plane_equipment
	producer = GER
	amount = 1
}
# 9 out of 12 Breguet XIX
add_equipment_to_stockpile = {
	type = iw_bomber_equipment
	producer = SPR
	amount = 9
}
#3 out of 5 Fokker C.V
add_equipment_to_stockpile = {
	type = iw_bomber_equipment
	producer = HOL
	amount = 3
}
add_equipment_to_stockpile = {
	type = infantry_equipment
	producer = BOL
	amount = 2000
}
add_equipment_to_stockpile = {
	type = support_equipment
	producer = BOL
	amount = 400
}
add_equipment_to_stockpile = {
	type = artillery_equipment
	producer = BOL
	amount = 200
}
add_equipment_to_stockpile = {
	type = motorized_equipment
	producer = BOL
	amount = 60
}
add_equipment_to_stockpile = {
	type = light_tank_equipment_1
	producer = ENG
	amount = 8
}
