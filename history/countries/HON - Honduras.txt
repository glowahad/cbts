﻿capital = 312

oob = "HON_1936"

# Starting tech
set_technology = {
	infantry_weapons1 = 1
}
set_country_flag = monroe_doctrine

set_convoys = 10

set_politics = {

	ruling_party = monarchism
	last_election = "1932.10.28"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 0
	authoritarian_socialism = 0
	socialism = 0
	social_democracy = 1
	social_liberalism = 2
	market_liberalism = 30
	jacobin = 5
	democratic = 5
	neutrality = 4
	monarchism = 50
	nationalism = 3
	fascism = 0
}

COALITION_add_RIGHTIST = yes

create_country_leader = {
	name = "Tiburcio Carías Andino"
	desc = "POLITICS_TIBURCIO_CARIAS_ANDINO_DESC"
	picture = "Tiburcio_Andino.dds"
	expire = "1965.1.1"
	ideology = despotism
	traits = {
		#
	}
}
