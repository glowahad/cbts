﻿capital = 834

oob = "HUN_1933"

set_research_slots = 2
set_stability = 0.39
set_war_support = 0.25
give_guarantee = AUS #Austria
add_state_claim	= 664 #Southern Slovakia
add_state_claim	= 73 #Carpatho-Ukraine
set_country_flag = historical_path_only_completed

set_technology = {
	infantry_weapons1 = 1
	gwtank = 1
	tech_support = 1		
	tech_recon = 1
	gw_artillery = 1
	gw_fighter = 1
	interwar_antiair = 1
	gw_bomber = 1
	plane_engine1 = 1
	fuel_silos = 1
}

add_ideas = {
	popularity_45_50
	disarmed_nation
	HUN_treaty_of_trianon
	HUN_effects_of_the_great_depression_1
	HUN_hungarian_irredentism
	HUN_gyula_gombos_de_jakfa
}

set_politics = {
	ruling_party = nationalism
	last_election = "1922.6.28"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 0
	authoritarian_socialism = 0
	socialism = 7
	social_democracy = 5
	social_liberalism = 3
	market_liberalism = 3
	jacobin = 13
	democratic = 12
	neutrality = 7
	monarchism = 2
	nationalism = 40
	fascism = 8
}

set_cosmetic_tag = HUN_kingdom
COALITION_add_fas = yes

create_corps_commander = {
	name = "Géza Lakatos"
	gfx = GFX_portrait_hun_geza_lakatos
	traits = {  }
	skill = 3
	attack_skill = 3
	defense_skill = 2
	planning_skill = 3
	logistics_skill = 2
}

create_corps_commander = {
	name = "Iván Hindy"
	gfx = GFX_Portrait_hungary_ivan_hindy
	traits = {  trickster }
	skill = 4
	attack_skill = 3
	defense_skill = 4
	planning_skill = 2
	logistics_skill = 4
}

create_corps_commander = {
	name = "Károly Beregfy"
	gfx = GFX_Portrait_hungary_karoly_beregfy
	traits = { }
	skill = 1
	attack_skill = 1
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}

create_corps_commander = {
	name = "Lajos Veress"
	gfx = GFX_Portrait_hungary_lajos_veress
	traits = { armor_officer }
	skill = 2
	attack_skill = 2
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 2
}

create_corps_commander = {
	name = "Dezső László"
	gfx = GFX_portrait_hun_deszo_laszlo
	traits = {  }
	skill = 2
	attack_skill = 1
	defense_skill = 2
	planning_skill = 2
	logistics_skill = 3
}

create_corps_commander = {
	name = "Gusztáv Jány"
	gfx = GFX_portrait_hun_gusztav_jany
	traits = { urban_assault_specialist }
	skill = 3
	attack_skill = 2
	defense_skill = 3
	planning_skill = 2
	logistics_skill = 4
}


create_field_marshal = {
	name = "Ferenc Feketehalmy-Czeydner"
	gfx = GFX_Portrait_hungary_ferenc_feketehalmy_czeydner
	traits = { }
	skill = 2
	attack_skill = 2
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 2
}

create_country_leader = {
	name = "Mátyás Rákosi"
	desc = "POLITICS_MATYAS_RAKOSKI_DESC"
	picture = "Portrait_Hungary_Matyas_Rakoski.dds"
	expire = "1965.1.1"
	ideology = stalinism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Mátyás Rákosi"
	desc = "POLITICS_MATYAS_RAKOSKI_DESC"
	picture = "Portrait_Hungary_Matyas_Rakoski.dds"
	expire = "1965.1.1"
	ideology = rev_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Árpád Szakasits"
	desc = "POLITICS_ARPAD_SZAKASITS_DESC"
	picture = "gfx/leaders/Europe/Portrait_Europe_Generic_2.dds"
	expire = "1965.1.1"
	ideology = dem_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Ferenc Szalasi"
	desc = "POLITICS_MIKLOS_HORTHY_DESC"
	picture = "Portrait_Hungary_Miklos_Horthy_cbts.tga"
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = {

	}
}

create_country_leader = {
	name = "Miklós Horthy"
	desc = "POLITICS_MIKLOS_HORTHY_DESC"
	picture = "Portrait_Hungary_Miklos_Horthy_cbts.tga"
	expire = "1965.1.1"
	ideology = agrarianism
	traits = {

	}
}

create_country_leader = {
	name = "Miklós Horthy"
	desc = "POLITICS_MIKLOS_HORTHY_DESC"
	picture = "Portrait_Hungary_Miklos_Horthy_cbts.tga"
	expire = "1965.1.1"
	ideology = military
	traits = {

	}
}

create_country_leader = {
	name = "Miklós Horthy"
	desc = "POLITICS_MIKLOS_HORTHY_DESC"
	picture = "Portrait_Hungary_Miklos_Horthy_cbts.tga"
	expire = "1965.1.1"
	ideology = social_conservatism
	traits = {

	}
}

create_country_leader = {
	name = "Miklós Horthy"
	desc = "POLITICS_MIKLOS_HORTHY_DESC"
	picture = "Portrait_Hungary_Miklos_Horthy_cbts.tga"
	expire = "1965.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Miklós Horthy"
	desc = "POLITICS_MIKLOS_HORTHY_DESC"
	picture = "Portrait_Hungary_Miklos_Horthy_cbts.tga"
	expire = "1965.1.1"
	ideology = market_liberal
	traits = {
		#
	}
}

create_country_leader = {
	name = "Miklós Horthy"
	desc = "POLITICS_MIKLOS_HORTHY_DESC"
	picture = "Portrait_Hungary_Miklos_Horthy_cbts.tga"
	expire = "1965.1.1"
	ideology = dem_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Miklós Horthy"
	desc = "POLITICS_MIKLOS_HORTHY_DESC"
	picture = "Portrait_Hungary_Miklos_Horthy_cbts.tga"
	expire = "1965.1.1"
	ideology = real_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Miklós Horthy"
	desc = "POLITICS_MIKLOS_HORTHY_DESC"
	picture = "Portrait_Hungary_Miklos_Horthy_cbts.tga"
	expire = "1965.1.1"
	ideology = stalinism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Miklós Horthy"
	desc = "POLITICS_MIKLOS_HORTHY_DESC"
	picture = "Portrait_Hungary_Miklos_Horthy_cbts.tga"
	expire = "1965.1.1"
	ideology = despotism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Miklós Horthy"
	desc = "POLITICS_MIKLOS_HORTHY_DESC"
	picture = "Portrait_Hungary_Miklos_Horthy_cbts.tga"
	expire = "1965.1.1"
	ideology = liberal_nationalism
	traits = {
		#
	}
}
create_country_leader = {
	name = "Miklós Horthy"
	desc = "POLITICS_MIKLOS_HORTHY_DESC"
	picture = "Portrait_Hungary_Miklos_Horthy_cbts.tga"
	expire = "1965.1.1"
	ideology = nationalist
	traits = {
		fascist_sympathies
		anti_communist	
	}
}