capital = 329

oob = "TAN_1936"

set_research_slots = 1
set_stability = 0.35
set_war_support = 0.5

# Starting tech
set_technology = {
	infantry_weapons1 = 1
	tech_recon = 1
	gw_artillery = 1 #Based off the tuvan army in 1931
	tech_support = 1
	electronic_mechanical_engineering = 1 # Some form of electronic communications existed in Tuva in 1929


}

add_ideas = {
	TAN_high_sov_infl
	TAN_mass_illiteracy
	TAN_nomadic_peoples
	TAN_tere_kholsky_unrest_idea
	Adyg_Tyulyush_Khemchik_ool
}

set_politics = {

	ruling_party = communism
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	communism = 63
	authoritarian_socialism = 22
	socialism = 0
	social_democracy = 0
	social_liberalism = 0
	market_liberalism = 0
	jacobin = 0
	democratic = 8
	neutrality = 0
	monarchism = 7
	nationalism = 0
	fascism = 0
}

create_country_leader = {
	name = "Salchak Toka"
	desc = "POLITICS_SALCHAK_TOKA_DESC"
	picture = "Salchak_Toka.tga"
	expire = "1965.1.1"
	ideology = stalinism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Sat Churmit-Dazhy"
	desc = "POLITICS_SAT_CHURMIT_TAZHY_DESC"
	picture = "Sat_Churmit_Dazhy.tga"
	expire = "1965.1.1"
	ideology = council_communism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Salchak Toka"
	desc = "POLITICS_SALCHAK_TOKA_DESC"
	picture = "Salchak_Toka.tga"
	expire = "1965.1.1"
	ideology = real_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Sat Churmit-Dazhy"
	desc = "POLITICS_SAT_CHURMIT_TAZHY_DESC"
	picture = "Sat_Churmit_Dazhy.tga"
	expire = "1965.1.1"
	ideology = dem_socialism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Sat Churmit-Dazhy"
	desc = "POLITICS_SAT_CHURMIT_TAZHY_DESC"
	picture = "Sat_Churmit_Dazhy.tga"
	expire = "1965.1.1"
	ideology = liberalism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Sat Churmit-Dazhy"
	desc = "POLITICS_SAT_CHURMIT_TAZHY_DESC"
	picture = "Sat_Churmit_Dazhy.tga"
	expire = "1965.1.1"
	ideology = market_liberal
	traits = {
		#
	}
}

create_country_leader = {
	name = "Sat Churmit-Dazhy"
	desc = "POLITICS_SAT_CHURMIT_TAZHY_DESC"
	picture = "Sat_Churmit_Dazhy.tga"
	expire = "1965.1.1"
	ideology = centrism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Sat Churmit-Dazhy"
	desc = "POLITICS_SAT_CHURMIT_TAZHY_DESC"
	picture = "Sat_Churmit_Dazhy.tga"
	expire = "1965.1.1"
	ideology = national_conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Tyulyush Dagbaldai"
	desc = "POLITICS_TYULYUSH_DAGBALDAI_DESC"
	picture = ""
	expire = "1965.1.1"
	ideology = military
	traits = {
		#
	}
}

create_country_leader = {
	name = "Sat Churmit-Dazhy"
	desc = "POLITICS_SAT_CHURMIT_TAZHY_DESC"
	picture = "Sat_Churmit_Dazhy.tga"
	expire = "1965.1.1"
	ideology = despotism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Tyulyush Dagbaldai"
	desc = "POLITICS_TYULYUSH_DAGBALDAI_DESC"
	picture = ""
	expire = "1965.1.1"
	ideology = nationalist
	traits = {
		#
	}
}

create_country_leader = {
	name = "Tyulyush Dagbaldai"
	desc = "POLITICS_TYULYUSH_DAGBALDAI_DESC"
	picture = ""
	expire = "1965.1.1"
	ideology = fascism_ideology
	traits = {
		#
	}
}

create_field_marshal = {
	name = "Tyulyush Dagbaldai"
	picture = ""
	traits = {
		old_guard

		homeland_connoisseur
	}
	skill = 1
	attack_skill = 1
	defense_skill = 1
	planning_skill = 2
	logistics_skill = 1
}

create_corps_commander = {
	name = "Seren Kuzhuget"
	portrait_path = ""
	traits = {
		old_guard
		harsh_leader
		
	}
	id = 4120
	skill = 1
	attack_skill = 2
	defense_skill = 1
	planning_skill = 1
	logistics_skill = 1
}