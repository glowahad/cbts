
state={
	id=915
	name="STATE_915"
	state_category = rural
	manpower = 30000
	history={
		owner = YUG
		buildings = {
			infrastructure = 3
		}
		add_core_of = YUG
		add_core_of = SER
		add_core_of = MNT
		add_core_of = KOS
		add_claim_by = ALB
	}
	provinces={
		6913 
	}
	resources = {
		chromium = 45
	}
}
