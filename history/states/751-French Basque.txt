state={
	id=751
	name="STATE_751"
	provinces={
		743 
	}
	resources={
		steel=5
		aluminium=3
	}
	history={
		owner = FRA
		buildings = {
			infrastructure = 4
		}
		add_core_of = FRA
		victory_points = { 743 1 }
	}
	manpower=35601
	state_category = rural
	buildings_max_level_factor=1.000
}
