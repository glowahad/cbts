
state={
	id=553
	name="STATE_553"
	#adjusted
	manpower = 1057301
	
	state_category = large_town

	history={
		owner = LEB
		
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			air_base = 5
			792 = {
				naval_base = 3
			}
		}

		add_core_of = SYR
		add_core_of = LEB
	}

	provinces={
		792 1108 11919 
	}
}
