
state={
	id=825
	name="STATE_825"
	#adjusted
	manpower = 25541
	
	
	state_category = rural
	
	resources={
		steel = 3
	}
	history={
		owner = SPR
		add_claim_by = MRC
		add_core_of = SPR
		victory_points = {
		    12100 5 
	    }
		buildings = {
			infrastructure = 3
			12100 = {
				naval_base = 3
			}
		}
	}

	provinces={
		12100 
	}
}
