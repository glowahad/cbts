
state={
	id=681
	name="STATE_681" # Cape
	#adjusted
	manpower = 989864

	state_category = large_town

	history={
		owner = SAF
		buildings = {
			infrastructure = 5
			industrial_complex = 2
			air_base = 1
			12589 = {
				naval_base = 4
			}
		}
		victory_points = {
			12589 20
		}
		add_core_of = SAF
	}

	provinces={
		12589 13575 13576 12547 12692 10415 4822
	}
}
