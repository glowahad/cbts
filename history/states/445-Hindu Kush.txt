
state={
	id=445
	name="STATE_445" # Quetta
	#adjusted
	manpower = 387964
	
	state_category = pastoral

	history={
		owner = RAJ
		buildings = {
			infrastructure = 3
			air_base = 4
		}
		victory_points = {
			7847 1
		}
		add_core_of = PAK
		add_core_of = RAJ
	}

	provinces={
		1107 4990 7847 7903 7975 8044 10813 
	}
}
