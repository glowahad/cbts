
state={
	id=1125
	name="STATE_1125"
	resources={
		steel=5.000
	}

	history={
		victory_points = {
			11972 5
		}
		owner = GZC
		add_claim_by = CHI
		add_core_of = LWH
		add_claim_by = PRC
		add_claim_by = GZC
		buildings = {
			infrastructure = 3

		}
		victory_points = {
			11972 5 #Yibin
		}

	}

	provinces={
		11972
	}
	manpower=73433
	buildings_max_level_factor=1.000
	state_category=city
}
