state = {
	id=989
	name="STATE_989"
	manpower = 1173202
	state_category = rural
	history = {
		owner = INS
		add_core_of = INS
		buildings = {
			infrastructure = 2
			10153 = {
				naval_base = 3
			}
		}
		victory_points = {
			10153 1
		}
		victory_points = {
			12358 1
		}
	}
	provinces={
		1098 1289 1484 1505 1518 4262 4493 4537 7311 10153 10169 10197 12358 
	}
}