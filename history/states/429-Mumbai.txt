
state={
	id=429
	name="STATE_429"
	#adjusted
	manpower = 22403454
	state_category = large_town
	
	resources={
		tungsten=26
	}

	history={
		owner = RAJ
		victory_points = {
			12182 1 
		}
		buildings = {
			infrastructure = 4
			industrial_complex = 1
			air_base = 2
			1349 = {
				naval_base = 6
			}
		}
		victory_points = {
			1349 10 
		}

	}

	provinces={
		1349 1405 4072 7119 7177 7232 10019 10210 10270 12182 12269 4395 7411 4278 12105 12208 7395 7244 10190 7391

	}
}
