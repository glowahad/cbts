state={
	id=979
	name="STATE_979"
	manpower = 284710
	state_category = pastoral
	resources={
		rubber=27
		oil=2
	}
	history={
		owner = ENG
		add_core_of = SRW
		buildings = {
			infrastructure = 3
		}
		victory_points = {
			10212 1
		}
	}
	provinces={
		1306 4282 4338 7358 10143 10199 10212 10240 12171 12186 12228 13254
	}
}
