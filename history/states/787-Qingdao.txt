state={
	id=787
	name="STATE_787"
	history={
		victory_points = {
			10000 5
		}
		owner = SHD
		add_core_of = SHD
		add_claim_by = CHI
		add_claim_by = PRC
		victory_points = {
			10000 5
		}
		buildings = {
			infrastructure = 3
			air_base = 3
			10000 = {
				naval_base = 5
				coastal_bunker = 1
				bunker = 1

			}

		}

	}
	provinces={
		10000 
	}
	manpower=566420
	state_category=town
	buildings_max_level_factor=1.000
}
