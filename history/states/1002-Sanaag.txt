state=
{
	id=1002
	name="STATE_1002"
	#adjusted
	manpower = 120657
	state_category = pastoral
	history= {
		owner = ENG
		buildings = {
			infrastructure = 2
		}
		add_core_of = SMA
	}
	provinces={
		1905 8094 10921 12814 13297 13424
	}
}
