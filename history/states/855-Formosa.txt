state={
	id=855
	name="STATE_855"
	state_category = rural
	resources={
		rubber=14
	}
	history = {
		owner = ARG
		buildings = {
			infrastructure = 2
		}
		add_core_of = ARG
		victory_points = {
			12922 1
		}	
		victory_points = {
			2106 2
		}
	}
	provinces={
		2106 10986 12922 12943
	}
	manpower=30489
	buildings_max_level_factor=1.000
}
