
state={
	id=383
	name="STATE_383"
	#adjusted
	manpower = 1801432
	resources={
		oil=42
	}
	
	state_category = city

	history={
		owner = USA
		buildings = {
			infrastructure = 4
			arms_factory = 1
		}
		add_core_of = USA
	}

	provinces={
		1352 1753 4757 4811 4814 4871 7834 7892 9802 10532 12024 12513 4740 10340 4586 
	}
	
}
