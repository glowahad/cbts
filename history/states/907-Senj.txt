
state={
	id=907
	name="STATE_907" #senj
	manpower = 70000
	state_category = rural
	history={
		owner = YUG
		buildings = {
			infrastructure = 4
		}
		add_core_of = YUG
		add_core_of = CRO
	}
	provinces={
		3601 591 11901
	}
	resources = {
		aluminium = 20
	}
}
