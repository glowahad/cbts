
state={
	id=923
	name="STATE_923"
	#adjusted
	manpower = 7096780
	
	state_category = large_city

	history={
		owner = NEA
		add_claim_by = CHI
		add_core_of = NEA
		add_claim_by = PRC
		buildings = {
			infrastructure = 5
			industrial_complex = 2
			10068 = {
				naval_base = 2
			}
		}

		1938.10.25 = {
			controller = JAP
		}
	}

	provinces={
		4137 9969 10068 11996 10003 11980 10119 11944
	}
}