
state={
	id=17
	name="STATE_17" # Lorraine
	#adjusted
	manpower = 1453377
	
	state_category = city
	
	resources={
		steel=48
	}

	history={
		owner = FRA
		victory_points = {
			11516 5 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 1
			air_base = 5
		}
		add_core_of = FRA
	}

	provinces={
		521 11516 11642 9505 3546 5291 3560 11488
	}
}
