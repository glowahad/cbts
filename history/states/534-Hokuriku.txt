
state={
	id=534
	name="STATE_534" # Hokuriku
	#adjusted
	manpower = 4173400
	
	state_category = city

	history={
		owner = JAP
		add_core_of = JAP
		buildings = {
			infrastructure = 4
			arms_factory = 2
			industrial_complex = 2
		}
		
		victory_points = {
			1117 10
		}
		victory_points = {
			10032 15
		}


	}

	provinces={
		998 1117 7102 9952 10032 11930 12007 13342 13344
	}
}
