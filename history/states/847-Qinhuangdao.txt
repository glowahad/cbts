state={
	id=847
	name="STATE_847" # Shanhai Pass
	#adjusted
	manpower = 500000
	
	state_category = town

	history={
		victory_points = {
			3900 3
		}
		owner = NEA
		add_claim_by = CHI
		add_core_of = NEA
		add_claim_by = PRC
		victory_points = {
			3900 3
		}
		buildings = {
			infrastructure = 5
			3900 = {
				naval_base = 2
			}
		}

	}

	provinces={
		3900
	}
}
