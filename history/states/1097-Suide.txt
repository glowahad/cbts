state=
{
	id=1097
	name="STATE_1097" # Shaanxi
	#adjusted
	manpower = 309354
	
	state_category = town
	
	history=
	{
		owner = YUL
		add_claim_by = CHI
		add_claim_by = PRC
		add_core_of = YUL
		victory_points = {
			10880 1
		}
		buildings = {
			infrastructure = 1
			
		}

	}
	provinces={
		1458 10880
	}
}
