state={
	id=692
	name="STATE_692"
	#adjusted
	manpower=73250
	state_category = small_island

	history={
		owner = ENG
		victory_points = {
			11350 1
		}
		buildings = {
			infrastructure = 4
			11350 = {
				naval_base = 1
			}
		}
	}
	
	provinces={
		379 11106 11350 13009 
	}
}
