
state={
	id=826
	name="STATE_826"
	resources={
		steel=13.000
	}

	history={
		owner = SPR
		victory_points = {
			11707 3 
		}
		victory_points = {
			13567 3 
		}
		buildings = {
			infrastructure = 6
			arms_factory = 1
			11707 = {
				naval_base = 2
			}
		}
		add_core_of = SPR

	}

	provinces={
		2068 3729 3744 6749 13685 9719 11707 13567
	}
	manpower=430771
	buildings_max_level_factor=1.000
	state_category=large_town
}
