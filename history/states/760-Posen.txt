state={
	id=760
	name="STATE_760"
	provinces={
		444 537 3473 6236 9387 11478 3438 3572 9252 11260 11288 6595 9470
	}
	manpower=684852
	state_category = town
	history={
		owner = GER
		buildings = {
			infrastructure = 5
			industrial_complex = 0
		}
		victory_points = { 6236 1 }
		victory_points = { 3572 1 }
		add_core_of = GER

	}
	buildings_max_level_factor=1.000
}
