state={
	id=731
	name="STATE_731"
	#adjusted
	manpower=1106814
	
	state_category = town

	history={
		owner = GRE
		victory_points = {
			11818 5 
		}
		buildings = {
			infrastructure = 4
			arms_factory = 1
			industrial_complex = 1
			air_base = 2
			11818 = {
				naval_base = 3

			}

		}
		add_core_of = GRE
	}
	
	provinces={
		936 966 3844 9837 10163 11818 
	}
}
