
state={
	id=498
	name="STATE_498"
	#adjusted
	manpower = 4814029

	state_category = town

	history={
		owner = BRA
		buildings = {
			infrastructure = 3
			5168 = {
				naval_base = 2
			}
		}
		add_core_of = BRA
		victory_points = {
			5168 5 
		}

	}

	provinces={
		5182 5168 8227 10345 10967 12961 
	}
	
}
