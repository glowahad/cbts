state={
	id=972
	name="STATE_972"
	#adjusted
	manpower = 2073877
	state_category = rural
	history={
		owner = SIA
		add_core_of = SIA
		buildings = {
			infrastructure = 1
		}
		victory_points = {
			7516 3
		}
		victory_points = {
			12360 1
		}
		victory_points = {
			1548 1
		}
	}
	provinces={
		1357 1535 1548 4511 4582 7516 10384 12360 12373
	}
}
