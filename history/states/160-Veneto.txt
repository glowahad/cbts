
state={
	id=160
	name="STATE_160"
	resources={
		steel=32.000
		aluminium=18.000
	}

	history={
		owner = ITA
		victory_points = {
			11584 20 
		}
		victory_points = { 3604 1 }
		victory_points = { 603 2 }
		buildings = {
			infrastructure = 6
			arms_factory = 1
			industrial_complex = 3
			dockyard = 1
			air_base = 3
			11584 = {
				naval_base = 6

			}

		}
		add_core_of = ITA
	}

	provinces={
		603 3604 9582 11584 
	}
	manpower=3795510
	buildings_max_level_factor=1.000
	state_category=large_city
}
