state={
	id=1046
	name="STATE_1046"
	manpower=400576
	
	state_category = pastoral
	
	history={
		owner = ROM
		buildings = { infrastructure = 3 }
		victory_points = { 9670 2 }
		victory_points = { 3689 1 }
		add_core_of = ROM
	}

	provinces={ 711 3689 9668 9670 }
}
