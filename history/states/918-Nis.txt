
state={
	id=918
	name="STATE_918"
	state_category = large_town
	manpower = 880000
	history={
		owner = YUG
		buildings = {
			infrastructure = 5
		}
		add_core_of = YUG
		add_core_of = SER
		victory_points = {
			11887 2
		}
	}
	provinces={
		6953 9906 9890 11887 
	}
}
