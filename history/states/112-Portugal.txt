
state={
	id=112
	name="STATE_112"
	#adjusted
	manpower = 2268993
	resources={
		tungsten=76
	}
	
	state_category = city

	history={
		owner = POR
		victory_points = {
			11805 30
		}
		victory_points = {
			924 2
		}
		victory_points = {
			970 1
		}
		buildings = {
			infrastructure = 6
			arms_factory = 1
			industrial_complex = 1
			air_base = 5
			11805 = {
				naval_base = 8
			}
		}
		add_core_of = POR
	}

	provinces={
		844 924 970 3861 11768 11805 
	}
}
