
state={
	id=221
	name="STATE_221"
	#adjusted
	manpower = 2133296
	
	state_category = city
	
	resources={
		steel=56
	}

	history={
		owner = SOV
		victory_points = {
			418 10
		}
		victory_points = {
			11461 1
		}
		buildings = {
			infrastructure = 7
			arms_factory = 1
			air_base = 5
		}
		add_core_of = SOV
		add_core_of = UKR
	}

	provinces={
		418 3409 3508 3531 6530 6554 9531 9556 11397 11461 11541 
	}
}
