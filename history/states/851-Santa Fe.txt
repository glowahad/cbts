state={
	id=851
	name="STATE_851"
	state_category = large_town
	resources={
		rubber=6
	}
	history = {
		owner = ARG
		buildings = {
			infrastructure = 5
			industrial_complex = 1
		}
		add_core_of = ARG
		victory_points = {
			13270 10
		}
		victory_points = {
			13269 15
		}
	}
	provinces={
		2172 5163 5213 7500 13269 13270 13271
	}
	manpower=1387084
	buildings_max_level_factor=1.000
}
