
state={
	id=932
	name="STATE_932"
	manpower = 541260
	state_category = town
	history={
		owner = COL
		buildings = {
			infrastructure = 4
			industrial_complex = 1
			air_base = 4
			1937 = {
				naval_base = 5
			}
		}
		victory_points = {
			12703 6
		}
		victory_points = {
			1937 8
		}
		add_core_of = COL
	}
	provinces={
		12703 1937 2085 8056
	}
}
