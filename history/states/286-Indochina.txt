state={
	id=286
	name="STATE_286"
	manpower = 3307064
	state_category = city
	resources={
		rubber=74.000
	}
	history={
		add_core_of = VIN
		owner = VIN
		buildings = {
			infrastructure = 4
			industrial_complex = 1
			air_base = 2
			4401 = {
				naval_base = 4

			}
		}
		victory_points = {
			4401 15 
		}
		victory_points = {
			12232 1
		}
	}
	provinces={
		1396 1423 4401 7238 7347 10232 10261 12150 12176 12232
	}
}
