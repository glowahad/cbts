state={
	id=784
	name="STATE_784"
	history={
		owner = USA
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			air_base = 5
			7552 = {
				naval_base = 5

			}

		}
		add_core_of = USA
		victory_points = {
			7552 10 
		}

	}
	provinces={
		7524 7552 12398 
	}
	manpower=1381247
	state_category = city
	buildings_max_level_factor=1.000
}
