
state={
	id=180
	name="STATE_180"
	#adjusted
	manpower = 923893
	
	state_category = city

	history={
		owner = POR
		victory_points = {
			10523 20 
		}
		victory_points = {
			9817 5
		}
		victory_points = {
			11673 3
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 1
			dockyard = 2
			9817 = {
				naval_base = 3
			}
		}
		add_core_of = POR
	}

	provinces={
		9817 10523 11673
	}
}
