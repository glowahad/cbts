state={
	id=492
	name="STATE_492"
	resources={
		tungsten=2.000
	}
	
	state_category = rural

	history={
		owner = PRU
		buildings = {
			infrastructure = 3
			industrial_complex = 1
		}
		add_core_of = PRU
		victory_points = {
			2210 1 
		}

	}

	provinces={
		2210 5202
	}
	#adjusted
	manpower = 315773
	buildings_max_level_factor=1.000
}
