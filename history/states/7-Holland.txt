state={
	id=7
	name="STATE_7"
	resources={
		steel=8
	}

	history={
		owner = HOL
		add_core_of = HOL
		victory_points = { 391 40 }
		victory_points = { 3211 10 }
		buildings = {
			infrastructure = 6
			arms_factory = 2
			industrial_complex = 1
			dockyard = 2
			air_base = 4
			3314 = {
				naval_base = 10
			}
		}
	}
	provinces={
		68 391 3211 3314
	}
	#adjusted
	manpower=3880135
	buildings_max_level_factor=1
	state_category= metropolis
}
