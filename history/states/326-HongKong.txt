state=
{
	id=326
	name="STATE_326"
	#adjusted
	manpower = 1095145
	state_category = large_town
	history=
	{
		owner = ENG
		add_claim_by = CHI
		add_claim_by = PRC
		victory_points = {
			10062 3 
		}
		buildings = {
			infrastructure = 5
			air_base = 1
			10062 = {
				naval_base = 6
				coastal_bunker = 1
			}
		}
	}
	provinces={
		10062
	}
}
