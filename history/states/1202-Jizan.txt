
state={
	id=1202
	name="STATE_1202"
	#adjusted
	manpower = 11092
	state_category = wasteland

	history={
		owner = SAU
		buildings = {
			infrastructure = 1
		}
		add_core_of = SAU
		add_claim_by = YEM
		set_demilitarized_zone = yes
	}

	provinces={
		8073
	}
}
