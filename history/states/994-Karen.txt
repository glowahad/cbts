state={
	id=994
	name="STATE_994"
	#adjusted
	manpower = 4879239
	resources={
		oil=34
	}
	state_category = town
	history={
		victory_points = {
			13366 3
		}
		victory_points = {
			1492 5
		}
		victory_points = {
			7163 1
		}
		owner = RAJ
		add_core_of = BRM
		add_core_of = RAJ
		buildings = {
			infrastructure = 3
		}
	}
	provinces={
		10344 1650 7034 12449 12404 4202 1333 4059 4019 7163 7190 4541 10386 12492 1492 13366
	}
}
