
state={
	id=909
	name="STATE_909"
	manpower = 948000
	state_category = large_town
	history={
		owner = YUG
		buildings = {
			infrastructure = 4
			industrial_complex = 1
		}
		add_core_of = YUG
		add_core_of = CRO
		add_core_of = BOS
		add_core_of = SER
		victory_points = {
			982 8
		}
	}
	provinces={
		606 953 6799 11899 11872 982 11741
	}
	resources = {
		aluminium = 30
	}
}
