
state={
	id=303
	name="STATE_303" # Lima
	manpower = 672103
	resources={
		oil=18
	}
	state_category = city
	history={
		owner = PRU
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 2
			air_base = 3
			12997 = {
				naval_base = 5
			}
		}
		add_core_of = PRU
		victory_points = {
			12997 5 
		}

	}

	provinces={
		12983 12997
	}
}
