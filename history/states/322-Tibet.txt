
state={
	id=322
	name="STATE_322"
	#adjusted
	manpower = 416683
	
	state_category = town
	
	resources={
		tungsten=2

	}

	history={
		owner = TIB
		buildings = {
			infrastructure = 3
		}
		victory_points = {
			5033 20
		}
		add_core_of = TIB
		add_claim_by = CHI
		add_claim_by = PRC
	}

	provinces={
		7926 5033 2098 5018 10741
	}
}
