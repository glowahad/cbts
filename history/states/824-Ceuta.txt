
state={
	id = 824
	name="STATE_824"
	#adjusted
	manpower = 26810
	
	
	state_category = rural
	
	resources={
		steel = 4
	}

	history={
		owner = SPR
		add_core_of = SPR
		add_claim_by = MRC
		victory_points = {
			9945 5 
		}
		buildings = {
			infrastructure = 4
		}
	}

	provinces={
		9945
	}
}
