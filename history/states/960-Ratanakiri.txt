state={
	id=960
	name="STATE_960"
	manpower = 82577
	state_category = pastoral
	history = {
		owner = BOL
		add_core_of = BOL 
		victory_points = {
			5229 3
		}
		victory_points = {
			5179 5
		}
		buildings = {
			infrastructure = 3
			arms_factory = 1
			air_base = 1
		}
	}
	provinces={
		2174 5179 5229 10987 12930
	}
}
