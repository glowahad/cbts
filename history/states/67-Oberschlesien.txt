
state={
	id=67
	name="STATE_67"
	#adjusted
	manpower = 1343324
	resources={
		steel=60
		aluminium=20
	}
	
	state_category = city

	history={
		owner = GER
		buildings = {
			infrastructure = 6
		}
		add_core_of = GER
		victory_points = { 11467 5 }
		victory_points = { 6512 1 }
	}

	provinces={
		479 6512 9457 9511 11467
	}
}
