
state={
	id=43
	name="STATE_43" # Northern Hungary
	#adjusted
	manpower = 3060897
	resources={
		steel=4
		aluminium=175
	}
	
	state_category = large_town

	history={
		owner = HUN
		buildings = {
			infrastructure = 5
			arms_factory = 1
			industrial_complex = 1
			air_base = 3
		}
		victory_points = { 6751 2 }
		victory_points = { 684 1 }
		add_core_of = HUN
	}

	provinces={
		684 716 3713 3731 6716 6751 11520 
	}
}
