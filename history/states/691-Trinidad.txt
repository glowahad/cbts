state={
	id=691
	name="STATE_691"
	#adjusted
	manpower = 395644
	resources={
		oil=16
	}
	
	state_category = rural


	history={
		owner = ENG
		buildings = {
			infrastructure = 3
			3284 = {
				naval_base = 1
			}
		}
		victory_points = {
			3284 2
		}
	}
	
	provinces={ 3284 13012 }
}
