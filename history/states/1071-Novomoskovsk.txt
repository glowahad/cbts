
state={
	id=1071
	name="STATE_1071" # Novomoskovsk
	#adjusted
	manpower = 870165
	
	state_category = rural
	history={
		owner = SOV
		victory_points = {
			472 1
		}
		buildings = {
			infrastructure = 4
			air_base = 2
		}
		add_core_of = SOV
		add_core_of = UKR
	}

	provinces={
		453 472 486 9463 9479 11452
	}
}
