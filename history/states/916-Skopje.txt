
state={
	id=916
	name="STATE_916"
	state_category = town
	manpower = 470000
	history={
		owner = YUG
		buildings = {
			infrastructure = 4
		}
		add_core_of = YUG
		add_core_of = SER
		add_core_of = MCE
		add_claim_by = BUL
		victory_points = {
			3882 5	
		}
	}
	provinces={
		3882 
	}
	resources = {
		chromium = 35
	}
}
