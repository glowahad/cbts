
state={
	id=831
	name="STATE_831"
	#adjusted
	manpower = 321235
	
	state_category = rural

	history={
		owner = SOV
		buildings = {
			infrastructure = 4
		}
		victory_points = {
			683 1 
		}
		add_core_of = SOV
	}

	resources={
		oil=4
	}

	provinces={
		683 700 712 728 6730 6748 11632
	}
}
