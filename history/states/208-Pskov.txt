
state={
	id=208
	name="STATE_208" # Luga
	#adjusted
	manpower = 327360

	state_category = rural

	history={
		owner = SOV
		buildings = {
			infrastructure = 5
			air_base = 3
		}
		victory_points = {
			126 2
		}
		victory_points = {
			9098 1
		}
		victory_points = {
			9097 3
		}
		add_core_of = SOV
	}

	provinces={
		104 126 181 3120 6139 6197 9097 9098 11032 11080 11139 11186
	}
}
