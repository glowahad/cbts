﻿### Pakistan OOB ###

division_template = {
	name = "Infantry Division"
	division_names_group = BRENGL_INF_01		# Uses generic English templates

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
        infantry = { x = 1 y = 0 }
		
	}
}

division_template = {
	name = "Armoured Division"
	division_names_group = BRENGL_ARM_01		# Uses generic English templates

	regiments = {
		light_armor = { x = 0 y = 0 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
	}
}

units = {
	division = {			
		name = "1st Pakistan Division"		
		location = 8022
		division_template = "Infantry Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.05

	}
	division = {			
		name = "2nd Pakistan Division"		
		location = 8022
		division_template = "Infantry Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.05

	}
	division = {			
		name = "3rd Pakistan Division"		
		location = 3456
		division_template = "Infantry Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.05

	}
	division = {			
		name = "4th Pakistan Division"		
		location = 3456
		division_template = "Infantry Division"
		start_experience_factor = 0.2
		start_equipment_factor = 0.05

	}





}

##### Starting Production #####
instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "PAK"
		}
		requested_factories = 1
		progress = 0.12
		efficiency = 100
	}
}