division_template = {
	name = "Guerrilleros de Resistencia"				# Militia units (lowest experience, oldest equipment)

	regiments = {
		militia = { x = 0 y = 0 }
		militia = { x = 0 y = 1 }
		militia = { x = 0 y = 2 }
	}
	priority = 0
}

units = {
    division = {
		name = "1a Guerrilla Republicana"		
		location = 896
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.3
		start_equipment_factor = 0.6

	}
	division = {
		name = "2a Guerrilla Republicana"		
		location = 3986
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.3
		start_equipment_factor = 0.6

	}
	division = {
		name = "3a Guerrilla Republicana"		
		location = 6914
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.3
		start_equipment_factor = 0.6

	}
	division = {
		name = "4a Guerrilla Republicana"		
		location = 4080
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.3
		start_equipment_factor = 0.6

	}
	division = {
		name = "5a Guerrilla Republicana"		
		location = 10009
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.3
		start_equipment_factor = 0.6

	}
	division = {
		name = "6a Guerrilla Republicana"		
		location = 10111
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.3
		start_equipment_factor = 0.6

	}
	
	division = {
		name = "1er Tercio del Requeté"		
		location = 3931
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.5
		start_equipment_factor = 0.8

	}
	division = {
		name = "2ndo Tercio del Requeté"		
		location = 932
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.5
		start_equipment_factor = 0.8

	}
	division = {
		name = "3er Tercio del Requeté"		
		location = 740
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.5
		start_equipment_factor = 0.8

	}
	
	division = {
		name = "1a Milicia de la MOP"		
		location = 11779
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8

	}
	division = {
		name = "2a Milicia de la MOP"		
		location = 9896
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8

	}
	division = {
		name = "1a Milicia de las MRPE"		
		location = 871
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8

	}
	division = {
		name = "2a Milicia de las MRPE"		
		location = 854
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8

	}
	division = {
		name = "3a Milicia de las MRPE"		
		location = 3899
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8

	}
	division = {
		name = "4a Milicia de las MRPE"		
		location = 6856
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8

	}
	division = {
		name = "5a Milicia de las MRPE"		
		location = 962
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8

	}
	division = {
		name = "6a Milicia de las MRPE"		
		location = 3873
		division_template = "Guerrilleros de Resistencia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8

	}
}

