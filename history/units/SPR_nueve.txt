division_template = {
	name = "División Blindada" 				# Armor Regiment
	regiments = {
		light_armor = { x = 0 y = 0 }
		light_armor = { x = 0 y = 1 }
		light_armor = { x = 0 y = 2 }
        motorized = { x = 1 y = 0 }
        motorized = { x = 1 y = 1 }	
        motorized = { x = 1 y = 2 }			
	}
	support = {
		recon = { x = 0 y = 0 }      # Recon consisted of ACs, motorcycle inf, cavalry
		engineer = { x = 0 y = 1 }
	}
}

units = {
	division = {
		name = "9a División Blindada"
		location = 3938
		division_template = "División Blindada"
		start_experience_factor = 0.8
		start_equipment_factor = 0.9

	}
}