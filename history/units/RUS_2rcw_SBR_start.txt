﻿##### Division Templates #####
division_template = {
	name = "Strelkovaya Diviziya"			# Rifle Division
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
	support = {
		recon = { x = 0 y = 0 } 		# Recon bn had 16 L tanks, 10 ACs, 1x mot inf co #Eng and recon removed and they seem rather OP. Could revisit if balance changes
		engineer = { x = 0 y = 1 } 	# (semi-mot) Engineer bn
	}
}
division_template = {
	name = "Militsiya"			# militia Division
	regiments = {
		militia = { x = 0 y = 0 }
		militia = { x = 0 y = 1 }
		militia = { x = 0 y = 2 }
		militia = { x = 1 y = 0 }
		militia = { x = 1 y = 1 }
		militia = { x = 1 y = 2 }
	}
	support = {
	}
}

division_template = {
	name = "Motostrelkovaya Diviziya"		# Motor Rifle Division Note: only represents Moscow Proletariat Division in 1936; after 1938-40, Motor Rifle Divisions had +1 mot regiment (3 bns) 
	regiments = {
		motorized = { x = 0 y = 0 }
		motorized = { x = 0 y = 1 }
		motorized = { x = 0 y = 2 }
		motorized = { x = 1 y = 0 }
		motorized = { x = 1 y = 1 }
		motorized = { x = 1 y = 2 }
	}
	support = {
		recon = { x = 0 y = 0 } 		# Recon bn had 16x L tanks, 45 ACs #Eng and recon removed and they seem rather OP. Could revisit if balance changes 
		engineer = { x = 0 y = 1 } 	# (mot) Engineer bn
	}
}
division_template = {
	name = "Gornostrelkovaya Diviziya"		# Mountaineers Division
	regiments = {
		mountaineers = { x = 0 y = 0 }	
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }	
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }
		mountaineers = { x = 2 y = 0 }	
		mountaineers = { x = 2 y = 1 }	
		mountaineers = { x = 2 y = 2 }	
		mountaineers = { x = 3 y = 0 }	
		mountaineers = { x = 3 y = 1 }	
		mountaineers = { x = 3 y = 2 }	
	}
	support = {
		engineer = { x = 0 y = 0 } 		# Engineer bn
		artillery = { x = 0 y = 1 } 	# Heavy Arty Regiment had 2x 122mm bn
	}
}
division_template = {
	name = "Kavaleriyskaya Diviziya" 	# Kavaleriyskaya Diviziya
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }
	}
	#support = {
	#	light_armor = { x = 0 y = 0 }	# Tank "rgt" of 64 light tanks, 28 ACs - (removed)
	#}
}

##### OOB #####
units = {
	division= {			
		name = "1ya Strelkovaya Diviziya"		
		location = 7274
		division_template = "Strelkovaya Diviziya"
		start_experience_factor = 0.35
		start_equipment_factor = 0.8
	}
	division= {			
		name = "2ya Strelkovaya Diviziya"		
		location = 7789
		division_template = "Strelkovaya Diviziya"
		start_experience_factor = 0.35
		start_equipment_factor = 0.8
	}
	division= {			
		name = "3ya Strelkovaya Diviziya"		
		location = 10696
		division_template = "Strelkovaya Diviziya"
		start_experience_factor = 0.35
		start_equipment_factor = 0.8
	}
	division= {			
		name = "4ya Strelkovaya Diviziya"		
		location = 7757
		division_template = "Strelkovaya Diviziya"
		start_experience_factor = 0.35
		start_equipment_factor = 0.8
	}
	division= {			
		name = "5ya Strelkovaya Diviziya"		
		location = 1369
		division_template = "Strelkovaya Diviziya"
		start_experience_factor = 0.35
		start_equipment_factor = 0.8
	}
	division= {			
		name = "6ya Strelkovaya Diviziya"		
		location = 1779
		division_template = "Strelkovaya Diviziya"
		start_experience_factor = 0.35
		start_equipment_factor = 0.8
	}
	division= {			
		name = "7ya Strelkovaya Diviziya"		
		location = 7274
		division_template = "Strelkovaya Diviziya"
		start_experience_factor = 0.35
		start_equipment_factor = 0.8
	}
	division= {			
		name = "8ya Strelkovaya Diviziya"		
		location = 1800
		division_template = "Strelkovaya Diviziya"
		start_experience_factor = 0.35
		start_equipment_factor = 0.8
	}
	division= {			
		name = "1ya Kavaleriyskaya Diviziya"		
		location = 10531
		division_template = "Kavaleriyskaya Diviziya"
		start_experience_factor = 0.35
		start_equipment_factor = 0.8
	}
	division= {			
		name = "2ya Kavaleriyskaya Diviziya"		
		location = 4803
		division_template = "Kavaleriyskaya Diviziya"
		start_experience_factor = 0.35
		start_equipment_factor = 0.8
	}
	division= {			
		name = "3ya Kavaleriyskaya Diviziya"		
		location = 7800
		division_template = "Kavaleriyskaya Diviziya"
		start_experience_factor = 0.35
		start_equipment_factor = 0.8
	}
	division= {			
		name = "4ya Kavaleriyskaya Diviziya"		
		location = 4767
		division_template = "Kavaleriyskaya Diviziya"
		start_experience_factor = 0.35
		start_equipment_factor = 0.8
	}
	division= {			
		name = "5ya Kavaleriyskaya Diviziya"		
		location = 7849
		division_template = "Kavaleriyskaya Diviziya"
		start_experience_factor = 0.35
		start_equipment_factor = 0.8
	}
	division= {			
		name = "6ya Kavaleriyskaya Diviziya"		
		location = 11410
		division_template = "Kavaleriyskaya Diviziya"
		start_experience_factor = 0.35
		start_equipment_factor = 0.8
	}
}