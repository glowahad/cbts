
division_template = {
	name = "Provincial Sefari 6"	
	is_locked = yes	
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		infantry = { x = 0 y = 4 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 1 y = 3 }
		infantry = { x = 1 y = 4 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
		infantry = { x = 2 y = 3 }
		infantry = { x = 2 y = 4 }
	}
}
division_template = {
	name = "Mountain Sefari 2"	
	is_locked = yes	
	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }
		mountaineers = { x = 0 y = 3 }
		mountaineers = { x = 0 y = 4 }	
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }
		mountaineers = { x = 1 y = 3 }
		mountaineers = { x = 1 y = 4 }
	}
}


division_template = {
	name = "Provincial Sefari 4"
	is_locked = yes
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
	}
}
#division_template = { #This might work
#	name = "Provincial Sefari w Artillery 2"
#	is_locked = yes		
#	regiments = {
#		infantry = { x = 0 y = 0 }
#		infantry = { x = 0 y = 1 }
#		infantry = { x = 0 y = 2 }
#		infantry = { x = 0 y = 3 }
#		infantry = { x = 0 y = 4 }
#		infantry = { x = 1 y = 0 }
#		infantry = { x = 1 y = 1 }
#		infantry = { x = 1 y = 2 }
#		infantry = { x = 1 y = 3 }
#		infantry = { x = 1 y = 4 }
#		infantry = { x = 2 y = 0 }
#		infantry = { x = 2 y = 1 }
#	}
#	support = {
#		artillery = { x = 0 y = 0}
#	}
#}

units = {
	division = {
		name = "Tigray Sefari"
		location = 7944
		division_template = "Provincial Sefari 6"
		start_experience_factor = 0
		start_equipment_factor = 0.5
	}
	division = {
		name = "Wag Sefari"
		location = 13265
		division_template = "Mountain Sefari 2"
		start_experience_factor = 0
		start_equipment_factor = 0.5
	}
	division = {
		name = "Lasta Sefari"
		location = 13265
		division_template = "Provincial Sefari 8"
		start_experience_factor = 0
		start_equipment_factor = 0.5
	}
	division = {
		name = "Yejju Sefari"
		location = 4954
		division_template = "Provincial Sefari 8"
		start_experience_factor = 0
		start_equipment_factor = 0.5
	}
	division = {
		name = "Danakil Sefari"
		location = 13435
		division_template = "Provincial Sefari 8"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
	division = {
		name = "Welega-Ardjo Sefari"
		location = 2009
		division_template = "Provincial Sefari w Artillery 2"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
	division = {
		name = "Illubabor Sefari"
		location = 7912
		division_template = "Provincial Sefari 7"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
	division = {
		name = "Gemu Gofa Sefari"
		location = 10734
		division_template = "Provincial Sefari 8"
		start_experience_factor = 0
		start_equipment_factor = 0.5
	}
	division = {
		name = "Gemu Gofa Sefari"
		location = 4995
		division_template = "Provincial Sefari 4"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5
	}
}