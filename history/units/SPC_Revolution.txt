﻿division_template = {
	name = "Milicia Revolucionaria"				# Militia units (lowest experience, oldest equipment)

	regiments = {
		militia = { x = 0 y = 0 }
		militia = { x = 0 y = 1 }
		militia = { x = 0 y = 2 }
	}
	priority = 0
        is_locked = yes
}

division_template = {
	name = "División de Infantería Proletaria" 		# Used for both regular infantry divisions and larger garrison divisions
	# Note: Spanish divisions were 2x brigades of 2x2 rgts each, + support
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 1 y = 3 }
	}
	support = {
		artillery = { x = 0 y = 0 }  # Spanish Arty Rgt/Bge had 2 Bn 105mm arty
		engineer = { x = 0 y = 1 }   # Eng Bn
	}
	priority = 1
}
division_template = {
	name = "División de Caballería Proletaria"  		# Only one Cavalry Division (3x bge of 2x2 Rgts)

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		cavalry = { x = 0 y = 3 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }
		cavalry = { x = 1 y = 3 }
		cavalry = { x = 2 y = 0 }
		cavalry = { x = 2 y = 1 }
		cavalry = { x = 2 y = 2 }
		cavalry = { x = 2 y = 3 }
	}
	support = {
		recon = { x = 0 y = 0 }      # Recon consisted of motorcycles and ACs
	}
}
division_template = {
	name = "Brigada de Montaña"  		# Mountain Brigades were 2x2 Rgts + support

	regiments = {
		mountaineers = { x = 0 y = 0 }	
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
	}
	support = {
		artillery = { x = 0 y = 0 }  # Spanish Arty Rgt/Bge had 2 Bn 105mm arty
	}
}

units = {
	#####LAND UNITS #####
	### MAINLAND UNITS ###
	division = {
		name = "1os Mineros Asturianos"		
		location = 11707
		division_template = "Milicia Revolucionaria"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8

	}			
	division = {			
		name = "2os Mineros Asturianos"		
		location = 11707
		division_template = "Milicia Revolucionaria"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8

	}			
}