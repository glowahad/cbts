division_template = {
	name = "Juntuán"				# Represents: two-division infantry corps (generally poorly-equipped militias), and local militia groups

	regiments = {
		militia = { x = 0 y = 0 }	# Note: Chinese divisions were brigade-sized compared other nations' armies
		militia = { x = 0 y = 1 }
		militia = { x = 1 y = 0 }
		militia = { x = 1 y = 1 }
	}
}

division_template = {
	name = "Bubing Shi"				

	regiments = {
		infantry = { x = 0 y = 0 }	
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
	}
}


### OOB ###
units = {
	division = {
		name = "62 Juntuán"
		location = 10404
		start_experience_factor = 0.1
		division_template = "Juntuán"
		start_equipment_factor = 0.7

	}
	division = {
		name = "63 Juntuán"
		location = 1047
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7

	}
	division = {
		name = "64 Juntuán"
		location = 7137
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "68 Juntuán"
		location = 7137
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "69 Juntuán"
		location = 7137
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "70 Juntuán"
		location = 7137
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "71 Juntuán"
		location = 7137
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "72 Juntuán"
		location = 7137
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "65 Juntuán"
		location = 9938
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7

	}
	division = {
		name = "66 Juntuán"
		location = 994
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7

	}
	division = {
		name = "67 Juntuán"
		location = 1047
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7

	}
	division = {
		name = "Guangzhou Jingbei"
		location = 1047
		division_template = "Juntuán"		# Provincial militia unit (poor equipment and training)
		start_equipment_factor = 0.5

	}
	division = {
		name = "Hainan Jingbei"
		location = 994
		division_template = "Juntuán"		# Provincial militia unit (poor equipment and training)
		start_equipment_factor = 0.5

	}
	division = {
		name = "Guilin Jingbei"
		location = 10404
		division_template = "Juntuán"		# Provincial militia unit (poor equipment and training)
		start_equipment_factor = 0.5

	}
	division = {
		name = "73 Juntuán"
		location = 10404
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "74 Juntuán"
		location = 10404
		division_template = "Juntuán"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "1 Bubing Shi"
		location = 7128
		division_template = "Bubing Shi"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "2 Bubing Shi"
		location = 7128
		division_template = "Bubing Shi"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "3 Bubing Shi"
		location = 7128
		division_template = "Bubing Shi"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "4 Bubing Shi"
		location = 7128
		division_template = "Bubing Shi"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "5 Bubing Shi"
		location = 10404
		division_template = "Bubing Shi"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	division = {
		name = "6 Bubing Shi"
		location = 7137
		division_template = "Bubing Shi"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}

}
