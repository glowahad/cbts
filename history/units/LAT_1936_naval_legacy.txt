units = {

	### Naval OOB ###
	fleet = {					
		name = "Latvijas Juras Speki"				
		naval_base = 9340
		task_force = {
			name = "Latvijas Juras Speki"				
			location =  9340 # Riga
			ship = { name = "Ronis" definition = submarine equipment = { submarine_1 = { amount = 1 owner = LAT } } }
			ship = { name = "Spidola" definition = submarine equipment = { submarine_1 = { amount = 1 owner = LAT } } }
		}					
	}
}
