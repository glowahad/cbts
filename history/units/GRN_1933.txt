﻿division_template = {
	name = "Garrison Division"	# represents first part of enlarged divisions

	regiments = {
		garrison = { x = 0 y = 0 }
		garrison = { x = 0 y = 1 }
		garrison = { x = 1 y = 0 }
		garrison = { x = 1 y = 1 }
	}
	support = {
	}
}