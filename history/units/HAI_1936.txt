﻿division_template = {
	name = "Brigade de Infanterie"		# Garde d'Haiti was a limited reserve force created in 1934
	# Note: militia level training and equipment
	regiments = {
		militia = { x = 0 y = 0 }
		militia = { x = 0 y = 1 }
		militia = { x = 0 y = 2 }
	}
}