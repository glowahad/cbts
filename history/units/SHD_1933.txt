﻿﻿division_template = {
	name = "Juntuán"				# Represents local militia groups (generally poorly-equipped )

	regiments = {
		militia = { x = 0 y = 0 }	# Note: Chinese divisions were brigade-sized compared other nations' armies
		militia = { x = 0 y = 1 }
		militia = { x = 1 y = 0 }
		militia = { x = 1 y = 1 }
	}

}
division_template = {
	name = "Sanjiao Jun"			# Represents three-division infantry corps (generally poorly-equipped militias)

	regiments = {
		militia = { x = 0 y = 0 }
		militia = { x = 0 y = 1 }
		militia = { x = 1 y = 0 }
		militia = { x = 1 y = 1 }
		militia = { x = 2 y = 0 }
		militia = { x = 2 y = 1 }
	}
}

division_template = {
	name = "Infantry"			

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
	}
}
division_template = {
	name = "Qibing Jun"		# Represents two-division cavalry corps (generally poorly-equipped)

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
	}
}

### OOB ###
units = {
	### Provincial Forces ###
	division = {
		name = "9 Juntuán"
		location = 1069
		division_template = "Sanjiao Jun"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5

	}
	division = {
		name = "1 Qibing Jun"
		location = 1069
		division_template = "Qibing Jun"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5

	}
	division = {
		name = "2 Qibing Jun"
		location = 1069
		division_template = "Qibing Jun"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5

	}
	division = {
		name = "3 Qibing Jun"
		location = 10000
		division_template = "Qibing Jun"
		start_experience_factor = 0.1
		start_equipment_factor = 0.5

	}
	division = {
		name = "Qingdao Jingbei"
		location = 10000
		division_template = "Sanjiao Jun"		# Garrison unit (poor equipment and training)
		start_equipment_factor = 0.5

	}
	## 3rd Army (CO: Han Fuqu) ##
	division = {
		name = "12 Juntuán"
		location = 4205
		division_template = "Infantry"
		start_experience_factor = 0.1
		start_equipment_factor = 0.7
	}
	
	## Expanded Divisions ##
	division = {
		name = "1 Infantry"
		location = 1069
		division_template = "Infantry"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
    division = {
		name = "2 Infantry"
		location = 1069
		division_template = "Infantry"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
    division = {
		name = "3 Infantry"
		location = 4205
		division_template = "Infantry"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
    division = {
		name = "4 Infantry"
		location = 10000
		division_template = "Infantry"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
}
