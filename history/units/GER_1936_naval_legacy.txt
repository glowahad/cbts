units = {

	### Naval OOB ###
	fleet = {
		name = "Reichsmarine" #Later renamed to Kriegsmarine
		naval_base = 241
		task_force = {
			name = "Marinestation der Nordsee" # Wilhelmshaven
			location = 241  # Wilhelmshaven
			ship = { name = "Schleswig-Holstein" definition = battleship equipment = { battleship_1 = { amount = 1 owner = GER } } }
			ship = { name = "Schlesien" definition = battleship equipment = { battleship_1 = { amount = 1 owner = GER } } }
			ship = { name = "Nürnberg" definition = light_cruiser equipment = { light_cruiser_1 = { amount = 1 owner = GER } } } 		
			ship = { name = "Leipzig" definition = light_cruiser equipment = { light_cruiser_1 = { amount = 1 owner = GER } } }		
			ship = { name = "Königsberg" definition = light_cruiser equipment = { light_cruiser_1 = { amount = 1 owner = GER } } }			
			ship = { name = "Karlsruhe" definition = light_cruiser equipment = { light_cruiser_1 = { amount = 1 owner = GER } } }	
			ship = { name = "Köln" definition = light_cruiser equipment = { light_cruiser_1 = { amount = 1 owner = GER } } }			
			ship = { name = "Emden" definition = light_cruiser equipment = { light_cruiser_1 = { amount = 1 owner = GER } } }
			ship = { name = "Arcona" definition = light_cruiser equipment = { light_cruiser_1 = { amount = 1 owner = GER } } }		
			ship = { name = "Jaguar" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = GER } } }
			ship = { name = "Leopard" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = GER } } }
			ship = { name = "Luchs" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = GER } } }
			ship = { name = "Tiger" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = GER } } }
			ship = { name = "Möwe" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = GER } } }
			ship = { name = "Albatros" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = GER } } }
			ship = { name = "Seeadler" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = GER } } }
			ship = { name = "Greif" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = GER } } }
			ship = { name = "Falke" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = GER } } }
			ship = { name = "Kondor" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = GER } } }
			ship = { name = "Wolf" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = GER } } }
			ship = { name = "Iltis" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = GER } } }
		}
	}
}

instant_effect = {

	# CA: "Deutschland"
	add_equipment_production = {
		equipment = {
			type = heavy_cruiser_1
			creator = "GER"
			version_name = "Hipper Class"
		}
		requested_factories = 1
		progress = 0.70
		amount = 1
	}
# CA: "Admiral Scheer"
	add_equipment_production = {
		equipment = {
			type = heavy_cruiser_1
			creator = "GER"
			version_name = "Hipper Class"
		}
		requested_factories = 1
		progress = 0.30
		amount = 1
	}
# CA: "Admiral Graf Spee"
	add_equipment_production = {
		equipment = {
			type = heavy_cruiser_1
			creator = "GER"
			version_name = "Hipper Class"
		}
		requested_factories = 1
		progress = 0.01
		amount = 1
	}
# CL: "Nürnberg"
	add_equipment_production = {
		equipment = {
			type = light_cruiser_1
			creator = "GER"
		}
		requested_factories = 1
		progress = 0.75
		amount = 2
	}
}




