division_template = {
	name = "Bubing Shi"				

	regiments = {
		infantry = { x = 0 y = 0 }	
		infantry = { x = 0 y = 1 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
	}
}
units = {
    division = {
		name = "Yulin Jingbei"
		location = 7314
		division_template = "Bubing Shi"
		start_equipment_factor = 0.8
	}
    division = {
		name = "Yan'an Jingbei"
		location = 12356
		division_template = "Bubing Shi"
		start_experience_factor = 0.1
		start_equipment_factor = 0.8
	}
}
