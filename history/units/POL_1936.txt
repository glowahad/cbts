﻿division_template = {
	name = "Dywizja Piechoty"		

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
	support = {
        recon = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Dywizja Piechoty Górskiej"

	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }	
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }
		mountaineers = { x = 2 y = 0 }	
		mountaineers = { x = 2 y = 1 }	
		mountaineers = { x = 2 y = 2 }	
	}
	support = {
        recon = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Brygada Kawalerii" 

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
	}
	support = {
		recon = { x = 0 y = 0 }     
	}
}
division_template = {
	name = "Brygada KOP"		

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
	}
	support = {
        recon = { x = 0 y = 0 }
	}
}


units = {
	######## Sztab Generalny Wojska Polskiego ########
	### Armia Pomorze ###	
	division= {	
		name = "4 Dywizja Piechoty"
		location = 362
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "15 Dywizja Piechoty"
		location = 389
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "16 Dywizja Piechoty"
		location = 9263
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}

	### Armia Poznan ###	
	division= {	
		name = "14 Dywizja Piechoty"
		location = 6558
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "17 Dywizja Piechoty"
		location = 11232
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "25 Dywizja Piechoty"
		location = 3381
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.9

	}
	division= {	
		name = "Podolska B.K."
		location = 9532
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_1 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.9

	}
	division= {	
		name = "Wielkopolska B.K."
		location = 6558
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_1 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.9

	}

	### Armia Lódz ###	
	division= {	
		name = "2 Dywizja Piechoty"
		location = 9508
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "7 Dywizja Piechoty"
		location = 9508
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "10 Dywizja Piechoty"
		location = 9508
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "Kresowa B.K."
		location = 9508
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_1 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.9

	}
	division= {	
		name = "Wolynska B.K."
		location = 9508
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_1 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.9

	}

	### Armia Kraków ###	
	division= {	
		name = "6 Dywizja Piechoty"
		location = 9427
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "21 Dywizja Piechoty Górskiej"
		location = 506
		division_template = "Dywizja Piechoty Górskiej"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "23 Dywizja Piechoty"
		location = 6464
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.9

	}

	### Armia Modlin ###	
	division= {	
		name = "8 Dywizja Piechoty"
		location = 3544
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "26 Dywizja Piechoty"
		location = 3544
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.9

	}
	division= {	
		name = "28 Dywizja Piechoty"
		location = 3544
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.9

	}
	division= {	
		name = "Nowogródzka B.K."
		location = 11492
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_1 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.9

	}
	division= {	
		name = "Mazowiecka B.K."
		location = 11492
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_1 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.9

	}

	### Armia Wilno ###	
	division= {	
		name = "1 DP Legionów im. Pilsudskiego"
		location = 3320
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "20 Dywizja Piechoty"
		location = 406
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}

	### Armia Wolyn ###	
	division= {	
		name = "3 Dywizja Piechoty Legionów"
		location = 11543
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "11 Karpacka Dywizja Piechoty"
		location = 6557
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}

	## GO Lublin ##	
	division= {	
		name = "9 Dywizja Piechoty"
		location = 6580
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "30 Dywizja Piechoty"
		location = 6580
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.9

	}

	## SGO Narew ##	
	division= {	
		name = "29 Dywizja Piechoty"
		location = 290
		division_template = "Dywizja Piechoty"
		start_experience_factor = 0.2
		start_equipment_factor = 0.9

	}
	division= {	
		name = "Suwalska B.K."
		location = 290
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_1 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.9

	}
	division= {	
		name = "Pomorska B.K."
		location = 290
		division_template = "Brygada Kawalerii" 
		force_equipment_variants = { infantry_equipment_1 = { owner = "POL" } }
		start_experience_factor = 0.2
		start_equipment_factor = 0.9

	}

	## SGO Polesie ##	
	#division= {	
	#	name = "27 Dywizja Piechoty"
	#	location = 6579
	#	division_template = "Dywizja Piechoty"
	#	start_experience_factor = 0.2
	#	start_equipment_factor = 0.9
#
#	}

	## SGO Lwów ##	
	#division= {	
	#	name = "5 Dywizja Piechoty"
	#	location = 11479
	#	division_template = "Dywizja Piechoty"
	#	start_experience_factor = 0.3
	#	start_equipment_factor = 0.9
#
	#}
	
	## Korpus Ochrony Przygranicza ##
	division= {	
		name = "Brygada KOP Grodno"
		location = 3393
		division_template = "Brygada KOP"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "Brygada KOP Wilno"
		location = 3320
		division_template = "Brygada KOP"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "Brygada KOP Podole"
		location = 3483
		division_template = "Brygada KOP"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "Brygada KOP Wołyń"
		location = 11543
		division_template = "Brygada KOP"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "Brygada KOP Polesie"
		location = 3556
		division_template = "Brygada KOP"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}
	division= {	
		name = "Brygada KOP Nowogródek"
		location = 3309
		division_template = "Brygada KOP"
		start_experience_factor = 0.3
		start_equipment_factor = 0.9

	}

	
	######## NAVAL OOB ########
}

