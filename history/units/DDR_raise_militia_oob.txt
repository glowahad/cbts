﻿division_template = {
	name = "People's Militia"

	regiments = {
		militia = { x = 0 y = 0 }
		militia = { x = 0 y = 1 }
		militia = { x = 0 y = 2 }
       		militia = { x = 1 y = 0 }
		militia = { x = 1 y = 1 }
		militia = { x = 1 y = 2 }
	}
	
}

units = {

	##### Reichswehr #####
	division= {	
		name = "1. People's Militia"
		location = 6521
		division_template = "People's Militia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7
	}
	division= {	
		name = "2. People's Militia"
		location = 9347
		division_template = "People's Militia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7
	}
	division= {	
		name = "3. People's Militia"
		location = 6377
		division_template = "People's Militia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7
	}
	division= {	
		name = "4. People's Militia"
		location = 6488
		division_template = "People's Militia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7
	}
	division= {	
		name = "5. People's Militia"
		location = 692
		division_template = "People's Militia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7
	}
	division= {	
		name = "6. People's Militia"
		location = 6332
		division_template = "People's Militia"
		start_experience_factor = 0.2
		start_equipment_factor = 0.7
	}
}