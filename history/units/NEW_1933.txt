﻿division_template = {
	name = "District Militia"
	regiments = {
		militia = { x = 0 y = 0 }
		militia = { x = 0 y = 1 }
		militia = { x = 1 y = 0 }
		militia = { x = 1 y = 1 }
	}
}

division_template = {
	name = "Infantry Division"
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
	}
}
division_template = {
	name = "Cavalry Militia"  
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
	}
}

units = {
	division= {	
		name = "Newfoundland Royal Militia"
		location = 12505
		division_template = "District Militia"
		start_experience_factor = 0.1
		start_equipment_factor = 0.2
	}
}
air_wings = { 
	331 = {
		gw_fighter_equipment = {
			#name = "Squadron No. 1 - The Cod in the Ring"
			owner = "NEW" 
			amount = 9
		}
		gw_fighter_equipment = {
			#name = "Squadron No. 2 - The Blue Puttees"
			owner = "NEW" 
			amount = 9
		}
	}
}