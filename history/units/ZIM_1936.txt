﻿

division_template = {
	name = "Infantry Division"		
	# Note: Represents both regular infantry and militia units
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }

		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }

		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
	}
}

division_template = {
	name = "Colonial Militia"		
	regiments = {
		militia = { x = 0 y = 0 }
		militia = { x = 0 y = 1 }

		militia = { x = 1 y = 0 }
		militia = { x = 1 y = 1 }

		militia = { x = 2 y = 0 }
		militia = { x = 2 y = 1 }
	}
}



units = {
	division= {	
		name = "Rhodesia Militia"
		location = 10929 # Salisbury
		division_template = "Colonial Militia"
		start_experience_factor = 0.05
		start_equipment_factor = 0.1

	}
}


### No air forces (small handful of various aircraft in 1935) ###

#########################
## STARTING PRODUCTION ##
#########################


#################################