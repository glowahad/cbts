division_template = {
	name = "Infanterie Divisie"

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
}

division_template = {
	name = "Koloniale Garnizoen"		# miltia forces, lower experience and equipment

	regiments = {
		garrison = { x = 0 y = 0 }
		garrison = { x = 0 y = 1 }
	}
	priority = 0
}

units = {
	##### OLZ Indië #####
	### Koninklijk Nederlands-Indisch Leger ### ###transfered to INS control as of 1.3
	division= {	
		name = "Ie Divisie 'Java'"
		location = 7381  # Batavia
		division_template = "Infanterie Divisie"
		force_equipment_variants = { infantry_equipment_1 = { owner = "HOL" } }
		start_equipment_factor = 0.3

	}
	division= {	
		name = "IIe Divisie 'Java'"
		location = 4608  # Soerjabaja
		division_template = "Infanterie Divisie"
		force_equipment_variants = { infantry_equipment_1 = { owner = "HOL" } }
		start_equipment_factor = 0.3

	}
	division= {	
		name = "IIIe Divisie 'Java'"
		location = 7642  # Malang
		division_template = "Infanterie Divisie"
		force_equipment_variants = { infantry_equipment_1 = { owner = "HOL" } }
		start_equipment_factor = 0.3

	}

	division= {	
		name = "Ie Territoriaal Commando 'Sumatra'"
		location = 4652  # Medan
		division_template = "Koloniale Garnizoen"
		force_equipment_variants = { infantry_equipment_1 = { owner = "HOL" } }
		start_equipment_factor = 0.3

	}
	division= {	
		name = "IIe Territoriaal Commando 'Sumatra'"
		location = 4446 # Padang
		division_template = "Koloniale Garnizoen"
		force_equipment_variants = { infantry_equipment_1 = { owner = "HOL" } }
		start_equipment_factor = 0.3

	}
	division= {	
		name = "IIIe Territoriaal Commando 'Sumatra'"
		location = 12268  # Palembang
		division_template = "Koloniale Garnizoen"
		force_equipment_variants = { infantry_equipment_1 = { owner = "HOL" } }
		start_equipment_factor = 0.3

	}

	##### OLZ Borneo #####
	division= {	
		name = "Ie Territoriaal Commando 'Borneo'"
		location = 10237  # Balikpapan
		division_template = "Koloniale Garnizoen"
		force_equipment_variants = { infantry_equipment_1 = { owner = "HOL" } }
		start_equipment_factor = 0.3

	}
	division= {	
		name = "IIe Territoriaal Commando 'Borneo'"
		location = 1316  # Pontianak
		division_template = "Koloniale Garnizoen"
		force_equipment_variants = { infantry_equipment_1 = { owner = "HOL" } }
		start_equipment_factor = 0.3

	}

	##### OLZ Celebes en Nieuw-Guinea #####
	division= {	
		name = "Territoriaal Commando 'Celebes'"
		location = 10153  # Manado
		division_template = "Koloniale Garnizoen"
		force_equipment_variants = { infantry_equipment_1 = { owner = "HOL" } }
		start_equipment_factor = 0.3

	}


	##### NAVAL UNITS #####
	### ROYAL NAVY ###

	### EAST INDIES SQUADRON ###
}