﻿division_template = {
	name = "Royal Militia"
	regiments = {
		militia = { x = 0 y = 0 }
		militia = { x = 0 y = 1 }
		militia = { x = 1 y = 0 }
		militia = { x = 1 y = 1 }
	}
}

division_template = {
	name = "Infantry Division"
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
	}
}
division_template = {
	name = "Cavalry Division"  
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 2 y = 1 }
		cavalry = { x = 3 y = 2 }
	}
}

units = {
	division= {	
		name = "Royal Guard"
		location = 7151
		division_template = "Infantry Division"
		start_experience_factor = 0.3
		start_equipment_factor = 0.4

	}
}