units = {

	### Naval OOB ###
	fleet = {				
		name = "Armada Argentina"			
		naval_base = 12364 
		task_force = {
			name = "Armada Argentina"			
			location = 12364 # Buenos Aires
			ship = { name = "ARA Rivadavia" definition = battleship equipment = { battleship_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA Moreno" definition = battleship equipment = { battleship_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA Veinticinco de Mayo" definition = heavy_cruiser equipment = { heavy_cruiser_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA Almirante Brown" definition = heavy_cruiser equipment = { heavy_cruiser_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA Pueyrredon" definition = heavy_cruiser equipment = { heavy_cruiser_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA Libertad" definition = light_cruiser equipment = { light_cruiser_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA Independencia" definition = light_cruiser equipment = { light_cruiser_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA San Martín" definition = heavy_cruiser equipment = { heavy_cruiser_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA Belgrano" definition = heavy_cruiser equipment = { heavy_cruiser_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA Pueyrredón" definition = heavy_cruiser equipment = { heavy_cruiser_1 = { amount = 1 owner = ARG } } }
			# Flotilla de Destructores nro. 1
			ship = { name = "ARA Catamarca" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA Jujuy" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA Córdoba" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA La Plata" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = ARG } } }
			# Flotilla de Destructores nro. 2
			ship = { name = "ARA Cervantes" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA Juan de Garay" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA Mendoza" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA La Rioja" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = ARG } } }
			ship = { name = "ARA Tucumán" definition = destroyer equipment = { destroyer_1 = { amount = 1 owner = ARG } } }
		}
	}
}
