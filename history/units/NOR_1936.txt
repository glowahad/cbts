﻿division_template = {
	name = "Infanteridivisjon"	
	division_names_group = NOR_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
        garrison = { x = 1 y = 0 }
		garrison = { x = 1 y = 1 }
        garrison = { x = 1 y = 2 }
	}
	support = {
		engineer = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Infanteriregiment"
	division_names_group = NOR_INF_01

	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
	}
	support = {
		artillery = { x = 0 y = 0 }
	}
}
division_template = {
	name = "Dragonregiment"
	division_names_group = NOR_CAV_01

	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
	}
}


units = {
	#Den norske hær i 1933 - source https://www.ssb.no/a/histstat/nos/nos_ix_097.pdf
	division = {
		name = "1. Divisjon"
		location = 6115
		division_template = "Infanteriregiment"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3
	}

	division = {
		name = "2. Divisjon"
		location = 6115
		division_template = "Infanteridivisjon"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3
	}

	division = {
		name = "3. Divisjon"
		location = 9296
		division_template = "Infanteriregiment"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3
	}

	division = {
		name = "4. Divisjon"
		location = 122
		division_template = "Infanteriregiment"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3
	}

	division = {
		name = "5. Divisjon"
		location = 3022
		division_template = "Infanteriregiment"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3
	}

	#Fleischers divisjon
	division = {
		name = "6. Divisjon"
		location = 192
		division_template = "Infanteriregiment"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3
	}

	#Akershus dragonreg. nr.1
	division = {
		name = "1. Dragonregiment"
		location = 9157
		division_template = "Dragonregiment"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3
	}

	#Trøndelag dragonreg. nr. 2
	division = {
		name = "2. Dragonregiment"
		location = 3022
		division_template = "Dragonregiment"
		start_experience_factor = 0.2
		start_equipment_factor = 0.3
	}

}

air_wings = {
	110 = {
		gw_bomber_equipment =  {
			owner = "NOR" 
			amount = 7
		}
	}
}