#Uzbekistan
#auto-state transfer in 2rcw, as Turkestan
on_actions = {
	#ROOT is new controller #FROM is old controller #FROM.FROM is state ID
	on_state_control_changed = {
		effect = {
			if = {
				limit = {
					FROM.FROM = {
						OR = {
							is_core_of = ROOT
							is_claimed_by = ROOT
						}
					}
					ROOT = {
						tag = UZB
						has_global_flag = Russian_Civil_War
					}
				}
				FROM.FROM = {
					set_state_owner = ROOT
				}
			}
			if = {
				limit = {
					FROM.FROM = {
						state = 585 #Karakalpakstan
					}
					ROOT = {
						tag = UZB
						has_global_flag = Russian_Civil_War
						NOT = {
							has_country_flag = UZB_karakalpakstan_militia
						}
					}
				}
				ROOT = {
					load_oob = "UZB_2rcw_kar_militia"
					set_country_flag = UZB_karakalpakstan_militia
				}
			}
			if = {
				limit = {
					FROM.FROM = {
						state = 584 #Turkmenistan
					}
					ROOT = {
						tag = UZB
						has_global_flag = Russian_Civil_War
						NOT = {
							has_country_flag = UZB_turkmenistan_militia
						}
					}
				}
				ROOT = {
					load_oob = "UZB_2rcw_trk_militia"
					set_country_flag = UZB_turkmenistan_militia
				}
			}
			if = {
				limit = {
					FROM.FROM = {
						state = 732 #Kyrgyzstan
					}
					ROOT = {
						tag = UZB
						has_global_flag = Russian_Civil_War
						NOT = {
							has_country_flag = UZB_kyrgyzstan_militia
						}
					}
				}
				ROOT = {
					load_oob = "UZB_2rcw_kyr_militia"
					set_country_flag = UZB_kyrgyzstan_militia
				}
			}
			if = {
				limit = {
					FROM.FROM = {
						state = 586 #Almaty
					}
					ROOT = {
						tag = UZB
						has_global_flag = Russian_Civil_War
						NOT = {
							has_country_flag = UZB_almaty_militia
						}
					}
				}
				ROOT = {
					if = {
						limit = {
							NOT = {
								has_template = "Qozoq Militsia"
							}
						}
						division_template = {
							name = "Qozoq Militsia" # Kazakh militia Division
							regiments = {
								militia = { x = 0 y = 0 }
								militia = { x = 0 y = 1 }
								militia = { x = 1 y = 0 }
								militia = { x = 1 y = 1 }
							}
							support = {
							}
							is_locked = yes
						}
					}
					load_oob = "UZB_2rcw_alm_militia"
					set_country_flag = UZB_almaty_militia
				}
			}
			if = {
				limit = {
					FROM.FROM = {
						state = 590 #Akmolinsk
					}
					ROOT = {
						tag = UZB
						has_global_flag = Russian_Civil_War
						NOT = {
							has_country_flag = UZB_akmolinsk_militia
						}
					}
				}
				ROOT = {
					if = {
						limit = {
							NOT = {
								has_template = "Qozoq Militsia"
							}
						}
						division_template = {
							name = "Qozoq Militsia" # Kazakh militia Division
							regiments = {
								militia = { x = 0 y = 0 }
								militia = { x = 0 y = 1 }
								militia = { x = 1 y = 0 }
								militia = { x = 1 y = 1 }
							}
							support = {
							}
							is_locked = yes
						}
					}
					load_oob = "UZB_2rcw_akm_militia"
					set_country_flag = UZB_akmolinsk_militia
				}
			}
			if = {
				limit = {
					FROM.FROM = {
						state = 588 #Semipalatinsk
					}
					ROOT = {
						tag = UZB
						has_global_flag = Russian_Civil_War
						NOT = {
							has_country_flag = UZB_semipalitinsk_militia
						}
					}
				}
				ROOT = {
					if = {
						limit = {
							NOT = {
								has_template = "Qozoq Militsia"
							}
						}
						division_template = {
							name = "Qozoq Militsia" # Kazakh militia Division
							regiments = {
								militia = { x = 0 y = 0 }
								militia = { x = 0 y = 1 }
								militia = { x = 1 y = 0 }
								militia = { x = 1 y = 1 }
							}
							support = {
							}
							is_locked = yes
						}
					}
					load_oob = "UZB_2rcw_smp_militia"
					set_country_flag = UZB_semipalitinsk_militia
				}
			}
			if = {
				limit = {
					FROM.FROM = {
						state = 406 #Gurev
					}
					ROOT = {
						tag = UZB
						has_global_flag = Russian_Civil_War
						NOT = {
							has_country_flag = UZB_gureyev_militia
						}
					}
				}
				ROOT = {
					if = {
						limit = {
							NOT = {
								has_template = "Qozoq Militsia"
							}
						}
						division_template = {
							name = "Qozoq Militsia" # Kazakh militia Division
							regiments = {
								militia = { x = 0 y = 0 }
								militia = { x = 0 y = 1 }
								militia = { x = 1 y = 0 }
								militia = { x = 1 y = 1 }
							}
							support = {
							}
							is_locked = yes
						}
					}
					load_oob = "UZB_2rcw_gur_militia"
					set_country_flag = UZB_gureyev_militia
				}
			}
		}
	}
}
