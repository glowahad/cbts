on_actions = {

	on_startup = {
		effect = {
			FRA = {
				country_event = france.1
				country_event = { id = france.3 days = 17 }
				country_event = { id = france.200 days = 96 }
				country_event = { id = france.5 days = 401 }
				country_event = { id = france.201 days = 308 }
				country_event = { id = france.204 days = 1274 }
				country_event = { id = france.301 days = 1209 }
			}
		}
	}

	on_new_term_election = {
		effect = {
			# 1936 Election - Hopefully more complex after 0.1
			if = {
				limit = {
					has_capitulated = no
					is_puppet = no
					tag = FRA
					date < 1937.1.1
				}

				if = {
					limit = {
						has_country_flag = SFIO_radical
						has_country_flag = AD_concentration
					}
					country_event = france.302
				}
				else_if = {
					limit = {
						has_country_flag = SFIO_radical
						has_country_flag = AD_right_wing
					}
					if = {
						limit = {
							is_historical_focus_on = no
						}
						random_list = {
							75 = { # Popular Front Wins
								country_event = france.302
							}
							35 = { # Right AD Coalition Wins
								country_event = france.303
							}
						}
					}
					else = {
						country_event = france.302
					}
				}
				else_if = {
					limit = {
						has_country_flag = SFIO_gauche
						OR = {
							has_country_flag = AD_right_wing
							has_country_flag = AD_concentration
						}
					}
					country_event = france.303
				}

				else_if = { #fallback trigger just in case
					limit = {
						tag = FRA
						date < 1937.1.1
					}
					country_event = france.302
				}
			}
		}
	}

	on_declare_war = {
		effect = {
			if = { # Check if they are members of Bloc
				limit = {
					FROM = {
						has_country_flag = member_bloc_dor
					}
					ROOT = {
						has_country_flag = member_bloc_dor
					}
				}

				custom_effect_tooltip = bloc_dor_war_tt
				disband_bloc_dor = yes
			}
		}
	}

	on_capitulation = {
		effect = {
			if = {
				limit = {
					original_tag = FRA
					NOT = { has_global_flag = FRA_has_fallen }
					is_subject = no
					if = {
						limit = {
							GER = {
								OR = {
									has_country_flag = GER_hitler_in_power
									has_country_flag = GER_Military_Coup
									has_country_flag = GER_Himmler_Coup
								}
							}
						}
						is_in_faction_with = ENG
					}
					has_war_with = GER
					surrender_progress > 0.7
				}
				country_event = france.400
			}
			if = {
				limit = {
					tag = FRA
					has_idea = FRA_surrender_limit
				}
				remove_ideas = FRA_surrender_limit
			}
			if = {
				limit = {
					is_puppet_of = VIC
					FROM = {
						has_war_together_with = FRA
					}
				}
				end_puppet = ROOT
				FRA = {
					if = {
						limit = {
							has_dlc = "Together for Victory"
							OR = {
								tag = MRC
								tag = TNS
							}
						}
						set_autonomy = {
							target = ROOT
							autonomy_state = autonomy_integrated_puppet
						}
					}
					else_if = {
						limit = {
							has_dlc = "Together for Victory"
							OR = {
								tag = SYR
								tag = LEB
								tag = VIN
							}
						}
						set_autonomy = {
							target = SYR
							autonomy_state = autonomy_colony
						}
					}
					else = {
						puppet = ROOT
					}
				}
			}
		}
	}

	on_uncapitulation = {
		effect = {
			if = {
				limit = {
					tag = FRA
					is_puppet = no
				}
				drop_cosmetic_tag = yes
			}
		}
	}

	on_state_control_changed = {
		effect = {
			if = {
				limit = {
					FROM = {
						original_tag = FRA
						NOT = { has_global_flag = FRA_has_fallen }
						FROM.FROM = {
							OR = {
								state = 16
								16 = { is_controlled_by = GER }	
							}
						}
						if = {
							limit = {
								GER = {
									OR = {
										has_country_flag = GER_hitler_in_power
										has_country_flag = GER_Military_Coup
										has_country_flag = GER_Himmler_Coup
									}
								}
							}
							is_in_faction_with = ENG
						}
						has_war_with = GER
						has_civil_war = no
						is_subject = no
						surrender_progress > 0.8
						NOT = {
							#Don't fire if France has pushed pretty far into ITA or GER
							any_state = {
								is_on_continent = europe
								NOT = {
									state = 158
									state = 114
									state = 50
									state = 42
								}
								is_controlled_by = FROM
								OR = {
									is_owned_by = GER
									AND = {
										FRA = { has_War_with = ITA }
										is_owned_by = ITA
									}
								}
							}
							#Don't fire if any states in France are controlled by a different human player
							any_country = {
								is_ai = no
								NOT = {
									tag = GER
									is_in_faction_with = GER
								}
								has_war_with = FROM
								any_state = {
									is_on_continent = europe
									NOT = {
										state = 1
										state = 735
										state = 21
										state = 31
									}
									is_owned_by = FROM
									is_controlled_by = PREV
								}
							}
						}
					}
				}
				FROM = { country_event = france.400 }
			}

			if = {
				limit = {
					FROM = {
						tag = FRA
						NOT = {	
							has_country_flag = FRA_sacked_gamelin
							has_country_flag = FRA_promote_new_blood
						}
					}
					OR = {
						AND = {
							BEL = {
								OR = {
									has_capitulated = yes
									exists = no
								}
							}
							OR = {
								FROM.FROM = {
									OR = {
										state = 758
										state = 757
										state = 1185
										state = 29
										state = 18
										state = 17
										state = 836
									}
								}
								controls_state = 758
								controls_state = 757
								controls_state = 1185
								controls_state = 29
								controls_state = 18
								controls_state = 17
								controls_state = 836
							}
						}
						AND = {
							OR = {
								FROM.FROM = {
									OR = {
										state = 28
										state = 17
										state = 836
									}
								}
								OR = {
									controls_state = 758
									controls_state = 757
									controls_state = 1185
								}
							}
						}
					}
				}
				FROM = { country_event = { id = france.26 hours = 5 } }
			}
		}
	}

	on_annex = {
		effect = {
			FROM = {
				if = {
					limit = {
						tag = FRA
					}
					drop_cosmetic_tag = yes
				}
				if = {
					limit = {
						tag = FRA
						has_country_leader = {
							name = "Albert Lebrun"
						}	
					}
					retire_ideology_leader = communism
					retire_ideology_leader = authoritarian_socialism
					retire_ideology_leader = socialism
					retire_ideology_leader = social_democracy
					retire_ideology_leader = social_liberalism
					retire_ideology_leader = market_liberalism
				}
			}
		}
	}

	on_monthly_FRA = {
		effect = {
			if = {
				limit = {
					NOT = { has_country_flag = FRA_mauricegamelin_incapable }
					date > 1937.9.20
					has_country_flag = FRA_promote_new_blood
				}
				country_event = {
					id = france.27
					days = 14
				}
			}
		}
	}
}

