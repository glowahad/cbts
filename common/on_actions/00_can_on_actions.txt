on_actions = {
    on_startup ={
        effect = {
            CAN = {
                country_event = {
                    id = can_internal.1
                    days = 1036
                }
            }
        }
    }
	on_new_term_election = {
		random_events = {
			100 =  can_elections.1
			100 =  can_elections.2
			100 =  can_elections.3
		}
	}
}
