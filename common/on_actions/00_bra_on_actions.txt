on_actions = {
	on_startup = {
		effect = {
			BRA = {
				country_event = {
					id = bra.intro.1
					days = 1
				}
			}
			501 = {
				damage_building = {
					type = infrastructure
					damage = 1
				}
				damage_building = {
					type = industrial_complex
					damage = 0.5
				}
			}
		}
	}
}