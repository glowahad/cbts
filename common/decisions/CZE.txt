##########################
# Czechoslovak Decisions #
##########################

CZE_petka_decisions = {
	CZE_remove_minister_of_supply_of_people = {
		visible = {
			tag = CZE
			has_country_flag = agrarians_in_petka
			has_country_flag = Petka_alive
		}
		fire_only_once = yes
		ai_will_do = { factor = 15 }
		cost = 15
		available = {
			has_war = no
			is_subject = no
		}
		complete_effect = {
			add_to_variable = { 
			    var = CZE_agrarian_influence
			    value = 5 
			}
		    subtract_from_variable = { 
		        var = CZE_national_socialist_influence 
		        value = 5 
		    }
		    custom_effect_tooltip = CZE_remove_minister_of_supply_of_people_comp
		}
	}
	CZE_support_nat_soc_decision = {
		visible = {
			tag = CZE
			has_country_flag = Petka_alive
		}
		ai_will_do = { factor = 15 }
		cost = 30
		available = {
			has_war = no
			is_subject = no
		}
		complete_effect = {
			add_to_variable = { 
			    var = CZE_national_socialist_influence
			    value = 3 
			}
		    subtract_from_variable = { 
		        var = CZE_agrarian_influence 
		        value = 3
		    }
		}
		days_remove = 14
		days_re_enable = 14
	}
	CZE_support_nat_dem_decision = {
		visible = {
			tag = CZE
			has_country_flag = Petka_alive
			has_completed_focus = CZE_petka_democracy
		}
		ai_will_do = { factor = 15 }
		cost = 30
		available = {
			has_war = no
			is_subject = no
		}
		complete_effect = {
			add_to_variable = { 
			    var = CZE_national_democrat_influence
			    value = 3 
			}
		    subtract_from_variable = { 
		        var = CZE_agrarian_influence 
		        value = 3
		    }
		}
		days_remove = 14
		days_re_enable = 14
	}
	CZE_support_christ_dem_decision = {
		visible = {
			tag = CZE
			has_country_flag = Petka_alive
		}
		ai_will_do = { factor = 15 }
		cost = 30
		available = {
			has_war = no
			is_subject = no
		}
		complete_effect = {
			add_to_variable = { 
			    var = CZE_christian_democrat_influence
			    value = 3 
			}
		    subtract_from_variable = { 
		        var = CZE_agrarian_influence 
		        value = 3
		    }
		}
		days_remove = 14
		days_re_enable = 14
	}
	CZE_support_soc_dem_decision = {
		visible = {
			tag = CZE
			has_country_flag = Petka_alive
		}
		ai_will_do = { factor = 15 }
		cost = 30
		available = {
			has_war = no
			is_subject = no
		}
		complete_effect = {
			add_to_variable = { 
			    var = CZE_national_socialist_influence
			    value = 2 
			}
		    subtract_from_variable = { 
		        var = CZE_agrarian_influence 
		        value = 2
		    }
		}
		#days_remove = 14
		days_re_enable = 1
	}
	CZE_support_trade_decision = {
		visible = {
			tag = CZE
			has_country_flag = Petka_alive
		}
		ai_will_do = { factor = 15 }
		cost = 30
		available = {
			has_war = no
			is_subject = no
		}
		complete_effect = {
			add_to_variable = { 
			    var = CZE_national_socialist_influence
			    value = 2 
			}
		    subtract_from_variable = { 
		        var = CZE_agrarian_influence 
		        value = 2
		    }
		}
		#days_remove = 14
		days_re_enable = 1
	}
}

CZE_minorities_decisions = {
	CZE_decision_2 = {
		visible = {
			tag = CZE
		}
		fire_only_once = yes
		ai_will_do = { factor = 15 }
		cost = 50
		available = {
			has_war = no
			is_subject = no
		}
		complete_effect = {
			#
		}
	}
}