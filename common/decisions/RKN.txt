###########################################
# RKN by SomeRandomEu4Fan
###########################################

RK_coring = {
	
	RKN_restore_order_ommeland = {
		icon = oppression

		highlight_states = {
			state = 36
		}

		allowed = {
				tag = RKN
		}

		available = {
			36 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 75
		days_remove = coring_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			add_state_core = 36
		}
	}
	
	RKN_restore_order_gelderland = {
		icon = oppression

		highlight_states = {
			state = 756
		}

		allowed = {
				tag = RKN
		}

		available = {
			756 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 75
		days_remove = coring_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			add_state_core = 756
		}
	}
	
	RKN_restore_order_eindhoven = {
		icon = oppression

		highlight_states = {
			state = 35
		}

		allowed = {
				tag = RKN
		}

		available = {
			35 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 75
		days_remove = coring_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			add_state_core = 35
		}
	}
	
	RKN_restore_order_limburg = {
		icon = oppression

		highlight_states = {
			state = 821
		}

		allowed = {
				tag = RKN
		}

		available = {
			821 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 75
		days_remove = coring_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			add_state_core = 821
		}
	}
	
	RKN_restore_order_rotterdam = {
		icon = oppression

		highlight_states = {
			state = 997
		}

		allowed = {
				tag = RKN
		}

		available = {
			997 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 75
		days_remove = coring_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			add_state_core = 997
		}
	}
	
	RKN_restore_order_zeeuws_vlaanderen = {
		icon = oppression

		highlight_states = {
			state = 822
		}

		allowed = {
			tag = RKN
		}

		available = {
			822 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 75
		days_remove = coring_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			add_state_core = 822
		}
	}
}

RK_germanization = {

	RKN_germanize_dunkirk = {
		icon = infiltrate_state

		highlight_states = {
			state = 758
		}

		allowed = {
				tag = RKN
		}

		available = {
			758 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		visible = {
			NOT = {
				any_owned_state = {
					is_core_of = HOL
					NOT = { is_core_of = ROOT }
				}
			}
			758 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 100
		days_remove = germanization_days_short
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			GER = {
				add_state_core = 758
			}
		}
	}
	
	RKN_germanize_vlaanderen = {
		icon = infiltrate_state

		highlight_states = {
			state = 996
		}

		allowed = {
				tag = RKN
		}

		available = {
			996 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		visible = {
			NOT = {
				any_owned_state = {
					is_core_of = HOL
					NOT = { is_core_of = ROOT }
				}
			}
			996 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 125
		days_remove = germanization_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			GER = {
				add_state_core = 996
			}
		}
	}
	
	RKN_germanize_brussels = {
		icon = infiltrate_state

		highlight_states = {
			state = 6
		}

		allowed = {
			tag = RKN
		}

		available = {
			6 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		visible = {
			NOT = {
				any_owned_state = {
					is_core_of = HOL
					NOT = { is_core_of = ROOT }
				}
			}
			6 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 125
		days_remove = germanization_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			GER = {
				add_state_core = 6
			}
		}
	}
	
	RKN_germanize_ommeland = {
		icon = infiltrate_state

		highlight_states = {
			state = 36
		}

		allowed = {
			tag = RKN
		}

		available = {
			36 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		visible = {
			NOT = {
				any_owned_state = {
					is_core_of = HOL
					NOT = { is_core_of = ROOT }
				}
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 125
		days_remove = germanization_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			GER = {
				add_state_core = 36
			}
		}
	}
	
	RKN_germanize_gelderland = {
		icon = infiltrate_state

		highlight_states = {
			state = 756
		}

		allowed = {
			tag = RKN
		}

		available = {
			756 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		visible = {
			NOT = {
				any_owned_state = {
					is_core_of = HOL
					NOT = { is_core_of = ROOT }
				}
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 125
		days_remove = germanization_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			GER = {
				add_state_core = 756
			}
		}
	}
	
	RKN_germanize_eindhoven = {
		icon = infiltrate_state

		highlight_states = {
			state = 35
		}

		allowed = {
			tag = RKN
		}

		visible = {
			NOT = {
				any_owned_state = {
					is_core_of = HOL
					NOT = { is_core_of = ROOT }
				}
			}
		}

		available = {
			35 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 125
		days_remove = germanization_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			GER = {
				add_state_core = 35
			}
		}
	}
	
	RKN_germanize_limburg = {
		icon = infiltrate_state

		highlight_states = {
			state = 821
		}

		allowed = {
			tag = RKN
		}

		available = {
			821 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		visible = {
			NOT = {
				any_owned_state = {
					is_core_of = HOL
					NOT = { is_core_of = ROOT }
				}
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 125
		days_remove = germanization_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			GER = {
				add_state_core = 821
			}
		}
	}
	
	RKN_germanize_rotterdam = {
		icon = infiltrate_state

		highlight_states = {
			state = 997
		}

		allowed = {
			tag = RKN
		}

		available = {
			997 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		visible = {
			NOT = {
				any_owned_state = {
					is_core_of = HOL
					NOT = { is_core_of = ROOT }
				}
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 125
		days_remove = germanization_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			GER = {
				add_state_core = 997
			}
		}
	}
	
	RKN_germanize_zeeuws_vlaanderen = {
		icon = infiltrate_state

		highlight_states = {
			state = 822
		}

		allowed = {
			tag = RKN
		}

		available = {
			822 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		visible = {
			NOT = {
				any_owned_state = {
					is_core_of = HOL
					NOT = { is_core_of = ROOT }
				}
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 125
		days_remove = germanization_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			GER = {
				add_state_core = 822
			}
		}
	}
}