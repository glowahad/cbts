BAV_reconcile_germany = {
    icon = generic_political_actions
    picture = GFX_decision_category_BAV_Reconcile_with_Germany
    allowed = { tag = BAV }
    visible = {
	has_completed_focus = BAV_reconciliation_with_germany
    }
}

BAV_become_germany = {
    icon = generic_political_actions
    picture = GFX_decision_become_germany
    allowed = { tag = BAV }
    visible = {
		OR = {
			has_completed_focus = BAV_new_germany
			has_war_with = GER
		}
		NOT = { has_country_flag = BAV_formed_germany }
    }
}

BAV_conflict_with_the_army = {
    icon = generic_political_actions
    picture = GFX_decision_BAV_agitation
    allowed = { tag = BAV }
    visible = {
		NOT = {
			has_government = fascism
			has_country_flag = BAV_fascist_coup
			has_country_flag = BAV_fascist_coup_averted
		}
		has_country_flag = BAV_fascist_irritation
    }
}
BAV_centralization = {
    icon = generic_political_actions
    picture = GFX_decision_BAV_centralization_gfx
    allowed = { tag = BAV }
    visible = {
		has_variable = BAV_Centralization
    }
}