CZE_petka_decisions = {
	
	icon = GFX_decision_category_generic_formable_nations
	picture = GFX_decision_category_CZE_petka_big_framed
	allowed = {
		tag = CZE
		always = yes
	}
}

CZE_minorities_decisions = {
	
	icon = GFX_decision_generic_construction
	picture = GFX_decision_category_CZE_coat_of_arms
	allowed = {
		tag = CZE
		always = yes
	}
}