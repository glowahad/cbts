#### Germany, by s_team337
GER_Alliances = {
	picture = GFX_decision_GER_Tripartite_Pact
	icon = GFX_decision_category_generic_formable_nations
	visible = {
		OR = {
			tag = GER
			tag = DDR
			tag = WGR
		}
		NOT = {
			has_country_flag = WGR_neutral_foreign_policy
		}
	}
	allowed = {
		OR = {
			tag = GER
			tag = DDR
			tag = WGR
		}
		is_subject = no
	}
}
GER_Promotions = {
	icon = GFX_decision_category_army_reform
	visible = {
		tag = GRM
	}
	allowed = {
		OR = {
			tag = GER
			tag = WGR
		}
	}
}
GER_Bitter_Loser = {
	picture = GFX_decision_GER_Victory
	icon = GFX_decision_category_generic_fascism
	visible = {
		tag = GER
		has_idea = sour_loser
	}
	allowed = {
		tag = GER
	}
}
GER_DAF = {
	picture = GFX_decision_GER_Labor_Front
	icon = GFX_decision_category_generic_fascism
	allowed = {
		tag = GER
	}
	visible = {
		has_country_flag = GER_hitler_in_power
		has_government = fascism
		has_completed_focus = GER_Hitler_Economics_DAF
	}
}
GER_MEFO = {
	picture = GFX_decision_cat_generic_mefo_bills
	icon = GFX_decision_category_ger_mefo_bills
	allowed = {
		tag = GER
	}
	visible = {
		has_country_flag = GER_MEFO_event
	}
}
GER_Naval_Expansion = {
	picture = GFX_decision_GER_Kriegsmarine_Category
	icon = GFX_decision_category_army_reform
	allowed = {
		tag = GER
	}
	visible = {
		
	}
}
GER_atlantikwall_decisions = {
	picture = GFX_decision_GER_Kriegsmarine_Category
	icon = GFX_decision_category_generic_industry
	allowed = {
		tag = GER
	}
	visible = {
		has_completed_focus = GER_atlantikwall
	}
}

GER_special_projects = {
	icon = GFX_decision_category_army_reform
	allowed = { tag = GER }
	visible = {}
}
GER_Welthaupstadt_Germania = {
	picture = GFX_decision_GER_Big_Buildings_In_Neu_Berlin
	icon = GFX_decision_category_generic_industry
	allowed = {
		tag = GER
	}
	visible = {
		has_completed_focus = GER_Hitler_Victory_Economics_Projects_Germania_Project
		owns_state = 65
		has_government = fascism
	}
}
GER_Linz_Project = { #152, 9665
	picture = GFX_decision_GER_Fuhrermuseum
	icon = GFX_decision_category_generic_industry
	allowed = {
		tag = GER
	}
	visible = {
		has_completed_focus = GER_Hitler_Victory_Economics_Projects_Linz_Project
		has_government = fascism
		owns_state = 152
	}
}
GER_Bavarian_Projects = { #52,53,54,60
	picture = GFX_decision_GER_Ruined_Wurzburg
	icon = GFX_decision_category_generic_industry
	allowed = {
		tag = GER
	}
	visible = {
		has_completed_focus = GER_Hitler_Victory_Economics_Projects_Bavarian_Projects
		has_government = fascism
		owns_state = 52
		owns_state = 53
		owns_state = 54
		owns_state = 60
	}
}
GER_Rhine_Projects = { #51,56,745,864
	picture = GFX_decision_GER_Destroyed_Koln
	icon = GFX_decision_category_generic_industry
	allowed = {
		tag = GER
	}
	visible = {
		has_completed_focus = GER_Hitler_Victory_Economics_Projects_Rhine_Projects
		has_government = fascism
		owns_state = 51
		owns_state = 56
		owns_state = 745
		owns_state = 864
		owns_state = 809
	}
}
GER_Hannover_Projects = {
	picture = GFX_decision_GER_Hannover
	icon = GFX_decision_category_generic_industry
	allowed = {
		tag = GER
	}
	visible = {
		has_completed_focus = GER_Hitler_Victory_Economics_Projects_Rhine_Projects
		has_government = fascism
		owns_state = 59
		owns_state = 56
	}
}
GER_Nordstern_Projects = { #3022
	picture = GFX_decision_NOR_Trondheim
	icon = GFX_decision_category_generic_industry
	allowed = {
		tag = GER
	}
	visible = {
		has_completed_focus = GER_Hitler_Victory_Economics_Projects_Nordstern_Projects
		has_government = fascism
		owns_state = 143
	}
}
GER_Prussian_Projects = {
	picture = GFX_decision_LIT_Silvestras_Zakauskas
	icon = GFX_decision_category_generic_industry
	allowed = {
		tag = GER
	}
	visible = {
		has_completed_focus = GER_Hitler_Victory_Economics_Projects_Prussian_Projects
		has_government = fascism
		owns_state = 66
		owns_state = 86
		owns_state = 773
		owns_state = 762
		owns_state = 188
	}
}
GER_Saxon_Projects = {
	icon = GFX_decision_category_generic_industry
	allowed = {
		tag = GER
	}
	visible = {
		has_completed_focus = GER_Hitler_Victory_Economics_Projects_Saxon_Projects
		has_government = fascism
		owns_state = 65
	}
}
GER_Austrian_Projects = {
	icon = GFX_decision_category_generic_industry
	picture = GFX_decision_AUS_Graz
	allowed = {
		tag = GER
	}
	visible = {
		has_completed_focus = GER_Hitler_Victory_Economics_Projects_Austrian_Projects
		has_government = fascism
		owns_state = 4
		owns_state = 152
		owns_state = 153
	}
}
GER_Pabst_Plan = {
	icon = GFX_decision_category_generic_industry
	picture = GFX_decision_GER_Pabst
	allowed = {
		tag = GER
	}
	visible = {
		has_completed_focus = GER_Hitler_Victory_Economics_Projects_Pabst_Plan_Projects
		has_government = fascism
		OR = {
			POL = {
				is_subject_of = GER
				controls_state = 10
			}
			controls_state = 10
		}
	}
}
GER_Prag_Plan = {
	icon = GFX_decision_category_generic_industry
	picture = GFX_decision_GER_Pabst
	allowed = {
		tag = GER
	}
	visible = {
		has_completed_focus = GER_Hitler_Victory_Economics_Projects_Prag_Project
		has_government = fascism
		OR = {
			CZE = {
				is_subject_of = GER
				controls_state = 9
			}
			controls_state = 9
		}
	}
}

#836 Metz, 28, Alsace
GER_SS_decisions = {
	picture = GFX_decision_GER_SS_Category
	icon = GFX_decision_category_generic_fascism
	visible = {
		has_country_flag = GER_hitler_in_power
		has_government = fascism
		NOT = {
			has_country_flag = GER_SS_destroyed
		}
	}
	allowed = {
		tag = GER
	}
}

GER_RK_Forming_cat = { ###For Forming RKs, originally SREF###
	icon = GFX_decision_category_ger_reichskommissariats
	
	visible = {
		has_country_flag = GER_hitler_in_power
	}
	
	allowed = {
		tag = GER
	}
}