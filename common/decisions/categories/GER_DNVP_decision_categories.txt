####DNVP Germany, by s_team337
GER_DNVP_Colonial_Management_cat = { #I'd like to get this into a GUI when possible, but that's a 0.2 goal - s_team
	picture = GFX_decision_GER_Colonial
	icon = GFX_decision_category_MTG_naval_treaties
	visible = {
		has_country_flag = GER_DNVP_in_power
		OR = {
			has_completed_focus = GER_DNVP_Colonial_Management
			has_country_flag = GER_colonies_test
		}
	}
	allowed = {
		tag = GER
	}
}
GER_DNVP_Anschluss_cat = { #For Anschluss
	picture = GFX_decision_AUS_Army
	icon = GFX_decision_category_generic_fascism
	visible = {
		#has_country_flag = GER_DNVP_in_power
		country_exists = AUS
		OR = {
			has_country_flag = GER_GDVP_cooperation
			has_completed_focus = GER_DNVP_AUS_DNSAP
		}
	}
	allowed = {
		tag = GER
	}
}
AUS_DNVP_Anschluss_cat = { #For Anschluss, austrian side
	picture = GFX_decision_AUS_Army
	icon = GFX_decision_category_generic_fascism
	visible = {
		has_country_flag = AUS_counter_anschluss
	}
	allowed = {
		tag = AUS
	}
}
GER_DNVP_Reichstag = { #For Goerdeler
	picture = GFX_decision_GER_Reichstag
	icon = GFX_decision_category_generic_democracy
	visible = {
		has_country_flag = GER_DNVP_Parliament
	}
	allowed = {
		tag = GER
	}
}
