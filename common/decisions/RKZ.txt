###########################################
# RKZ by SomeRandomEu4Fan
###########################################

RK_coring = {
	
	RKZ_restore_order_ardennes = {
		icon = oppression

		highlight_states = {
			state = 755
		}

		allowed = {
			tag = RKZ
		}

		available = {
			755 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 100
		days_remove = coring_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			add_state_core = 755
		}
	}
	
	RKZ_restore_order_lille = {
		icon = oppression

		highlight_states = {
			state = 1188
		}

		allowed = {
			tag = RKZ
		}

		available = {
			1188 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 50
		days_remove = coring_days_short
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			add_state_core = 1188
		}
	}
}

RK_germanization = {

	RKZ_germanize_ardennes = {
		icon = infiltrate_state

		highlight_states = {
			state = 755
		}

		allowed = {
			tag = RKZ
		}

		available = {
			755 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		visible = {
			NOT = {
				any_owned_state = {
					is_core_of = WAL
					NOT = { is_core_of = ROOT }
				}
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 125
		days_remove = germanization_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			GER = {
				add_state_core = 755
			}
		}
	}
	
	RKZ_germanize_hainaut = {
		icon = infiltrate_state

		highlight_states = {
			state = 34
		}

		allowed = {
			tag = RKZ
		}

		available = {
			34 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		visible = {
			NOT = {
				any_owned_state = {
					is_core_of = WAL
					NOT = { is_core_of = ROOT }
				}
			}
		}
		
		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 125
		days_remove = germanization_days_long
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			GER = {
				add_state_core = 34
			}
		}
	}
	
	RKZ_germanize_lille = {
		icon = infiltrate_state

		highlight_states = {
			state = 1188
		}

		allowed = {
			tag = RKZ
		}

		available = {
			1188 = {
				is_owned_and_controlled_by = ROOT
			}
		}

		visible = {
			NOT = {
				any_owned_state = {
					is_core_of = WAL
					NOT = { is_core_of = ROOT }
				}
			}
		}

		modifier = {
			political_power_cost = 0.05
		}

		fire_only_once = yes
		cost = 100
		days_remove = germanization_days_short
		ai_will_do = {
			factor = 10
		}

		remove_effect = {
			GER = {
				add_state_core = 1188
			}
		}
	}
}