LIT_political_actions = {

	LIT_iron_wolf_plot = {

		icon = GFX_decision_generic_ignite_civil_war
                                
        allowed = { 
        	tag = LIT 
        }

        activation = {                          
            tag = LIT
            is_ai = no			
        }

        available = {                           
            has_completed_focus = GEN_reform_command_structure                    
        }
        
        is_good = no                           
        days_mission_timeout = 465 #1933.01.01-1934.04.10

        timeout_effect = {                      
	        add_political_power = -50
            country_event = lit_internal.2              
	    }
		complete_effect = {                    
		    add_political_power = 50
            country_event = lit_internal.3 
		}             
    }
}
