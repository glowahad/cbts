technologies = {


#####################################################################################################################
	#Mountaineers Path

	mountain_infantry = {

		enable_subunits = {
			mountaineers
		}

		on_research_complete = {
			hidden_effect = {
				load_oob = "unlock_mountineers"
			}
			custom_effect_tooltip = UNLOCK_DIVISION_TEMPLATE
		}
		on_research_complete_limit = {
			NOT = {
				has_template_containing_unit = mountaineers
			}
		}
		
		path = {
			leads_to_tech = tech_mountaineers
			research_cost_coeff = 1
		}

		research_cost = 1.5
		start_year = 1918
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 0 }
		}
		
		categories = {
			category_special_forces
			mountaineers_tech
		}
		
		ai_will_do = {
			factor = 5
		}
	}
	
	tech_mountaineers = {
	
		mountaineers = {
			max_organisation = 5
		}

		
		path = {
			leads_to_tech = tech_mountaineers2
			research_cost_coeff = 1
		}

		research_cost = 1
		start_year = 1936
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 10 }
		}
		
		categories = {
			category_special_forces
			mountaineers_tech
		}
		
		ai_will_do = {
			factor = 5
		}
	}
	
	tech_mountaineers2 = {

		mountaineers = {
			max_organisation = 5
		}
		path = {
			leads_to_tech = tech_mountaineers3
			research_cost_coeff = 1
		}


		research_cost = 1
		start_year = 1939
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 13 }
		}
		
		categories = {
			category_special_forces
			mountaineers_tech
		}
		
		ai_will_do = {
			factor = 5
		}
	}
	
	tech_mountaineers3 = {

		mountaineers = {
			max_organisation = 5
		}
		path = {
			leads_to_tech = tech_mountaineers4
			research_cost_coeff = 1
		}

		research_cost = 1
		start_year = 1943
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 19 }
		}
		
		categories = {
			category_special_forces
			mountaineers_tech
		}
		
		ai_will_do = {
			factor = 5
		}
	}
	
	tech_mountaineers4 = {

		mountaineers = {
			max_organisation = 5
		}

		research_cost = 1
		start_year = 1946
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 25 }
		}
		
		categories = {
			category_special_forces
			mountaineers_tech
		}
		
		ai_will_do = {
			factor = 5
		}
	}

#####################################################################################################################
	#Paratroopers Path
	airborne_infantry = {

		enable_subunits = {
			paratrooper
		}
		
		path = {
			leads_to_tech = paratroopers
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1930
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 4 }
		}
		
		on_research_complete = {
			hidden_effect = {
				load_oob = "unlock_paratroopers"
			}
			custom_effect_tooltip = UNLOCK_DIVISION_TEMPLATE
		}
		on_research_complete_limit = {
			NOT = {
				has_template_containing_unit = paratrooper
			}
		}
		
		categories = {
			category_special_forces
			para_tech
		}
		
		ai_will_do = {
			factor = 0

		}
	}
	
	paratroopers = {

		paratrooper = {
			max_organisation = 5
		}
		
		path = {
			leads_to_tech = paratroopers2
			research_cost_coeff = 1
		}

		research_cost = 1
		start_year = 1939
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 13 }
		}
		
		categories = {
			category_special_forces
			para_tech
		}
		
		ai_will_do = {
			factor = 0

		}
	}
	
	paratroopers2 = {

		paratrooper = {
			max_organisation = 5
		}
		
		path = {
			leads_to_tech = paratroopers3
			research_cost_coeff = 1
		}

		research_cost = 1
		start_year = 1943
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 19 }
		}
		
		categories = {
			category_special_forces
			para_tech
		}
		
		ai_will_do = {
			factor = 0
		}
	}

	paratroopers3 = {

		paratrooper = {
			max_organisation = 5
		}
		
		path = {
			leads_to_tech = paratroopers4
			research_cost_coeff = 1
		}
		

		research_cost = 1
		start_year = 1946
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 25 }
		}
		
		categories = {
			category_special_forces
			para_tech
		}
		
		ai_will_do = {
			factor = 0
		}
	}

	paratroopers4 = {

		paratrooper = {
			max_organisation = 5
		}
		

		research_cost = 1.5
		start_year = 1948
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 28 }
		}
		
		categories = {
			category_special_forces
			para_tech
		}
		
		ai_will_do = {
			factor = 0
		}
	}

#####################################################################################################################
	#Marine Path

	naval_infantry = {

		enable_subunits = {
			marine
		}
		
		on_research_complete = {
			hidden_effect = {
				load_oob = "unlock_marines"
			}			
			custom_effect_tooltip = UNLOCK_DIVISION_TEMPLATE
		}
		on_research_complete_limit = {
			NOT = {
				has_template_containing_unit = marine
			}
		}

		path = {
			leads_to_tech = marines
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1918
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 0 }
		}
		
		categories = {
			category_special_forces
			marine_tech
		}
		
		ai_will_do = {
			factor = 5
		}
	}
	
	marines = {

		marine = {
			max_organisation = 5
		}

		path = {
			leads_to_tech = marines2
			research_cost_coeff = 1
		}

		research_cost = 1
		start_year = 1933
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 7 }
		}
		
		categories = {
			category_special_forces
			marine_tech
		}
		
		ai_will_do = {
			factor = 5
		}
	}
	
	marines2 = {

		marine = {
			max_organisation = 5
		}
		path = {
			leads_to_tech = marines3
			research_cost_coeff = 1
		}
		

		research_cost = 1
		start_year = 1939
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 13 }
		}
		
		categories = {
			category_special_forces
			marine_tech
		}
		
		ai_will_do = {
			factor = 5
		}
	}
	
	marines3 = {

		marine = {
			max_organisation = 5
		}
		path = {
			leads_to_tech = mechanized_marines
			research_cost_coeff = 1
		}
		path = {
			leads_to_tech = infantry_based_marines
			research_cost_coeff = 1
		}

		research_cost = 1
		start_year = 1943
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 19 }
		}
		
		categories = {
			category_special_forces
			marine_tech
		}
		
		ai_will_do = {
			factor = 5
		}
	}
	
	mechanized_marines = {
	
		amphibious_mechanized = {
			max_organisation = 10
			soft_attack = 0.15
			hard_attack = 0.15
		}
		
		amphibious_armor = {
			max_organisation = 2
			soft_attack = 0.15
			hard_attack = 0.15
		}
		
		path = {
			leads_to_tech = marines4
			research_cost_coeff = 1
		}

		research_cost = 1
		start_year = 1944
		folder = {
			name = special_forces_folder
			position = { x = -1 y = 22 }
		}
		XOR = {
			infantry_based_marines
		}
		
		categories = {
			category_special_forces
			marine_tech
		}
		
		ai_will_do = {
			factor = 5
			modifier = {
				factor = 0.1
				OR = {
					NOT = { has_tech = amphibious_mechanized_infantry }
					NOT = { has_tech = amphibious_tank }
					NOT = { has_tech = amphibious_tank_2 }
				}
			}
		}
	}
	
	infantry_based_marines = {

		marine = {
			soft_attack = 0.10
			hard_attack = 0.15
		}
		path = {
			leads_to_tech = marines4
			research_cost_coeff = 1
		}
		
		research_cost = 1
		start_year = 1944
		folder = {
			name = special_forces_folder
			position = { x = 1 y = 22 }
		}
		XOR = {
			mechanized_marines
		}
		
		categories = {
			category_special_forces
			marine_tech
		}
		
		ai_will_do = {
			factor = 5
			modifier = {
				factor = 2
				OR = {
					NOT = { has_tech = amphibious_mechanized_infantry }
					NOT = { has_tech = amphibious_tank }
					NOT = { has_tech = amphibious_tank_2 }
				}
			}
		}
	}
	
	marines4 = {

		marine = {
			max_organisation = 5
			soft_attack = 0.05
		}

		amphibious_mechanized = {
			max_organisation = 3
			soft_attack = 0.05
		}

		amphibious_armor = {
			max_organisation = 1
			soft_attack = 0.05
		}
		
		path = {
			leads_to_tech = marines5
			research_cost_coeff = 1
		}

		research_cost = 1.5
		start_year = 1946
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 25 }
		}
		
		categories = {
			category_special_forces
			marine_tech
		}
		
		ai_will_do = {
			factor = 5
		}
	}
	
	marines5 = {

		marine = {
			max_organisation = 5
			soft_attack = 0.05
		}

		amphibious_mechanized = {
			max_organisation = 3
			soft_attack = 0.05
		}

		amphibious_armor = {
			max_organisation = 1
			soft_attack = 0.05
		}

		research_cost = 1.5
		start_year = 1948
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 28 }
		}
		
		categories = {
			category_special_forces
			marine_tech
		}
		
		ai_will_do = {
			factor = 5
		}
	}

#####################################################################################################################
	#Special Forces Path

	special_forces = {
	
		category_special_forces = {
			soft_attack = 0.05
			hard_attack = 0.05
		}

		path = {
			leads_to_tech = special_missions
			research_cost_coeff = 1
		}

		research_cost = 1.5
		start_year = 1933
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 7 }
		}
		
		categories = {
			mountaineers_tech
			marine_tech
			para_tech
			category_special_forces
		}
		
		ai_will_do = {
			factor = 1
		}
	}

	special_missions = {
	
		category_special_forces = {
			soft_attack = 0.05
			hard_attack = 0.05
		}

		path = {
			leads_to_tech = expand_the_special_forces_program
			research_cost_coeff = 1
		}

		path = {
			leads_to_tech = improve_the_special_forces
			research_cost_coeff = 1
		}

		research_cost = 1.5
		start_year = 1939
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 13 }
		}
		
		categories = {
			mountaineers_tech
			marine_tech
			para_tech
			category_special_forces
		}
		
		ai_will_do = {
			factor = 1
		}
	}

	improve_the_special_forces = {
	
		special_forces_no_supply_grace = 24
		#special_forces_training_time_factor = 0.1
		special_forces_attack_factor = 0.10
		special_forces_defence_factor = 0.10
		
		XOR = {
			expand_the_special_forces_program
		} 

		path = {
			leads_to_tech = extensive_enviroment_training
			research_cost_coeff = 1
		}

		research_cost = 1.5
		start_year = 1941
		folder = {
			name = special_forces_folder
			position = { x = -1 y = 16 }
		}
		
		categories = {
			mountaineers_tech
			marine_tech
			para_tech
			category_special_forces
		}
		
		ai_will_do = {
			factor = 1
		}
	}

	expand_the_special_forces_program = {
	
		special_forces_training_time_factor = -0.05
		special_forces_cap = 0.01
		
		XOR = {
			improve_the_special_forces
		} 

		path = {
			leads_to_tech = extensive_enviroment_training
			research_cost_coeff = 1
		}

		research_cost = 1.5
		start_year = 1941
		folder = {
			name = special_forces_folder
			position = { x = 1 y = 16 }
		}
		
		categories = {
			mountaineers_tech
			marine_tech
			para_tech
			category_special_forces
		}
		
		ai_will_do = {
			factor = 1
		}
	}

	extensive_enviroment_training = {
	
		category_special_forces = {
			acclimatization_hot_climate_gain_factor = 0.30
			acclimatization_cold_climate_gain_factor = 0.30
		}
		special_forces_no_supply_grace = 24

		path = {
			leads_to_tech = improved_special_forces
			research_cost_coeff = 1
		}

		research_cost = 1
		start_year = 1943
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 19 }
		}
		
		categories = {
			mountaineers_tech
			marine_tech
			para_tech
			category_special_forces
		}
		
		ai_will_do = {
			factor = 1
		}
	}

	improved_special_forces = {
	
		category_special_forces = {
			max_organisation = 5
			soft_attack = 0.10
		}
		special_forces_no_supply_grace = 12
		special_forces_training_time_factor = 0.05

		path = {
			leads_to_tech = semi_modern_special_forces
			research_cost_coeff = 1
		}

		research_cost = 2
		start_year = 1944
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 22 }
		}
		
		categories = {
			mountaineers_tech
			marine_tech
			para_tech
			category_special_forces
		}
		
		ai_will_do = {
			factor = 1
		}
	}

	semi_modern_special_forces = {
	
		category_special_forces = {
			max_organisation = 5
			soft_attack = 0.10
		}
		special_forces_no_supply_grace = 12
		special_forces_training_time_factor = 0.05

		research_cost = 2.5
		start_year = 1946
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 25 }
		}

		path = {
			leads_to_tech = modern_special_forces
			research_cost_coeff = 1
		}
		
		categories = {
			mountaineers_tech
			marine_tech
			para_tech
			category_special_forces
		}
		
		ai_will_do = {
			factor = 1
		}
	}

	modern_special_forces = {
	
		category_special_forces = {
			max_organisation = 5
			soft_attack = 0.10
		}
		special_forces_no_supply_grace = 12
		special_forces_training_time_factor = 0.05

		research_cost = 3
		start_year = 1948
		folder = {
			name = special_forces_folder
			position = { x = 0 y = 28 }
		}
		
		categories = {
			mountaineers_tech
			marine_tech
			para_tech
			category_special_forces
		}
		
		ai_will_do = {
			factor = 1
		}
	}
#####################################################################################################################
#End :(
}