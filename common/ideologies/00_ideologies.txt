ideologies = {
	################
	# Liberal Conservatism in the localization is called jacobin in the code. This is because the mod started out as an alt-history, then became CbtS.#
	################
	communism = {
		
		types = {
			leninism = {} #redone, Zinovievites
			stalinism = {} #redone, Stalinists
			right_communism = {} #redone, Bukharin and Bukharinists
			german_communism = {} #redone, KPD
		}
		
		dynamic_faction_names = {
			"FACTION_NAME_COMMUNIST_1"
			"FACTION_NAME_COMMUNIST_2"
			"FACTION_NAME_COMMUNIST_3"
			"FACTION_NAME_COMMUNIST_4"
			"FACTION_NAME_COMMUNIST_5"
		}
		
		color = { 60 5 10 }
		
		ai_neutral = yes
		
		rules = {
			can_force_government = no
			can_send_volunteers = yes
			can_puppet = no
			can_join_factions = no
		}
		
		modifiers = {
			generate_wargoal_tension = 0.5
			civilian_intel_to_others = 10.0
			army_intel_to_others = 7.5
			navy_intel_to_others = 12.5
			airforce_intel_to_others = 7.5
		}
		
		faction_modifiers = {
			
		}
		can_host_government_in_exile = yes
		can_be_boosted = no
		can_collaborate = yes
	}
	
	authoritarian_socialism = { # REV SOC
	
		types = {
			maoism = { #redone, Mao and Maoists
				can_be_randomly_selected = no
			}
			social_nationalism = {} #redone
			syndicalism = {
				can_be_randomly_selected = no
			}
			anarcho_syndicalism = {
				can_be_randomly_selected = no
			}
			centrist_marxism = {} #redone
			rev_socialism = {} #redone
			guesdism = {
				can_be_randomly_selected = no
			}
			caballerism = {
				can_be_randomly_selected = no
			}
			council_communism = {
				can_be_randomly_selected = no
			}
			de_leonism = {
				can_be_randomly_selected = no
			}
			agrarian_socialism_rev = {} #redone
		}
		
		dynamic_faction_names = {
			"FACTION_NAME_DEMOCRATIC_1"
			"FACTION_NAME_DEMOCRATIC_2"
			"FACTION_NAME_DEMOCRATIC_3"
			"FACTION_NAME_DEMOCRATIC_4"
			"FACTION_NAME_DEMOCRATIC_5"
			"FACTION_NAME_DEMOCRATIC_6"
		}
		
		color = { 135 0 0 }
		
		rules = {
			can_force_government = no
			can_send_volunteers = yes
			can_puppet = no
			can_join_factions = no
		}

		modifiers = {
			generate_wargoal_tension = 0.5
			civilian_intel_to_others = 10.0
			army_intel_to_others = 7.5
			navy_intel_to_others = 12.5
			airforce_intel_to_others = 7.5
		}
		
		faction_modifiers = {
			faction_trade_opinion_factor = 0.50 #plus 50% trade opinion
		}
		ai_neutral = yes
		can_host_government_in_exile = yes
		can_be_boosted = no
		can_collaborate = yes
	}
	
	socialism = {
	
		types = {
			real_socialism = {} #redone
			british_labour = {
				can_be_randomly_selected = no
			}	
			prietism = {
				can_be_randomly_selected = no
			}
			blumism = {
				can_be_randomly_selected = no
			}
			dang_guo_demsoc = {
				can_be_randomly_selected = no
			}
		}
		
		dynamic_faction_names = {
			"FACTION_NAME_DEMOCRATIC_1"
			"FACTION_NAME_DEMOCRATIC_2"
			"FACTION_NAME_DEMOCRATIC_3"
			"FACTION_NAME_DEMOCRATIC_4"
			"FACTION_NAME_DEMOCRATIC_5"
			"FACTION_NAME_DEMOCRATIC_6"
		}
		
		color = { 255 0 0 }
		
		rules = {
			can_force_government = no
			can_send_volunteers = yes
			can_puppet = no
			can_join_factions = no
		}

		modifiers = {
			generate_wargoal_tension = 0.6
			civilian_intel_to_others = 15.0
			army_intel_to_others = 6.0
			navy_intel_to_others = 15.0
			airforce_intel_to_others = 6.0
			lend_lease_tension = 0.50
			send_volunteers_tension = 0.50
			guarantee_tension = 0.25
		}
		
		faction_modifiers = {

		}
		ai_neutral = yes
		can_host_government_in_exile = yes
		can_be_boosted = no
		can_collaborate = yes
	}
	
	social_democracy = {
	
		types = {
			dem_socialism = {} #redone
			republicanism_spr = {
				can_be_randomly_selected = no
			}
			american_progressivism = {
				can_be_randomly_selected = no
			}
			french_neosocialism = {
				can_be_randomly_selected = no
			}
			brazilian_labourism = {
				can_be_randomly_selected = no
			}
		}
		
		dynamic_faction_names = {
			"FACTION_NAME_DEMOCRATIC_1"
			"FACTION_NAME_DEMOCRATIC_2"
			"FACTION_NAME_DEMOCRATIC_3"
			"FACTION_NAME_DEMOCRATIC_4"
			"FACTION_NAME_DEMOCRATIC_5"
			"FACTION_NAME_DEMOCRATIC_6"
		}
		
		color = { 195 20 85 }
		
		rules = {
			can_force_government = no
			can_send_volunteers = yes
			can_puppet = no
			can_join_factions = no
		}

		modifiers = {
			generate_wargoal_tension = 0.8
			civilian_intel_to_others = 20.0
			army_intel_to_others = 5.0
			navy_intel_to_others = 20.0
			airforce_intel_to_others = 5
			lend_lease_tension = 0.50
			send_volunteers_tension = 0.50
			guarantee_tension = 0.25
		}
		
		faction_modifiers = {
		}
		ai_neutral = yes
		can_host_government_in_exile = yes
		can_be_boosted = no
		can_collaborate = yes
	}

	social_liberalism = {
	
		types = {
			liberalism = {} #redone
			jadidism = {
				can_be_randomly_selected = no
			}
			french_radicalism = {
				can_be_randomly_selected = no
			}
			sa_radicalism = {
				can_be_randomly_selected = no
			}
			american_soclib = {
				can_be_randomly_selected = no
			}
		}
		
		dynamic_faction_names = {
			"FACTION_NAME_DEMOCRATIC_1"
			"FACTION_NAME_DEMOCRATIC_2"
			"FACTION_NAME_DEMOCRATIC_3"
			"FACTION_NAME_DEMOCRATIC_4"
			"FACTION_NAME_DEMOCRATIC_5"
			"FACTION_NAME_DEMOCRATIC_6"
		}
		
		color = { 255 170 0 }
		
		rules = {
			can_force_government = no
			can_send_volunteers = yes
			can_puppet = no
			can_join_factions = no
		}

		
		modifiers = {
			generate_wargoal_tension = 0.8
			civilian_intel_to_others = 20.0
			army_intel_to_others = 5.0
			navy_intel_to_others = 20.0
			airforce_intel_to_others = 5
			lend_lease_tension = 0.50
			send_volunteers_tension = 0.50
			guarantee_tension = 0.25
		}
		
		faction_modifiers = {
		}
		ai_neutral = yes
		can_host_government_in_exile = yes
		can_be_boosted = no
		can_collaborate = yes
	}

	market_liberalism = {
	
		types = {
			market_liberal = {} #redone
			usa_classical_liberal = {
				can_be_randomly_selected = no
			}
		}
		
		dynamic_faction_names = {
			"FACTION_NAME_DEMOCRATIC_1"
			"FACTION_NAME_DEMOCRATIC_2"
			"FACTION_NAME_DEMOCRATIC_3"
			"FACTION_NAME_DEMOCRATIC_4"
			"FACTION_NAME_DEMOCRATIC_5"
			"FACTION_NAME_DEMOCRATIC_6"
		}
		
		color = { 255 216 0 }
		
		rules = {
			can_force_government = no
			can_send_volunteers = yes
			can_puppet = no
			can_join_factions = no
		}
		
		modifiers = {
			generate_wargoal_tension = 0.8
			civilian_intel_to_others = 20.0
			army_intel_to_others = 5.0
			navy_intel_to_others = 20.0
			airforce_intel_to_others = 5
			lend_lease_tension = 0.50
			send_volunteers_tension = 0.50
			guarantee_tension = 0.25
		}
		
		faction_modifiers = {
		}
		ai_neutral = yes
		can_host_government_in_exile = yes
		can_be_boosted = no
		can_collaborate = yes
	}
	
	jacobin = {
		types = {
			liberal_nationalism = {} #redone
			national_liberalism = {} #redone
			liberal_conservatism = {} #redone
			centrism = {} #redone
			agrarianism = {} #redone
			lerrouxism = {
				can_be_randomly_selected = no
			}
			christian_democracy = {
				can_be_randomly_selected = no
			}
			american_moderate_conservatism = {
				can_be_randomly_selected = no
			}
		}
		dynamic_faction_names = {
			"FACTION_NAME_DEMOCRATIC_1"
			"FACTION_NAME_DEMOCRATIC_2"
			"FACTION_NAME_DEMOCRATIC_3"
			"FACTION_NAME_DEMOCRATIC_4"
			"FACTION_NAME_DEMOCRATIC_5"
			"FACTION_NAME_DEMOCRATIC_6"
		}
		color = { 50 140 255 }
		rules = {
			can_force_government = no
			can_send_volunteers = yes
			can_puppet = no
			can_join_factions = no
		}
		modifiers = {
			generate_wargoal_tension = 0.7
			civilian_intel_to_others = 20.0
			army_intel_to_others = 5.0
			navy_intel_to_others = 20.0
			airforce_intel_to_others = 5
			lend_lease_tension = 0.50
			send_volunteers_tension = 0.50
			guarantee_tension = 0.25
		}
		faction_modifiers = {
		}
		ai_democratic = yes # uses the democratic AI behaviour
		can_host_government_in_exile = yes
		can_be_boosted = no
		can_collaborate = yes
	}
	democratic = {
		types = {
			conservatism = {} #Going to be the same as social_conservatism, redone
			christian_democracy_soccon = {
				can_be_randomly_selected = no
			}
			british_conservatism = {
				can_be_randomly_selected = no
			}
			national_conservatism = {} #redone
			social_conservatism = {} #redone
			american_social_conservatism = {
				can_be_randomly_selected = no
			}
			soccon_agrarianism = {} #soccon agrarianism, redone
			dang_guo_soccon = {
				can_be_randomly_selected = no
			}
		}
		dynamic_faction_names = {
			"FACTION_NAME_DEMOCRATIC_1"
			"FACTION_NAME_DEMOCRATIC_2"
			"FACTION_NAME_DEMOCRATIC_3"
			"FACTION_NAME_DEMOCRATIC_4"
			"FACTION_NAME_DEMOCRATIC_5"
			"FACTION_NAME_DEMOCRATIC_6"
		}
		color = { 0 0 255 }
		rules = {
			can_force_government = no
			can_send_volunteers = yes
			can_puppet = no
			can_join_factions = no
		}
		modifiers = {
			generate_wargoal_tension = 0.7
			civilian_intel_to_others = 20.0
			army_intel_to_others = 5.0
			navy_intel_to_others = 20.0
			airforce_intel_to_others = 5
			lend_lease_tension = 0.50
			send_volunteers_tension = 0.50
			guarantee_tension = 0.25
		}
		faction_modifiers = {
		}
		ai_neutral = yes
		can_host_government_in_exile = yes
		can_be_boosted = no
		can_collaborate = yes
	}
	neutrality = {
	
		types = {
			oligarchism = {} #redone
			military = {} #redone
			big_tent = {} #redone
			colonial = {} #redone
			authoritarian = {} #redone
			sanation = {
				can_be_randomly_selected = no
			}
			theocratic = {} #redone
			kemalism = {
				can_be_randomly_selected = no
			}
			est_auth_democrat = {
				can_be_randomly_selected = no
			}
			rocquism = {
				can_be_randomly_selected = no
			}
          	maximato = {
				can_be_randomly_selected = no
			}
			spr_ceda = {
				can_be_randomly_selected = no
			}
			longism = {
				can_be_randomly_selected = no
			}
			vlasovism = {
				can_be_randomly_selected = no
			}
			social_credit = {
				can_be_randomly_selected = no
			}
			dang_guo_aut = {
				can_be_randomly_selected = no
			}
		}

		dynamic_faction_names = {
			"FACTION_NAME_NONALIGNED_1"
			"FACTION_NAME_NONALIGNED_2"
			"FACTION_NAME_NONALIGNED_3"
			"FACTION_NAME_NONALIGNED_4"
			"FACTION_NAME_NONALIGNED_5"
		}
		
		color = { 60 60 100 }
		
		
		war_impact_on_world_tension = 0.6	#no major danger
		faction_impact_on_world_tension = 0.1
		
		rules = {
			can_force_government = no
			can_send_volunteers = yes
			can_puppet = no
			can_join_factions = no
		}
		
		modifiers = {
			generate_wargoal_tension = 0.25
			civilian_intel_to_others = 15.0
			army_intel_to_others = 6.0
			navy_intel_to_others = 15.0
			airforce_intel_to_others = 6.0
			lend_lease_tension = 0.4
			send_volunteers_tension = 0.4
			guarantee_tension = 0.4
		}
		
		faction_modifiers = {
		}

		can_be_boosted = no
		can_collaborate = yes

		ai_neutral = yes
	}
	monarchism = {
	
		types = {
			despotism = {} #redone
			semi_constitutional = {} #redone
			sanationist_despotism = {
				can_be_randomly_selected = no
			}
			RK_Heer = {
				can_be_randomly_selected = no
			}
			warlordism = {
				can_be_randomly_selected = no
			}
			ubiquismo = {
				can_be_randomly_selected = no
			}
			dang_guo = {
				can_be_randomly_selected = no
			}
			kokka_shugi = {
				can_be_randomly_selected = no
			}
			brazilian_estadonovismo_autdes = {
				can_be_randomly_selected = no
			}
			integral_nationalism = {
				can_be_randomly_selected = no
			}
		}
		
		dynamic_faction_names = {
			"FACTION_NAME_DEMOCRATIC_1"
			"FACTION_NAME_DEMOCRATIC_2"
			"FACTION_NAME_DEMOCRATIC_3"
			"FACTION_NAME_DEMOCRATIC_4"
			"FACTION_NAME_DEMOCRATIC_5"
			"FACTION_NAME_DEMOCRATIC_6"
		}
		
		color = { 130 130 130 }
		
		rules = {
			can_force_government = no
			can_send_volunteers = yes
			can_puppet = no
			can_join_factions = no
		}
		
		modifiers = {
			generate_wargoal_tension = 0.5
			civilian_intel_to_others = 15.0
			army_intel_to_others = 6.0
			navy_intel_to_others = 15.0
			airforce_intel_to_others = 6.0
			lend_lease_tension = 0.4
			send_volunteers_tension = 0.4
			guarantee_tension = 0.4
		}
		
		faction_modifiers = {
		}
		can_be_boosted = no
		can_host_government_in_exile = yes
		can_collaborate = yes
		ai_neutral = yes
	}
	
	
	nationalism = {
		types = {
			nationalist = {} #redone
			austrofascism = {
				can_be_randomly_selected = no
			}
			metaxism = {
				can_be_randomly_selected = no
			}
			religious_nationalism = {} #done, needs icon
			carlism = {
				can_be_randomly_selected = no
			}
			afrikaner_nationalism = {
				can_be_randomly_selected = no
			}
			thai_cultural_revolution = {
				can_be_randomly_selected = no
			}
			endecja = {
				can_be_randomly_selected = no
			}
			kokuhon_shugi = {
				can_be_randomly_selected = no
			}
			salazar_estado_novo = {
				can_be_randomly_selected = no
			}
            brazilian_estadonovismo_faraut = {
				can_be_randomly_selected = no
			}
            dang_guo_faraut = {
				can_be_randomly_selected = no
			}
		}
		
		dynamic_faction_names = {
			"FACTION_NAME_DEMOCRATIC_1"
			"FACTION_NAME_DEMOCRATIC_2"
			"FACTION_NAME_DEMOCRATIC_3"
			"FACTION_NAME_DEMOCRATIC_4"
			"FACTION_NAME_DEMOCRATIC_5"
			"FACTION_NAME_DEMOCRATIC_6"
		}
		
		color = { 65 65 65 }
		
		rules = {
			can_force_government = no
			can_send_volunteers = yes
			can_puppet = no
			can_join_factions = no
		}
		
		modifiers = {
			generate_wargoal_tension = 0.5
			civilian_intel_to_others = 15.0
			army_intel_to_others = 10.0
			navy_intel_to_others = 10.0
			airforce_intel_to_others = 10.0
		}
		
		faction_modifiers = {
			faction_trade_opinion_factor = 0.50 #plus 50% trade opinion
		}
		ai_neutral = yes
		can_be_boosted = no
		can_collaborate = yes
		can_host_government_in_exile = yes
	}
	
	fascism = {
		
		types = {
			nazism = {
				can_be_randomly_selected = no
			}
			german_fascism = {
				can_be_randomly_selected = no
			}
			fascism_ideology = {} #Generic fascism, written
			italian_fascism = {
				can_be_randomly_selected = no
			}
			falangism = {
				can_be_randomly_selected = no
			}
			francoism = {
				can_be_randomly_selected = no
			}
			rexism = {
				can_be_randomly_selected = no
			}
			clerical = {}
			integralism = {
				can_be_randomly_selected = no
			}
			british_fascism = {
				can_be_randomly_selected = no
			}
			japanese_fascism = {
				can_be_randomly_selected = no
			}
			legionarism = {
				can_be_randomly_selected = no
			}
			RK_SS = {
				can_be_randomly_selected = no
			}
           	synarquism = {
				can_be_randomly_selected = no
			}
			military_socialism = {
				can_be_randomly_selected = no
			}
			national_radicalism = {
				can_be_randomly_selected = no
			}
			ustashe = {
				can_be_randomly_selected = no
			}
			russian_fascism = {
				can_be_randomly_selected = no
			}
			dang_guo_fascist = {
				can_be_randomly_selected = no
			}
		}
		
		dynamic_faction_names = {
			"FACTION_NAME_FASCIST_1"
			"FACTION_NAME_FASCIST_2"
			"FACTION_NAME_FASCIST_3"
			"FACTION_NAME_FASCIST_4"
			"FACTION_NAME_FASCIST_5"
		}
		
		color = { 5 5 5 }
		
		rules = {
			can_force_government = no
			can_send_volunteers = yes
			can_puppet = no
			can_join_factions = no
		}	
		modifiers = {
			generate_wargoal_tension = 0.4
			civilian_intel_to_others = 15.0
			army_intel_to_others = 10.0
			navy_intel_to_others = 10.0
			airforce_intel_to_others = 10.0
		}

		ai_neutral = yes
		can_be_boosted = no
		can_collaborate = yes
		can_host_government_in_exile = yes
	}
}
