SAF_not_negotiating = {
	NOT = {
		has_country_flag = SAF_negotiating
	}
}
SAF_is_voting = {
	has_country_flag = SAF_voting
}
SAF_is_not_voting = {
	NOT = {
		has_country_flag = SAF_voting
	}
}
SAF_law_passed = {
	OR = {
		check_variable = { SAF_supseat > SAF_seats_needed } #Effect if the law passes, change the number depending on seats in your parliament 
		check_variable = { SAF_supseat = SAF_seats_needed } #If there's a tie
	}
}
SAF_law_not_passed = {
	check_variable = { SAF_supseat < SAF_seats_needed } #Effect if you lose
}
SAF_need_clear_len = {
	has_country_flag = SAF_negotiated_with_communists
	OR = {
		is_in_array = {
			array = coalition_party_list
			value = 0
		}
		has_country_flag = SAF_len_agreed
	}
}
SAF_need_clear_revsoc = {
	has_country_flag = SAF_negotiated_with_RevSocs
	OR = {
		is_in_array = {
			array = coalition_party_list
			value = 1
		}
		has_country_flag = SAF_revsoc_agreed
	}
}
SAF_need_clear_demsoc = {
	has_country_flag = SAF_negotiated_with_DemSocs
	OR = {
		is_in_array = {
			array = coalition_party_list
			value = 2
		}
		has_country_flag = SAF_demsoc_agreed
	}
}
SAF_need_clear_socdem = {
	has_country_flag = SAF_negotiated_with_SocDems
	OR = {
		is_in_array = {
			array = coalition_party_list
			value = 3
		}
		has_country_flag = SAF_socdem_agreed
	}
}
SAF_need_clear_soclib = {
	has_country_flag = SAF_negotiated_with_SocLibs
	OR = {
		is_in_array = {
			array = coalition_party_list
			value = 4
		}
		has_country_flag = SAF_soclib_agreed
	}
}
SAF_need_clear_marlib = {
	has_country_flag = SAF_negotiated_with_MarLibs
	OR = {
		is_in_array = {
			array = coalition_party_list
			value = 5
		}
		has_country_flag = SAF_marlib_agreed
	}
}
SAF_need_clear_libcon = {
	has_country_flag = SAF_negotiated_with_Centrists
	OR = {
		is_in_array = {
			array = coalition_party_list
			value = 6
		}
		has_country_flag = SAF_libcon_agreed
	}
}
SAF_need_clear_soccon = {
	has_country_flag = SAF_negotiated_with_Conservatives
	OR = {
		is_in_array = {
			array = coalition_party_list
			value = 7
		}
		has_country_flag = SAF_soccon_agreed
	}
}
SAF_need_clear_autdes = {
	has_country_flag = SAF_negotiated_with_Monarchists
	OR = {
		is_in_array = {
			array = coalition_party_list
			value = 8
		}
		has_country_flag = SAF_autdes_agreed
	}
}
SAF_need_clear_aut = {
	has_country_flag = SAF_negotiated_with_Authoritarians
	OR = {
		is_in_array = {
			array = coalition_party_list
			value = 9
		}
		has_country_flag = SAF_aut_agreed
	}
}
SAF_need_clear_faraut = {
	has_country_flag = SAF_negotiated_with_Nationalists
	OR = {
		is_in_array = {
			array = coalition_party_list
			value = 10
		}
		has_country_flag = SAF_faraut_agreed
	}
}
SAF_need_clear_fascist = {
	has_country_flag = SAF_negotiated_with_Nazis
	OR = {
		is_in_array = {
			array = coalition_party_list
			value = 11
		}
		has_country_flag = SAF_fascists_agreed
	}
}