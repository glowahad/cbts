Ignore_CHL_during_chaco_war= {
	enable = {
		tag = BOL
		country_exists = CHL
		has_global_flag = SA_chaco_war_flag
	}
	abort_when_not_enabled = yes
	
	ai_strategy = {
		type = ignore
		id = "CHL"
		value = 100
	}
}

Ignore_PRU_during_chaco_war= {
	enable = {
		tag = BOL
		country_exists = PRU
		has_global_flag = SA_chaco_war_flag
	}
	abort_when_not_enabled = yes
	
	ai_strategy = {
		type = ignore
		id = "PRU"
		value = 100
	}
}

Ignore_BRA_during_chaco_war= {
	enable = {
		tag = BOL
		country_exists = BRA
		has_global_flag = SA_chaco_war_flag
	}
	abort_when_not_enabled = yes
	
	ai_strategy = {
		type = ignore
		id = "BRA"
		value = 100
	}
}

Attack_fase_in_the_chaco = {
	enable = {
		tag = BOL
		has_global_flag = SA_chaco_war_flag
		has_active_mission = BOL_loss_of_momentum_mission
	}

	abort_when_not_enabled = yes

	ai_strategy = {
		type = ignore_army_incompetence
		value = 100
	}	
	ai_strategy = {
		type = front_control
		tag = PAR
		state = 869
   #country_trigger = { always = no } 	# a trigger to check agaisnt a specific country. scope is enemy country from scope is our country
   #state_trigger = { always = no } 	# a trigger to check agaisnt a state. scope is state. from scope is enemy country from.from scope is our country
   
   #ratio = 0.0 # the strategy is enabled only if ratio of the front covered by this strat's targets greater than this ratio
		priority = 0
		ordertype = front
		execution_type = careful
		execute_order = yes
		manual_attack = yes
   }
}

defensive_fase_in_the_chaco = {
	enable = {
		tag = BOL
		has_global_flag = SA_chaco_war_flag
		NOT = {has_active_mission = BOL_loss_of_momentum_mission}
	}

	abort = {
		NOT = { has_global_flag = SA_chaco_war_flag }
	}

	ai_strategy = {
		type = front_control
		tag = PAR
		state = 869
   #country_trigger = { always = no } 	# a trigger to check agaisnt a specific country. scope is enemy country from scope is our country
   #state_trigger = { always = no } 	# a trigger to check agaisnt a state. scope is state. from scope is enemy country from.from scope is our country
   
   #ratio = 0.0 # the strategy is enabled only if ratio of the front covered by this strat's targets greater than this ratio
		priority = 0
		ordertype = front
		execution_type = careful
		execute_order = no
		manual_attack = no
   }
}