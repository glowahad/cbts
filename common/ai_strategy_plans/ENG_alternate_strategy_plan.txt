ENG_ahistorical_plan = {
	name = "British Ahistorical Plan"
	desc = "Ahistorical behavior for Britain"

	enable = {
		original_tag = ENG
		OR = {
			AND = {
				is_historical_focus_on = no
			}
		}
		WGR = { exists = yes }
        GER = { exists = no }		
	}
	abort = {
		OR = {
		    GER = { exists = yes }
			SOV = { has_war_with = RUS }
			has_government = socialism
		}
	}

	ai_national_focuses = {
		ENG_NAT_Start #30
		ENG_NAT_Protect_Animals #35
		ENG_NAT_Visiting_Forces #20
		ENG_NAT_Children_Young_Persons #30
		ENG_NAT_Justice_Reform #70
		ENG_NAT_Scotland_Acts #70
		ENG_NAT_Foreign_Judgements #35
		ENG_NAT_Firearms #46
		ENG_NAT_Nationality #20
		ENG_Colonial_Start #10
		#Total: 366 2 Jan 1934
		ENG_NAT_Incitement_to_Disaffection #20
		ENG_NAT_Animal_Regulations #45
		ENG_NAT_Petrol #38
		ENG_NAT_Maritime_Museum #20
		ENG_Colonial_Africa #10
		ENG_Colonial_Africa_Education #15
		ENG_Colonial_Europe #10
		ENG_Colonial_Europe_Malta_Rescind_Autonomy #7
		ENG_Colonial_Europe_Malta_English_as_Official_Language #10
		ENG_Colonial_Africa_Infrastructure #35
		ENG_Colonial_Kenya_Elites #30
		ENG_Colonial_India #10
		ENG_Colonial_Kenya_Marketing_Controls #21
		ENG_NAT_Married_Women #20
		ENG_NAT_Wind_Up_Land_Purchase #20
		ENG_NAT_Electricity_Supply_Act #25
		ENG_Rearm_Invest_Wales #35
		ENG_Rearm_Invest_Northern_England #40
		ENG_Rearm_Invest_NI #35
		ENG_NAT_Vagrancy #20
		ENG_Rearm_Invest_Scotland #40
		ENG_Rearm_Invest_Southern_England #40
		#Total: 912 Jul 1935
		ENG_Colonial_Gov_of_India_Act #40
		ENG_Colonial_Africa_Ghana_Authorities #15
		ENG_Colonial_Middle_East #10
		ENG_Colonial_Europe_Gibraltar_Airfield #30
		#No more foci until November, the elections-Nov 14
		ENG_Tory_Start #30
		ENG_Tory_Housing_Act #25
		ENG_Tory_Public_Health_Act #50
		ENG_Tory_Tithe_Act #24
		ENG_Tory_Land_Registration_Act #22
		ENG_Tory_Public_Order_Act #15
		ENG_Anti_Communist_Start #10
		ENG_Tory_Hours_of_Employment_Act #20
		ENG_Tory_Civil_List_Act #30
		ENG_Rearm_Shadow_Scheme #15
		ENG_Tory_Crown_Lands_Act #30
		ENG_Rearm_Contact_Private_Companies #40
		ENG_Colonial_Anglo_Egyptian_Treaty #7
		ENG_Colonial_East_Asia #10
		ENG_Colonial_East_Asia_Reorganize_Malaya #7
		ENG_Support_Spanish_Nationalists #20
		#Total: 308 Nov 1936
		ENG_Tory_Trunk_Roads_Act #20
		ENG_Colonial_Africa_Nyasaland #20
		ENG_Rearm_Dispersion #42
		ENG_Rearm_Infantry_Equipment #35
		ENG_Rearm_Support_Equipment #35
		ENG_Rearm_Shadow_Factories #45
		ENG_Colonial_Europe_Gibraltar_Bunkers #35
		ENG_Colonial_Europe_Gibraltar_Anti_Air #35
		ENG_Colonial_East_Asia_Singapore_Fuel_Silo #40
		ENG_Colonial_East_Asia_Singapore_Naval_Base #30
		ENG_Rearm_Factory_Scotland #38
		ENG_French_Summit #14
		ENG_French_Research #14
		ENG_French_Trade
		#Total: 711 Dec 1937
		ENG_Colonial_Separate_Burma #25
		ENG_Tory_Factories_Act #50
		ENG_Consult_Admirality #35
		ENG_Rearm_Dockyard #42
		ENG_Tory_Geneva_Convention_Act #15
		ENG_Tory_Cinematograph_Act #35
		ENG_Anti_Communist_Soviet #20
		ENG_Colonial_East_Asia_Malaya_Status_Quo #7
		ENG_Colonial_Kenya_Settlers #25
		ENG_Rearm_Shadow_Switching #36
		ENG_Rearm_Air_Panic #30
		ENG_French_Alliance #20
		ENG_Rearm_Air_Command #35
		ENG_Anti_Communist_Arm_Finland
		ENG_Anti_Communist_Guarantee_Baltics
		ENG_Rearm_Expand_Capability #36
		#Total: 1122 Jan 1939
		ENG_Rearm_Continue_Mechanization #35
		ENG_Anti_Communist_Poland
		ENG_Anti_Communist_Arm_Poland #14
		ENG_Anti_Communist_Poland_Ally
		ENG_Rearm_Army_Doctrine #35
		ENG_Anti_Communist_Ally_Finland
		ENG_Rearm_Conscription #35
		ENG_German_Summit #10
		ENG_Anti_Communist_Ally_Romania #14
		ENG_Peace_Ally_Poland #14
		ENG_German_Trade #10
		ENG_Anti_Communist_Embargo_USSR
		ENG_German_Payment_Reduction #10
		ENG_German_Alliance #10
		ENG_Recognize_NatSpa
		ENG_Recognize_Send_Arms
		ENG_Recognize_Send_Investment
		ENG_Ally_NatSpa
		ENG_Ally_Portugal
		#I'll let the AI go free from now on
	}

	focus_factors = {
		ENG_War_Foreign_Intervene_in_Spain = 0 # well done, not medium rare
	}
	research = {

	}

	ideas = {

	}
	traits = {
	}

	ai_strategy = {
	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}

}