GER_dnvp_plan = {
	name = "German DNVP Plan"
	desc = "Monarchist behavior for Germany"

	enable = {
		original_tag = GER
		has_government = nationalism
		OR = {
			AND = {
				is_historical_focus_on = yes
			}
			has_game_rule = { rule = GER_select_path option = GER_NO_PREFERENCE }
			has_game_rule = { rule = GER_select_path option = GER_FAR_RIGHT_PREFERENCE }
			has_game_rule = { rule = GER_select_path option = GER_DNVP_PREFERENCE }
		}
	}
	abort = {
		OR = {
		    SOV = { has_war_with = RUS }
			NOT = { has_government = nationalism }
			has_country_flag = GER_DNVP_nazi_coal
		}
	}

	ai_national_focuses = {
	    GER_DNVP_Politics_Start #0
		GER_DNVP_Politics_Coalition #10
		GER_DNVP_Politics_No_Coalition #7
		GER_DNVP_Politics_Nazi_Coalition #Just in case
		GER_DNVP_Politics_Stahlhelm #15
		GER_DNVP_Politics_Paramilitary_Emergency #23
		GER_DNVP_Politics_Ban_Communism #25
		GER_DNVP_Politics_Fight_Paramilitaries #70
		GER_DNVP_Politics_Ban_SPD #40
		GER_DNVP_Politics_Ban_Liberals #49
		GER_DNVP_Politics_Arrest_Nazis #32
		GER_DNVP_Politics_Junker_Confidence #21
		GER_DNVP_Politics_Nazi_Voters #25
		GER_DNVP_Politics_Political_Protestantism #35
		GER_DNVP_Politics_Break_Nazis #42
		#Total: 422 Feb 34
		GER_DNVP_Politics_DNVP_Women #31
		GER_DNVP_Politics_New_Bismarck #35
		GER_DNVP_Politics_Laud_Farmer #20
		GER_DNVP_Farmer_Start #20
		GER_DNVP_Farmer_Stimulate #24
		GER_HitStr_Military_Tree_Start #35
		GER_DNVP_Politics_Kaiser #29
		GER_DNVP_Foreign_Policy_Start #10
		GER_DNVP_Leave_League #7
		GER_DNVP_Farmer_Debt_Cancelling #14
		GER_DNVP_Farmer_Tariffs #24
		GER_DNVP_Farmer_No_Forclosures #10
		GER_DNVP_Farmer_Internal_Colonization #50
		GER_DNVP_Farmer_Done #35
		#Total: 766 Feb 35
		GER_HitStr_Military_Tree_Award_More_Contracts #35
		GER_HitStr_Military_Tree_Dockyards #35
		GER_HitStr_Military_Tree_Re_Introduce_Conscription #35
		GER_DNVP_Politics_Gerepo #21
		GER_DNVP_Politics_Nobility #15
		GER_DNVP_Industry_Start #20
		GER_DNVP_Industry_Laissez_Faire #50
		GER_DNVP_Industry_Privatization #35
		GER_DNVP_Industry_Lower_Taxes #30
		GER_DNVP_Industry_Deregulation #45
		GER_DNVP_Industry_Protectionism #21
		GER_DNVP_Industry_Done #10
		GER_HitStr_Military_Tree_Army_Start #35
		#Total: 1153 Feb 36
		GER_DNVP_Rhineland #20
		GER_DNVP_Politics_Remove_Nazi #35
		GER_DNVP_Rearmament #49
		GER_army_side_with_reactionary_officers #35
		GER_army_superior_firepower #35
		GER_army_superior_firepower_arty_effort #35
		GER_HitStr_Military_Tree_Airforce_Start #35
		GER_DNVP_Rearmament_Land #35
		GER_DNVP_Rearmament_Land_IG_Farben #50
		GER_DNVP_Rearmament_Infrastructure #42
		GER_DNVP_Rearmament_Naval #35
		GER_HitStr_Military_Tree_Navy_Start #35
		GER_HitStr_Military_Tree_Navy_Reader_Grand_Navy #35
		#Total 1629 Jun 37
		GER_DNVP_Politics_Destroy_Unions #42
		GER_DNVP_Politics_Imperial_Youth_League #20
		GER_army_superior_firepower_offensive_posture #35
		GER_DNVP_Claim_Lands #25
		GER_DNVP_Customs_Union_Aus #30
		GER_DNVP_Rearmament_Naval_Infrastructure #43
		GER_DNVP_AUS_GDVP #21
		GER_DNVP_Rearmament_Airports #42
		GER_DNVP_Rearmament_German_Coal #30
		GER_DNVP_Rearmament_Mil_Factory #49
		GER_DNVP_Claim_Poland #7
		GER_DNVP_Agitate_in_Poland #10
		#Total: 1983 Jun 38
		GER_army_superior_firepower_rocket_arty_effort #35
		GER_DNVP_Rearmament_Dockyards #49
		GER_DNVP_Rearmament_Focus_on_Fuel #30
		GER_army_superior_firepower_infantry_weapons #35
		GER_DNVP_Sudetenland #30
		GER_army_superior_firepower_support_equipment #35
		GER_HitStr_Military_Tree_Navy_Submarine_Designs #35
		GER_HitStr_Military_Tree_Navy_Destroyers #35
		GER_army_doctrines #35
		GER_HitStr_Military_Tree_Navy_Dockyards #35
		GER_DNVP_Politics_Pan_Germanism #38
		GER_DNVP_Rearmament_Dockyards_2 #42
		GER_DNVP_Work_With_Danzig #15
		#Total 2362 Jun 38
		GER_DNVP_Demand_Memel #16
		GER_DNVP_Summit_with_Italy #7
		GER_DNVP_War_Economy #46
		GER_DNVP_Invade_Poland
		GER_DNVP_War_With_France
		GER_DNVP_War_Economy_Rations
		GER_DNVP_War_Economy_Order_for_Labour
		GER_DNVP_Politics_Traditionalism
		GER_DNVP_War_Economy_Factory_Conversion
		GER_DNVP_Operation_Moltke
		GER_DNVP_Basing_Norway
		GER_HitStr_Military_Tree_Navy_Ports
		GER_DNVP_Manstein_Plan
		#The end: The AI is free from now on
	}

	focus_factors = {
		
	}
	research = {

	}

	ideas = {

	}
	traits = {
	}

	ai_strategy = { 
		type = role_ratio
		id = garrison
		value = 5
	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}

}