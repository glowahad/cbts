WGR_adenauer_plan = {
	name = "German Adenauer Plan"
	desc = "Adenauer behavior for Germany"

	enable = {
		original_tag = WGR
		has_government = democratic
		has_idea = adenauer
	}
	abort = {
		OR = {
		    SOV = { has_war_with = RUS }
			NOT = { has_government = democratic }
		}
	}

	ai_national_focuses = {
	    #Start: Oct 16, 1934 (655-290)
		WGR_Zentrum_start #10
		WGR_Zentrum_Adenauer #10
		WGR_Out_of_the_Tower #15
		WGR_Zentrum_Support_Democracy #20
		WGR_Zentrum_Fight_Prussian_Influence #30
		WGR_Zentrum_Legitimize_Rhenish_Nationalism #23
		WGR_Zentrum_Work_With_Regionalists #30
		WGR_Zentrum_Dissolve_Prussia #50
		WGR_status_quo_start #35
		WGR_austerity_start #35
		WGR_austerity_budget_cuts #35
		WGR_austerity_military_cuts #40
		WGR_military_start #35
		WGR_army_amnesty_for_nazi_officers #35
		WGR_Zentrum_Promote_Christian_Teachings #20
		WGR_austerity_raise_taxes #30
		WGR_austerity_privatize_infrastructure #30
		WGR_austerity_hire_new_contractors #28
		WGR_Zentrum_Appeal_to_SPD #15
		WGR_army_grand_battleplan #35
		WGR_austerity_weather_the_storm #10
		WGR_austerity_more_public_works #50
		WGR_austerity_small_bailouts #20
		WGR_Zentrum_Primacy_Of_Chancellor #28
		WGR_austerity_end_austerity #10
		WGR_foreign_policy_start #35
		WGR_foreign_policy_western_europe #35
		WGR_foreign_policy_renegotiate_rhineland #35
		#Total 1439 Dec 36
		WGR_army_battleplan_infantry_weapons #35
		WGR_foreign_policy_western_europe #35
		WGR_army_battleplan_planning_office #35
		WGR_airforce_light_bombers #35
		WGR_foreign_policy_anglo_german_conciliation #35
		WGR_Zentrum_Green_Belts #20
		WGR_foreign_policy_anglo_german_naval_treaty #35
		WGR_airforce_carrier_CAS #35
		WGR_army_support_equipment #35
		WGR_army_battleplan_spec_ops #35
		WGR_foreign_policy_franco_german_conciliation #35
		WGR_austerity_restore_budget #28
		WGR_austerity_keep_the_tax_break #25
		WGR_austerity_lower_taxes #50
		WGR_foreign_policy_trade_agreement_with_france #35
		WGR_austerity_military_effort #45
		#Total 1992 Jun 38
		WGR_foreign_policy_anglo_german_trade #35
		WGR_army_battleplan_defensive_posture #35
		WGR_Zentrum_Encourage_Foreign_Investors #25
		WGR_austerity_military_effort2 #40
		WGR_foreign_policy_anglo_german_research #35
		WGR_Zentrum_Social_Conservatism #30
		WGR_austerity_dispersed_development #50
		WGR_foreign_policy_denounce_soviet #35
		WGR_navy_go_around_britain #35
		WGR_army_doctrines #35
		WGR_austerity_resources #49
		WGR_foreign_policy_denounce_soviet #35
		WGR_foreign_policy_fra_research_agreement #35
		WGR_foreign_policy_align_west_defend_Belgium #35
		WGR_foreign_policy_align_west_defend_netherlands #35
		WGR_foreign_policy_benelux_trade_agreements #35
		#Total 2571 Jan 40: freedom	
	} 

	focus_factors = {
		
	}
	research = {

	}

	ideas = {

	}
	traits = {
	}

	ai_strategy = { 
		type = role_ratio
		id = garrison
		value = 5
	}

	# Keep small, as it is used as a factor for some things (such as research needs)
	# Recommended around 1.0. Useful for relation between plans
	weight = {
		factor = 1.0
		modifier = {
			factor = 1.0
		}
	}

}