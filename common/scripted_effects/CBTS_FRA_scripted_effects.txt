FRA_fall_government = {
	if = {
		limit = {
			has_government = social_liberalism
		}
		country_event = {
			id = france.10
			hours = 3
		}
	}
	else_if = {
		limit = {
			has_government = market_liberalism
		}
		country_event = {
			id = france.11
			hours = 3
		}
	}
	else_if = {
		limit = {
			has_government = social_democracy
		}
		country_event = {
			id = france.12
			hours = 3
		}
	}
	else_if = {
		limit = {
			has_government = socialism
		}
		country_event = {
			id = france.17
			hours = 3
		}
	}
}

FRA_coalition_stability_change = {
	if = {
		limit = {
			has_variable = coalition_stability
		}
		add_to_variable = { coalition_stability = tempchange }
		custom_effect_tooltip = change_coalition_stability_tt
		hidden_effect = {
			if = {
				limit = {
					check_variable = { coalition_stability < 0.25 }
					NOT = {
						has_government = social_liberalism
					}
				}
				country_event = france.14
			}
			else_if = {
				limit = {
					check_variable = { coalition_stability > 0.65 }
					NOT = {
						has_government = socialism
					}
				}
				country_event = france.15
			}
			else_if = {
				limit = {
					check_variable = { coalition_stability < 0 }
				}
				country_event = france.16
			}
			clamp_variable = { var = coalition_stability min = -1 max = 1 }
		}
	}
}


##Parliament effects##

FRA_parliament_clr_effects = {
	clr_country_flag = law_passing
	clr_country_flag = no_neg_communists
	clr_country_flag = no_neg_socs
	clr_country_flag = no_neg_socdem
	clr_country_flag = no_neg_soclib
	clr_country_flag = no_neg_marlib
	clr_country_flag = no_neg_centre
	clr_country_flag = no_neg_cons
	clr_country_flag = no_neg_agra
	clr_country_flag = no_neg_rad
	clr_country_flag = no_neg_indsoc
	clr_country_flag = already_communists
	clr_country_flag = already_socs
	clr_country_flag = already_socdem
	clr_country_flag = already_soclib
	clr_country_flag = already_marlib
	clr_country_flag = already_centre
	clr_country_flag = already_cons
	clr_country_flag = already_aut
	clr_country_flag = already_far_right
	clr_country_flag = already_agra
	clr_country_flag = already_rad
	clr_country_flag = already_indsoc
	clr_country_flag = yes_neg_soclib
	clr_country_flag = yes_neg_cons
	clr_country_flag = yes_neg_centre
	clr_country_flag = yes_neg_socdem
	clr_country_flag = yes_neg_demsoc
	clr_country_flag = yes_neg_communist
	clr_country_flag = promised_church
	clr_country_flag = senate_no
	clr_country_flag = senate_yes
	clr_country_flag = yes_referendum
	clr_country_flag = already_defend
	set_variable = { supseat = 0 }
}

FRA_parliament_parcial_clr_effects = {
	clr_country_flag = already_communists
	clr_country_flag = already_socs
	clr_country_flag = already_socdem
	clr_country_flag = already_soclib
	clr_country_flag = already_marlib
	clr_country_flag = already_centre
	clr_country_flag = already_cons
	clr_country_flag = already_aut
	clr_country_flag = already_far_right
	clr_country_flag = already_agra
	clr_country_flag = already_rad
	clr_country_flag = already_indsoc
	clr_country_flag = yes_neg_soclib
	clr_country_flag = yes_neg_cons
	clr_country_flag = yes_neg_centre
	clr_country_flag = yes_neg_socdem
	clr_country_flag = yes_neg_demsoc
	clr_country_flag = yes_neg_communist
	clr_country_flag = promised_church
	clr_country_flag = already_defend
	clr_country_flag = yes_referendum
}

FRA_generic_negotiation = {
	hidden_effect = {
		if = {
			limit = {
				has_country_flag = yes_neg_communist
			}
			add_timed_idea = {
				idea = FRA_temp_worker_condition_commune_edition
				days = 60
			}
		}
		
		if = { # DemSoc
			limit = {
				has_country_flag = yes_neg_demsoc
			}
			add_timed_idea = {
				idea = FRA_temp_worker_condition
				days = 60
			}
		}
		
		if = { # SocDem
			limit = {
				has_country_flag = yes_neg_socdem
			}
			add_timed_idea = {
				idea = FRA_temp_public_spending
				days = 60
			}
		}

		if = { # Soclib
			limit = {
				has_country_flag = yes_neg_soclib
			}
			add_timed_idea = {
				idea = FRA_temp_gov_help
				days = 60
			}
		}

		if = { # Market Lib
			limit = {
				has_country_flag = yes_neg_marlib
			}
			add_timed_idea = {
				idea = FRA_temp_worker_condition
				days = 60
			}
		}

		if = { # Conservative SocCon
			limit = {
				has_country_flag = yes_neg_cons
			}
			add_timed_idea = {
				idea = FRA_temp_protectionism
				days = 60
			}
		}


		if = { # Centre LibCon
			limit = {
				has_country_flag = yes_neg_centre
			}
			add_timed_idea = {
				idea = FRA_temp_tax_cuts
				days = 60
			}
		}	}
}

calculate_agra_seats = {
	set_variable = { agra_seats = cons_seats }
	divide_variable = { agra_seats = 3 }
	round_variable = agra_seats
}

calculate_rad_seats = {
	set_variable = { rad_seats = soclib_seats }
	divide_variable = { rad_seats = 3 }
	round_variable = rad_seats
}

calculate_indsoc_seats = {
	set_variable = { indsoc_seats = communist_seats }
	add_to_variable = { indsoc_seats = demsoc_seats }
	add_to_variable = { indsoc_seats = socdem_seats }
	divide_variable = { indsoc_seats = 4 }
	round_variable = indsoc_seats
}

calculate_nonid_seats = {
	calculate_agra_seats = yes
	calculate_indsoc_seats = yes
	calculate_rad_seats = yes
}

##Ideas effects##

# Letat Reforms
letat_reform = {
	if = {
		limit = {
			has_idea = FRA_letat_reform0
		}
		swap_ideas = {
			remove_idea = FRA_letat_reform0
			add_idea = FRA_letat_reform1
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_letat_reform1
		}
		swap_ideas = {
			remove_idea = FRA_letat_reform1
			add_idea = FRA_letat_reform2
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_letat_reform3
		}
		swap_ideas = {
			remove_idea = FRA_letat_reform3
			add_idea = FRA_letat_reform4
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_letat_reform4
		}
		swap_ideas = {
			remove_idea = FRA_letat_reform4
			add_idea = FRA_letat_reform5
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_letat_reform5
		}
		swap_ideas = {
			remove_idea = FRA_letat_reform5
			add_idea = FRA_letat_reform6
		}
	}
	else = {
		swap_ideas = {
			remove_idea = FRA_letat_reform2
			add_idea = FRA_letat_reform3
		}
	}
}

# Liquidity Crisis
worsen_liquidity_crisis = {
	if = {
		limit = {
			has_idea = FRA_restored_liquidity2
		}
		swap_ideas = {
			remove_idea = FRA_restored_liquidity2
			add_idea = FRA_restored_liquidity1
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_restored_liquidity1
		}
		swap_ideas = {
			remove_idea = FRA_restored_liquidity1
			add_idea = FRA_liquidity_crisis1
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_liquidity_crisis1
		}
		swap_ideas = {
			remove_idea = FRA_liquidity_crisis1
			add_idea = FRA_liquidity_crisis2
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_liquidity_crisis2
		}
		swap_ideas = {
			remove_idea = FRA_liquidity_crisis2
			add_idea = FRA_liquidity_crisis3
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_liquidity_crisis3
		}
		swap_ideas = {
			remove_idea = FRA_liquidity_crisis3
			add_idea = FRA_liquidity_crisis4
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_liquidity_crisis4
		}
		swap_ideas = {
			remove_idea = FRA_liquidity_crisis4
			add_idea = FRA_liquidity_crisis5
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_liquidity_crisis5
		}
		swap_ideas = {
			remove_idea = FRA_liquidity_crisis5
			add_idea = FRA_liquidity_crisis6
		}
	}
	else = {
		add_stability = -0.05
	}
}

improve_liquidity_crisis = {
	if = {
		limit = {
			has_idea = FRA_liquidity_crisis5
		}
		swap_ideas = {
			remove_idea = FRA_liquidity_crisis5
			add_idea = FRA_liquidity_crisis4
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_liquidity_crisis4
		}
		swap_ideas = {
			remove_idea = FRA_liquidity_crisis4
			add_idea = FRA_liquidity_crisis3
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_liquidity_crisis3
		}
		swap_ideas = {
			remove_idea = FRA_liquidity_crisis3
			add_idea = FRA_liquidity_crisis2
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_liquidity_crisis2
		}
		swap_ideas = {
			remove_idea = FRA_liquidity_crisis2
			add_idea = FRA_liquidity_crisis1
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_liquidity_crisis1
		}
		swap_ideas = {
			remove_idea = FRA_liquidity_crisis1
			add_idea = FRA_restored_liquidity1
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_restored_liquidity1
		}
		swap_ideas = {
			remove_idea = FRA_restored_liquidity1
			add_idea = FRA_restored_liquidity2
		}
	}
	else = {
		add_stability = 0.05
	}
}

remove_liquidity_ideas = {
	if = {
		limit = {
			has_idea = FRA_restored_liquidity2
		}
		remove_ideas = FRA_restored_liquidity2
	}
	else_if = {
		limit = {
			has_idea = FRA_restored_liquidity1
		}
		remove_ideas = FRA_restored_liquidity1
	}
	else_if = {
		limit = {
			has_idea = FRA_liquidity_crisis1
		}
		remove_ideas = FRA_liquidity_crisis1
	}
	else_if = {
		limit = {
		has_idea = FRA_liquidity_crisis2
		}
		remove_ideas = FRA_liquidity_crisis2
	}
	else_if = {
		limit = {
			has_idea = FRA_liquidity_crisis3
		}
		remove_ideas = FRA_liquidity_crisis3
	}
	else_if = {
		limit = {
			has_idea = FRA_liquidity_crisis4
		}
		remove_ideas = FRA_liquidity_crisis4
	}
	else = {
		remove_ideas = FRA_liquidity_crisis5
	}
}

# Agricultural Crisis

improve_agricultural_crisis = {
	if = {
		limit = {
			has_idea = FRA_agriculture_crisis3
		}
		swap_ideas = {
			remove_idea = FRA_agriculture_crisis3
			add_idea = FRA_agriculture_crisis2
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_agriculture_crisis2
		}
		swap_ideas = {
			remove_idea = FRA_agriculture_crisis2
			add_idea = FRA_agriculture_crisis1
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_agriculture_crisis1
		}
		remove_ideas = { FRA_agriculture_crisis1 }
	}
	else = {
		add_stability = 0.05
	}
}

worsen_agricultural_crisis = {
	if = {
		limit = {
			has_idea = FRA_agriculture_crisis2
		}
		swap_ideas = {
			remove_idea = FRA_agriculture_crisis2
			add_idea = FRA_agriculture_crisis3
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_agriculture_crisis1
		}
		swap_ideas = {
			remove_idea = FRA_agriculture_crisis1
			add_idea = FRA_agriculture_crisis2
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_agriculture_crisis3
		}
		add_stability = -0.05
	}
	else = {
		add_stability = -0.05
	}
}

remove_agricultural_crisis = {
	if = {
		limit = {
			has_idea = FRA_agriculture_crisis3
		}
		remove_ideas = FRA_agriculture_crisis3
	}
	else_if = {
		limit = {
			has_idea = FRA_agriculture_crisis2
		}
		remove_ideas = FRA_agriculture_crisis2
	}
	else = {
		remove_ideas = FRA_agriculture_crisis1
	}
}

# Social Crisis

worsen_social_crisis = {
	if = {
		limit = {
			has_idea = FRA_welfare_state3
		}
		swap_ideas = {
			remove_idea = FRA_welfare_state3
			add_idea = FRA_welfare_state2
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_welfare_state2
		}
		swap_ideas = {
			remove_idea = FRA_welfare_state2
			add_idea = FRA_welfare_state1
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_welfare_state1
		}
		swap_ideas = {
			remove_idea = FRA_welfare_state1
			add_idea = FRA_social_crisis1
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_social_crisis1
		}
		swap_ideas = {
			remove_idea = FRA_social_crisis1
			add_idea = FRA_social_crisis2
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_social_crisis2
		}
		swap_ideas = {
			remove_idea = FRA_social_crisis2
			add_idea = FRA_social_crisis3
		}
	}
	else = {
		add_stability = -0.05
	}
}

improve_social_crisis = {
	if = {
		limit = {
			has_idea = FRA_social_crisis3
		}
		swap_ideas = {
			remove_idea = FRA_social_crisis3
			add_idea = FRA_social_crisis2
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_social_crisis2
		}
		swap_ideas = {
			remove_idea = FRA_social_crisis2
			add_idea = FRA_social_crisis1
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_social_crisis1
		}
		swap_ideas = {
			remove_idea = FRA_social_crisis1
			add_idea = FRA_welfare_state1
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_welfare_state1
		}
		swap_ideas = {
			remove_idea = FRA_welfare_state1
			add_idea = FRA_welfare_state2
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_welfare_state2
		}
		swap_ideas = {
			remove_idea = FRA_welfare_state2
			add_idea = FRA_welfare_state3
		}
	}
	else = {
		add_stability = 0.05
	}
}

remove_social_crisis = {
	if = {
		limit = {
			has_idea = FRA_social_crisis3
		}
		remove_ideas = FRA_social_crisis3
	}
	else_if = {
		limit = {
			has_idea = FRA_social_crisis2
		}
		remove_ideas = FRA_social_crisis2
	}
	else_if = {
		limit = {
			has_idea = FRA_social_crisis1
		}
		remove_ideas = FRA_social_crisis1
	}
	else_if = {
		limit = {
			has_idea = FRA_welfare_state1
		}
		remove_ideas = FRA_welfare_state1
	}
	else_if = {
		limit = {
			has_idea = FRA_welfare_state2
		}
		remove_ideas = FRA_welfare_state2
	}
	else = {
		remove_ideas = FRA_welfare_state3
	}
}

# Economic Downturn

improve_economic_downturn = {
	if = {
		limit = {
			has_idea = FRA_economic_downturn3
		}
		swap_ideas = {
			remove_idea = FRA_economic_downturn3
			add_idea = FRA_economic_downturn2
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_economic_downturn2
		}
		swap_ideas = {
			remove_idea = FRA_economic_downturn2
			add_idea = FRA_economic_downturn1
		}
	}
	else_if = {
		limit = {
			has_idea = FRA_economic_downturn1
		}
		remove_ideas = { FRA_economic_downturn1 }
	}
	else = {
		custom_effect_tooltip = FRA_finished_economic_downturn_tt
	}
}

##Bloc d'Or effects

add_bloc_dor = { # Add a member, used in the corresponding event
	add_to_array = { global.member_bloc_dor = var:ROOT }

	add_to_variable = { global.num_member_bloc_dor = 1 }

	if = {
		limit = {
			NOT = { has_variable = global.num_member_bloc_dor }
		}
		effect_tooltip = {
			add_ideas = { bloc_dor1 }
		}
	}

	for_each_scope_loop = {
		array = global.member_bloc_dor


		if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 1 }
			}
			add_ideas = { bloc_dor1 }
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 2 }
				NOT = { has_country_flag = member_bloc_dor }
			}
			add_ideas = { bloc_dor2 }
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 3 }
				NOT = { has_country_flag = member_bloc_dor }
			}
			add_ideas = { bloc_dor3 }
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 4 }
				NOT = { has_country_flag = member_bloc_dor }
			}
			add_ideas = { bloc_dor4 }
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 5 }
				NOT = { has_country_flag = member_bloc_dor }
			}
			add_ideas = { bloc_dor5 }
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 6 }
				NOT = { has_country_flag = member_bloc_dor }
			}
			add_ideas = { bloc_dor6 }
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 2 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor1
				add_idea = bloc_dor2
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 3 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor2
				add_idea = bloc_dor3
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 4 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor3
				add_idea = bloc_dor4
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 5 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor4
				add_idea = bloc_dor5
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 6 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor5
				add_idea = bloc_dor6
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 7 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor6
				add_idea = bloc_dor7
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 8 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor7
				add_idea = bloc_dor8
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 9 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor8
				add_idea = bloc_dor9
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 10 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor9
				add_idea = bloc_dor10
			}
		}
		if = {
			limit = {
				has_country_flag = member_bloc_dor
			}
			country_event = france.32
		}

		add_opinion_modifier = {
			target = PREV
			modifier = FRA_member_bloc_dor
		}
		reverse_add_opinion_modifier = {
			target = PREV
			modifier = FRA_member_bloc_dor
		}
	}

	set_country_flag = member_bloc_dor
}

remove_block_dor = { # Removes a member, should be used in any focus that abandons the gold standard
	remove_from_array = { global.member_bloc_dor = var:ROOT }

	subtract_from_variable = { global.num_member_bloc_dor = 1 }
	clr_country_flag = member_bloc_dor

	if = {
		limit = {
			has_idea = bloc_dor1
		}
		remove_ideas = { bloc_dor1 }
	}
	else_if = {
		limit = {
			has_idea = bloc_dor2
		}
		remove_ideas = { bloc_dor2 }
	}
	else_if = {
		limit = {
			has_idea = bloc_dor3
		}
		remove_ideas = { bloc_dor3 }
	}
	else_if = {
		limit = {
			has_idea = bloc_dor4
		}
		remove_ideas = { bloc_dor4 }
	}
	else_if = {
		limit = {
			has_idea = bloc_dor5
		}
		remove_ideas = { bloc_dor5 }
	}
	else_if = {
		limit = {
			has_idea = bloc_dor6
		}
		remove_ideas = { bloc_dor6 }
	}
	else_if = {
		limit = {
			has_idea = bloc_dor7
		}
		remove_ideas = { bloc_dor7 }
	}
	else_if = {
		limit = {
			has_idea = bloc_dor8
		}
		remove_ideas = { bloc_dor8 }
	}
	else_if = {
		limit = {
			has_idea = bloc_dor9
		}
		remove_ideas = { bloc_dor9 }
	}
	else_if = {
		limit = {
			has_idea = bloc_dor10
		}
		remove_ideas = { bloc_dor10 }
	}

	for_each_scope_loop = {
		array = global.member_bloc_dor

		remove_opinion_modifier = {
			target = ROOT
			modifier = FRA_member_bloc_dor
		}
		ROOT = {
			remove_opinion_modifier = {
				target = PREV
				modifier = FRA_member_bloc_dor
			}
		}

		if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 2 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor3
				add_idea = bloc_dor2
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 3 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor4
				add_idea = bloc_dor3
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 4 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor5
				add_idea = bloc_dor4
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 5 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor6
				add_idea = bloc_dor5
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 6 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor7
				add_idea = bloc_dor6
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 7 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor8
				add_idea = bloc_dor7
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 8 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor9
				add_idea = bloc_dor8
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 9 }
				has_country_flag = member_bloc_dor
			}
			swap_ideas = {
				remove_idea = bloc_dor10
				add_idea = bloc_dor9
			}
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 1 }
				has_country_flag = member_bloc_dor
				FRA = {
					OR = {
						has_completed_focus = FRA_invite_czechoslovakia
						NOT = { country_exists = CZE }
					}
					OR = {
						has_completed_focus = FRA_invite_germany
						AND = {
							GER = {
								OR = {
									has_government = fascism
									has_government = nationalism
									exists = no
								}
							}
							WGR = {
								OR = {
									has_government = fascism
									has_government = nationalism
									exists = no
								}
							}
							DDR = {
								OR = {
									has_government = fascism
									has_government = nationalism
									exists = no
								}
							}
						}
					}
					OR = {
						has_completed_focus = FRA_invite_yugoslavia
						NOT = { country_exists = YUG }
					}
				}
			}
			
			disband_bloc_dor = yes
		}
		else_if = {
			limit = {
				check_variable = { global.num_member_bloc_dor = 1 }
				has_country_flag = member_bloc_dor
				FRA = {
					OR = {
						has_completed_focus = FRA_invite_czechoslovakia
						NOT = { country_exists = CZE }
					}
					OR = {
						has_completed_focus = FRA_invite_germany
						AND = {
							GER = {
								OR = {
									has_government = fascism
									has_government = nationalism
									exists = no
								}
							}
							WGR = {
								OR = {
									has_government = fascism
									has_government = nationalism
									exists = no
								}
							}
							DDR = {
								OR = {
									has_government = fascism
									has_government = nationalism
									exists = no
								}
							}
						}
					}
					OR = {
						has_completed_focus = FRA_invite_yugoslavia
						NOT = { country_exists = YUG }
					}
				}
			}
			swap_ideas = {
				remove_idea = bloc_dor2
				add_idea = bloc_dor1
			}
		}

		country_event = {
			id = france.34
			days = 1
		}
	}
}

abandon_gold_standard = {
	set_country_flag = abandoned_gold_standard
	if = {
		limit = {
			in_bloc_dor = yes
		}
		remove_block_dor = yes
	}
}

disband_bloc_dor = { #Disbands the Bloc, used if france is alone in the bloc and can't invite anyone, a war is declared between one of the members or France abandons the gold standard
	
	for_each_scope_loop = {
		array = global.member_bloc_dor

		clr_country_flag = member_bloc_dor

		if = {
			limit = {
				has_idea = bloc_dor1
			}
			remove_ideas = { bloc_dor1 }
		}
		else_if = {
			limit = {
				has_idea = bloc_dor2
			}
			remove_ideas = { bloc_dor2 }
		}
		else_if = {
			limit = {
				has_idea = bloc_dor3
			}
			remove_ideas = { bloc_dor3 }
		}
		else_if = {
			limit = {
				has_idea = bloc_dor4
			}
			remove_ideas = { bloc_dor4 }
		}
		else_if = {
			limit = {
				has_idea = bloc_dor5
			}
			remove_ideas = { bloc_dor5 }
		}
		else_if = {
			limit = {
				has_idea = bloc_dor6
			}
			remove_ideas = { bloc_dor6 }
		}
		else_if = {
			limit = {
				has_idea = bloc_dor7
			}
			remove_ideas = { bloc_dor7 }
		}
		else_if = {
			limit = {
				has_idea = bloc_dor8
			}
			remove_ideas = { bloc_dor8 }
		}
		else_if = {
			limit = {
				has_idea = bloc_dor9
			}
			remove_ideas = { bloc_dor9 }
		}
		else_if = {
			limit = {
				has_idea = bloc_dor10
			}
			remove_ideas = { bloc_dor10 }
		}

		country_event = {
			id = france.35
			days = 2
		}
	}
}

# Idk how usefull is this and probably will never be use
set_representative_parliament_seats = {

	set_temp_variable = { total_popularity = 0 }

	## Here it takes the party's popularity
	if = {
		limit = {
			NOT = { has_country_flag = communist_ban }
		}
		set_temp_variable = { communist_popularity = party_popularity@communism }
		multiply_temp_variable = { communist_popularity = 100 }
		add_to_temp_variable = { total_popularity = communist_popularity }
	}
	if = {
		limit = {
			NOT = { has_country_flag = demsoc_ban }
		}
		set_temp_variable = { demsoc_popularity = party_popularity@socialism }
		add_to_temp_variable = { demsoc_popularity = party_popularity@authoritarian_socialism }
		multiply_temp_variable = { demsoc_popularity = 100 }
		add_to_temp_variable = { total_popularity = demsoc_popularity}
	}
	if = {
		limit = {
			NOT = { has_country_flag = socdem_ban }
		}
		set_temp_variable = { socdem_popularity = party_popularity@social_democracy }
		multiply_temp_variable = { socdem_popularity = 100 }
		add_to_temp_variable = { total_popularity = socdem_popularity }
	}
	if = {
		limit = {
			NOT = { has_country_flag = soclib_ban }
		}
		set_temp_variable = { soclib_popularity = party_popularity@social_liberalism }
		multiply_temp_variable = { soclib_popularity = 100 }
		add_to_temp_variable = { total_popularity = soclib_popularity }
	}
	if = {
		limit = {
			NOT = { has_country_flag = marlib_ban }
		}
		set_temp_variable = { marlib_popularity = party_popularity@market_liberalism }
		multiply_temp_variable = { marlib_popularity = 100 }
		add_to_temp_variable = { total_popularity = marlib_popularity }
	}
	if = {
		limit = {
			NOT = { has_country_flag = centre_ban }
		}
		set_temp_variable = { centre_popularity = party_popularity@jacobin }
		multiply_temp_variable = { centre_popularity = 100 }
		add_to_temp_variable = { total_popularity = centre_popularity }
	}
	if = {
		limit = {
			NOT = { has_country_flag = cons_ban }
		}
		set_temp_variable = { cons_popularity = party_popularity@democratic }
		multiply_temp_variable = { cons_popularity = 100 }
		add_to_temp_variable = { total_popularity = cons_popularity }
	}
	if = {
		limit = {
			NOT = { has_country_flag = aut_ban }
		}
		set_temp_variable = { aut_popularity = party_popularity@neutrality }
		multiply_temp_variable = { aut_popularity = 100 }
		add_to_temp_variable = { total_popularity = aut_popularity }
	}
	if = {
		limit = {
			NOT = { has_country_flag = mon_ban }
		}
		set_temp_variable = { mon_popularity = party_popularity@monarchism }
		multiply_temp_variable = { mon_popularity = 100 }
		add_to_temp_variable = { total_popularity = mon_popularity }
	}
	if = {
		limit = {
			NOT = { has_country_flag = nat_ban }
		}
		set_temp_variable = { nat_popularity = party_popularity@nationalism }
		multiply_temp_variable = { nat_popularity = 100 }
		add_to_temp_variable = { total_popularity = nat_popularity }
	}
	if = {
		limit = {
			NOT = { has_country_flag = fas_ban }
		}
		set_temp_variable = { fas_popularity = party_popularity@fascism }
		multiply_temp_variable = { fas_popularity = 100 }
		add_to_temp_variable = { total_popularity = fas_popularity}
	}

	## So here is where the seats are calculated, if the party is banned, it will default to 0
	##
	## Equation: ideology_seats = total_seats / ( total_popularity / party_popularity )
	##           ideology_seats = total_seats / party_factor

	set_temp_variable = { temp_total_seats = 0 }

	if = {
		limit = {
			NOT = { has_country_flag = communist_ban }
			check_variable = { communist_popularity > 0 }
		}
		set_temp_variable = { communist_factor = total_popularity }
		set_variable = { communist_seats =  total_seats }
		divide_temp_variable = { communist_factor = communist_popularity }
		divide_variable = { communist_seats = communist_factor }
		round_variable = communist_seats
		add_to_temp_variable = { temp_total_seats = communist_seats }
	}
	else = {
		set_variable = { communist_seats = 0 }
	}

	if = {
		limit = {
			NOT = { has_country_flag = demsoc_ban }
			check_variable = { demsoc_popularity > 0 }
		}
		set_temp_variable = { demsoc_factor = total_popularity }
		set_variable = { demsoc_seats =  total_seats }
		divide_temp_variable = { demsoc_factor = demsoc_popularity }
		divide_variable = { demsoc_seats = demsoc_factor }
		round_variable = demsoc_seats
		add_to_temp_variable = { temp_total_seats = demsoc_seats }
	}
	else = {
		set_variable = { demsoc_seats = 0 }
	}

	if = {
		limit = {
			NOT = { has_country_flag = socdem_ban }
			check_variable = { socdem_popularity > 0 }
		}
		set_temp_variable = { socdem_factor = total_popularity }
		set_variable = { socdem_seats =  total_seats }
		divide_temp_variable = { socdem_factor = socdem_popularity }
		divide_variable = { socdem_seats = socdem_factor }
		round_variable = socdem_seats
		add_to_temp_variable = { temp_total_seats = socdem_seats }
	}
	else = {
		set_variable = { socdem_seats = 0 }
	}

	if = {
		limit = {
			NOT = { has_country_flag = soclib_ban }
			check_variable = { soclib_popularity > 0 }
		}
		set_temp_variable = { soclib_factor = total_popularity }
		set_variable = { soclib_seats =  total_seats }
		divide_temp_variable = { soclib_factor = soclib_popularity }
		divide_variable = { soclib_seats = soclib_factor }
		round_variable = soclib_seats
		add_to_temp_variable = { temp_total_seats = soclib_seats }
	}
	else = {
		set_variable = { soclib_seats = 0 }
	}

	if = {
		limit = {
			NOT = { has_country_flag = marlib_ban }
			check_variable = { marlib_popularity > 0 }
		}
		set_temp_variable = { marlib_factor = total_popularity }
		set_variable = { marlib_seats =  total_seats }
		divide_temp_variable = { marlib_factor = marlib_popularity }
		divide_variable = { marlib_seats = marlib_factor }
		round_variable = marlib_seats
		add_to_temp_variable = { temp_total_seats = marlib_seats }
	}
	else = {
		set_variable = { marlib_seats = 0 }
	}

	if = {
		limit = {
			NOT = { has_country_flag = centre_ban }
			check_variable = { centre_popularity > 0 }
		}
		set_temp_variable = { centre_factor = total_popularity }
		set_variable = { centre_seats =  total_seats }
		divide_temp_variable = { centre_factor = centre_popularity }
		divide_variable = { centre_seats = centre_factor }
		round_variable = centre_seats
		add_to_temp_variable = { temp_total_seats = centre_seats }
	}
	else = {
		set_variable = { centre_seats = 0 }
	}

	if = {
		limit = {
			NOT = { has_country_flag = cons_ban }
			check_variable = { cons_popularity > 0 }
		}
		set_temp_variable = { cons_factor = total_popularity }
		set_variable = { cons_seats =  total_seats }
		divide_temp_variable = { cons_factor = cons_popularity }
		divide_variable = { cons_seats = cons_factor }
		round_variable = cons_seats
		add_to_temp_variable = { temp_total_seats = cons_seats }
	}
	else = {
		set_variable = { cons_seats = 0 }
	}

	if = {
		limit = {
			NOT = { has_country_flag = aut_ban }
			check_variable = { aut_popularity > 0 }
		}
		set_temp_variable = { aut_factor = total_popularity }
		set_variable = { aut_seats =  total_seats }
		divide_temp_variable = { aut_factor = aut_popularity }
		divide_variable = { aut_seats = aut_factor }
		round_variable = aut_seats
		add_to_temp_variable = { temp_total_seats = aut_seats }
	}
	else = {
		set_variable = { aut_seats = 0 }
	}

	if = {
		limit = {
			NOT = { has_country_flag = mon_ban }
			check_variable = { mon_popularity > 0 }
		}
		set_temp_variable = { mon_factor = total_popularity }
		set_variable = { mon_seats =  total_seats }
		divide_temp_variable = { mon_factor = mon_popularity }
		divide_variable = { mon_seats = mon_factor }
		round_variable = mon_seats
		add_to_temp_variable = { temp_total_seats = mon_seats }
	}
	else = {
		set_variable = { mon_seats = 0 }
	}

	if = {
		limit = {
			NOT = { has_country_flag = nat_ban }
			check_variable = { nat_popularity > 0 }
		}
		set_temp_variable = { nat_factor = total_popularity }
		set_variable = { nat_seats =  total_seats }
		divide_temp_variable = { nat_factor = nat_popularity }
		divide_variable = { nat_seats = nat_factor }
		round_variable = nat_seats
		add_to_temp_variable = { temp_total_seats = nat_seats }
	}
	else = {
		set_variable = { nat_seats = 0 }
	}

	if = {
		limit = {
			NOT = { has_country_flag = fas_ban }
			check_variable = { fas_popularity > 0 }
		}
		set_temp_variable = { fas_factor = total_popularity }
		set_variable = { fas_seats =  total_seats }
		divide_temp_variable = { fas_factor = fas_popularity }
		divide_variable = { fas_seats = fas_factor }
		round_variable = fas_seats
		add_to_temp_variable = { temp_total_seats = fas_seats }
	}
	else = {
		set_variable = { fas_seats = 0 }
	}

	# Checks if the total seats requested are equal to the seats given, if not it will give or remove a random seat from a random party
	if = {
		limit = {
			check_variable = { total_seats > temp_total_seats }
		}
		set_temp_variable = { randomvar1 = random }
		set_temp_variable = { randomvar2 = -1 }
	}
	else_if = {
		limit = {
			check_variable = { total_seats < temp_total_seats }
		}
		set_temp_variable = { randomvar2 = random }
		set_temp_variable = { randomvar1 = -1 }
	}
	else = {
		set_temp_variable = { randomvar1 = -1 }
		set_temp_variable = { randomvar2 = -1 }
	}

	if = {
		limit = {
			NOT = {
				has_country_flag = communist_ban
				check_variable = { communist_seats = 0 }
			}
			check_variable = { randomvar1 > 0.917 }
		}
		add_to_variable = { communist_seats = 1}
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = radsoc_ban
				check_variable = { radsoc_seats = 0 }
			}
			check_variable = { randomvar1 > 0.834 }
		}
		add_to_variable = { radsoc_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = demsoc_ban
				check_variable = { demsoc_seats = 0 }
			}
			check_variable = { randomvar1 > 0.751 }
		}
		add_to_variable = { demsoc_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = socdem_ban
				check_variable = { socdem_seats = 0 }
			}
			check_variable = { randomvar1 > 0.668 }
		}
		add_to_variable = { socdem_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = soclib_ban
				check_variable = { soclib_seats = 0 }
			}
			check_variable = { randomvar1 > 0.585 }
		}
		add_to_variable = { soclib_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = marlib_ban
				check_variable = { marlib_seats = 0 }
			}
			check_variable = { randomvar1 > 0.502 }
		}
		add_to_variable = { marlib_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = centre_ban
				check_variable = { centre_seats = 0 }
			}
			check_variable = { randomvar1 > 0.419 }
		}
		add_to_variable = { centre_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = cons_ban
				check_variable = { cons_seats = 0 }
			}
			check_variable = { randomvar1 > 0.336 }
		}
		add_to_variable = { cons_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = aut_ban
				check_variable = { aut_seats = 0 }
			}
			check_variable = { randomvar1 > 0.253 }
		}
		add_to_variable = { aut_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = mon_ban
				check_variable = { mon_seats = 0 }
			}
				check_variable = { randomvar1 > 0.170 }
		}
		add_to_variable = { mon_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = nat_ban
				check_variable = { nat_seats = 0 }
			}
			check_variable = { randomvar1 > 0.87 }
		}
		add_to_variable = { nat_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = fas_ban
				check_variable = { fas_seats = 0 }
			}
			check_variable = {
				var = randomvar1
				value = 0
				compare = greater_than_or_equals
			}
		}
		add_to_variable = { fas_seats = 1 }
	}

	if = {
		limit = {
			NOT = {
				has_country_flag = communist_ban
				check_variable = { communist_seats = 0 }
			}
			check_variable = { randomvar2 > 0.917 }
		}
		subtract_from_variable = { communist_seats = 1}
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = radsoc_ban
				check_variable = { radsoc_seats = 0 }
			}
			check_variable = { randomvar2 > 0.834 }
		}
		subtract_from_variable = { radsoc_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = demsoc_ban
				check_variable = { demsoc_seats = 0 }
			}
			check_variable = { randomvar2 > 0.751 }
		}
		subtract_from_variable = { demsoc_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = socdem_ban
				check_variable = { socdem_seats = 0 }
			}
			check_variable = { randomvar2 > 0.668 }
		}
		subtract_from_variable = { socdem_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = soclib_ban
				check_variable = { soclib_seats = 0 }
			}
			check_variable = { randomvar2 > 0.585 }
		}
		subtract_from_variable = { soclib_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = marlib_ban
				check_variable = { marlib_seats = 0 }
			}
			check_variable = { randomvar2 > 0.502 }
		}
		subtract_from_variable = { marlib_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = centre_ban
				check_variable = { centre_seats = 0 }
			}
			check_variable = { randomvar2 > 0.419 }
		}
		subtract_from_variable = { centre_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = cons_ban
				check_variable = { cons_seats = 0 }
			}
			check_variable = { randomvar2 > 0.336 }
		}
		subtract_from_variable = { cons_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = aut_ban
				check_variable = { aut_seats = 0 }
			}
			check_variable = { randomvar2 > 0.253 }
		}
		subtract_from_variable = { aut_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = mon_ban
				check_variable = { mon_seats = 0 }
			}
				check_variable = { randomvar2 > 0.170 }
		}
		subtract_from_variable = { mon_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = nat_ban
				check_variable = { nat_seats = 0 }
			}
			check_variable = { randomvar2 > 0.87 }
		}
		subtract_from_variable = { nat_seats = 1 }
	}
	else_if = {
		limit = {
			NOT = {
				has_country_flag = fas_ban
				check_variable = { fas_seats = 0 }
			}
			check_variable = {
				var = randomvar2
				value = 0
				compare = greater_than_or_equals
			}
		}
		subtract_from_variable = { fas_seats = 1 }
	}

	set_coalition_seats = yes
}