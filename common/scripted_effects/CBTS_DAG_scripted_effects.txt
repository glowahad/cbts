DAG_add_2_escalation = {
	custom_effect_tooltip = add_2_esc
	add_to_variable  = {
		var = global.NC_Tension
		value = 2
	}
	clamp_variable = {
		var = global.NC_Tension
		min = 0
		max = 100
	}
}
DAG_add_1_escalation = {
	custom_effect_tooltip = add_1_esc
	add_to_variable  = {
		var = global.NC_Tension
		value = 1
	}
	clamp_variable = {
		var = global.NC_Tension
		min = 0
		max = 100
	}
}