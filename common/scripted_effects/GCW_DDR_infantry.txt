WGR_DDR_deploy_infantry = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_2 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_2 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_3 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_3 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_4 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_4 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_5 = yes
    }
    else = {
        add_political_power = 1
    }	
}
WGR_DDR_deploy_infantry_5 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_6 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_6 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_7 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_7 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_8 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_8 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_9 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_9 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_10 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_10 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_11 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_11 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_12 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_12 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_13 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_13 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_14 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_14 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_15 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_15 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_16 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_16 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_17 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_17 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_18 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_18 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_19 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_19 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_20 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_20 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_21 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_21 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_22 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_22 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_23 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_23 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_24 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_24 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
		WGR_DDR_deploy_infantry_25 = yes
    }
    else = {
        add_political_power = 1
    }	
}

WGR_DDR_deploy_infantry_25 = {
    if = {
	    limit = {
		    check_variable = { DDR_starting_infantry > 0 }
		}
		add_to_variable = { DDR_starting_infantry = -1 } 
		DDR = {
		    random_owned_state = {
                create_unit = {
                    division = "name = \"Kampfgruppen der Arbeiterklasse\" division_template = \"Infanterie-Division\" start_experience_factor = 0.5 start_equipment_factor = 0.3"
                    owner = DDR
                }
            }
        }
    }
    else = {
        add_political_power = 1
    }	
}