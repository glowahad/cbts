###Modifiers, for provinces, regions, etc


usa_mob_agreement_modifier = { # on Province
	army_speed_factor = 0.15
	army_core_defence_factor = -0.75
	dig_in_speed_factor = -0.8
}

unplanned_chaco_offensive = { # on Province
	army_defence_factor = 0.80
}

chaco_no_infrastructure = {
	army_speed_factor = -0.30
	pocket_penalty = 0.1
	org_loss_when_moving = 0.05
	out_of_supply_factor = 0.1
	army_attack_factor = -0.5
}