ideas = {
	country = {
	SAF_Racial_Segregation = {
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = yes
            }
            removal_cost = -1
            modifier = {
                stability_factor = -0.30
                war_support_factor = -0.30
				conscription_factor = -0.70
				political_power_gain = -0.30
            }
		}
	SAF_Slackened_Segregation_Reserve = {
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = yes
            }
            removal_cost = -1
            modifier = {
                stability_factor = -0.20
                war_support_factor = -0.20
				conscription_factor = -0.60
				political_power_gain = -0.20
            }
		}
	SAF_Slackened_Segregation = {
            allowed = {
                always = no
            }
            allowed_civil_war = {
                always = yes
            }
            removal_cost = -1
            modifier = {
                stability_factor = -0.40
                war_support_factor = -0.30
				conscription_factor = -0.65
				political_power_gain = -0.20
            }
		}
	SAF_depression_1 = {
		picture = great_depression
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}	
			removal_cost = -1
			modifier = {
				stability_factor = -0.10
				political_power_gain = -0.15
                industrial_capacity_factory = -0.33
                industrial_capacity_dockyard = -0.33
                production_speed_buildings_factor  = -0.25
				consumer_goods_factor = 0.10
				}
			}
	SAF_depression_2 = {
		picture = great_depression
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}	
			removal_cost = -1
			modifier = {
				stability_factor = -0.075
				political_power_gain = -0.12
                industrial_capacity_factory = -0.25
                industrial_capacity_dockyard = -0.25
                production_speed_buildings_factor  = -0.20
				consumer_goods_factor = 0.08
				}
			}
	SAF_depression_3 = {
		picture = great_depression
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}	
			removal_cost = -1
			modifier = {
				stability_factor = -0.05
				political_power_gain = -0.10
                industrial_capacity_factory = -0.20
                industrial_capacity_dockyard = -0.20
                production_speed_buildings_factor  = -0.15
				consumer_goods_factor = 0.06
				}
			}
	SAF_depression_4 = {
		picture = great_depression
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}	
			removal_cost = -1
			modifier = {
				stability_factor = -0.025
				political_power_gain = -0.07
                industrial_capacity_factory = -0.20
                industrial_capacity_dockyard = -0.20
                production_speed_buildings_factor  = -0.10
				consumer_goods_factor = 0.04
				}
			}
	SAF_depression_5 = {
		picture = great_depression
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}	
			removal_cost = -1
			modifier = {
				stability_factor = -0.01
				political_power_gain = -0.05
                industrial_capacity_factory = -0.15
                industrial_capacity_dockyard = -0.15
                production_speed_buildings_factor  = -0.05
				consumer_goods_factor = 0.02
				}
			}
	SAF_Afrikaner_Dominance = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				stability_factor = -0.025
				war_support_factor = 0.10
				political_power_gain = 0.10
				consumer_goods_factor = 0.05
			}
		}	
	SAF_Anglo_Afrikaner_Coalition = {
		picture = SAF_coalition_1
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				stability_factor = 0.025
				war_support_factor = -0.05
				consumer_goods_factor = -0.05
			}
		}	
	SAF_Growing_Unionism = {
			picture = Unionism
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.10
				 production_speed_buildings_factor  = -0.10
				 political_power_gain = -0.05
			}
		}
	SAF_Sidelined_Unionism = {
		picture = General_Strikes
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				stability_factor = -0.05
				consumer_goods_factor = 0.05
				political_power_gain = -0.10
				production_speed_buildings_factor  = 0.10
			}
		}
	SAF_Tempered_Unionism = {
		picture = anger_against_gov
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				stability_factor = 0.05
				political_power_gain = 0.05
				consumer_goods_factor = 0.15
				production_speed_buildings_factor  = -0.15
			}
		}
	SAF_Kommando_Legacy = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				out_of_supply_factor = -0.30
				army_core_attack_factor = 0.1
				army_core_defence_factor = 0.1
			}
		}
	SAF_Industrializing_Nation = {
		picture = SAF_Developing_Economy
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.10
				research_speed_factor = 0.1
				production_factory_max_efficiency_factor = -0.1
				production_speed_buildings_factor = -0.1
				industrial_capacity_factory = -0.1
				industrial_capacity_dockyard = -0.1
			}
		}
		SAF_Progressed_Industrialization = {
			picture = SAF_Developing_Economy
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.05
				production_factory_max_efficiency_factor = -0.05
				production_speed_buildings_factor = -0.05
				industrial_capacity_factory = -0.05
				industrial_capacity_dockyard = -0.05
			}
		}
	SAF_Sterling_Area = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = -0.05
				stability_factor = 0.025
			}
		}
	SAF_Independent_Currency = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.05
				stability_factor = 0.05
			}
		}
	SAF_agricultural_tech = {
		picture = Agricultural_Tech
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.03
				MONTHLY_POPULATION = 0.05
			}
		}
	SAF_industrial_tech = {
		picture = RUS_Western_Industrial_Practices
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = -0.05
			}
		}
	SAF_import_taxes_temp = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.05
			}
		}
	SAF_coalition_1 = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = -0.05
				war_support_factor = 0.10
				consumer_goods_factor = 0.05
				
			}
			removal_cost = -1
		}
	SAF_coalition_2 = {
		picture = SAF_coalition_1
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = -0.025
				war_support_factor = 0.05
				consumer_goods_factor = 0.025
				political_power_gain = 0.05
				
			}
			removal_cost = -1
		}
	SAF_coalition_3 = {
		picture = SAF_coalition_1
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				political_power_gain = 0.10
			}
			removal_cost = -1
		}
	SAF_coalition_4 = {
		picture = SAF_coalition_1
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = 0.025
				war_support_factor = -0.05
				consumer_goods_factor = -0.025
				political_power_gain = 0.05
				
			}
			removal_cost = -1
		}
	SAF_coalition_5 = {
		picture = SAF_coalition_1
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = 0.05
				war_support_factor = -0.10
				consumer_goods_factor = -0.05
				
			}
			removal_cost = -1
		}
	SAF_Coalition_Failed = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = -0.20
				war_support_factor = -0.15
				political_power_gain = -0.05
				
			}
			removal_cost = -1
		}
	SAF_Coalition_Success = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = 0.05
				war_support_factor = 0.05
				political_power_gain = 0.10
				
			}
			removal_cost = -1
		}
	SAF_Economy_Collapsed = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = -0.15
				political_power_gain = -0.20
                industrial_capacity_factory = -0.33
                industrial_capacity_dockyard = -0.33
                production_speed_buildings_factor  = -0.25
				consumer_goods_factor = 0.20
				
			}
			removal_cost = -1
		}
	SAF_Depression_Fixed = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = 0.10
				political_power_gain = 0.10
                industrial_capacity_factory = 0.33
                industrial_capacity_dockyard = 0.33
                production_speed_buildings_factor  = 0.10
				consumer_goods_factor = -0.10
				
			}
			removal_cost = -1
		}
		SAF_1934_Status_Of_The_Union_Act = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = 0.05
				political_power_gain = 0.05
			}
		}
		SAF_Improve_Railways = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				production_speed_infrastructure_factor = 0.1
			}
		}
		SAF_Improved_Rhodesian_Trade = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				consumer_goods_factor = -0.02
				
				}
			}
		SAF_Tighter_Stirling_Area = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				consumer_goods_factor = -0.03
				
				}
			}
		SAF_Loose_Stirling_Area = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				consumer_goods_factor = -0.01
				
				}
			}
		SAF_Britain_Shadow = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				political_power_gain = 0.05
				consumer_goods_factor = -0.02
				industrial_capacity_dockyard = 0.10
			}
		}
		SAF_American_Cars = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				 production_speed_buildings_factor  = 0.10
			}
		}
		SAF_Work_With_Madagascar_Spirit = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				 consumer_goods_factor = -0.03
			}
		}
		SAF_French_Officers = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				 experience_gain_army = 0.05
			}
		}
		SAF_Foreign_Merchants = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				 consumer_goods_factor = -0.02
			}
		}
		SAF_Moving_Away_Britain_Mild = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = -0.05
				political_power_gain = -0.10
				consumer_goods_factor = 0.05
			}
		}
		SAF_Independent_Dem_SAF = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				consumer_goods_factor = -0.05
				production_speed_buildings_factor  = 0.05
			}
		}
		SAF_Policing_Actions_Idea = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = 0.003
				foreign_subversive_activites = -0.10
			}
		}
		SAF_Defensive_Plans_Idea = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				defence = 0.025
				army_core_defence_factor = 0.05
				ai_focus_defense_factor = 0.5
			}
		}
		SAF_Reform_Commandos_Idea = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				army_org_Factor = 0.05
				army_morale_factor = 0.05
			}
		}
		SAF_War_College_Idea = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				experience_gain_army = 0.05
				army_org_Factor = 0.05
				planning_speed = 0.10
			}
		}
		SAF_Necessary_Force_Idea = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				defence = 0.05
				army_core_defence_factor = 0.10
				ai_focus_defense_factor = 0.5
				stability_factor = 0.003
				foreign_subversive_activites = -0.15
			}
		}
		SAF_Proud_Force_Idea = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				experience_gain_army = 0.10
				army_org_Factor = 0.10
				planning_speed = 0.15
				army_morale_factor = 0.10
			}
		}
		SAF_British_Naval_Schools_Idea = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				experience_gain_navy = 0.05
				consumer_goods_factor = 0.03
			}
		}
		SAF_Expanded_Naval_Service = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				convoy_escort_efficiency = 0.15
				naval_coordination = 0.05
				spotting_chance = 0.1
				navy_max_range_factor = 0.15
			}
		}
		SAF_Civilian_Focused_Navy = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				experience_gain_navy = -0.05
				consumer_goods_factor = -0.03
			}
		}
		SAF_Coastal_Patrol_Fleet = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				navy_submarine_detection_factor = 0.05
				naval_speed_factor = -0.05
			}
		}
		
		SAF_Subsidized_White_Farming = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = 0.003
				consumer_goods_factor = 0.05
			}
		}
		SAF_Ended_Subsidies = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				consumer_goods_factor = -0.03
			}
		}
		SAF_Agri_Harsh = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				consumer_goods_factor = 0.15
				research_speed_factor = 0.15
				production_factory_max_efficiency_factor = -0.20
				production_speed_buildings_factor = -0.15
				industrial_capacity_factory = -0.15
				industrial_capacity_dockyard = -0.15
				stability_factor = 0.10
			}
		}
		SAF_Agri_Med = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				consumer_goods_factor = 0.10
				research_speed_factor = 0.15
				production_factory_max_efficiency_factor = -0.15
				production_speed_buildings_factor = -0.10
				industrial_capacity_factory = -0.15
				industrial_capacity_dockyard = -0.10
				stability_factor = 0.05
			}
		}
		SAF_Expanding_Infrastructure = {
			picture = generic_build_infrastructure
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				production_speed_infrastructure_factor = 0.20
			}
		}
		SAF_Expanding_Mining = {
			picture = generic_exploit_mines
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				consumer_goods_factor = 0.05
				production_speed_buildings_factor = 0.20
			}
		}
		SAF_Investment_Scheme = {
			picture = RUS_capitalism
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				consumer_goods_factor = 0.05
				production_speed_buildings_factor = 0.20
				industrial_capacity_factory = 0.10
				industrial_capacity_dockyard = 0.10
			}
		}
		SAF_African_Campaign = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			targeted_modifier = {
				tag = ITA
				attack_bonus_against = 0.15
				defense_bonus_against = 0.15
			}
		}
		SAF_Defending_Mediterranean = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				defence = 0.15
				dig_in_speed_factor = 0.25
				out_of_supply_factor = -0.25
			}
		}
		SAF_Armored_Warfare = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				army_armor_speed_factor = 0.1
				army_armor_attack_factor = 0.1
				army_armor_defence_factor = 0.1
			}
		}
		SAF_Improved_Planning = {
		allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				planning_speed = 0.1
				max_planning = 0.1
			}
		}
		SAF_Protecting_Convoys = {
			allowed = {
				always = no
			}
			
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				convoy_escort_efficiency = 0.20
			}
		}
		
	} 
		
	#HoG
	head_of_gov = {
		SAF_hog_JBM_Hertzog = {
			allowed = {
				original_tag = SAF
			}
			available = {
				always = yes
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				conservative_minister
				old_general
				hog
			}
		}
	}
	head_of_gov = {
		SAF_hog_Jan_Smuts = {
			allowed = {
				original_tag = SAF
			}
			available = {
				always = yes
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				jacobin_minister
				minister_of_everything
				hog
			}
		}
	}
	#Foreign
	foreign_minister = {
		SAF_frn_JBM_Hertzog = {
			allowed = {
				original_tag = SAF
			}
			available = {
				OR = {
					has_government = social_liberalism
					has_government = market_liberalism
					has_government = jacobin
					has_government = democratic
					has_government = neutrality
				}
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				conservative_minister
				biased_intellectual
				frnmin
			}
		}
	}
	foreign_minister = {
		SAF_frn_Jan_Smuts = {
			allowed = {
				original_tag = SAF
			}
			available = {
				OR = {
					has_government = social_liberalism
					has_government = market_liberalism
					has_government = jacobin
					has_government = democratic
					has_government = neutrality
					has_government = fascism
				}
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				jacobin_minister
				competent_ambassador
				frnmin
			}
		}
	}
	#Economics
	economy_minister = {
		SAF_eco_Nicolaas_Havenga = {
			allowed = {
				original_tag = SAF
			}
			available = {
				OR = {
					has_government = social_liberalism
					has_government = market_liberalism
					has_government = jacobin
					has_government = democratic
					has_government = neutrality
					has_government = monarchism
					has_government = fascism
				}
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				conservative_minister
				mixed_economy
				ecomin
			}
		}
	}
	economy_minister = {
		SAF_eco_Jan_Smuts = {
			allowed = {
				original_tag = SAF
			}
			available = {
				OR = {
					has_government = social_liberalism
					has_government = market_liberalism
					has_government = jacobin
					has_government = democratic
					has_government = neutrality
				}
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				jacobin_minister
				balanced_budget
				ecomin
			}
		}
	}
	economy_minister = {
		SAF_eco_Jan_Hofmeyer = {
			allowed = {
				original_tag = SAF
			}
			available = {
				OR = {
					has_government = social_liberalism
					has_government = market_liberalism
					has_government = jacobin
					has_government = democratic
					has_government = neutrality
				}
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				liberal_minister
				mobilizer
				ecomin
			}
		}
	}
	#Interior
	security_minister = {
		SAF_sec_DF_Malan = {
			allowed = {
				original_tag = SAF
			}
			available = {
				OR = {
					has_government = social_liberalism
					has_government = market_liberalism
					has_government = jacobin
					has_government = democratic
					has_government = neutrality
					has_government = monarchism
					has_government = fascism
				}
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				nationalist_minister
				opportunist_secmin
				secmin
			}
		}
	}
	security_minister = {
		SAF_sec_Jan_Smuts = {
			allowed = {
				original_tag = SAF
			}
			available = {
				OR = {
					has_government = social_liberalism
					has_government = market_liberalism
					has_government = jacobin
					has_government = democratic
					has_government = neutrality
				}
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				jacobin_minister
				compassionate_gentleman
				secmin
			}
		}
	}
	security_minister = {
		SAF_sec_Jan_Hofmeyer = {
			allowed = {
				original_tag = SAF
			}
			available = {
				OR = {
					has_government = social_liberalism
					has_government = market_liberalism
					has_government = jacobin
					has_government = democratic
					has_government = neutrality
				}
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				liberal_minister
				man_of_the_people
				secmin
			}
		}
	}
	security_minister = {
		SAF_sec_Harry_Lawrence = {
			allowed = {
				original_tag = SAF
			}
			available = {
				OR = {
					has_government = social_liberalism
					has_government = market_liberalism
					has_government = jacobin
					has_government = democratic
					has_government = neutrality
				}
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				liberal_minister
				silent_lawyer
				secmin
			}
		}
	}
	#Intel
	intel_minister = {
		SAF_int_Oswald_Pirow = {
			allowed = {
				original_tag = SAF
			}
			available = {
				OR = {
					has_government = social_democracy
					has_government = social_liberalism
					has_government = market_liberalism
					has_government = jacobin
					has_government = democratic
					has_government = neutrality
					has_government = monarchism
					has_government = fascism
				}
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				nationalist_minister
				military_specialist
				intmin
			}
		}
	}
	intel_minister = {
		SAF_int_Jan_Smuts = {
			allowed = {
				original_tag = SAF
			}
			available = {
				OR = {
					has_government = social_democracy
					has_government = social_liberalism
					has_government = market_liberalism
					has_government = jacobin
					has_government = democratic
					has_government = neutrality
					has_government = monarchism
				}
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				jacobin_minister
				political_specialist
				intmin
			}
		}
	}
	army_chief = {

		army_jan_smuts = {
			allowed = {
				original_tag = SAF
			}
			ledger = army
			traits = { doctrine_of_autonomy }			
			ai_will_do = {
				factor = 1
			}
		}

		army_oswald_pirow  = {
			allowed = {
				original_tag = SAF
			}
			
			traits = { decisive_battle_army }
			
			ai_will_do = {
				factor = 1
			}
		}
	}
	navy_chief = {
		con_Guy_Halifax = {
			allowed = {
				original_tag = SAF
			}
			available = {
			}
			ai_will_do = {
				factor = 1
			}
			traits = {
				navigator
			}
		}
	}
	air_chief = {
		coaf_Pierre_van_Ryneveld = {
			allowed = {
				original_tag = SAF
			}
			available = {
			}
			ai_will_do = {
				factor = 1
			}
			traits = {
				political_aviator
			}
		}
	}
	high_command = {
		hc_jan_smuts = {
			allowed = {
				original_tag = SAF
			}
			ledger = army
			traits = { school_of_manoeuvre }			
			ai_will_do = {
				factor = 1
			}
		}
	}
	tank_manufacturer = {
		iscor = {
			allowed = {
				original_tag = SAF
			}
			picture = generic_tank_manufacturer_3
			
			research_bonus = {
				armor = 0.15
			}
			
			traits = { medium_tank_manufacturer }
			
			modifier = {
			}
		}
	}
}
