ideas = {

	###companies###
	industrial_concern = {
		
		ztt = {
			picture = "GFX_idea_ztt"
			allowed = { original_tag = POL }
			traits = { electronics_manufacturer }
			research_bonus = {
				electronics = 0.05 
			}
		}
		
		tsfriz = {
			picture = "GFX_idea_tsfriz"
			allowed = { original_tag = POL }
			traits = { industrial_manufacturer }
			research_bonus = {
				industry = 0.10 
			}
		}

		belma = {
			picture = "GFX_idea_belma"
			cost = 200
			allowed = {
				original_tag = POL
			}
			traits = {
				railroad_manufacturer
			}
			research_bonus = {
				electronics  = 0.05
			}
		}

		hsw = {
			picture = "GFX_idea_hsw"
			allowed = {
				original_tag = POL
			}
			traits = {
				railroad_manufacturer2
			}
			research_bonus = {
				industry = 0.10
			}
		}
		pfza = {
			picture = "GFX_"
			allowed = {
				original_tag = POL
			}
			research_bonus = {
				synth_resources = 0.05
				industry = 0.10
			}
			traits = { chemical_manufacturer }
		}
	}

	#weapons
	material_manufacturer = {

		pocisk = {
			picture = "GFX_idea_pocisk"
			allowed = {
				original_tag = POL
			}
			traits = {
				infantry_equipment_manufacturer2
			}
			research_bonus = {
				infantry_weapons = 0.1
			}	
		}

		pwu = {
			picture = "GFX_idea_pwu"
			allowed = {
				original_tag = POL
			}
			traits = {
				infantry_equipment_manufacturer 
			}
			research_bonus = {
				infantry_weapons = 0.1
			}
		}
	}

	naval_manufacturer = {
		pzl2 = {
			allowed = {
				OR = {
					original_tag = POL
				}
			}
				
			research_bonus = {
				air_equipment = 0.10
			}
				
			traits = { light_aircraft_manufacturer3 }
			ledger = air
				
			ai_will_do = {
				factor = 1
			}
		}

		dwl = {	
			allowed = {
				original_tag = POL
			}
				
			research_bonus = {
				air_equipment = 0.10
			}
			ledger = air
			traits = { light_aircraft_manufacturer }
				
			ai_will_do = {
				factor = 1
			}
		}
		sgdynia = {
			allowed = {
				original_tag = POL
			}
			available = {
				owns_state = 1060
			}
				
			research_bonus = {
				naval_equipment = 0.10
			}
			ledger = navy
			traits = { screen_ship_manufacturer }
				
			ai_will_do = {
				factor = 1
			}
		}
		sgdansk = {
			allowed = {
				original_tag = POL
			}
			available = {
				OR = {
					AND = {	
						DZG = { 
							owns_state = 773 
							is_puppet_of = POL 
						}
					}
					owns_state = 773
				}
			}
			ledger = navy
			research_bonus = {
				naval_equipment = 0.10
			}
				
			traits = { submarine_manufacturer }
				
			ai_will_do = {
				factor = 1
			}
		}
	}

	tank_manufacturer = {
		designer = yes

		ursus = {
			picture = "GFX_idea_ursus"
			allowed = {
				original_tag = POL
			}
			traits = {
				fast_tank_manufacturer2 
			}
			research_bonus = {
				armor = 0.10
			}
		}
		ursus2 = {
			picture = "GFX_idea_ursus2"
			allowed = {
				original_tag = POL
			}
			traits = {
				motorized_manufacturer2 
			}
			research_bonus = {
				motorized_equipment = 0.10
			}
		}
	}
	country = {

		POL_poland_aib = {
			picture = POL_poland_aib
			modifier = {
				production_speed_infrastructure_factor = -0.1
				consumer_goods_factor = 0.1
				local_resources_factor = -0.05
				production_factory_max_efficiency_factor = -0.1
				production_speed_industrial_complex_factor = -0.05
				production_speed_arms_factory_factor = -0.05
				research_speed_factor = -0.1
			}
		}
		POL_poland_aib2 = {
			picture = POL_poland_aib
			modifier = {
				production_speed_infrastructure_factor = -0.05
				consumer_goods_factor = 0.05
				local_resources_factor = -0.025
				production_factory_max_efficiency_factor = -0.05
				production_speed_industrial_complex_factor = -0.025
				production_speed_arms_factory_factor = -0.025
				research_speed_factor = -0.025
			}
		}
			
		POL_total_opposition = {
			picture = generic_democratic_drift_bonus
			modifier = {
				political_power_gain = -0.2
				stability_factor = -0.05
			}
		}
		
		POL_great_crisis = {
			picture = FRA_factory_strikes
			modifier = {
				production_speed_buildings_factor = -0.1
				consumer_goods_factor = 0.2
				stability_factor = -0.15
				industrial_capacity_factory = -0.2
				production_factory_max_efficiency_factor = -0.05
				production_factory_efficiency_gain_factor = -0.1
			}
		}
		POL_great_crisis2 = {
			picture = FRA_factory_strikes
			modifier = {
				production_speed_buildings_factor = -0.08
				consumer_goods_factor = 0.2
				stability_factor = -0.10
				industrial_capacity_factory = -0.2
				production_factory_max_efficiency_factor = -0.05
				production_factory_efficiency_gain_factor = -0.08
			}
		}
		POL_great_crisis3 = {
			picture = FRA_factory_strikes
			modifier = {
				production_speed_buildings_factor = -0.05
				consumer_goods_factor = 0.15
				stability_factor = -0.10
				industrial_capacity_factory = -0.15
				production_factory_max_efficiency_factor = -0.04
				production_factory_efficiency_gain_factor = -0.05
			}
		}
		POL_great_crisis4 = {
			picture = FRA_factory_strikes
			modifier = {
				production_speed_buildings_factor = -0.05
				consumer_goods_factor = 0.13
				stability_factor = -0.1
				industrial_capacity_factory = -0.1
				production_factory_max_efficiency_factor = -0.04
				production_factory_efficiency_gain_factor = -0.05
			}
		}
		POL_great_crisis5 = {
			picture = FRA_factory_strikes
			modifier = {
				production_speed_buildings_factor = -0.05
				consumer_goods_factor = 0.1
				stability_factor = -0.08
				industrial_capacity_factory = -0.1
				production_factory_max_efficiency_factor = -0.05
			}
		}
		POL_great_crisis6 = {
			picture = FRA_factory_strikes
			modifier = {
				production_speed_buildings_factor = -0.02
				consumer_goods_factor = 0.05
				stability_factor = -0.05
				industrial_capacity_factory = -0.05
				production_factory_max_efficiency_factor = -0.05
			}
		}
		POL_great_crisis7 = {
			picture = FRA_factory_strikes
			modifier = {
				production_speed_buildings_factor = -0.02
				consumer_goods_factor = 0.05
				stability_factor = -0.03
			}
		}
			
		POL_bbwr_crumbling = {
			picture = POL_bbwr
			modifier = {
				stability_factor = -0.1
				political_power_gain = -0.1
			}
		}
			
		POL_underfunded_military = {
			picture = underfunded_military
			modifier = {
				research_speed_factor = -0.1
				production_factory_max_efficiency_factor = -0.1
				consumer_goods_factor = 0.05
			}
		}
		POL_underfunded_military2 = {
			picture = underfunded_military
			modifier = {
				research_speed_factor = -0.1
				production_factory_max_efficiency_factor = -0.05
				consumer_goods_factor = 0.03
			}
		}
		POL_underfunded_military3 = {
			picture = underfunded_military
			modifier = {
				research_speed_factor = -0.05
			}
		}
		POL_educated = {
			picture = generic_research_bonus
			modifier = {
				research_speed_factor = 0.02
			}
		}
		POL_ukrainian_nationalism = {
			picture = POL_Ukranian_Nationalists
			modifier = {
				stability_factor = -0.1
				foreign_subversive_activites = 0.05
			}
		}
		POL_communist_threat = {
			picture = POL_Communist_Threat
			modifier = {
				stability_factor = -0.025
				war_support_factor = 0.05
				foreign_subversive_activites = 0.05
			}
		}
		POL_nationalist_universities = {
			picture = generic_research_bonus
			modifier = {
				stability_factor = -0.05
				research_speed_factor = -0.02
				foreign_subversive_activites = 0.05
			}
		}
		POL_state_universities = {
			picture = generic_research_bonus
			modifier = {
				research_speed_factor = -0.02
				foreign_subversive_activites = -0.05
			}
		}
		POL_march_constitution = {
			picture = generic_pp_unity_bonus
			modifier = {
				political_power_gain = -0.08
			}
		}
		POL_april_thesis = {
			picture = generic_political_support
			modifier = {
				political_power_gain = 0.05
				stability_factor = -0.05
			}
		}
		POL_april_constitution_idea = {
			picture = generic_political_support
			modifier = {
				political_power_gain = 0.1
			}
		}
		POL_state_planning = {
			picture = generic_goods_red_bonus 
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.05
				consumer_goods_factor = -0.025
				line_change_production_efficiency_factor = 0.10
				production_speed_buildings_factor = 0.1
			}
		}
		POL_state_planning2 = {
			picture = generic_goods_red_bonus  
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.10
				consumer_goods_factor = -0.06
				line_change_production_efficiency_factor = 0.15
				production_speed_buildings_factor = 0.05
			}
		}
		POL_state_planning3 = {
			picture = generic_goods_red_bonus 
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.05
				consumer_goods_factor = -0.05
				line_change_production_efficiency_factor = 0.15
				production_speed_buildings_factor = 0.05
			}
		}
		POL_labour_inspectorate = {
			picture = generic_production_bonus 
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = 0.05
				stability_factor = 0.025
			}
		}
		POL_labour_inspectorate2 = {
			picture = generic_production_bonus 
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = 0.06
				production_factory_max_efficiency_factor = 0.05
				consumer_goods_factor = 0.01
				stability_factor = 0.03
			}
		}
		POL_interministerial = {
			picture = neutral_foreign_policy
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.05
				consumer_goods_factor = -0.01
				stability_factor = -0.05
			}
		}
		POL_interministerial2 = {
			picture = neutral_foreign_policy
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.05
				consumer_goods_factor = -0.05
				stability_factor = -0.05
			}
		}
		POL_interministerial3 = {
			picture = neutral_foreign_policy
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.02
				consumer_goods_factor = -0.05
				stability_factor = -0.05
			}
		}
		POL_interministerial4 = {
			picture = neutral_foreign_policy
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.02
				consumer_goods_factor = -0.05
			}
		}
		landowner_angry = {
			picture = FRA_matignon_agreements
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.02
				stability_factor = -0.04
				war_support_factor = -0.05
			}
		}
		recently_loaned = {
			picture = generic_political_support
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = 0.03
				consumer_goods_factor = 0.03
				production_factory_efficiency_gain_factor = 0.05
			}
		}
		POL_agricultural_reform_idea = {
			picture = generic_pp_unity_bonus
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = -0.02
				stability_factor = 0.025
			}
		}
		POL_increased_work_hours = {
			picture = generic_production_bonus
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				stability_factor = -0.05
				war_support_factor = -0.05
				production_factory_efficiency_gain_factor = 0.05
				industrial_capacity_factory = 0.02
			}
		}
			POL_increased_work_hours2 = {
			picture = generic_production_bonus
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				stability_factor = -0.1
				war_support_factor = -0.1
				production_factory_efficiency_gain_factor = 0.1
				industrial_capacity_factory = 0.05
				}
			}
		POL_taxes_raised = {
			picture = generic_political_support	
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = 0.10
				consumer_goods_factor = -0.05
				stability_factor = -0.10
			}
		}
		POL_lower_taxes = {
			picture = generic_political_support	
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.05
				production_speed_buildings_factor = 0.1
			}
		}
		POL_unemployed_fund_idea = {
			picture = generic_pp_unity_bonus	
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.05
				stability_factor = 0.1
				political_power_gain = -0.05
				production_speed_buildings_factor = 0.05
			}
		}
		POL_privatized_healthcare = {
			picture = the_great_depression
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = -0.05
				MONTHLY_POPULATION = -0.05
			}
		}
		POL_cut_budget = {
			picture = the_great_depression
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = 0.10
				consumer_goods_factor = 0.05
			}
		}
		POL_cut_budget2 = {
			picture = the_great_depression
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = 0.05
				consumer_goods_factor = -0.05
				stability_factor = -0.05
			}
		}
		POL_issued_gov_bonds = {
			picture = generic_political_support
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				production_speed_industrial_complex_factor = 0.05
				production_speed_arms_factory_factor = 0.05
				political_power_gain = -0.05
			}
		}
		POL_issued_gov_bonds2 = {
			picture = generic_political_support
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				production_speed_industrial_complex_factor = 0.1
				production_speed_arms_factory_factor = 0.1
				political_power_gain = -0.1
			}
		}
		POL_deleveraged = {
			picture = generic_political_support
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = 0.05
				production_factory_efficiency_gain_factor = 0.1
				consumer_goods_factor = 0.05
				production_speed_buildings_factor = 0.05
			}
		}
		POL_deleveraged2 = {
			picture = generic_political_support
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = 0.05
				production_factory_efficiency_gain_factor = 0.1
				consumer_goods_factor = 0.05
				production_speed_buildings_factor = 0.05
			}
		}
		POL_deleveraged3 = {
			picture = generic_political_support
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = 0.05
				production_factory_efficiency_gain_factor = 0.1
				consumer_goods_factor = 0.05
				production_speed_buildings_factor = 0.05
			}
		}
		POL_deleveraged4 = {
			picture = generic_political_support
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = 0.05
				production_factory_efficiency_gain_factor = 0.1
				consumer_goods_factor = 0.05
				production_speed_buildings_factor = 0.05
			}
		}
		POL_deleveraged_us = {
			picture = generic_political_support
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = -0.05
				production_speed_buildings_factor = 0.05
				industrial_capacity_factory = 0.05
			}
		}
		POL_increased_public_investment = {
			picture = generic_political_support
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.05
				production_speed_buildings_factor = 0.05
				industrial_capacity_factory = 0.05
				production_factory_efficiency_gain_factor = 0.05
			}
		}
		POL_mantain_payment = {
			picture = generic_political_support
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.1
				industrial_capacity_factory = -0.05
			}
		}
		POL_loan_balance = {
			picture = neutral_foreign_policy
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.05
				production_factory_efficiency_gain_factor = -0.05
			}
		}
		POL_welfare_lowered = {	
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = -0.1
				stability_factor = -0.05
				war_support_factor = -0.05
				production_factory_efficiency_gain_factor = -0.05
				MONTHLY_POPULATION = -0.05
			}
		}
		POL_agricultural_industrial_balance = {	
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = -0.05
				stability_factor = 0.05
				war_support_factor = 0.05
				production_factory_efficiency_gain_factor = 0.05
			}
		}
		POL_no_barriers = {	
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				production_factory_efficiency_gain_factor = 0.05
				local_resources_factor = 0.05
				min_export = 0.05
				industrial_capacity_factory = 0.05
				production_speed_buildings_factor = 0.05
			}
		}
		POL_savings_found = {		
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				production_factory_efficiency_gain_factor = -0.05			
				consumer_goods_factor = -0.05
				research_speed_factor = -0.05
			}
		}
		POL_protectionism = {	
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {			
				consumer_goods_factor = -0.05
				research_speed_factor = -0.05
				local_resources_factor = 0.05
			}
		}
		POL_protectionism2 = {		
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {			
				consumer_goods_factor = -0.05
				research_speed_factor = -0.05
				local_resources_factor = 0.05
				production_speed_buildings_factor = 0.05
			}
		}
		POL_protectionism3 = {	
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {			
				consumer_goods_factor = -0.05
				research_speed_factor = -0.05
				local_resources_factor = 0.05
				production_speed_buildings_factor = 0.05
				industrial_capacity_factory = 0.05
			}
		}
		POL_dollar_decree_idea = {	
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {			
				war_support_factor = 0.05
				stability_factor = 0.05
				political_power_gain = 0.05
				industrial_capacity_factory = 0.05
			}
		}
		POL_solidarism_idea = {
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {	
				political_power_gain = 0.05
				stability_factor = 0.1
			}
		}
		POL_sanation_united = {
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {	
				political_power_gain = 0.1
				stability_factor = 0.05
			}
		}
		#########
		POL_state_capitalism = {
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = 0.05
				stability_factor = 0.05
				consumer_goods_factor = 0.03
				production_factory_max_efficiency_factor = 0.05
				production_factory_start_efficiency_factor = 0.05
				production_speed_buildings_factor = 0.05
			}
		}
		POL_enlightened_autarky = {
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {	
				research_speed_factor = -0.025
				local_resources_factor = 0.05
				line_change_production_efficiency_factor = 0.05
				global_building_slots_factor = 0.1
				production_speed_arms_factory_factor = 0.05
				production_speed_industrial_complex_factor = 0.05
			}
		}
		POL_sufficiency = {
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				research_speed_factor = -0.025
				local_resources_factor = 0.05
				line_change_production_efficiency_factor = 0.05
				production_speed_infrastructure_factor = 0.05
				industry_repair_factor = 0.1
				industrial_capacity_factory = 0.05
				production_speed_synthetic_refinery_factor = 0.05
			}
		}
		POL_countryside_cities = {
			allowed = {
				always = no
			}
			removal_cost = -1
			modifier = {
				MONTHLY_POPULATION = 0.05
				production_factory_efficiency_gain_factor = 0.05
				production_speed_buildings_factor = 0.1
				industrial_capacity_factory = 0.05
				stability_factor = -0.05
			}
		}
	}
}