ideas = {
	head_of_gov = {
		Carlos_Hernandez = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				gentleman_politician
				liberal_minister
				hog
			}
		}
		Machado = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				autocratic_charmer
				liberal_minister
				hog
			}
		}
		Manuel = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				jacobin_minister
				insignificant_layman
				hog
			}
		}
		Guiteras = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				die_hard_reformer
				authoritarian_socialist_minister
				hog
			}
		}
	    Grau = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				reformer
				nationalist_minister
				hog
			}
		}
		Batista = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				pragmatic_statesman
				conservative_minister
				hog
			}
		}
		Roca = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				pragmatic_statesman
				communism_minister
				hog
			}
		}
		Mendieta = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				pragmatic_statesman
				conservative_minister
				hog
			}
		}
		Barnet = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				pragmatic_statesman
				conservative_minister
				hog
			}
		}
		Laredo = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				pragmatic_statesman
				conservative_minister
				hog
			}
		} 
		Vizcaino = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				die_hard_reformer
				authoritarian_socialist_minister
				hog
			}
		} 
	}
	foreign_minister = {
		
		de_Armas = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
				}
			traits = {
				biased_intellectual
				authoritarian_socialist_minister 
				frnmin
			}	
		}
		Sterling = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
				}
			traits = {
				biased_intellectual
				jacobin_minister
				frnmin
			}
		}
	Guell = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
				}
			traits = {
				biased_intellectual
				conservative_minister 
				frnmin
			}
		}
	}
	economy_minister = {
		Miguel = {
		allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				planned_economy
				authoritarian_socialist_minister
				ecomin
			}
		}
		Despaigne = {
		allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				mixed_economy
				conservative_minister
				ecomin
			}
		}
		Martinez = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				laissez_faire_capitalist
				conservative_minister
				ecomin
			}
		}
		Herrera_Arango = {
			allowed = {
			original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				laissez_faire_capitalist
				conservative_minister
				ecomin
			}
		} 
	}
	security_minister = {
		Castillo  = {
			allowed = {
				original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				compassionate_gentleman
				conservative_minister
				secmin
			}
		}
		Aguado = {
			allowed = {
				original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				compassionate_gentleman
				nationalist_minister
				secmin
			}
		}
		Penichet = {
			allowed = {
				original_tag = CUB
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				compassionate_gentleman
				authoritarian_socialist_minister
				secmin
			}
		}
	} 
}