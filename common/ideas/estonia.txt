ideas = {
	country = {
		EST_Great_Depression = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = -0.10
				industrial_capacity_factory = -0.20
                industrial_capacity_dockyard = -0.20
                production_speed_buildings_factor  = -0.15
				consumer_goods_factor = 0.075
			}
		}
		EST_Vaps_Movement = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = -0.15
				political_power_gain = -0.15
			}
		}
	}
	head_of_gov = {
		EST_hog_Konstantin_Pats = {
			allowed = {
				original_tag = EST
				NOT = { has_country_flag = EST_pats_incapable }
			}
			picture = no_minister
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				centre_minister
				autocratic_charmer
				hog
			}
		}
	}
	foreign_minister = {
		EST_frn_August_Rei = {
			allowed = {
				original_tag = EST
				NOT = { has_country_flag = EST_Rei_incapable }
			}
			picture = no_minister
			available = {
				NOT = { has_country_flag = EST_Rei_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = social_liberalism
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				socialist_minister
				biased_intellectual
				frnmin
			}
		}
	}
	economy_minister = {
		EST_eco_August_Jurman = {
			allowed = {
				original_tag = EST
				NOT = { has_country_flag = EST_Jurman_incapable }
			}
			picture = no_minister
			available = {
				NOT = { has_country_flag = EST_Jurman_incapable }
				OR = {
					has_government = socialism
					has_government = social_democracy
					has_government = social_liberalism
					has_government = market_liberalism
					has_government = jacobin
					has_government = democratic
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				centre_minister
				bank_president
				ecomin
			}
		}
	}
	security_minister = {
		EST_sec_Ado_Anderkopp = {
			allowed = {
				original_tag = EST
				NOT = { has_country_flag = EST_Anderkopp_incapable }
			}
			picture = no_minister
			available = {
				NOT = { has_country_flag = EST_Anderkopp_incapable }
				OR = {
					has_government = social_democracy
					has_government = social_liberalism
					has_government = market_liberalism
					has_government = jacobin
					has_government = democratic
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				jacobin_minister
				dutiful_cleric
				secmin
			}
		}
	}
	intel_minister = {
		EST_int_Arnold_Sinka = {
			allowed = {
				original_tag = EST
				NOT = { has_country_flag = EST_Sinka_incapable }
			}
			picture = no_minister
			available = {
				NOT = { has_country_flag = EST_Sinka_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = social_liberalism
					has_government = market_liberalism
					has_government = jacobin
					has_government = democratic
					has_government = monarchism
					has_government = nationalism
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				centre_minister
				military_specialist
				intmin
			}
		}
	}
}