ideas = {   
	country = {
	    
		### National Unity Government until 1936 ###
		
		JAP_showa_crisis = {
			picture = JAP_showa_economic_crisis
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.05
				production_factory_max_efficiency_factor = -0.10
				production_speed_buildings_factor = -0.10
				political_power_gain = -0.10
				industrial_capacity_factory = -0.05
				industrial_capacity_dockyard = -0.05
			}
		}		
		JAP_showa_crisis_A = {
			picture = JAP_showa_economic_crisis
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.03 #-0.02
				production_factory_max_efficiency_factor = -0.10
				production_speed_buildings_factor = -0.10
				political_power_gain = -0.07 #+0.03
				industrial_capacity_factory = -0.05
				industrial_capacity_dockyard = -0.05
			}
		}		
		JAP_showa_crisis_B = {
			picture = JAP_showa_economic_crisis
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.05
				production_factory_max_efficiency_factor = -0.08 #+0.02
				production_speed_buildings_factor = -0.09 #+0.01
				political_power_gain = -0.10
				industrial_capacity_factory = -0.04 #+0.01
				industrial_capacity_dockyard = -0.04 #+0.01
			}
		}		
		JAP_showa_crisis_AB = {
			picture = JAP_showa_economic_crisis
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.03
				production_factory_max_efficiency_factor = -0.08
				production_speed_buildings_factor = -0.09
				political_power_gain = -0.07
				industrial_capacity_factory = -0.04
				industrial_capacity_dockyard = -0.04
			}	
		}		
		JAP_showa_crisis_C = {
			picture = JAP_showa_economic_crisis
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.03
				production_factory_max_efficiency_factor = -0.05 #+0.03
				production_speed_buildings_factor = -0.07 #+0.02
				political_power_gain = -0.07
				industrial_capacity_factory = -0.03 #+0.01
				industrial_capacity_dockyard = -0.03 #0.01
			}	
		}		
		JAP_showa_crisis_D = {
			picture = JAP_showa_economic_crisis
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.03
				production_factory_max_efficiency_factor = -0.08
				production_speed_buildings_factor = -0.09
				political_power_gain = -0.04 #+0.03
				industrial_capacity_factory = -0.04
				industrial_capacity_dockyard = -0.04
			}	
		}		
		JAP_showa_crisis_E = {
			picture = JAP_showa_economic_crisis
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.02 #-0.01
				production_factory_max_efficiency_factor = -0.06 #+0.02
				production_speed_buildings_factor = -0.06 #+0.03
				political_power_gain = -0.06 #+0.01
				industrial_capacity_factory = -0.03 #+0.01
				industrial_capacity_dockyard = -0.03 #+0.01
			}	
		}		
		JAP_showa_crisis_CD = {
			picture = JAP_showa_economic_crisis
			allowed = {
				always = no
			}	
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.03
				production_factory_max_efficiency_factor = -0.05
				production_speed_buildings_factor = -0.07
				political_power_gain = -0.04
				industrial_capacity_factory = -0.03
				industrial_capacity_dockyard = -0.03
			}	
		}		
		JAP_showa_crisis_DE = {
			picture = JAP_showa_economic_crisis
			allowed = {
				always = no
			}	
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.02
				production_factory_max_efficiency_factor = -0.06
				production_speed_buildings_factor = -0.06
				political_power_gain = -0.03
				industrial_capacity_factory = -0.03
				industrial_capacity_dockyard = -0.03
			}	
		}		
		JAP_showa_crisis_CE = {
			picture = JAP_showa_economic_crisis
			allowed = {
				always = no
			}	
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.02
				production_factory_max_efficiency_factor = -0.03
				production_speed_buildings_factor = -0.04
				political_power_gain = -0.06
				industrial_capacity_factory = -0.02
				industrial_capacity_dockyard = -0.02
			}
		}			
		JAP_showa_crisis_CDE = {
			picture = JAP_showa_economic_crisis
			allowed = {
				always = no
			}		
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				consumer_goods_factor = 0.02
				production_factory_max_efficiency_factor = -0.03
				production_speed_buildings_factor = -0.04
				political_power_gain = -0.03
				industrial_capacity_factory = -0.02
				industrial_capacity_dockyard = -0.02
			}
		}		
		JAP_fading_democracy = {
			picture = fading_democracy
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.15
				stability_factor = -0.10
			}
		}		
		JAP_fading_democracy_A = {
			picture = JAP_fading_democracy
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.10
				stability_factor = -0.08
			}
		}				
		JAP_fading_democracy_B = {
			picture = JAP_fading_democracy
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.07
				stability_factor = -0.07
			}
		}				
		JAP_fading_democracy_C = {
			picture = JAP_fading_democracy
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.05
				stability_factor = -0.05
			}
		}		
		JAP_social_anxiety = {
			picture = JAP_social_anxiety
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.10
				stability_factor = -0.15
				war_support_factor = -0.20
			}
		}		
		JAP_social_anxiety_A = {
			picture = JAP_social_anxiety
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.08
				stability_factor = -0.12
				war_support_factor = -0.18
			}
		}		
		JAP_social_anxiety_AB = {
			picture = JAP_social_anxiety

			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.06 #+0.02
				stability_factor = -0.11 #+0.01
				war_support_factor = -0.17 #+0.01
			}
		}		
		JAP_social_anxiety_AC = {
			picture = JAP_social_anxiety
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.07
				stability_factor = -0.11
				war_support_factor = -0.16
			}
		}		
		JAP_social_anxiety_ABC = {
			picture = JAP_social_anxiety
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.05
				stability_factor = -0.10
				war_support_factor = -0.15
			}
		}
		JAP_social_anxiety_D = {
			picture = JAP_social_anxiety
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.02 #+0.03
				stability_factor = -0.08 #+0.02
				war_support_factor = -0.13 #+0.02
			}
		}
		JAP_social_anxiety_E = {
			picture = JAP_social_anxiety
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.03 #+0.02
				stability_factor = -0.07 #+0.03
				war_support_factor = -0.12 #+0.03
			}
		}
		JAP_social_anxiety_DE = {
			picture = JAP_social_anxiety
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				stability_factor = -0.05
				war_support_factor = -0.10
			}
		}
		JAP_social_anxiety_F = {
			picture = JAP_social_anxiety
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				stability_factor = -0.02 #+0.03
				war_support_factor = -0.05 #+0.05
			}
		}
		JAP_social_anxiety_G = {
			picture = JAP_social_anxiety
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				stability_factor = -0.03 #+0.02
				war_support_factor = -0.05 #+0.05
			}
		}
		JAP_rising_militarism = {
			picture = rising_militarism
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.15
				stability_factor = -0.20
				war_support_factor = 0.10
			}
		}		
		JAP_rising_militarism_A = {
			picture = JAP_rising_militarism
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.10
				stability_factor = -0.17
				war_support_factor = 0.08
			}
		}				
		JAP_rising_militarism_AB = {
			picture = JAP_rising_militarism
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.08 #+0.03
				stability_factor = -0.14 #+0.03
				war_support_factor = 0.07 #-0.01
			}
		}		
		JAP_rising_militarism_AC = {
			picture = JAP_rising_militarism
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.08 #+0.02
				stability_factor = -0.13 #+0.04
				war_support_factor = 0.06 #-0.02
			}
		}		
		JAP_rising_militarism_ABC = {
			picture = JAP_rising_militarism
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1

			modifier = {
				political_power_gain = -0.05
				stability_factor = -0.10
				war_support_factor = 0.05
			}
		}		
		JAP_bent_to_military = {		
			picture = JAP_Bent_to_military
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1

			modifier = {
				political_power_gain = -0.10
				stability_factor = -0.15
				war_support_factor = 0.05
			}
		}
		JAP_bent_to_military_A = {		
			picture = JAP_Bent_to_military
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1

			modifier = {
				political_power_gain = -0.07 #+0.03
				stability_factor = -0.13 #+0.02
				war_support_factor = 0.05
			}
		}		
		JAP_bent_to_military_B = {		
			picture = JAP_Bent_to_military
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1

			modifier = {
				political_power_gain = -0.08 #+0.02
				stability_factor = -0.15 #+0.03
				war_support_factor = 0.05
			}
		}
		JAP_bent_to_military_AB = {		
			picture = JAP_Bent_to_military
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1

			modifier = {
				political_power_gain = -0.05
				stability_factor = -0.10
				war_support_factor = 0.05
			}
		}
		JAP_bent_to_military_C = {		
			picture = JAP_Bent_to_military
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1

			modifier = {
				political_power_gain = -0.05
				stability_factor = -0.10
				war_support_factor = 0.05
			}
		}
		JAP_bent_to_military_D = {		
			picture = JAP_Bent_to_military
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1

			modifier = {
				political_power_gain = -0.05
				stability_factor = -0.10
				war_support_factor = 0.05
			}
		}
		JAP_bent_to_military_CD = {		
			picture = JAP_Bent_to_military
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1

			modifier = {
				political_power_gain = -0.05
				stability_factor = -0.10
				war_support_factor = 0.05
			}
		}
		JAP_bent_to_military_E = {		
			picture = JAP_Bent_to_military
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1

			modifier = {
				political_power_gain = -0.03 #+0.02
				stability_factor = -0.08 #+0.02
				war_support_factor = 0.08 #+0.03
			}
		}
		JAP_bent_to_military_F = {		
			picture = JAP_Bent_to_military
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1

			modifier = {
				political_power_gain = -0.02
				stability_factor = -0.07
				war_support_factor = 0.07
			}
		}
		JAP_bent_to_military_EF = {		
			picture = JAP_Bent_to_military
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1

			modifier = {
				stability_factor = -0.05
				war_support_factor = 0.10
			}
		}
		JAP_bent_to_military_G = {		
			picture = JAP_Bent_to_military
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1

			modifier = {
				war_support_factor = 0.10
				conscription_factor = 0.05
				political_power_gain = 0.05
			}
		}
		JAP_army_minister_mediation = {	
			picture = JAP_successful_mediation
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.10
				stability_factor = -0.15
				war_support_factor = 0.05
			}
		}
		JAP_army_minister_mediation_A = {	
			picture = JAP_successful_mediation
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.07
				stability_factor = -0.12
				war_support_factor = 0.03
			}
		}
		JAP_army_minister_mediation_B = {	
			picture = JAP_successful_mediation
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.18
				stability_factor = -0.13
				war_support_factor = 0.02
			}
		}
		JAP_army_minister_mediation_AB = {	
			picture = JAP_successful_mediation
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.05
				stability_factor = -0.10
			}
		}
		JAP_army_minister_mediation_C = {	
			picture = JAP_successful_mediation
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.03
				stability_factor = -0.07
			}
		}
		JAP_army_minister_mediation_D = {	
			picture = JAP_successful_mediation
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.02
				stability_factor = -0.08
			}
		}
		JAP_army_minister_mediation_CD = {	
			picture = JAP_successful_mediation
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				stability_factor = -0.05
			}
		}
		JAP_army_minister_mediation_E = {	
			picture = JAP_successful_mediation
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				stability_factor = -0.02
				political_power_gain = 0.02
			}
		}
		JAP_army_minister_mediation_F = {	
			picture = JAP_successful_mediation
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				stability_factor = -0.03
				political_power_gain = 0.03
			}
		}
		JAP_army_minister_mediation_EF = {	
			picture = JAP_successful_mediation
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = 0.05
			}
		}
		JAP_army_minister_mediation_G = {	
			picture = JAP_successful_mediation
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				stability_factor = 0.05
				political_power_gain = 0.10
			}
		}
		JAP_bureaucratic_victory = {		
			picture = JAP_bureaucratic_victory
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.10
				stability_factor = -0.15
				war_support_factor = 0.05
			}
		}
		JAP_bureaucratic_victory_A = {		
			picture = JAP_bureaucratic_victory
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.08
				stability_factor = -0.12
				war_support_factor = 0.02
			}
		}
		JAP_bureaucratic_victory_B = {		
			picture = JAP_bureaucratic_victory
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.07
				stability_factor = -0.13
				war_support_factor = 0.03
			}
		}
		JAP_bureaucratic_victory_AB = {		
			picture = JAP_bureaucratic_victory
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.05
				stability_factor = -0.10
			}
		}
		JAP_bureaucratic_victory_C = {		
			picture = JAP_bureaucratic_victory
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.02
				stability_factor = -0.08
			}
		}
		JAP_bureaucratic_victory_D = {		
			picture = JAP_bureaucratic_victory
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.03
				stability_factor = -0.07
			}
		}
		JAP_bureaucratic_victory_CD = {		
			picture = JAP_bureaucratic_victory
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				stability_factor = -0.05
			}
		}
		JAP_bureaucratic_victory_E = {		
			picture = JAP_bureaucratic_victory
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				political_power_gain = 0.05
			}
		}
		JAP_bureaucratic_victory_F = {		
			picture = JAP_bureaucratic_victory
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1
			modifier = {
				stability_factor = 0.05
				political_power_gain = 0.10
			}
		}
		JAP_divided_army = {
			picture = divided_army
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = no
			}
			removal_cost = -1			
			research_bonus = {
				land_doctrine = -0.25
				naval_doctrine = -0.25
				air_doctrine = -0.25
			}	
			modifier = {
				army_org_factor = -0.05
				planning_speed = 0.20
				max_planning = -0.30
				command_power_gain_mult = -0.25
			}
		}
		JAP_chinese_expansion = {
			picture = JAP_northern_china_expedition
			allowed = {
				always = no
			}
			removal_cost = -1
			allowed_civil_war = {
				always = no
			}
		}		
		### National Unity Government until 1936 ###		
	}

	material_manufacturer = {
		designer = yes

		JAP_tokyo_army_arsenal = {

			allowed = {
				original_tag = JAP
			}

			research_bonus = {
				infantry_weapons = 0.15
			}
			
			traits = { infantry_equipment_manufacturer }
			ledger = army
		}

		JAP_nagoya_army_arsenal = {

			allowed = {
				original_tag = JAP
			}
			
			research_bonus = {
				infantry_weapons = 0.15
			}
			
			traits = { infantry_equipment_manufacturer }
			ledger = army
		}

		JAP_osaka_army_arsenal = {

			allowed = {
				original_tag = JAP
			}
			
			research_bonus = {
				artillery = 0.15
			}
			
			traits = { artillery_manufacturer }
			ledger = army
		}

		JAP_kokura_army_arsenal = {

			allowed = {
				original_tag = JAP
			}
			
			research_bonus = {
				artillery = 0.15
			}
			
			traits = { artillery_manufacturer }
			ledger = army
		}

		JAP_isuzu_motors = {

			allowed = {
				original_tag = JAP
			}
			
			research_bonus = {
				motorized_equipment = 0.15
			}
			
			traits = { motorized_manufacturer }
			ledger = army
		}

		JAP_tokyo_gas = {

			allowed = {
				original_tag = JAP
			}
			
			research_bonus = {
				motorized_equipment = 0.15
			}
			
			traits = { motorized_manufacturer }
			ledger = army
		}

		JAP_toyota = {

			allowed = {
				original_tag = JAP
			}
			
			research_bonus = {
				motorized_equipment = 0.15
			}
			
			traits = { motorized_manufacturer }
			ledger = army
		}
	}
}	