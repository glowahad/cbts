ideas = {
	country = {
		ARM_ethnic_tensions = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				stability_factor = -0.20
				conscription_factor = -0.20
				political_power_gain = -0.20
			}
		}
		ARM_ethnic_tensions2 = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			picture = ARM_ethnic_tensions
			removal_cost = -1
			modifier = {
				stability_factor = -0.15
				conscription_factor = -0.15
				political_power_gain = -0.15
			}
		}
		ARM_ethnic_tensions3 = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			picture = ARM_ethnic_tensions
			removal_cost = -1
			modifier = {
				stability_factor = -0.25
				conscription_factor = -0.25
				political_power_gain = -0.25
			}
		}
		ARM_foreign_diaspora = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				political_power_gain = 0.05
				trade_opinion_factor = 0.10
				opinion_gain_monthly = 0.10
			}
		}
		ARM_dreams_of_ararat = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			picture = ARM_dreams_of_ararat
			removal_cost = -1
			modifier = {
				army_core_attack_factor = 0.15
				army_core_defence_factor = 0.15
			}
		}
		ARM_dreams_of_ararat2 = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			picture = ARM_dreams_of_ararat
			removal_cost = -1
			modifier = {
				army_core_attack_factor = 0.20
				army_core_defence_factor = 0.20
			}
		}
		ARM_agrarian_population = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				industrial_capacity_factory = -0.20
				production_speed_buildings_factor = -0.20
				consumer_goods_factor = -0.05
			}
		}
		ARM_red_guard = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				conscription_factor = 0.10
				army_core_defence_factor = 0.05
			}
		}
		ARM_collective_reform = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.20
				MONTHLY_POPULATION = 0.025
				consumer_goods_factor = 0.05
				stability_factor = 0.05
			}
		}
		ARM_people_market = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				political_power_gain = -0.05
				production_speed_buildings_factor = 0.05
				industrial_capacity_factory = 0.05
			}
		}
		ARM_guerilla = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				army_defence_factor = 0.075
				encryption = 0.15
				attrition = -0.075
				army_org_factor = -0.075
				max_command_power = -15
			}
		}
	}
	head_of_gov = {
		ARM_hog_Grigor_Harutyunyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Harutyunyan_incapable }
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				insignificant_layman
				stalinist_minister
				hog
			}
		}
		ARM_hog_Hamo_Ohanjanyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Ohanjanyan_incapable }
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				silent_workhorse
				dashnak_minister
				hog
			}
		}
		ARM_hog_Simon_Vratsian = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Vratsian_incapable }
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				die_hard_reformer
				dashnak_minister
				hog
			}
		}
		ARM_hog_Arshak_Jamalyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Jamalyan_incapable }
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				insignificant_layman
				dashnak_minister
				hog
			}
		}
		ARM_hog_Sose_Mayrig = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Mayrig_incapable }
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				reformist_soldier
				dashnak_minister
				hog
			}
		}
		ARM_hog_Nikol_Agbalian = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Agbalian_incapable }
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				naive_optimist
				dashnak_minister
				hog
			}
		}
		ARM_hog_Drastamat_Kanayan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Kanayan_incapable }
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				old_general
				dashnak_minister
				hog
			}
		}
		ARM_hog_Garegin_Nzhdeh = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Nzhdeh_incapable }
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				autocratic_charmer
				taronist_minister
				hog
			}
		}
		ARM_hog_Hayk_Asatryan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Asatryan_incapable }
			}
			available = {
				always = no
			}
			cancel_if_invalid = no
			removal_cost = -1
			ai_will_do = {
				factor = 1
			}
			traits = {
				political_protege
				taronist_minister
				hog
			}
		}
	}
	foreign_minister = {
		ARM_frn_Sahak_Karapetyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Karapetyan_incapable }
			}
				
			available = {
				NOT = { has_country_flag = ARM_Karapetyan_incapable }
				OR = {
					has_government = communism
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				adequate_diplomat
				stalinist_minister
				frnmin
			}
		}
		ARM_frn_Alexander_Khatisian = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Karapetyan_incapable }
			}
				
			available = {
				NOT = { has_country_flag = ARM_Karapetyan_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				great_compromiser
				dashnak_minister
				frnmin
			}
		}
		ARM_frn_Artashes_Chilingaryan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Chilingaryan_incapable }
			}
				
			available = {
				NOT = { has_country_flag = ARM_Chilingaryan_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				ideological_crusader
				dashnak_minister
				frnmin
			}
		}
		ARM_frn_Vahan_Papazian = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Papazian_incapable }
			}
				
			available = {
				NOT = { has_country_flag = ARM_Papazian_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
					has_government = nationalism
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				ideological_crusader
				dashnak_minister
				frnmin
			}
		}
		ARM_frn_Artashes_Abeghyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Abeghyan_incapable }
			}
				
			available = {
				NOT = { has_country_flag = ARM_Abeghyan_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				biased_intellectual
				dashnak_minister
				frnmin
			}
		}
		ARM_frn_Gerasim_Balayan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Balayan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Balayan_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				adequate_diplomat
				dashnak_minister
				frnmin
			}
		}
		ARM_frn_Nerses_Astavatsaturyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Astavatsaturyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Astavatsaturyan_incapable }
				OR = {
					has_government = neutrality
					has_government = monarchism
					has_government = nationalism
					has_government = fascism
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				iron_fisted_brute
				taronist_minister
				frnmin
			}
		}
		ARM_frn_Armen_Barseghyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Barseghyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Barseghyan_incapable }
				OR = {
					has_government = neutrality
					has_government = monarchism
					has_government = nationalism
					has_government = fascism
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				biased_intellectual
				taronist_minister
				frnmin
			}
		}
	}
	economy_minister = {
		ARM_eco_Ashot_Hovhannisyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Hovhannisyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Hovhannisyan_incapable }
				OR = {
					has_government = communism
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				collectivizer
				stalinist_minister
				ecomin
			}
		}
		ARM_eco_Sargis_Araratyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Araratyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Araratyan_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				resource_industrialist
				dashnak_minister
				ecomin
			}
		}
		ARM_eco_Meshach_Torlakyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Torlakyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Torlakyan_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				infantry_proponent
				dashnak_minister
				ecomin
			}
		}
		ARM_eco_Grigor_Amiryan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Amiryan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Amiryan_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				artillery_proponent
				dashnak_minister
				ecomin
			}
		}
		ARM_eco_Catharine_Manoogian = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Manoogian_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Manoogian_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				medical_expert
				dashnak_minister
				ecomin
			}
		}
		ARM_eco_Gaspar_Gasparyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Gasparyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Gasparyan_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				corrupt_kleptocrat
				dashnak_minister
				ecomin
			}
		}
		ARM_eco_Avedis_Aharonyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Gasparyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Gasparyan_incapable }
				OR = {
					has_government = neutrality
					has_government = monarchism
					has_government = nationalism
					has_government = fascism
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				corporatist
				taronist_minister
				ecomin
			}
		}
		ARM_eco_Garegin_Nzhdeh = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Nzhdeh_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Nzdeh_incapable }
				OR = {
					has_government = neutrality
					has_government = monarchism
					has_government = nationalism
					has_government = fascism
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				infantry_proponent
				taronist_minister
				ecomin
			}
		}
		ARM_eco_Hovhannes_Devedjian = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Devedjian_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Devedjian_incapable }
				OR = {
					has_government = neutrality
					has_government = monarchism
					has_government = nationalism
					has_government = fascism
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				mobilizer
				taronist_minister
				ecomin
			}
		}
	}
	security_minister = {
		ARM_sec_Serik_Davtyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Davtyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Davtyan_incapable }
				OR = {
					has_government = communism
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				silent_lawyer
				stalinist_minister
				secmin
			}
		}
		ARM_sec_Apyar_Aslanyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Aslanyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Aslanyan_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				propagandist
				dashnak_minister
				secmin
			}
		}
		ARM_sec_Sedrak_Jalalyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Jalalyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Jalalyan_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				man_of_the_people
				dashnak_minister
				secmin
			}
		}
		ARM_sec_Aram_Salpy = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Salpy_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Salpy_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				healthcare_specialist
				dashnak_minister
				secmin
			}
		}
		ARM_sec_Artashes_Babalyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Babalyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Babalyan_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				scientific_specialist
				dashnak_minister
				secmin
			}
		}
		ARM_sec_Abraham_Gyulkhandanyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Gyulkhandanyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Gyulkhandanyan_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				crime_fighter
				dashnak_minister
				secmin
			}
		}
		ARM_sec_Simon_Vratsyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Vratsyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Vratsyan_incapable }
				OR = {
					has_government = authoritarian_socialism
					has_government = socialism
					has_government = social_democracy
					has_government = neutrality
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				dutiful_cleric
				dashnak_minister
				secmin
			}
		}
		ARM_sec_Nerses_Astavatsaturyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Astavatsaturyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Astavatsaturyan_incapable }
				OR = {
					has_government = neutrality
					has_government = monarchism
					has_government = nationalism
					has_government = fascism
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				secret_police_chief
				taronist_minister
				secmin
			}
		}
		ARM_sec_Shaven_Arushanyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Arushanyan_incapable }
			}
			available = {
				NOT = { has_country_flag = ARM_Arushanyan_incapable }
				OR = {
					has_government = neutrality
					has_government = monarchism
					has_government = nationalism
					has_government = fascism
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				crime_fighter
				taronist_minister
				secmin
			}
		}
	}
	army_chief = {
		ARM_coa_Movses_Silikyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Silikyan_incapable }
			}
			available = {
				NOT = {
					has_country_flag = ARM_Silikyan_incapable
					has_government = communism
					SOV = { has_country_flag = SOV_stopped_armenia_purge }
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				doctrine_of_autonomy
				coa
			}
		}
		ARM_coa_Hakob_Bagratuni = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Bagratuni_incapable }
			}
			available = {
				NOT = {
					has_government = communism
					has_country_flag = ARM_Bagratuni_incapable
				}
			}
			ai_will_do = {
				factor = 1
			}
			cancel_if_invalid = no
			traits = {
				guns_and_butter_army
				coa
			}
		}
	}
	high_command = {
		ARM_coa_Kristapor_Araratyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Araratyan_incapable }
			}
			available = {
				NOT = {
					has_country_flag = ARM_Araratyan_incapable
					has_government = communism
					SOV = { has_country_flag = SOV_stopped_armenia_purge }
				}
			}
			ai_will_do = {
				factor = 1
			}
			ledger = army
			cancel_if_invalid = no
			traits = {
				school_of_attack
				cos
			}
		}
		ARM_coa_Smbat_Boroyan = {
			allowed = {
				original_tag = ARM
				NOT = { has_country_flag = ARM_Bagratuni_incapable }
			}
			available = {
				NOT = {
					has_government = communism
					has_country_flag = ARM_Bagratuni_incapable
				}
			}
			ai_will_do = {
				factor = 1
			}
			ledger = army
			cancel_if_invalid = no
			traits = {
				school_of_mass_combat
				cos
			}
		}
	}
}