ideas = {
	country = {
	    
		SPA_catholic_unions = {
			
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			picture = generic_production_bonus
			
			modifier = {
			    stability_factor = 0.75
                production_factory_efficiency_gain_factor = 0.1				
			}
		}
		
		SPA_eng_blockaded = {

			picture = generic_naval_bonus

			allowed = {
				always = no
			}

			modifier = {
				production_speed_buildings_factor = -0.1
				consumer_goods_factor = 0.2
				political_power_cost = 0.1
				industrial_capacity_factory = -0.02				
			}
		}
		
		SPA_military_coup = {

			picture = FRA_national_mobilization_focus

			allowed = {
				always = no
			}

			modifier = {
				army_attack_factor = 0.2
				army_speed_factor = 0.2
                no_supply_grace = 120
                supply_consumption_factor = -0.20 				
			}
		}
		
		SPA_political_disunity1 = {

			picture = generic_disjointed_gov

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = -0.75				
			}
		}
		
		SPA_political_disunity2 = {

			picture = generic_disjointed_gov

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = -0.6 				
			}
		}
		
		SPA_political_disunity3 = {

			picture = generic_disjointed_gov

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = -0.45 				
			}
		}
		
		SPA_political_disunity4 = {

			picture = generic_disjointed_gov

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = -0.3 				
			}
		}
		
		SPA_political_disunity5 = {

			picture = generic_disjointed_gov

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = -0.15 				
			}
		}
		
		SPA_righteous_war = {

			picture = FRA_national_mobilization_focus

			allowed = {
				always = no
			}

			modifier = {
				army_core_attack_factor = 0.1
                conscription_factor = 0.2 				
			}
		}
		
		SPA_aristo_funds = {

			picture = can_wartime_prices_and_trade_board

			allowed = {
				always = no
			}

			modifier = {
				consumer_goods_factor = -0.05   				
			}
		}
		
		SPA_milicia_esp = {

			picture = ast_volunteer_defence_corps

			allowed = {
				always = no
			}

			modifier = {
				army_attack_factor = 0.125
                army_org_factor = -0.075  				
			}
		}
		
		SPA_legion_espanola = {

			picture = ast_volunteer_defence_corps

			allowed = {
				always = no
			}

			modifier = {
				army_attack_factor = 0.15
                political_power_factor = -0.1 				
			}
		}
		
		SPA_direccion = {

			picture = generic_spy_coup

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = 0.15
                foreign_subversive_activites = -0.35 				
 
			}
		}
		
		SPA_agr_counter = {

			picture = ast_all_in

			allowed = {
				always = no
			}

			modifier = {
				consumer_goods_factor = -0.075
				industrial_capacity_factory = 0.05
				political_power_factor = -0.1
			}
		}
		
	    SPA_snt = {

			picture = ast_all_in

			allowed = {
				always = no
			}

			modifier = {
				consumer_goods_factor = -0.05
				industrial_capacity_factory = 0.075
				production_factory_max_efficiency_factor = 0.1 
			}
		}
		
		SPA_harsh_ration = {

			picture = ast_all_in

			allowed = {
				always = no
			}

			modifier = {
				consumer_goods_factor = -0.03
				industrial_capacity_factory = 0.05
				production_factory_max_efficiency_factor = 0.05
                political_power_factor = 0.1				
			}
		}
		
		SPA_guer_nac = {

			picture = ast_volunteer_defence_corps

			allowed = {
				always = no
			}

			modifier = {
				army_attack_factor = 0.075
				recon_factor = 0.1
                army_org_factor = -0.05  				
			}
		}
		
		SPA_inefficent_research = {

			picture = SOV_scientist_defect

			allowed = {
				always = no
			}

			modifier = {
				research_speed_factor = -0.15
			}
		}
		SPA_social_dissatisfaction = {

			picture = SOV_social_dissatisfaction

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = -0.1
				stability_factor = -0.1
				war_support_factor = -0.2
				industrial_capacity_factory = -0.075
			}
		}
		
		SPA_social_dissatisfaction2 = {

			picture = SOV_social_dissatisfaction

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = -0.075
				stability_factor = -0.05
				war_support_factor = -0.15
				industrial_capacity_factory = -0.05
			}
		}
		
		SPA_social_dissatisfaction3 = {

			picture = SOV_social_dissatisfaction

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = -0.05
				stability_factor = -0.025
				war_support_factor = -0.1
			}
		}
		
		SPA_monarchist_opposition = {

			picture = ast_abandon_the_westminster_system

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = -0.25
			}
		}
		
		SPA_chaotic_administration = {

			picture = generic_disjointed_gov

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = -0.1
				stability_factor = -0.1
				production_speed_buildings_factor = -0.075
			}
		}
		
		SPA_national_government = {

			picture = HUN_treaty_of_triannon

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = 0.25
				stability_factor = 0.1
				production_speed_buildings_factor = 0.05
				industrial_capacity_factory = 0.05
				war_support_factor = -0.1				
			}
		}
		
		SPA_free_trade = {

			picture = can_wartime_prices_and_trade_board

			allowed = {
				always = no
			}

			modifier = {
				research_speed_factor = 0.05
				production_speed_buildings_factor = 0.1
				industrial_capacity_factory = 0.05
			}
		}
		
		SPA_unrest_military = {

			picture = ast_volunteer_defence_corps

			allowed = {
				always = no
			}

			modifier = {
				army_org_Factor = -0.1
				political_power_factor = -0.15
			}
		}
		
		SPA_authoritarian_mon = {

			picture = ast_abandon_the_westminster_system

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = 0.1
				mobilization_laws_cost_factor = -0.15
			}
		}
		
		SPA_authoritarian_mon2 = {

			picture = ast_abandon_the_westminster_system

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = 0.2
				mobilization_laws_cost_factor = -0.25
			}
		}
		
		SPA_church_carlist = {

			picture = ENG_the_war_to_end_all_wars

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = 0.1
				stability_factor = 0.05
				conscription_factor = 0.2
				consumer_goods_factor = 0.03
				industrial_capacity_factory = -0.05
			}
		}
		
		SPA_catholic_nation = {

			picture = ENG_the_war_to_end_all_wars

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = 0.15
				stability_factor = 0.075
				conscription_factor = 0.4
				consumer_goods_factor = 0.03
				industrial_capacity_factory = -0.05
			}
		}
		
		SPA_catholic_nation2 = {

			picture = ENG_the_war_to_end_all_wars

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = 0.1
				stability_factor = 0.075
				conscription_factor = 0.4
				consumer_goods_factor = 0.01
			}
		}
		
		SPA_social_monarchy = {

			picture = ast_abandon_the_westminster_system

			allowed = {
				always = no
			}

			modifier = {
				stability_factor = 0.15
				industrial_capacity_factory = 0.075
				production_speed_buildings_factor = 0.05
				consumer_goods_factor = 0.025
			}
		}
		
		SPA_trade_britain = {

			picture = can_wartime_prices_and_trade_board

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = -0.15
				consumer_goods_factor = -0.03
				production_speed_buildings_factor = 0.075
				research_speed_factor = 0.05
				local_resources_factor = 0.1
			}
		}
		
	    SPA_german_capital = {

			picture = SOV_scientist_defect

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = -0.1
				consumer_goods_factor = -0.05
				production_speed_buildings_factor = 0.1
			}
		}
		
		SPA_decentralized_realm = {

			picture = HUN_treaty_of_triannon

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = 0.1
				war_support_factor = 0.15
				army_morale_factor = 0.1
				production_speed_buildings_factor = -0.075
			}
		}
		
		SPA_decentralized_realm2 = {

			picture = HUN_treaty_of_triannon

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = 0.15
				war_support_factor = 0.1
				army_morale_factor = 0.05
				production_speed_buildings_factor = -0.025
			}
		}
		
		SPA_dynastic_disputes = {

			picture = SPA_dynastic_disputes

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = -0.2
				war_support_factor = -0.1
				stability_factor = -0.1
			}
		}
		
		SPA_national_cath = {

			picture = ENG_the_war_to_end_all_wars

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = 0.15
				stability_factor = 0.08
			}
		}
		
		SPA_national_cath2 = {

			picture = ENG_the_war_to_end_all_wars

			allowed = {
				always = no
			}

			modifier = {
				political_power_factor = 0.25
				stability_factor = 0.12
				army_core_defence_factor = 0.1
			}
		}
		
		SPA_maquis_idea = {
		
		    picture = generic_communism_drift_bonus

			allowed = {
			}

			allowed_civil_war = {
				always = no
			}

			modifier = {
				stability_factor = -0.05
				political_power_factor = -0.15
			}
		}
		
		SPA_democratic_agitation = {
		
		    picture = generic_democratic_drift_bonus

			allowed = {
			}

			allowed_civil_war = {
				always = no
			}

			modifier = {
				stability_factor = -0.05
				political_power_factor = -0.15
			}
		}
		
		SPA_liberalism_return = {

			picture = generic_democratic_drift_bonus

			allowed = {
			}

			allowed_civil_war = {
				always = no
			}

			modifier = {
				stability_factor = 0.15
				research_speed_factor = 0.025
				trade_laws_cost_factor = -0.2
			}
		}
		
		SPA_famine_risk1 = {

			picture = raj_risk_of_famine

			allowed = {
			}

			allowed_civil_war = {
				always = no
			}

			modifier = {
				stability_factor = -0.2 
				conscription_factor = -0.25
				MONTHLY_POPULATION = -0.4
			}
		}
		
		SPA_famine_risk2 = {

			picture = raj_risk_of_famine

			allowed = {
			}

			allowed_civil_war = {
				always = no
			}

			modifier = {
				stability_factor = -0.1 
				conscription_factor = -0.15
				MONTHLY_POPULATION = -0.2
			}
		}
		
		SPA_ini = {

			picture = HUN_treaty_of_triannon

			allowed = {
			}

			allowed_civil_war = {
				always = no
			}

			modifier = {
				production_speed_buildings_factor = 0.1
			}
		}
		
		SPA_famine_risk3 = {

			picture = raj_risk_of_famine

			allowed = {
			}

			allowed_civil_war = {
				always = no
			}

			modifier = {
				stability_factor = -0.05
				conscription_factor = -0.1
				MONTHLY_POPULATION = -0.1
			}
		}
		
	    postwar_peseta = {

			picture = great_depression

			allowed = {
			}

			allowed_civil_war = {
				always = no
			}

			modifier = {
				production_speed_buildings_factor = -0.2
				consumer_goods_factor = 0.2
				political_power_cost = -0.3
				research_speed_factor = -0.15
				production_factory_max_efficiency_factor = -0.2
				industrial_capacity_factory = -0.4
			}
		}
		
		postwar_lib = {

			picture = great_depression

			allowed = {
			}

			allowed_civil_war = {
				always = no
			}

			modifier = {
				production_speed_buildings_factor = -0.1
				consumer_goods_factor = 0.25
				political_power_cost = -0.5
				production_factory_max_efficiency_factor = -0.1
				industrial_capacity_factory = -0.2
			}
		}
		
		postwar_close = {

			picture = great_depression

			allowed = {
			}

			allowed_civil_war = {
				always = no
			}

			modifier = {
				production_speed_buildings_factor = -0.25
				consumer_goods_factor = 0.3
				political_power_cost = 0.1
				research_speed_factor = -0.4
				production_factory_max_efficiency_factor = -0.25
				industrial_capacity_factory = -0.5
			}
		}
		
		postwar_close2 = {

			picture = great_depression

			allowed = {
			}

			allowed_civil_war = {
				always = no
			}

			modifier = {
				production_speed_buildings_factor = -0.25
				consumer_goods_factor = 0.3
				political_power_cost = 0.1
				research_speed_factor = -0.2
				production_factory_max_efficiency_factor = -0.25
				industrial_capacity_factory = -0.5
			}
		}
		
	    SPA_budget = {
			
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			picture = ast_volunteer_defence_corps
			
			modifier = {
			    industrial_capacity_factory = 0.15 
                army_morale_factor = 0.1
                production_speed_industrial_complex_factor = -0.1
                consumer_goods_factor = 0.05 				
			}
		}
		
	    SPA_mnp1 = {
			
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			picture = generic_infantry_bonus
			
			modifier = {
			    conscription = 0.015
                army_morale_factor = 0.05				
			}
		}
		
		SPA_mnp2 = {
			
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			picture = generic_infantry_bonus
			
			modifier = {
			    conscription = 0.025
                army_morale_factor = 0.05				
			}
		}
		 
		SPA_mnp3 = {
			
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			picture = generic_infantry_bonus
			
			modifier = {
			    conscription = 0.035
                army_morale_factor = 0.05
                mobilization_speed = 0.1				
			}
		}

        SPA_mnp4 = {
			
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			picture = generic_infantry_bonus
			
			modifier = {
			    conscription = 0.01
                army_morale_factor = 0.1				
			}
		} 
		
		SPA_dis_gov = {
			
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			picture = generic_disjointed_gov
			
			modifier = {
			    political_power_factor = -0.1			
			}
		}
		
		SPA_union = {
			
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			picture = idea_general_staff
			
			modifier = {
			    conscription = 0.01
                army_org_factor = 0.1
                max_command_power = 10  				
			}
		}
		
		SPA_sindicato = {
			
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			picture = idea_ast_all_in
			
			modifier = {
			    production_factory_max_efficiency_factor = 0.075
                industrial_capacity_factory = 0.05  				
			}
		}
		
		SPA_requete = {
			
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			picture = generic_manpower_bonus
			
			modifier = {
			    conscription = 0.01
                army_morale_factor = 0.1				
			}
		}
		SPA_requete2 = {
			
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			picture = generic_manpower_bonus
			
			modifier = {
			    conscription = 0.01
                army_morale_factor = 0.1
                army_core_attack_factor = 0.1				
			}
		}
		
		SPA_organized_army = {
			
			
			allowed = {
				always = no
			}
			
			removal_cost = -1
			
			picture = german_advisors
			
			modifier = {
                army_morale_factor = 0.15				
			}
		}
	}
}