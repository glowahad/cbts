ideas = {
	country = {
		RAJ_Hindu_Muslim_tension = {
			picture = RAJ_Hindu_Muslim_tension
			allowed = {
				original_tag = RAJ
			}
			allowed = {
				original_tag = BNG
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = -0.20
				conscription_factor = -0.20
			}
		}
		RAJ_Indian_Independance_Movement = {
			picture = RAJ_Indian_Independance_Movement
			allowed = {
				original_tag = RAJ
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				political_power_factor = -0.10
				war_support_factor = -0.10
				stability_factor = -0.05
			}
		}
		RAJ_Princely_States = {
			picture = RAJ_Princely_States
			allowed = {
				original_tag = RAJ
			}
			allowed = {
				original_tag = HND
			}
			allowed = {
				original_tag = PAK
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				conscription_factor = -0.10
				political_power_factor = -0.10
				consumer_goods_factor = 0.20
			}
		}
		RAJ_British_India_Army = {
			picture = RAJ_British_India_Army 
			allowed = {
				original_tag = RAJ
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				military_leader_cost_factor = 0.25
				training_time_factor = 0.1
				army_org_factor = 0.05
				max_planning = 0.25
			}
		}
		RAJ_Quit_India_Movement = {
			picture = RAJ_Indian_Independance_Movement
			allowed = {
				original_tag = RAJ
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				political_power_factor = -0.15
				war_support_factor = -0.25
				stability_factor = -0.10
				autonomy_gain = 0.1
			}
		}
		RAJ_Army_mutiny = {
			picture = idea_generic_army_problems
			allowed = {
				original_tag = RAJ
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				air_interception_attack_factor = -0.05
				air_air_superiority_attack_factor = -0.05
			}
		}
		RAJ_Army_mutiny2 = {
			picture = idea_generic_army_problems
			allowed = {
				original_tag = RAJ
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				naval_strike_attack_factor = -0.05
				naval_strike_targetting_factor = -0.05
				naval_strike_agility_factor = -0.05
				convoy_escort_efficiency = -0.05
				air_interception_attack_factor = -0.05
				air_air_superiority_attack_factor = -0.05
			}
		}
		RAJ_Army_mutiny3 = {
			picture = idea_generic_army_problems
			allowed = {
				original_tag = RAJ
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				naval_strike_attack_factor = -0.05
				naval_strike_targetting_factor = -0.05
				naval_strike_agility_factor = -0.05
				convoy_escort_efficiency = -0.05
				air_interception_attack_factor = -0.05
				air_air_superiority_attack_factor = -0.05
				army_org_factor = -0.05
				max_planning = -0.25
			}
		}
		RAJ_British_India_Army2 = {
			picture = RAJ_British_India_Army 
			allowed = {
				original_tag = RAJ
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				military_leader_cost_factor = 0.30
				army_org_factor = 0.07
				max_planning = 0.30
				land_reinforce_rate = 0.3
			}
		}
	}
}