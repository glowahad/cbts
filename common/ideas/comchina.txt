ideas = {
	material_manufacturer = {
		designer = yes
		
		Huangyadong_Arsenal = {
			allowed = {
				original_tag = PRC
			}
			available = {
			}

			ai_will_do = {
				factor = 1
			}
			research_bonus = {
				infantry_weapons = 0.10
			}
			picture = wip
			traits = { infantry_equipment_manufacturer }
		}
	}
    tank_manufacturer = {
		designer = yes
		Jianxin = {
			allowed = {
				original_tag = PRC
			}
			available = {
				date > 1947.01.01
                controls_state = 1146
			}

			ai_will_do = {
				factor = 1
			}
			research_bonus = {
				artillery = 0.10
			}
			picture = wip
			traits = { artillery_manufacturer3 }
		}
	}
}