#this file is temporary
ideas = {
	country = {
		GEN_western_bureaucracy = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = -0.05
				political_power_gain = 0.10
			}
		}
		GEN_local_bureaucracy = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = 0.05
				political_power_gain = -0.10
			}
		}
		GEN_military_influence = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				political_power_gain = -0.075
			}
		}
		GEN_reformist = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				political_power_gain = 0.15
				stability_factor = -0.10
			}
		}
		GEN_reactionary = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				political_power_gain = -0.15
				stability_factor = 0.10
			}
		}
		GEN_social_modern = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				political_power_gain = 0.10
				stability_factor = -0.05
			}
		}
		GEN_social_backwards = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				political_power_gain = -0.10
				stability_factor = 0.05
			}
		}
		GEN_modern_economy = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				political_power_gain = -0.10
				stability_factor = -0.05
				industrial_capacity_factory = 0.10
				industrial_capacity_dockyard = 0.10
				production_speed_buildings_factor = 0.10
			}
		}
		GEN_traditional_economy = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				political_power_gain = 0.10
				stability_factor = 0.05
				industrial_capacity_factory = -0.10
				industrial_capacity_dockyard = -0.10
				production_speed_buildings_factor = -0.10
			}
		}
		GEN_economic_statism = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				political_power_gain = -0.10
				consumer_goods_factor = -0.05
				production_speed_buildings_factor = 0.05
			}
		}
		GEN_free_market = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				political_power_gain = 0.10
				consumer_goods_factor = 0.05
				production_speed_buildings_factor = 0.10
			}
		}
		GEN_education = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				research_speed_factor = 0.075
				stability_factor = 0.025
			}
		}
		GEN_feudal_agriculture = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = 0.10
				consumer_goods_factor = -0.05
				production_speed_buildings_factor = -0.10
				industrial_capacity_factory = -0.05
				industrial_capacity_dockyard = -0.05
				MONTHLY_POPULATION = 0.10
			}
		}
		GEN_feudal_agriculture2 = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			modifier = {
				stability_factor = 0.15
				consumer_goods_factor = -0.10
				production_speed_buildings_factor = -0.20
				industrial_capacity_factory = -0.10
				industrial_capacity_dockyard = -0.10
				MONTHLY_POPULATION = 0.10
				local_building_slots_factor = -0.30
			}
		}
	}
}