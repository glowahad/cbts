ideas = {
	head_of_gov = {
        BUL_Nikola_Mushanov_hog = {
            allowed = { original_tag = BUL }
			available = {
				custom_trigger_tooltip = {
					has_country_flag = can_replace_hog
					NOT = { has_country_flag = Nikola_Mushanov_dead }
				}
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea BUL_Nikola_Mushanov_hog"
			}
			#picture = 
			traits = {
				hog
			}
			cancel_if_invalid = no
        }
    }
    foreign_minister = {
        BUL_Nikola_Mushanov_for = {
            allowed = { original_tag = BUL }
			available = {
				custom_trigger_tooltip = {
					has_country_flag = can_replace_hog
					NOT = { has_country_flag = Nikola_Mushanov_dead }
				}
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea BUL_Nikola_Mushanov_for"
			}
			#picture = 
			traits = {
				frnmin
			}
			cancel_if_invalid = no
        }
    }
    economy_minister = {
        BUL_Stefan_Stefanov_eco = {
            allowed = { original_tag = BUL }
			available = {
				custom_trigger_tooltip = {
					has_country_flag = can_replace_hog
					NOT = { has_country_flag =  Stefan_Stefanov_dead }
				}
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea BUL_Stefan_Stefanov_eco"
			}
			#picture = 
			traits = {
				ecomin
			}
			cancel_if_invalid = no
        }
    }
    security_minister = {
        BUL_Aleksandar_Girginov_sec = {
            allowed = { original_tag = BUL }
			available = {
				custom_trigger_tooltip = {
					has_country_flag = can_replace_hog
					NOT = { has_country_flag = Aleksandar_Girginov_dead }
				}
			}
			on_add = {
				log = "[GetDateText]: [Root.GetName]: add idea BUL_Aleksandar_Girginov_sec"
			}
			#picture = 
			traits = {
				secmin
			}
			cancel_if_invalid = no
        }
	}
}