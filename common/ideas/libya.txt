ideas = {
	country = {

		LBA_war_of_independence = {
			allowed = {
				always = no
			}
			allowed_civil_war = {
				always = yes
			}
			removal_cost = -1
			modifier = {
				conscription = 0.05
				army_core_attack_factor = 0.2
				army_core_defence_factor = 0.2
				surrender_limit = 0.5





			}
		}
	}

}

