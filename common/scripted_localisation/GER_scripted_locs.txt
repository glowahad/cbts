defined_text = {
	name = DNVP_faction_loc
	text = {
		trigger = {
			has_variable = GER_DNVP_Stahlhelm_Points
		}
		localization_key = GER_DNVP_loc_with_Stalhelm_key
	}
	text = {
		localization_key = GER_DNVP_loc_without_Stalhelm_key
	}
}
defined_text = {
	name = GetOpposingMilitaryFaction
	text = {
		trigger = {
			has_government = neutrality
		}
		localization_key = GetOpposingMilitaryFaction_Nats
	}
	text = {
		trigger = {
			has_government = nationalism
		}
		localization_key = GetOpposingMilitaryFaction_Stats
	}
}
defined_text = {
	name = did_reich_church
	text = {
		trigger = {
			has_country_flag = GER_established_reich_church
		}
		localization_key = yes_reich_church
	}
	text = {
		localization_key = no_reich_church
	}
}
defined_text = {
	name = did_reich_church_desc
	text = {
		trigger = {
			has_country_flag = GER_established_reich_church
		}
		localization_key = yes_reich_church_desc
	}
	text = {
		localization_key = no_reich_church_desc
	}
}
defined_text = {
	name = GER_franco_or_antonio
	text = {
		trigger = {
			SPA = {
				has_country_leader = {
					name = "José Antonio Primo de Rivera"
					ruling_only = yes
				}
			}
		}
		localization_key = GER_antonio
	}
	text = {
		localization_key = GER_franco
	}
}
defined_text = {
	name = GER_portugal_war
	text = {
		trigger = {
			POR = {
				OR = {
					is_in_faction_with = ENG
					has_war_with = GER
					gives_military_access_to = ENG
				}
			}
		}
		localization_key = GER_fd18
	}
	text = {
		localization_key = GER_Rechila
	}
}
defined_text = {
	name = GER_Autobahn
	text = {
		trigger = {
			has_completed_focus = GER_Hitler_Economics_Reinhardt_Program
		}
		localization_key = GER_Reinhardt
	}
	text = {
		localization_key = GER_Plain_Autobahns
	}
}
defined_text = {
	name = GER_Autobahn_Desc
	text = {
		trigger = {
			has_completed_focus = GER_Hitler_Economics_Reinhardt_Program
		}
		localization_key = GER_Reinhardt_Desc
	}
	text = {
		localization_key = GER_Plain_Autobahns_Desc
	}
}
defined_text = {
	name = GER_Prefer_Russia_Over_Soviet
	text = {
		trigger = {
			country_exists = RUS
		}
		localization_key = GER_RUS_Exists
	}
	text = {
		localization_key = GER_RUS_Not_Exsts
	}
}
defined_text = {
	name = GER_Prefer_Soviet_over_Russia
	text = {
		trigger = {
			country_exists = SOV
		}
		localization_key = GER_RUS_Not_Exsts
	}
	text = {
		localization_key = GER_RUS_Exists
	}
}
defined_text = {
	name = GER_Italy_Defends_Austria
	text = {
		trigger = {
			AUS = { has_country_flag = GER_ITA_Defends_AUS }
		}
		localization_key = GER_Italy_Defends_Austria_Yes
	}
	text = {
		localization_key = GER_Italy_Defends_Austria_No
	}
}
defined_text = {
	name = GER_Name_Of_Airship
	text = {
		trigger = {
			tag = DDR
		}
		localization_key = GER_Called_Engels
	}
	text = {
		localization_key = GER_Called_Hindenburg
	}
}
defined_text = {
	name = GER_SS_Exists
	text = {
		trigger = {
			has_country_flag = GER_SS_Destroyed
		}
		localization_key = GER_SS_Exists_no
	}
	text = {
		localization_key = GER_SS_Exists_yes
	}
}
defined_text = {
	name = GER_Hitlers_Death_Cause
	text = {
		trigger = {
			check_variable = {
				GER_Hitler_dies = 2
			}
		}
		localization_key = GER_Hitler_Died_From_Drugs
	}
	text = {
		localization_key = GER_Hitler_Died_From_Syphillis
	}
}
defined_text = {
	name = GER_Fuhrer_Loc
	text = {
		trigger = {
			has_country_flag = GER_Endsieg_Goebbels_Fuhrer
		}
		localization_key = GER_Make_Goebbels_Fuhrer
	}
	text = {
		trigger = {
			has_country_flag = GER_Endsieg_Bormann_Fuhrer
		}
		localization_key = GER_Make_Bormann_Fuhrer
	}
	text = {
		localization_key = GER_Make_Goring_Fuhrer
	}
}
#DNVP
defined_text = {
	name = GER_DNVP_coal_loc
	text = {
		trigger = {
			has_country_flag = GER_DNVP_WP_coal
		}
		localization_key = wp_coal_dnvp
	}
	text = {
		localization_key = dnvp_no_coal
	}
}
defined_text = {
	name = RestorationEventGer
	text = {
		trigger = {
			is_in_array = {
				array = coalition_party_list
				value = 11
			}
		}
		localization_key = DNVP_Restoration_Nazis
	}
	text = {
		localization_key = DNVP_Restoration_No_Nazis
	}
}
defined_text = {
	name = AustriaPartyCoop
	text = {
		trigger = {
			has_completed_focus = GER_DNVP_AUS_DNSAP
		}
		localization_key = DNVP_Work_With_Nazis
	}
	text = {
		localization_key = DNVP_Work_With_GDVP
	}
}
defined_text = {
	name = AustriaPartyCoopAcro
	text = {
		trigger = {
			has_completed_focus = GER_DNVP_AUS_DNSAP
		}
		localization_key = DNVP_Work_With_NazisAcro
	}
	text = {
		localization_key = DNVP_Work_With_GDVPAcro
	}
}
defined_text = {
	name = AustriaPartyCoopGetPop
	text = {
		trigger = {
			has_completed_focus = GER_DNVP_AUS_DNSAP
		}
		localization_key = DNVP_Work_With_NazisPop
	}
	text = {
		localization_key = DNVP_Work_With_GDVPPop
	}
}
defined_text = {
	name = AustriaPartyIncreaseActivity
	text = {
		trigger = {
			has_country_flag = AUS_counter_anschluss
		}
		localization_key = AUS_knows_about_interference
	}
	text = {
		localization_key = AUS_does_not_know_about_interference
	}
}
defined_text = { #for austria
	name = GetIdentity
	text = {
		trigger = {
			OR = {
				has_government = communism
				has_government = authoritarian_socialism
				has_government = socialism
				has_government = social_democracy
			}
		}
		localization_key = AUS_social_identity
	}
	text = {
		localization_key = AUS_catholic_identity
	}
}
defined_text = { #for austria
	name = GetIdentityDesc
	text = {
		trigger = {
			OR = {
				has_government = communism
				has_government = authoritarian_socialism
				has_government = socialism
				has_government = social_democracy
			}
		}
		localization_key = AUS_social_identitydesc
	}
	text = {
		localization_key = AUS_catholic_identitydesc
	}
}
defined_text = { #Hitler/Hugenberg
	name = GetGerKanzler
	text = {
		trigger = {
			has_idea = hitler
		}
		localization_key = GER_hitler_loc
	}
	text = {
		trigger = {
			has_idea = hugenberg
		}
		localization_key = GER_hugenberg_loc
	}
	text = {
		trigger = {
			has_idea = goerdeler2
		}
		localization_key = GER_goerdeler_loc
	}
	text = {
		trigger = {
			has_idea = seldte2
		}
		localization_key = GER_seldte_loc
	}
	text = {
		localization_key = GER_generic_loc
	}
}
defined_text = { #GetGerOrIta
	name = GetGerOrIta
	text = {
		trigger = {
			tag = GER
		}
		localization_key = ger_key
	}
	text = {
		localization_key = italy_key
	}
}
defined_text = { #GetGerOrIta
	name = GerOrItaRequest
	text = {
		trigger = {
			tag = GER
		}
		localization_key = ger_request
	}
	text = {
		localization_key = italy_request
	}
}
defined_text = { #ITAOffer
	name = ITAOffer
	text = {
		trigger = {
			has_country_flag = GER_ITA_repatriation
		}
		localization_key = ger_repatriation
	}
	text = {
		localization_key = ger_autonomy
	}
}
defined_text = { #GetClaimYUG
	name = GetClaimYUG
	text = {
		trigger = {
			tag = ITA
		}
		localization_key = Ljubljana
	}
	text = {
		localization_key = Prekmurje
	}
}
defined_text = { #ExpandedSLV
	name = ExpandedSLV
	text = {
		trigger = {
			OR = {
				has_country_flag = GER_ITA_Ljubljana
				has_country_flag = GER_HUN_Prekmurje
			}
		}
		localization_key = Expand_SLV
	}
	text = {
		localization_key = Blank
	}
}
defined_text = { #ExpandSLV2
	name = ExpandSLV2
	text = {
		trigger = {
			has_country_flag = GER_ITA_Ljubljana
			has_country_flag = GER_HUN_Prekmurje
		}
		localization_key = Expand_SLV_Both
	}
	text = {
		trigger = {
			has_country_flag = GER_ITA_Ljubljana
		}
		localization_key = Expand_SLV_ITA
	}
	text = {
		localization_key = Expand_SLV_HUN
	}
}
defined_text = { #TitlesOrNo
	name = TitlesOrNo
	text = {
		trigger = {
			has_country_flag = GER_no_nobility
		}
		localization_key = Restore_Titles
	}
	text = {
		localization_key = Blank
	}
}
defined_text = { #StahlhelmChoice
	name = StahlhelmChoice
	text = {
		trigger = {
			has_country_flag = GER_Stalhelm_exists
		}
		localization_key = Choose_Stahlhelm
	}
	text = {
		localization_key = Blank
	}
}
defined_text = { #Winning_Group
	name = GetWinningLocalization
	text = {
		trigger = {
			check_variable = {
				Winning_Group > 2.499
			}
		}
		localization_key = Rightist_Victory
	}
	text = {
		trigger = {
			check_variable = {
				Winning_Group < 2.5
			}
			check_variable = {
				Winning_Group > 1.749
			}
		}
		localization_key = Centre_Rightist_Victory
	}
	text = {
		trigger = {
			check_variable = {
				Winning_Group < 1.75
			}
			check_variable = {
				Winning_Group > 1.249
			}
		}
		localization_key = Centre_Victory
	}
	text = {
		trigger = {
			check_variable = {
				Winning_Group < 1.25
			}
			check_variable = {
				Winning_Group > 0.499
			}
		}
		localization_key = Centre_Leftist_Victory
	}
	text = {
		trigger = {
			check_variable = {
				Winning_Group < 0.5
			}
		}
		localization_key = Leftist_Victory
	}
	text = {
		localization_key = Blank
	}
}
defined_text = { #Winning_Group
	name = GetWinningDesc
	text = {
		trigger = {
			check_variable = {
				Winning_Group > 2.499
			}
		}
		localization_key = Rightist_Victory_Desc
	}
	text = {
		trigger = {
			check_variable = {
				Winning_Group < 2.5
			}
			check_variable = {
				Winning_Group > 1.749
			}
		}
		localization_key = Centre_Rightist_Victory_Desc
	}
	text = {
		trigger = {
			check_variable = {
				Winning_Group < 1.75
			}
			check_variable = {
				Winning_Group > 1.249
			}
		}
		localization_key = Centre_Victory_Desc
	}
	text = {
		trigger = {
			check_variable = {
				Winning_Group < 1.25
			}
			check_variable = {
				Winning_Group > 0.499
			}
		}
		localization_key = Centre_Leftist_Victory_Desc
	}
	text = {
		trigger = {
			check_variable = {
				Winning_Group < 0.5
			}
		}
		localization_key = Leftist_Victory_Desc
	}
	text = {
		localization_key = Blank
	}
}
defined_text = { #Win_Size
	name = GetWinSize
	text = {
		trigger = {
			check_variable = {
				Win_Size = 2
			}
		}
		localization_key = Major_Victory
	}
	text = {
		trigger = {
			check_variable = {
				Win_Size = 1
			}
		}
		localization_key = Regular_Victory
	}
	text = {
		localization_key = Minor_Victory
	}
}
defined_text = { #GetInterestGroup
	name = GetInterestGroup
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 0
			}
		}
		localization_key = LeninistInterestGroup
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 1
			}
		}
		localization_key = AutInterestGroup
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 2
			}
		}
		localization_key = FarAutInterestGroup
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 3
			}
		}
		localization_key = SocLibInterestGroup
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 4
			}
		}
		localization_key = AutDesInterestGroup
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 5
			}
		}
		localization_key = DemSocInterestGroup
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 6
			}
		}
		localization_key = LibConInterestGroup
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 7
			}
		}
		localization_key = SocConInterestGroup
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 8
			}
		}
		localization_key = SocDemInterestGroup
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 9
			}
		}
		localization_key = FasInterestGroup
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 10
			}
		}
		localization_key = RevSocInterestGroup
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 11
			}
		}
		localization_key = MarLibInterestGroup
	}
	text = {
		localization_key = Blank
	}
}
defined_text = { #GetInterestGroupDesc
	name = GetInterestGroupDesc
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 0
			}
		}
		localization_key = LeninistInterestGroupDesc
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 1
			}
		}
		localization_key = AutInterestGroupDesc
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 2
			}
		}
		localization_key = FarAutInterestGroupDesc
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 3
			}
		}
		localization_key = SocLibInterestGroupDesc
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 4
			}
		}
		localization_key = AutDesInterestGroupDesc
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 5
			}
		}
		localization_key = DemSocInterestGroupDesc
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 6
			}
		}
		localization_key = LibConInterestGroupDesc
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 7
			}
		}
		localization_key = SocConInterestGroupDesc
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 8
			}
		}
		localization_key = SocDemInterestGroupDesc
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 9
			}
		}
		localization_key = FasInterestGroupDesc
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 10
			}
		}
		localization_key = RevSocInterestGroupDesc
	}
	text = {
		trigger = {
			check_variable = {
				GER_intrest_group_count = 11
			}
		}
		localization_key = MarLibInterestGroupDesc
	}
	text = {
		localization_key = Blank
	}
}
defined_text = { #GetRussia
	name = GetRussia
	text = {
		trigger = {
			SOV = {
				exists = yes
				NOT = {
					is_in_faction_with = ROOT
					has_war_together_with = ROOT
					is_subject_of = ROOT
				}
			}
		}
		localization_key = GetRussiaBolsheviks
	}
	text = {
		localization_key = GetRussiaRussians
	}
}
defined_text = { #GetInsult
	name = GetInsult
	text = {
		trigger = {
			has_country_flag = GER_DNVP_Liberal_Compromiser
		}
		localization_key = LiberalCompromiserInsult
	}
	text = {
		localization_key = DefenderInsult
	}
}
defined_text = {
	name = GetGermany2rcwResponse
	text = {
		trigger = {
			has_government = communism
		}
		localization_key = Germany_Leninist_2rcw_Response
	}
	text = {
		trigger = {
			OR = {
				AND = {
					has_government = authoritarian_socialism
					POL = {
						NOT = {
							has_government = authoritarian_socialism
						}
					}
				}
				AND = {
					has_government = socialism
					POL = {
						NOT = {
							has_government = socialism
						}
					}
				}
			}
		}
		localization_key = Germany_Socialist_2rcw_Back_Poland
	}
	text = {
		trigger = {
			OR = {
				has_government = authoritarian_socialism
				has_government = socialism
			}
		}
		localization_key = Germany_Socialist_2rcw_Response
	}
	text = {
		trigger = {
			OR = {
				has_government = fascism
				has_government = nationalism
				has_government = monarchism
				has_government = neutrality
			}
		}
		localization_key = Germany_Rightist_2rcw_Response
	}
	text = {
		localization_key = Germany_Democratic_2rcw_Response
	}
}
defined_text = {
	name = GetPostHindenburg
	text = {
		trigger = {
			has_government = fascism #hitler
		}
		localization_key = GER_Hitler_takes_full_power
	}
	text = {
		trigger = {
			has_government = nationalism #wilhelm III
			has_country_leader = {
				ruling_only = yes
				name = "Wilhelm III"
			}
		}
		localization_key = GER_Wilhelm_III_loc
	}
	text = {
		trigger = {
			has_government = nationalism #hugenberg
		}
		localization_key = GER_Hugenberg_takes_full_power
	}
	text = {
		localization_key = GER_democratic_post_hindenburg
	}
}

defined_text = {
	name = WGR_chancellor
	text = {
		trigger = {
			WGR = { has_idea = GER_Hermann_Dietrich }
		}
		localization_key = GER_Hermann_Dietrich_loc
	}
	text = {
		trigger = {
			WGR = { has_idea = GER_Eduard_Dingeldy }
		}
		localization_key = GER_Eduard_Dingeldy_loc
	}
	text = {
		trigger = {
			WGR = { has_idea = adenauer }
		}
		localization_key = adenauer_loc
	}
	text = {
		trigger = {
			WGR = { has_idea = kaas }
		}
		localization_key = kaas_loc
	}
}