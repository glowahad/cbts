defined_text = {
	name = GetRodzaevskyName
	text = {
		localization_key = RFP_rod_name
	}
}
defined_text = {
	name = GetVonsyatskyName
	text = {
		localization_key = RFP_von_name
	}
}
defined_text = {
	name = GetSemyonovName
	text = {
		localization_key = RFP_cos_name
	}
}
defined_text = {
	name = RFPGetFaction
	text = {
		trigger = {
			has_country_flag = RFP_show_cos
		}
		localization_key = RFP_cos_faction_loc
	}
	text = {
		trigger = {
			has_country_flag = RFP_show_von
		}
		localization_key = RFP_von_faction_loc
	}
	text = {
		trigger = {
			has_country_flag = RFP_show_rod
		}
		localization_key = RFP_rod_faction_loc
	}
	text = {
		localization_key = Blank
	}
}
defined_text = {
	name = GetMemberOrgsFascist
	text = {
		trigger = {
			has_country_flag = RFP_show_rod
		}
		localization_key = RFP_rod_orgs
	}
	text = {
		trigger = {
			has_country_flag = RFP_show_von
		}
		localization_key = RFP_von_orgs
	}
	text = {
		trigger = {
			has_country_flag = RFP_show_cos
		}
		localization_key = RFP_cos_orgs
	}
	text = {
		localization_key = Blank
	}
}
defined_text = {
	name = GetMemberDescsFascist
	text = {
		trigger = {
			has_country_flag = RFP_show_rod
		}
		localization_key = RFP_rod_descs
	}
	text = {
		trigger = {
			has_country_flag = RFP_show_von
		}
		localization_key = RFP_von_descs
	}
	text = {
		trigger = {
			has_country_flag = RFP_show_cos
		}
		localization_key = RFP_cos_descs
	}
	text = {
		localization_key = Blank
	}
}
defined_text = {
	name = GetLargestFactionRFP
	text = {
		trigger = {
			OR = {
				check_variable = {
					RFP_rod_influence > RFP_cos_influence
				}
				check_variable = {
					RFP_rod_influence = RFP_cos_influence
				}
			}
			OR = {
				check_variable = {
					RFP_rod_influence > RFP_von_influence
				}
				check_variable = {
					RFP_rod_influence = RFP_von_influence
				}
			}
		}
		localization_key = RFP_rod_biggest
	}
	text = {
		trigger = {
			OR = {
				check_variable = {
					RFP_von_influence > RFP_cos_influence
				}
				check_variable = {
					RFP_von_influence = RFP_cos_influence
				}
			}
			OR = {
				check_variable = {
					RFP_von_influence > RFP_monarchist_influence
				}
				check_variable = {
					RFP_von_influence = RFP_monarchist_influence
				}
			}
		}
		localization_key = RFP_von_biggest
	}
	text = {
		localization_key = RFP_cos_biggest
	}
}