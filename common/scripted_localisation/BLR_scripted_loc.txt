defined_text = {
	name = GetBLR_NationalistName
	text = {
		localization_key = BLR_Zacharka_Name
	}
}
defined_text = {
	name = GetBulakName
	text = {
		localization_key = BLR_Bulak_Name
	}
}
defined_text = {
	name = GetTriunionistName
	text = {
		localization_key = BLR_Voysekhovskiy_Name
	}
}
defined_text = {
	name = GetAutonomistName
	text = {
		localization_key = BLR_Bodunova_name
	}
}
defined_text = {
	name = BLRGetFaction
	text = {
		trigger = {
			has_country_flag = BLR_show_autonomist
		}
		localization_key = BLR_autonomist_faction_loc
	}
	text = {
		trigger = {
			has_country_flag = BLR_show_bulak
		}
		localization_key = BLR_bulak_faction_loc
	}
	text = {
		trigger = {
			has_country_flag = BLR_show_blr_nationalist
		}
		localization_key = BLR_nationalist_faction_loc
	}
	text = {
		trigger = {
			has_country_flag = BLR_show_triunionist
		}
		localization_key = BLR_triunionist_faction_loc
	}
	text = {
		localization_key = Blank
	}
}
defined_text = {
	name = GetMemberOrgsBLR
	text = {
		trigger = {
			has_country_flag = BLR_show_blr_nationalist
		}
		localization_key = BLR_nat_orgs
	}
	text = {
		trigger = {
			has_country_flag = BLR_show_autonomist
		}
		localization_key = BLR_aut_orgs
	}
	text = {
		trigger = {
			has_country_flag = BLR_show_bulak
		}
		localization_key = BLR_bulak_orgs
	}
	text = {
		trigger = {
			has_country_flag = BLR_show_triunionist
		}
		localization_key = BLR_tri_orgs
	}
	text = {
		localization_key = Blank
	}
}
defined_text = {
	name = GetMemberDescsBLR
	text = {
		trigger = {
			has_country_flag = BLR_show_blr_nationalist
		}
		localization_key = BLR_nat_descs
	}
	text = {
		trigger = {
			has_country_flag = BLR_show_autonomist
		}
		localization_key = BLR_aut_descs
	}
	text = {
		trigger = {
			has_country_flag = BLR_show_bulak
		}
		localization_key = BLR_bulak_descs
	}
	text = {
		trigger = {
			has_country_flag = BLR_show_triunionist
		}
		localization_key = BLR_tri_descs
	}
	text = {
		localization_key = Blank
	}
}
defined_text = {
	name = GetBLRFarRight
	text = {
		trigger = {
			has_country_flag = BLR_Fascists_Rturn
		}
		localization_key = BLR_FarRight_Parties
	}
	text = {
		localization_key = Blank
	}
}
defined_text = {
	name = GetBLRRightDescs
	text = {
		trigger = {
			has_country_flag = BLR_Fascists_Rturn
		}
		localization_key = BLR_FarRight_Descs
	}
	text = {
		localization_key = Blank
	}
}
defined_text = {
	name = GetLeftistRada
	text = {
		trigger = {
			has_country_flag = BLR_Rada_Returns
		}
		localization_key = BLR_LeftRada
	}
	text = {
		localization_key = Blank
	}
}
defined_text = {
	name = GetBLRRadaDesc
	text = {
		trigger = {
			has_country_flag = BLR_Rada_Returns
		}
		localization_key = BLR_LeftRadaDesc
	}
	text = {
		localization_key = Blank
	}
}
defined_text = {
	name = GetLargestFactionBLR
	text = {
		trigger = {
			OR = {
				check_variable = {
					BLR_bulak_influence > BLR_nationalist_influence
				}
				check_variable = {
					BLR_bulak_influence = BLR_nationalist_influence
				}
			}
			OR = {
				check_variable = {
					BLR_bulak_influence > BLR_autonomist_influence
				}
				check_variable = {
					BLR_bulak_influence = BLR_autonomist_influence
				}
			}
			OR = {
				check_variable = {
					BLR_bulak_influence > BLR_triunionist_influence
				}
				check_variable = {
					BLR_bulak_influence = BLR_triunionist_influence
				}
			}
		}
		localization_key = BLR_bulak_biggest
	}
	text = {
		trigger = {
			OR = {
				check_variable = {
					BLR_nationalist_influence > BLR_autonomist_influence
				}
				check_variable = {
					BLR_nationalist_influence = BLR_autonomist_influence
				}
			}
			OR = {
				check_variable = {
					BLR_nationalist_influence > BLR_triunionist_influence
				}
				check_variable = {
					BLR_nationalist_influence = BLR_triunionist_influence
				}
			}
		}
		localization_key = BLR_nationalist_biggest
	}
	text = {
		trigger = {
			OR = {
				check_variable = {
					BLR_autonomist_influence > BLR_triunionist_influence
				}
				check_variable = {
					BLR_autonomist_influence = BLR_triunionist_influence
				}
			}
		}
		localization_key = BLR_autonomist_biggest
	}
	text = {
		localization_key = BLR_triunionist_biggest
	}
}
defined_text = {
	name = GetBLRAlly
	text = {
		trigger = {
			is_in_faction_with = POL
		}
		localization_key = BLR_pol_guns
	}
	text = {
		trigger = {
			is_in_faction_with = RUS
		}
		localization_key = BLR_rus_guns
	}
	text = {
		trigger = {
			OR = {
				is_in_faction_with = GER
				is_in_faction_with = WGR
				is_in_faction_with = DDR
			}
		}
		localization_key = BLR_ger_guns
	}
	text = {
		localization_key = BLR_foreign_guns
	}
}
defined_text = {
	name = GetBikeFocus
	text = {
		trigger = {
			tag = BLR
		}
		localization_key = Bike_Focus_BLR
	}
	text = {
		localization_key = Bike_Focus_UKR
	}
}
defined_text = {
	name = GetBikeFocusDesc
	text = {
		trigger = {
			tag = BLR
		}
		localization_key = Bike_Focus_Desc_BLR
	}
	text = {
		localization_key = Bike_Focus_Desc_UKR
	}
}
defined_text = {
	name = GetAlphabet
	text = {
		trigger = {
			has_completed_focus = BLR_2RCW_Poland
		}
		localization_key = BLR_Latin_Alphabet
	}
	text = {
		trigger = {
			has_completed_focus = BLR_2RCW_Russia
		}
		localization_key = BLR_Cyrillic_Alphabet
	}
	text = {
		localization_key = BLR_No_Alphabet
	}
}
defined_text = {
	name = GetAlphabetDesc
	text = {
		trigger = {
			has_completed_focus = BLR_2RCW_Poland
		}
		localization_key = BLR_Latin_Alphabet_desc
	}
	text = {
		trigger = {
			has_completed_focus = BLR_2RCW_Russia
		}
		localization_key = BLR_Cyrillic_Alphabet_desc
	}
	text = {
		localization_key = BLR_No_Alphabet_desc
	}
}
