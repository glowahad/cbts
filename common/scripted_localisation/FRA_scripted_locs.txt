# Events

defined_text = {
	name = france.401_eng
	text = {
		trigger = {
			GER = {
				OR = {
					has_country_flag = GER_hitler_in_power
					has_country_flag = GER_DNVP_in_power
				}
			}
		}
		localization_key = "Compromise With Germany"
	}
	text = {
		localization_key = "Friendship With Germany"
	}
}

# Focuses

defined_text = {
	name = FRA_compromise_germany_text
	text = {
		trigger = {
			GER = {
				OR = {
					has_country_flag = GER_hitler_in_power
					has_country_flag = GER_DNVP_in_power
				}
			}
		}
		localization_key = "Compromise With Germany"
	}
	text = {
		localization_key = "Friendship With Germany"
	}
}

# Faction System
defined_text = {
	name = FRA_influence_faction
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 2 }
		}
		localization_key = FRA_CdF_influence_faction
	}
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 1 }
		}
		localization_key = FRA_JP_influence_faction
	}
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 5 }
		}
		localization_key = FRA_PPF_influence_faction
	}
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 4 }
		}
		localization_key = FRA_UPR_influence_faction
	}
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 3 }
		}
		localization_key = FRA_FP_influence_faction
	}
}

defined_text = {
	name = FRA_FactionTitle
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 2 }
		}
		localization_key = FRA_neutrality_party_long
	}
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 1 }
		}
		localization_key = FRA_nationalism_party_long
	}
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 5 }
		}
		localization_key = FRA_french_popular_party_long
	}
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 4 }
		}
		localization_key = FRA_federation_republicaine_long
	}
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 3 }
		}
		localization_key = FRA_front_paysan_long
	}
}

defined_text = {
	name = FRA_FactionLeaderName
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 2 }
		}
		localization_key = "François de La Rocque"
	}
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 1 }
		}
		localization_key = "Pierre Taittinger"
	}
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 5 }
		}
		localization_key = "Jacques Doriot"
	}
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 4 }
		}
		localization_key = "Philippe Henriot"
	}
	text = {
		trigger = {
			check_variable = { FRA_faction_show = 3 }
		}
		localization_key = "Henri Drogere"
	}
}

defined_text = {
	name = GetLargestFactionFRA
	text = {
		trigger = {
			check_variable = {
				var = var:FRA_JP_influence
				value = var:FRA_CdF_influence
				compare = greater_than_or_equals
			}
			check_variable = {
				var = var:FRA_JP_influence
				value = var:FRA_FP_influence
				compare = greater_than_or_equals
			}
			check_variable = {
				var = var:FRA_JP_influence
				value = var:FRA_UPR_influence
				compare = greater_than_or_equals
			}
			check_variable = {
				var = var:FRA_JP_influence
				value = var:FRA_PPF_influence
				compare = greater_than_or_equals
			}
		}
		localization_key = FRA_JP
	}
	text = {
		trigger = {
			check_variable = {
				var = var:FRA_CdF_influence
				value = var:FRA_JP_influence
				compare = greater_than_or_equals
			}
			check_variable = {
				var = var:FRA_CdF_influence
				value = var:FRA_FP_influence
				compare = greater_than_or_equals
			}
			check_variable = {
				var = var:FRA_CdF_influence
				value = var:FRA_UPR_influence
				compare = greater_than_or_equals
			}
			check_variable = {
				var = var:FRA_CdF_influence
				value = var:FRA_PPF_influence
				compare = greater_than_or_equals
			}
		}
		localization_key = FRA_CdF
	}
	text = {
		trigger = {
			check_variable = {
				var = var:FRA_FP_influence
				value = var:FRA_JP_influence
				compare = greater_than_or_equals
			}
			check_variable = {
				var = var:FRA_FP_influence
				value = var:FRA_CdF_influence
				compare = greater_than_or_equals
			}
			check_variable = {
				var = var:FRA_FP_influence
				value = var:FRA_UPR_influence
				compare = greater_than_or_equals
			}
			check_variable = {
				var = var:FRA_FP_influence
				value = var:FRA_PPF_influence
				compare = greater_than_or_equals
			}
		}
		localization_key = FRA_FP
	}
	text = {
		trigger = {
			check_variable = {
				var = var:FRA_UPR_influence
				value = var:FRA_JP_influence
				compare = greater_than_or_equals
			}
			check_variable = {
				var = var:FRA_UPR_influence
				value = var:FRA_CdF_influence
				compare = greater_than_or_equals
			}
			check_variable = {
				var = var:FRA_UPR_influence
				value = var:FRA_FP_influence
				compare = greater_than_or_equals
			}
			check_variable = {
				var = var:FRA_UPR_influence
				value = var:FRA_PPF_influence
				compare = greater_than_or_equals
			}
		}
		localization_key = FRA_UPR
	}
	text = {
		localization_key = FRA_PPF
	}
}

# Parliament
defined_text = {
	name = FRA_yes_no_vote
	text = {
		trigger = {
			has_country_flag = law_passing
			has_country_flag = yes_referendum
		}
		localization_key = FRA_parliament_yes_referendum
	}
	text = {
		trigger = {
			FRA_has_parliament_win = yes
			has_country_flag = law_passing
		}
		localization_key = FRA_parliament_green
	}
	text = {
		trigger = {
			NOT = {
				FRA_has_parliament_win = yes
			}
			has_country_flag = law_passing
		}
		localization_key = FRA_parliament_red
	}
	text = {
		trigger = {
			NOT = {
				has_country_flag = law_passing
			}
		}
		localization_key = FRA_parliament_nothing
	}
}

defined_text = {
	name = FRA_senate_mayority_1
	text = {
		trigger = {
			has_country_flag = senate_mechanic
		}
		localization_key = FRA_senate_mayority
	}
}

defined_text = {
	name = FRA_senate_vote
	text = {
		trigger = {
			has_country_flag = senate_mechanic
			has_country_flag = law_passing
			NOT = {
				has_country_flag = senate_yes
				has_country_flag = senate_no
			}
		}
		localization_key = FRA_senate_nothing
	}
	text = {
		trigger = {
			has_country_flag = senate_mechanic
			has_country_flag = law_passing
			has_country_flag = senate_yes
		}
		localization_key = FRA_senate_support
	}
	text = {
		trigger = {
			has_country_flag = senate_mechanic
			has_country_flag = law_passing
			has_country_flag = senate_no
		}
		localization_key = FRA_senate_against
	}
}

defined_text = {
	name = FRA_communist_seats_1
	text = {
		trigger = {
			NOT = {
				has_country_flag = communist_ban
				check_variable =  { communist_seats = 0 }
			}
		}
		localization_key = FRA_communist_seats
	}
}

defined_text = {
	name = FRA_radsoc_seats_1
	text = {
		trigger = {
			NOT = {
				has_country_flag = radsoc_ban
				check_variable =  { radsoc_seats = 0 }
			}
		}
		localization_key = FRA_radsoc_seats
	}
}

defined_text = {
	name = FRA_demsoc_seats_1
	text = {
		trigger = {
			NOT = {
				has_country_flag = demsoc_ban
				check_variable =  { demsoc_seats = 0 }
			}
		}
		localization_key = FRA_demsoc_seats
	}
}

defined_text = {
	name = FRA_socdem_seats_1
	text = {
		trigger = {
			NOT = {
				has_country_flag = socdem_ban
				check_variable =  { socdem_seats = 0 }
			}
		}
		localization_key = FRA_socdem_seats
	}
}

defined_text = {
	name = FRA_soclib_seats_1
	text = {
		trigger = {
			NOT = {
				has_country_flag = soclib_ban
				check_variable =  { soclib_seats = 0 }
			}
		}
		localization_key = FRA_soclib_seats
	}
}

defined_text = {
	name = FRA_marlib_seats_1
	text = {
		trigger = {
			NOT = {
				has_country_flag = marlib_ban
				check_variable =  { marlib_seats = 0 }
			}
		}
		localization_key = FRA_marlib_seats
	}
}

defined_text = {
	name = FRA_centre_seats_1
	text = {
		trigger = {
			NOT = {
				has_country_flag = centre_ban
				check_variable =  { centre_seats = 0 }
			}
		}
		localization_key = FRA_centre_seats
	}
}

defined_text = {
	name = FRA_cons_seats_1
	text = {
		trigger = {
			NOT = {
				has_country_flag = cons_ban
				check_variable =  { cons_seats = 0 }
			}
		}
		localization_key = FRA_cons_seats
	}
}

defined_text = {
	name = FRA_aut_seats_1
	text = {
		trigger = {
			NOT = {
				has_country_flag = aut_ban
				check_variable =  { aut_seats = 0 }
			}
		}
		localization_key = FRA_aut_seats
	}
}

defined_text = {
	name = FRA_mon_seats_1
	text = {
		trigger = {
			NOT = {
				has_country_flag = mon_ban
				check_variable =  { mon_seats = 0 }
			}
		}
		localization_key = FRA_mon_seats
	}
}

defined_text = {
	name = FRA_nat_seats_1
	text = {
		trigger = {
			NOT = {
				has_country_flag = nat_ban
				check_variable =  { nat_seats = 0 }
			}
		}
		localization_key = FRA_nat_seats
	}
}

defined_text = {
	name = FRA_fas_seats_1
	text = {
		trigger = {
			NOT = {
				has_country_flag = fas_ban
				check_variable =  { fas_seats = 0 }
			}
		}
		localization_key = FRA_fas_seats
	}
}