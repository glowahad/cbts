defined_text = {
	name = GetCampaignIdeology
	text = {
		trigger = {
			check_variable = {
				WGR_rally_ideology = 0
			}
		}
		localization_key = WGR_Bolshevik_Campaign
	}
	text = {
		trigger = {
			check_variable = {
				WGR_rally_ideology = 1
			}
		}
		localization_key = WGR_RevSoc_Campaign
	}
	text = {
		trigger = {
			OR = {
				check_variable = {
					WGR_rally_ideology = 2
				}
				check_variable = {
					WGR_rally_ideology = 3
				}
			}
		}
		localization_key = WGR_DemSoc_Campaign
	}
	text = {
		trigger = {
			check_variable = {
				WGR_rally_ideology = 4
			}
		}
		localization_key = WGR_SocLib_Campaign
	}
	text = {
		trigger = {
			check_variable = {
				WGR_rally_ideology = 5
			}
		}
		localization_key = WGR_MarLib_Campaign
	}
	text = {
		trigger = {
			check_variable = {
				WGR_rally_ideology = 6
			}
		}
		localization_key = WGR_LibCon_Campaign
	}
	text = {
		trigger = {
			check_variable = {
				WGR_rally_ideology = 7
			}
		}
		localization_key = WGR_SocCon_Campaign
	}
	text = {
		trigger = {
			check_variable = {
				WGR_rally_ideology = 8
			}
		}
		localization_key = WGR_NatCon_Campaign
	}
	text = {
		trigger = {
			check_variable = {
				WGR_rally_ideology = 9
			}
		}
		localization_key = WGR_AutDes_Campaign
	}
	text = {
		trigger = {
			check_variable = {
				WGR_rally_ideology = 10
			}
		}
		localization_key = WGR_FarAut_Campaign
	}
	text = {
		localization_key = WGR_Fascist_Campaign
	}
}