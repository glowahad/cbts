#Redone by Dskod1, now with even more duct tape

default_peace = {
	enable = {
		always = yes
	}

	annex_randomness = 0 #350
	liberate_randomness = 0 #100
	puppet_randomness = 0 #200
	take_states_randomness = 0 #150
	force_government_randomness = 0 #50
	
	# ROOT = Taker, FROM = Giver
	annex = {
		factor = 10
	}

	# ROOT = Taker, FROM = Liberated
	liberate = {
		factor = 0 #100
	}

	# ROOT = Taker, FROM = Giver
	puppet = {
		factor = 0 #200

	}

	# ROOT = Taker, FROM = State
	take_states = {
		factor = 10
		
		####Priorities####
		modifier = { #Priortize taking claimed or cored land
			factor = 5
			FROM = {
				OR = {
					is_claimed_by = ROOT
					is_core_of = ROOT
				}
			}
		}
		modifier = { #Controls the state
			factor = 5
			FROM = {
				is_controlled_by = ROOT
			}
		}		
		modifier = { #Closer land is better land; 8 total
			factor = 1.25
			capital_scope = {
				distance_to = {
					target = FROM
					value < 1000
				}
			}
		}

		modifier = {
			factor = 1.25
			capital_scope = {
				distance_to = {
					target = FROM
					value < 2000
				}
			}
		}

		modifier = {
			factor = 1.25
			capital_scope = {
				distance_to = {
					target = FROM
					value < 3000
				}
			}
		}

		modifier = {
			factor = 1.25
			capital_scope = {
				distance_to = {
					target = FROM
					value < 4000
				}
			}
		}

		modifier = { #The more you neighbour a state, the more you will take it #8 Total
			factor = 2
			FROM = {
				OR = {
					num_owned_neighbour_states = {
						count > 1
						owner = ROOT
					}
					num_claimed_peace_conference_neighbour_states = {
						count > 1
						owner = ROOT
					}
				}
			}
		}

		modifier = {
			factor = 2
			FROM = {
				OR = {
					num_owned_neighbour_states = {
						count > 2
						owner = ROOT
					}
					num_claimed_peace_conference_neighbour_states = {
						count > 2
						owner = ROOT
					}
				}
			}
		}

		modifier = {
			factor = 2
			FROM = {
				OR = {
					num_owned_neighbour_states = {
						count > 3
						owner = ROOT
					}
					num_claimed_peace_conference_neighbour_states = {
						count > 3
						owner = ROOT
					}
				}
			}
		}

		modifier = {
			factor = 2
			FROM = {
				OR = {
					num_owned_neighbour_states = {
						count > 4
						owner = ROOT
					}
					num_claimed_peace_conference_neighbour_states = {
						count > 4
						owner = ROOT
					}
				}
			}
		}

		modifier = {
			factor = 2
			FROM = {
				OR = {
					num_owned_neighbour_states = {
						count > 5
						owner = ROOT
					}
					num_claimed_peace_conference_neighbour_states = {
						count > 5
						owner = ROOT
					}
				}
			}
		}
		modifier = { #prioritize states on the same continent as capital
			factor = 5
			OR = {
				AND = { #Europe
					FROM = { is_on_continent = europe }
					ROOT = {
						capital_scope = {
							is_on_continent = europe
						}
					}
				}
				AND = { #Asia and/or Australia
					OR = {
						FROM = { is_on_continent = asia }
						FROM = { is_on_continent = australia }
					}
					ROOT = {
						capital_scope = {
							OR = {
								is_on_continent = asia
								is_on_continent = australia
							}
						}
					}
				}
				AND = { #North America
					FROM = { is_on_continent = north_america }
					ROOT = {
						capital_scope = {
							is_on_continent = north_america
						}
					}
				}
				AND = { #South America
					FROM = { is_on_continent = south_america }
					ROOT = {
						capital_scope = {
							is_on_continent = south_america
						}
					}
				}
				AND = { #Middle East
					FROM = { is_on_continent = middle_east }
					ROOT = {
						capital_scope = {
							is_on_continent = middle_east
						}
					}
				}
				AND = { #Africa
					FROM = { is_on_continent = africa }
					ROOT = {
						capital_scope = {
							is_on_continent = africa
						}
					}
				}
			}
		}	
		
		modifier = { #Prevents states being taken without connection to them
			factor = 0		
			NOT = { FROM = { is_claimed_by = ROOT } }
			NOT = { FROM = { is_core_of = ROOT } }
			NOT = {
				FROM = {
					any_neighbor_state = {
						OR = {
							owner = { tag = ROOT }
								has_claimed_state_in_peace_conference = ROOT
						}
					}
				}
			}
			OR = {
				NOT = {
					FROM = {
						OR = {
							is_island_state = yes
								is_coastal = yes
						}
					}
				}
				NOT = { any_owned_state = { is_coastal = yes } }
			}

		}

		modifier = { #Only take claimed/core land if a puppet.
			factor = 0
			ROOT = {
				is_subject = yes
			}
			FROM = {
				NOT = {
					OR = {
						is_claimed_by = ROOT
						is_core_of = ROOT
					}
				}
			}
		}
		modifier = { #Do not take land if an ally has cores or claim and you do not
			factor = 0
			ROOT = {
				is_in_faction = yes
			}
			FROM = {
				NOT = {
					OR = {
						is_claimed_by = ROOT
						is_core_of = ROOT
					}
				}
			}
			any_allied_country = {
				is_in_peace_conference = yes
				FROM = {
					OR = {
						is_claimed_by = PREV
						is_core_of = PREV
					}
				}
			}
		}
	}
		
	force_government = {
		factor = 0 #10 - neutrals should never do this
	}
}