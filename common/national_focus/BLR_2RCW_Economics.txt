#12 focuses here
#s_team337
shared_focus = {
	id = BLR_2RCW_Economics_Start
	icon = GFX_focus_Renewed_Arms
	#prerequisite = { focus = BLR_2RCW_Economics_Start }
	x = 11
	y = 0
	#relative_position_id = BLR_2RCW_Economics_Start
	cost = 5.15
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		MOVE_UP_WAR_ECONOMY = yes
	}
}
shared_focus = {
	id = BLR_2RCW_Decollectivization
	icon = GFX_focus_Agriculture
	prerequisite = {
		focus = BLR_2RCW_Economics_Start
	}
	x = 0
	y = 1
	relative_position_id = BLR_2RCW_Economics_Start
	cost = 3
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
		has_full_control_of_state = 206
	}
	bypass = {
		
	}
	completion_reward = {
		add_timed_idea = {
			idea = RUS_decollectivize
			days = 730
		}
	}
}
shared_focus = {
	id = BLR_2RCW_Industrial_Base
	icon = GFX_focus_Industrialization
	prerequisite = {
		focus = BLR_2RCW_Decollectivization
	}
	x = -2
	y = 2
	relative_position_id = BLR_2RCW_Economics_Start
	cost = 7.15
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = BLR_2RCW_Legalize_Private_Economics
	icon = GFX_focus_Capitalist_Systems
	prerequisite = {
		focus = BLR_2RCW_Industrial_Base
	}
	x = -1
	y = 3
	relative_position_id = BLR_2RCW_Economics_Start
	cost = 2.15
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_ideas = RUS_capitalism
	}
}
shared_focus = {
	id = BLR_2RCW_Infra1
	icon = GFX_focus_Trains
	prerequisite = {
		focus = BLR_2RCW_Industrial_Base
	}
	x = -3
	y = 3
	relative_position_id = BLR_2RCW_Economics_Start
	cost = 5.72
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = BLR_2RCW_Infra2
	icon = GFX_focus_Highways
	prerequisite = {
		focus = BLR_2RCW_Infra1
	}
	prerequisite = {
		focus = BLR_2RCW_Legalize_Private_Economics
	}
	x = -2
	y = 4
	relative_position_id = BLR_2RCW_Economics_Start
	cost = 5.72
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = BLR_2RCW_Focus_on_Agriculture
	icon = GFX_focus_Bread_Trade
	prerequisite = {
		focus = BLR_2RCW_Decollectivization
	}
	x = 0
	y = 2
	relative_position_id = BLR_2RCW_Economics_Start
	cost = 5
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_political_power = -50
		if = {
			limit = {
				has_idea = RUS_decollectivize
			}
			modify_timed_idea = {
				idea = RUS_decollectivize
				days = 365
			}
		}
		else = {
			add_timed_idea = {
				idea = RUS_decollectivize
				days = 365
			}
		}
	}
}
shared_focus = {
	id = BLR_2RCW_Keep_Troops_Fed
	icon = GFX_focus_Agriculture_2
	prerequisite = {
		focus = BLR_2RCW_Focus_on_Agriculture
	}
	x = 0
	y = 4
	relative_position_id = BLR_2RCW_Economics_Start
	cost = 4.29
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_ideas = RUS_feed_troops
	}
}
shared_focus = {
	id = BLR_2RCW_Military_Capabilities
	icon = GFX_focus_Firearms
	prerequisite = {
		focus = BLR_2RCW_Decollectivization
	}
	x = 2
	y = 2
	relative_position_id = BLR_2RCW_Economics_Start
	cost = 6.43
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = BLR_2RCW_Convert
	icon = GFX_goal_generic_construct_mil_factory
	prerequisite = {
		focus = BLR_2RCW_Military_Capabilities
	}
	x = 1
	y = 3
	relative_position_id = BLR_2RCW_Economics_Start
	cost = 5.29
	ai_will_do = {
		factor = 10
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				industrial_complex > 0
			}
			remove_building = {
				type = industrial_complex
				level = 1
			}
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = BLR_2RCW_Military_Capabilities2
	icon = GFX_focus_Military_Intervention
	prerequisite = {
		focus = BLR_2RCW_Convert
	}
	x = 2
	y = 4
	relative_position_id = BLR_2RCW_Economics_Start
	cost = 6
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
				arms_factory > 0
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = BLR_2RCW_Foreign_Loans
	icon = GFX_focus_Grab_Money
	prerequisite = {
		focus = BLR_2RCW_Infra2
	}
	prerequisite = {
		focus = BLR_2RCW_Keep_Troops_Fed
	}
	prerequisite = {
		focus = BLR_2RCW_Military_Capabilities2
	}
	x = 0
	y = 5
	relative_position_id = BLR_2RCW_Economics_Start
	cost = 2.86
	ai_will_do = {
		factor = 20
	}
	available = {
		has_global_flag = Russian_Civil_War
		is_in_faction = yes
	}
	bypass = {
		
	}
	completion_reward = {
		add_political_power = 60
		add_ideas = RUS_foreign_loans
	}
}