focus_tree = {
	id = spain_revolution_tree
	country = {
		factor = 0
		modifier = {
			add = 0
			tag = SPR
		}
	}
	continuous_focus_position = { x = 50 y = 2500 }
	default = no
	shared_focus = SPR_amenaza_socialista
	shared_focus = SPR_review
	shared_focus = SPR_depression
	shared_focus = SPR_postwar
	shared_focus = SPR_world
}