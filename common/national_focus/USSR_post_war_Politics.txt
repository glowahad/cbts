#12 focuses here
#s_team337
shared_focus = {
	id = SOV_GPW_War_Is_Over
	icon = GFX_focus_Pacifist_State
	#prerequisite = { focus = SOV_GPW_War_Is_Over }
	x = 2
	y = 0
	#relative_position_id = SOV_GPW_War_Is_Over
	cost = 4.29
	ai_will_do = {
		factor = 1200
		modifier = {
			has_completed_focus = SOV_Post_4FYP_Start
			factor = 0
		}
	}
	available = {
		has_war = no
	}
	bypass = {
		
	}
	completion_reward = {
		add_political_power = 50
	}
}
shared_focus = {
	id = SOV_Post_Collectivize_New_Lands
	icon = GFX_focus_Ecomomic_Banditry
	prerequisite = { 
		focus = SOV_GPW_War_Is_Over
	}
	x = -1
	y = 1
	relative_position_id = SOV_GPW_War_Is_Over
	cost = 5
	ai_will_do = {
		factor = 1500
	}
	available = {
		has_war = no
	}
	bypass = {
		
	}
	completion_reward = {
		add_timed_idea = {
			idea = SOV_Post_War_Collectivization
			days = 1850
		}
	}
}
shared_focus = {
	id = SOV_Post_No_Freedom
	icon = GFX_focus_Anti_Democracy
	prerequisite = { 
		focus = SOV_Post_Collectivize_New_Lands
	}
	x = -2
	y = 2
	relative_position_id = SOV_GPW_War_Is_Over
	cost = 5.58
	ai_will_do = {
		factor = 1200
	}
	available = {
		has_war = no
	}
	bypass = {
		
	}
	completion_reward = {
		add_stability = 0.05
		add_political_power = -40
		add_popularity = {
			ideology = communism
			popularity = -0.03
		}
	}
}
shared_focus = {
	id = SOV_Post_Anti_Intellectualism
	icon = GFX_focus_Communist_Repression
	prerequisite = { 
		focus = SOV_GPW_War_Is_Over
	}
	x = 1
	y = 1
	relative_position_id = SOV_GPW_War_Is_Over
	cost = 3.86
	ai_will_do = {
		factor = 1400
	}
	available = {
		has_war = no
	}
	bypass = {
		
	}
	completion_reward = {
		add_stability = 0.04
		if = {
			limit = {
				has_idea = SOV_think_free
			}
			remove_ideas = SOV_think_free
		}
		else = {
			add_research_slot = -1
		}
	}
}
shared_focus = {
	id = SOV_Post_Lysenkoism
	icon = GFX_focus_Communist_Scientists
	prerequisite = { 
		focus = SOV_Post_Anti_Intellectualism
	}
	x = 2
	y = 2
	relative_position_id = SOV_GPW_War_Is_Over
	cost = 5
	ai_will_do = {
		factor = 1000
	}
	available = {
		has_war = no
	}
	bypass = {
		
	}
	completion_reward = {
		add_political_power = 57
		add_ideas = SOV_Lysenkoism
	}
}
shared_focus = {
	id = SOV_Post_Big_Deal
	icon = GFX_focus_Risky_Deal
	prerequisite = { 
		focus = SOV_Post_Collectivize_New_Lands
		focus = SOV_Post_Anti_Intellectualism
	}
	x = 0
	y = 2
	relative_position_id = SOV_GPW_War_Is_Over
	cost = 4.92
	ai_will_do = {
		factor = 2000
	}
	available = {
		has_war = no
	}
	bypass = {
		
	}
	completion_reward = {
		add_popularity = {
			ideology = communism
			popularity = 0.05
		}
	}
}
shared_focus = {
	id = SOV_Post_Deheroization
	icon = GFX_focus_CZE_german_puppet
	prerequisite = { 
		focus = SOV_Post_No_Freedom
	}
	prerequisite = { 
		focus = SOV_Post_Big_Deal
	}
	x = -1
	y = 3
	relative_position_id = SOV_GPW_War_Is_Over
	cost = 5.36
	ai_will_do = {
		factor = 700
	}
	available = {
		has_war = no
	}
	bypass = {
		
	}
	completion_reward = {
		add_war_support = -0.05
		add_political_power = 30
	}
}
shared_focus = {
	id = SOV_Post_Bureaucracy
	icon = GFX_focus_generic_soviet_politics
	prerequisite = { 
		focus = SOV_Post_Deheroization
	}
	x = -1
	y = 5
	relative_position_id = SOV_GPW_War_Is_Over
	cost = 6
	ai_will_do = {
		factor = 750
	}
	available = {
		has_war = no
	}
	bypass = {
		
	}
	completion_reward = {
		add_political_power = 75
		add_stability = 0.06
	}
}
shared_focus = {
	id = SOV_Post_Deproletarianization
	icon = GFX_focus_Anti_Communist
	prerequisite = { 
		focus = SOV_Post_Big_Deal
	}
	prerequisite = { 
		focus = SOV_Post_Lysenkoism
	}
	x = 1
	y = 3
	relative_position_id = SOV_GPW_War_Is_Over
	cost = 5.36
	ai_will_do = {
		factor = 600
	}
	available = {
		has_war = no
	}
	bypass = {
		
	}
	completion_reward = {
		if = {
			limit = {
				has_idea = SOV_embourgeoisement
			}
			swap_ideas = {
				remove_idea = SOV_embourgeoisement
				add_idea = SOV_big_deal
			}
		}
		else = {
			add_ideas = SOV_deproletarianization
		}
	}
}
shared_focus = {
	id = SOV_Post_Embourgeoisement
	icon = GFX_focus_Rich_Focus
	prerequisite = { 
		focus = SOV_Post_Deproletarianization
	}
	x = 1
	y = 5
	relative_position_id = SOV_GPW_War_Is_Over
	cost = 4.29
	ai_will_do = {
		factor = 800
	}
	available = {
		has_war = no
	}
	bypass = {
	}
	completion_reward = {
		if = {
			limit = {
				has_idea = SOV_deproletarianization
			}
			swap_ideas = {
				remove_idea = SOV_deproletarianization
				add_idea = SOV_big_deal
			}
		}
		else = {
			add_ideas = SOV_embourgeoisement
		}
	}
}
shared_focus = {
	id = SOV_Post_Socialist_Realism
	icon = GFX_focus_Union_Workers
	prerequisite = { 
		focus = SOV_Post_Big_Deal
	}
	x = 0
	y = 4
	relative_position_id = SOV_GPW_War_Is_Over
	cost = 4.17
	ai_will_do = {
		factor = 500
	}
	available = {
		has_war = no
	}
	bypass = {
		
	}
	completion_reward = {
		add_political_power = 30
		add_stability = -0.025
	}
}
shared_focus = {
	id = SOV_Post_Return_To_Normalcy
	icon = GFX_focus_eng_concessions_to_the_trade_unions
	prerequisite = { 
		focus = SOV_Post_Bureaucracy
	}
	prerequisite = { 
		focus = SOV_Post_Socialist_Realism
	}
	prerequisite = { 
		focus = SOV_Post_Embourgeoisement
	}
	x = 0
	y = 6
	relative_position_id = SOV_GPW_War_Is_Over
	cost = 5
	ai_will_do = {
		factor = 400
	}
	available = {
		has_war = no
	}
	bypass = {
		
	}
	completion_reward = {
		add_stability = 0.10
	}
}
