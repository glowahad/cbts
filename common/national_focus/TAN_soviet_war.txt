shared_focus = {
	id = TAN_russia_at_war
	icon = GFX_focus_Centralize_Provinces_GER
	x = 24
	y = 0
	cost = 1
	ai_will_do = {
		factor = 8
	}
	available = {
		is_in_faction_with = SOV
		has_war = yes
		SOV = {
			exists = yes
			OR = {
				has_war_with = GER
				has_war_with = ITA
				has_war_with = ENG
				has_war_with = JAP
				has_war_with = USA
				has_war_with = FRA
				surrender_progress > 0.15

			}
		}
	}	
	bypass = {
	}
	completion_reward = {
		country_event = { 
			id = tuva.9
		}
	}
}

shared_focus = {
	id = TAN_take_in_refugees
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_russia_at_war}
	x = -2
	y = 1
	relative_position_id = TAN_russia_at_war
	cost = 7
	ai_will_do = {
		factor = 8
	}
	available = {

	}
	bypass = {
	}
	completion_reward = {
		custom_effect_tooltip = TAN_gain_ten_influence
		add_to_variable = {
			var = TAN_soviet_influence
			value = 10
		}
		329 = { 
			add_manpower = 10000
		}
	}
}

shared_focus = {
	id = TAN_send_livestock
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_russia_at_war}
	x = 0
	y = 1
	relative_position_id = TAN_russia_at_war
	cost = 5
	ai_will_do = {
		factor = 8
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		custom_effect_tooltip = TAN_lose_three_influence
		add_to_variable = {
			var = TAN_soviet_influence
			value = -3
		}
		add_timed_idea = {
    			idea = TAN_livestock_gift_TAN
    			days = 180
		}
		SOV = {
			country_event = {
				id = tuva.70
			}
		}
	}
}

shared_focus = {
	id = TAN_defend_tuva
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_russia_at_war}
	x = 2
	y = 1
	relative_position_id = TAN_russia_at_war
	cost = 7
	ai_will_do = {
		factor = 8
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		329 = {
			add_building_construction = {
    				type = bunker
    				level = 1
    				instant_build = yes
    				province = 10627
			}
		}
	}
}

shared_focus = {
	id = TAN_provide_an_industry_refuge
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_take_in_refugees}
	x = -3
	y = 2
	relative_position_id = TAN_russia_at_war
	cost = 7
	ai_will_do = {
		factor = 8
	}
	available = {
		controls_state = 329
	}
	bypass = {
	}
	completion_reward = {
		custom_effect_tooltip = TAN_we_may_gain_or_lose_five_influence_if_USSR_accepts
		custom_effect_tooltip = TAN_ask_sov_for_industry_tt

		hidden_effect = {
			SOV = {
				country_event = {
					id = tuva.71
				}
			}
		}		
	}
}

shared_focus = {
	id = TAN_transfer_the_gold
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_send_livestock}
	x = -1
	y = 2
	relative_position_id = TAN_russia_at_war
	cost = 5
	ai_will_do = {
		factor = 8
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		custom_effect_tooltip = TAN_lose_three_influence
		add_to_variable = {
			var = TAN_soviet_influence
			value = -3
		}
		SOV = {
			country_event = {
				id = tuva.74
			}
		}
	}
}

shared_focus = {
	id = TAN_support_ten_fighters
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_send_livestock}
	x = 1
	y = 2
	relative_position_id = TAN_russia_at_war
	cost = 7
	ai_will_do = {
		factor = 8
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		custom_effect_tooltip = TAN_lose_three_influence
		add_to_variable = {
			var = TAN_soviet_influence
			value = -3
		}
		SOV = {
			country_event = { 
				id = tuva.75
			}
		}
	}
}

shared_focus = {
	id = TAN_tuvan_combat_unit
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_defend_tuva}
	x = 3
	y = 2
	relative_position_id = TAN_russia_at_war
	cost = 7
	ai_will_do = {
		factor = 8
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		custom_effect_tooltip = TAN_lose_three_influence
		add_to_variable = {
			var = TAN_soviet_influence
			value = -3
		}
		SOV = {
			country_event = { 
				id = tuva.20
			}
		}
	}
}

shared_focus = {
	id = TAN_defend_the_borders
	icon = GFX_focus_Centralize_Provinces_GER

	prerequisite = { focus = TAN_provide_an_industry_refuge}


	x = -4
	y = 3
	relative_position_id = TAN_russia_at_war
	cost = 7
	ai_will_do = {
		factor = 8
	}
	available = {
		is_in_faction_with = SOV
		has_war = yes
		SOV = {
			exists = yes
		}
	}
	bypass = {
	}

	completion_reward = {
		custom_effect_tooltip = TAN_bunkers_in_the_west_tt
		hidden_effect = {
			329 = {
				add_building_construction = {
    					type = bunker
    					level = 1
    					instant_build = yes
    					province = 7819
				}
			}
			329 = {
				add_building_construction = {
    					type = bunker
    					level = 1
    					instant_build = yes
    					province = 7804
				}
			}
			329 = {
				add_building_construction = {
    					type = bunker
    					level = 1
    					instant_build = yes
    					province = 1676
				}
			}
			329 = {
				add_building_construction = {
    					type = bunker
    					level = 1
    					instant_build = yes
    					province = 10646
				}
			}
		}

	}
}

shared_focus = {
	id = TAN_infantry_equipment_stockpiling
	icon = GFX_focus_Centralize_Provinces_GER

	prerequisite = { focus = TAN_tuvan_combat_unit}


	x = 4
	y = 3
	relative_position_id = TAN_russia_at_war
	cost = 5
	ai_will_do = {
		factor = 8
	}
	available = {
		is_in_faction_with = SOV
		has_war = yes
		SOV = {
			exists = yes
		}
	}
	bypass = {
	}

	completion_reward = {
		add_equipment_to_stockpile = {
    			type = infantry_equipment
    			amount = 1500
    			producer = SOV
		}
	}
}

shared_focus = {
	id = TAN_join_the_USSR
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_tuvan_combat_unit}
	prerequisite = { focus = TAN_support_ten_fighters}
	prerequisite = { focus = TAN_transfer_the_gold}
	prerequisite = { focus = TAN_provide_an_industry_refuge}


	x = 0
	y = 3
	relative_position_id = TAN_russia_at_war
	cost = 1
	ai_will_do = {
		factor = 8
	}
	available = {
		is_in_faction_with = SOV
		has_war = yes
		SOV = {
			exists = yes
		}
	}
	bypass = {
	}

	completion_reward = {
		IF = {
			limit = {
				is_ai = yes
			}
			custom_effect_tooltip = TAN_we_will_die_if_USSR_accepts_tt

			SOV = {
				country_event = { 
					id = tuva.65
					days = 3
				}

			}
			else = {
				custom_effect_tooltip = TAN_we_will_die_if_USSR_accepts_player_tt
				SOV = {
					country_event = { 
						id = tuva.67
						days = 3
					}

				}
			}
		}
	}
}

shared_focus = {
	id = TAN_adopt_cyrillic_alphabet
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_join_the_USSR}



	x = -2
	y = 4
	relative_position_id = TAN_russia_at_war
	cost = 5
	ai_will_do = {
		factor = 8
	}
	available = {
		has_war = no
		is_subject_of = SOV
		has_country_flag = TAN_soviet_integration

	}
	bypass = {
	}
	completion_reward = {
		country_event = {
			id = tuva.77
		}

	}
}

shared_focus = {
	id = TAN_reform_the_TPRP
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_join_the_USSR}



	x = 0
	y = 4
	relative_position_id = TAN_russia_at_war
	cost = 3
	ai_will_do = {
		factor = 8
	}
	available = {
		has_war = no
		is_subject_of = SOV
		has_country_flag = TAN_soviet_integration


	}
	bypass = {
	}
	completion_reward = {
		add_stability = 0.05
		add_political_power = 25
		
	}
}

shared_focus = {
	id = TAN_further_russian_immigration
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_join_the_USSR}



	x = 2
	y = 4
	relative_position_id = TAN_russia_at_war
	cost = 7
	ai_will_do = {
		factor = 8
	}
	available = {
		has_war = no
		is_subject_of = SOV
		has_country_flag = TAN_soviet_integration

		controls_state = 329
	}
	bypass = {
	}
	completion_reward = {
		329 = { 
			add_manpower = 15000
		}
	}
}

shared_focus = {
	id = TAN_adopt_soviet_tech
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_reform_the_TPRP}
	prerequisite = { focus = TAN_adopt_cyrillic_alphabet}



	x = -1
	y = 5
	relative_position_id = TAN_russia_at_war
	cost = 7
	ai_will_do = {
		factor = 8
	}
	available = {
		has_war = no
		is_subject_of = SOV
		has_country_flag = TAN_soviet_integration

	}
	bypass = {
	}
	completion_reward = {
		custom_effect_tooltip = TAN_inherit_sov_tech_tt
		hidden_effect = {
			inherit_technology = SOV
		}	
	}
}

shared_focus = {
	id = TAN_soviet_schools
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_adopt_cyrillic_alphabet}



	x = -3
	y = 5
	relative_position_id = TAN_russia_at_war
	cost = 7
	ai_will_do = {
		factor = 8
	}
	available = {
		has_war = no
		is_subject_of = SOV
		has_country_flag = TAN_soviet_integration

	}
	bypass = {
	}
	completion_reward = {
		add_timed_idea = {
			idea = TAN_soviet_schools
			days = 300
		}

	}
}
shared_focus = {
	id = TAN_industrialization_of_tuva
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_further_russian_immigration}



	x = 3
	y = 5
	relative_position_id = TAN_russia_at_war
	cost = 7
	ai_will_do = {
		factor = 8
	}
	available = {
		has_war = no
		is_subject_of = SOV
		has_country_flag = TAN_soviet_integration

		controls_state = 329

	}
	bypass = {
	}
	completion_reward = {
		329 = {
			add_extra_state_shared_building_slots = 1

			add_building_construction = {
    				type = industrial_complex
    				level = 1
    				instant_build = yes
			}
		}
	}
}

shared_focus = {
	id = TAN_entrench_the_party
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_further_russian_immigration}
	prerequisite = { focus = TAN_reform_the_TPRP}




	x = 1
	y = 5
	relative_position_id = TAN_russia_at_war
	cost = 5
	ai_will_do = {
		factor = 8
	}
	available = {
		has_war = no
		is_subject_of = SOV
		has_country_flag = TAN_soviet_integration

	}
	bypass = {
	}
	completion_reward = {
		add_popularity = {
    			ideology = communism
    			popularity = 0.1
		}
	}
}

shared_focus = {
	id = TAN_the_model_soviet_state
	icon = GFX_focus_Centralize_Provinces_GER
	prerequisite = { focus = TAN_entrench_the_party}
	prerequisite = { focus = TAN_industrialization_of_tuva}
	prerequisite = { focus = TAN_adopt_soviet_tech}
	prerequisite = { focus = TAN_soviet_schools}




	x = 0
	y = 6
	relative_position_id = TAN_russia_at_war
	cost = 5
	ai_will_do = {
		factor = 8
	}
	available = {
		has_war = no
		is_subject_of = SOV
		has_country_flag = TAN_soviet_integration

	}
	bypass = {
	}
	completion_reward = {
		add_ideas = TAN_model_soviet_state
	}
}

