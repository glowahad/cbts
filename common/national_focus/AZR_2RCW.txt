#44 focuses
focus_tree = {
	id = AZR_2RCW
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = AZR
		}
	}
	default = no
	continuous_focus_position = { x = 50 y = 2700 }
	shared_focus = AZR_2RCW_Politics_Start #13
	shared_focus = AZR_2RCW_Economics_Start #14
	shared_focus = AZR_2RCW_Military_Start #7
	shared_focus = AZR_2RCW_Foreign_Start #10
}