#############################
## Shared Stuff for France ##
#############################

## Pre 1936 Tree ##

# Camille Chautemps #
shared_focus = {
	id = FRA_zay_auriol_plan
	icon = GFX_focus_Zay-Auriol_Plan
	cost = 4.3
	x = 2
	y = 0

	mutually_exclusive = { focus = FRA_deflationary_policies }

	completion_reward = {
		custom_effect_tooltip = FRA_zay_auriol_plan_tt
		add_political_power = 35
		COALITION_add_demsoc = yes
		COALITION_clr_marlib = yes
		set_country_flag = FRA_zay_auriol_plan_gov
	}

	ai_will_do = {
		base = 1
		modifier = {
			is_historical_focus_on = yes
			factor = 0
		}
	}
}

shared_focus = {
	id = FRA_streamlined_tax_code
	icon = GFX_focus_Streamlined_Tax_Code
	cost = 7.2
	prerequisite = { focus = FRA_zay_auriol_plan }
	available = {
		if = {
			limit = {
				has_country_flag = FRA_deflationary_policies
				has_completed_focus = FRA_zay_auriol_plan
			}
			has_country_flag = FRA_zay_auriol_plan_gov
		}
	}
	relative_position_id = FRA_zay_auriol_plan
	x = -1
	y = 1

	completion_reward = {
		add_stability = -0.05
		improve_liquidity_crisis = yes
	}
}

shared_focus = {
	id = FRA_slash_military_funding
	icon = GFX_focus_Reduce_Military_Budget
	cost = 7.2
	prerequisite = { focus = FRA_zay_auriol_plan }
	available = {
		if = {
			limit = {
				has_country_flag = FRA_deflationary_policies
				has_completed_focus = FRA_zay_auriol_plan
			}
			has_country_flag = FRA_zay_auriol_plan_gov
		}
	}
	relative_position_id = FRA_zay_auriol_plan
	x = 1
	y = 1

	completion_reward = {
		add_timed_idea = {
			idea = idea_reduced_mil_spending
			days = 60
		}
		improve_liquidity_crisis = yes
	}
}

shared_focus = {
	id = FRA_modernized_finances
	icon = GFX_focus_Modernized_Finances
	cost = 7.2
	prerequisite = { focus = FRA_slash_military_funding }
	prerequisite = { focus = FRA_streamlined_tax_code }
	available = {
		if = {
			limit = {
				has_country_flag = FRA_deflationary_policies
				has_completed_focus = FRA_zay_auriol_plan
			}
			has_country_flag = FRA_zay_auriol_plan_gov
		}
	}
	relative_position_id = FRA_zay_auriol_plan
	x = 0
	y = 2

	completion_reward = {
		improve_liquidity_crisis = yes
	}
}

shared_focus = {
	id = FRA_office_des_bles
	icon = GFX_focus_Office_Blevin
	cost = 5.8
	prerequisite = { focus = FRA_modernized_finances }
	available = {
		if = {
			limit = {
				has_country_flag = FRA_deflationary_policies
				has_completed_focus = FRA_zay_auriol_plan
			}
			has_country_flag = FRA_zay_auriol_plan_gov
		}
	}
	relative_position_id = FRA_zay_auriol_plan
	x = -2
	y = 3

	completion_reward = {
		custom_effect_tooltip = law_unlocked
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			improve_agricultural_crisis = yes
			worsen_liquidity_crisis = yes
		}
	}
}

shared_focus = {
	id = FRA_caisse_de_liquidation
	icon = GFX_focus_War_Liquidation_Fund
	cost = 5.8
	prerequisite = { focus = FRA_modernized_finances }
	available = {
		if = {
			limit = {
				has_country_flag = FRA_deflationary_policies
				has_completed_focus = FRA_zay_auriol_plan
			}
			has_country_flag = FRA_zay_auriol_plan_gov
		}
	}
	relative_position_id = FRA_zay_auriol_plan
	x = 0
	y = 3

	completion_reward = {
		custom_effect_tooltip = law_unlocked
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			improve_social_crisis = yes
			worsen_liquidity_crisis = yes
		}
	}
}

shared_focus = {
	id = FRA_negotiate_with_strikers
	icon = GFX_focus_Negotiate_Strikes
	cost = 5.8
	prerequisite = { focus = FRA_modernized_finances }
	available = {
		if = {
			limit = {
				has_country_flag = FRA_deflationary_policies
				has_completed_focus = FRA_zay_auriol_plan
			}
			has_country_flag = FRA_zay_auriol_plan_gov
		}
	}
	relative_position_id = FRA_zay_auriol_plan
	x = 1
	y = 4

	completion_reward = {
		add_stability = 0.08

		add_popularity = {
			ideology = fascism
			popularity = 0.005
		}
		add_popularity = {
			ideology = nationalism
			popularity = 0.005
		}
		add_popularity = {
			ideology = monarchism
			popularity = 0.005
		}
		add_popularity = {
			ideology = neutrality
			popularity = 0.005
		}
	}
}

shared_focus = {
	id = FRA_service_national_des_assurances
	icon = GFX_focus_National_Assurance_Service
	cost = 5.8
	prerequisite = { focus = FRA_modernized_finances }
	available = {
		if = {
			limit = {
				has_country_flag = FRA_deflationary_policies
				has_completed_focus = FRA_zay_auriol_plan
			}
			has_country_flag = FRA_zay_auriol_plan_gov
		}
	}
	relative_position_id = FRA_zay_auriol_plan
	x = 3
	y = 3

	completion_reward = {
		custom_effect_tooltip = law_unlocked
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			improve_social_crisis = yes
			worsen_liquidity_crisis = yes
		}
	}
}

shared_focus = {
	id = FRA_caisse_de_grands_travaux
	icon = GFX_focus_Build_Houses
	cost = 5.8
	prerequisite = { focus = FRA_modernized_finances }
	available = {
		if = {
			limit = {
				has_country_flag = FRA_deflationary_policies
				has_completed_focus = FRA_zay_auriol_plan
			}
			has_country_flag = FRA_zay_auriol_plan_gov
		}
	}
	relative_position_id = FRA_zay_auriol_plan
	x = 5
	y = 3

	completion_reward = {
		custom_effect_tooltip = law_unlocked
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			add_ideas = FRA_caisse_grands_travaux
			worsen_liquidity_crisis = yes
		}
	}
}

shared_focus = {
	id = FRA_negotiate_fair_prices
	icon = GFX_focus_Negotiate_Fair_Prices
	cost = 5.8
	prerequisite = { focus = FRA_office_des_bles }
	available = {
		if = {
			limit = {
				has_country_flag = FRA_deflationary_policies
				has_completed_focus = FRA_zay_auriol_plan
			}
			has_country_flag = FRA_zay_auriol_plan_gov
		}
		else = {
			has_country_flag = office_des_bles_passed
		}
	}
	relative_position_id = FRA_zay_auriol_plan
	x = -2
	y = 4

	completion_reward = {
		improve_agricultural_crisis = yes
		add_political_power = -50
	}
}

shared_focus = {
	id = FRA_national_unemployment_policy
	icon = GFX_focus_National_Unemployment_Policy
	cost = 5.8
	prerequisite = { focus = FRA_service_national_des_assurances }
	available = {
		if = {
			limit = {
				has_country_flag = FRA_deflationary_policies
				has_completed_focus = FRA_zay_auriol_plan
			}
			has_country_flag = FRA_zay_auriol_plan_gov
		}
		else = {
			has_country_flag = service_national_des_assurances_passed
		}
	}
	relative_position_id = FRA_zay_auriol_plan
	x = 3
	y = 4

	completion_reward = {
		improve_social_crisis = yes
		worsen_liquidity_crisis = yes
	}
}

shared_focus = {
	id = FRA_budget_devolution
	icon = GFX_focus_Economic_Stimulus
	cost = 5.8
	prerequisite = { focus = FRA_caisse_de_grands_travaux }
	available = {
		if = {
			limit = {
				has_country_flag = FRA_deflationary_policies
				has_completed_focus = FRA_zay_auriol_plan
			}
			has_country_flag = FRA_zay_auriol_plan_gov
		}
		else = {
			has_country_flag = caisse_de_grands_travaux_passed
		}
	}
	relative_position_id = FRA_zay_auriol_plan
	x = 5
	y = 4

	completion_reward = {
		swap_ideas = {
			add_idea = FRA_caisse_grands_travaux1
			remove_idea = FRA_caisse_grands_travaux
		}
	}
}

shared_focus = {
	id = FRA_economic_renewal
	icon = GFX_focus_Economic_Self_Reliance
	cost = 7.2
	prerequisite = { focus = FRA_budget_devolution }
	prerequisite = { focus = FRA_national_unemployment_policy }
	prerequisite = { focus = FRA_negotiate_with_strikers }
	prerequisite = { focus = FRA_negotiate_fair_prices }
	prerequisite = { focus = FRA_caisse_de_liquidation }
	available = {
		if = {
			limit = {
				has_country_flag = FRA_deflationary_policies
				has_completed_focus = FRA_zay_auriol_plan
			}
			has_country_flag = FRA_zay_auriol_plan_gov
		}
		else = {
			has_country_flag = caisse_de_liquidation_passed
		}
	}
	relative_position_id = FRA_zay_auriol_plan
	x = 0
	y = 5

	completion_reward = {
		improve_economic_downturn = yes
		add_popularity = {
			ideology = socialism
			popularity = 0.035
		}
		add_popularity = {
			ideology = social_democracy
			popularity = 0.035
		}
		add_popularity = {
			ideology = social_liberalism
			popularity = 0.035
		}
	}
}

# Liberals with Edouard Daladier #

shared_focus = {
	id = FRA_deflationary_policies
	icon = GFX_focus_reintroduce_countercyclical_policy
	cost = 4.3
	x = 12
	y = 0

	mutually_exclusive = { focus = FRA_zay_auriol_plan }

	completion_reward = {
		add_political_power = 100
		set_country_flag = FRA_deflationary_policies
		if = {
			limit = {
				NOT = { has_active_mission = FRA_mission_liquidity }
			}
			activate_mission = FRA_mission_liquidity
		}
	}

	ai_will_do = {
		base = 1.5
	}
}


shared_focus = {
	id = FRA_reduce_administrative_duties
	icon = GFX_focus_Reduce_Administrative_Duties
	cost = 5.8
	prerequisite = { focus = FRA_deflationary_policies }
	relative_position_id = FRA_deflationary_policies
	x = -1
	y = 2

	completion_reward = {
		custom_effect_tooltip = law_unlocked
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			add_political_power = -50
			improve_liquidity_crisis = yes
		}
		custom_effect_tooltip = FRA_deflation_law_tt
	}
}

shared_focus = {
	id = FRA_cut_state_worker_expenses
	icon = GFX_focus_Cut_state_workers_expenses
	cost = 5.8
	prerequisite = { focus = FRA_deflationary_policies }
	relative_position_id = FRA_deflationary_policies
	x = -2
	y = 1

	completion_reward = {
		custom_effect_tooltip = law_unlocked
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			improve_liquidity_crisis = yes
			worsen_social_crisis = yes
		}
		custom_effect_tooltip = FRA_deflation_law_tt
	}
}

shared_focus = {
	id = FRA_cut_veteran_pensions
	icon = GFX_focus_Cut_Veteran_Pensions
	cost = 5.8
	prerequisite = { focus = FRA_deflationary_policies }
	relative_position_id = FRA_deflationary_policies
	x = 0
	y = 1

	completion_reward = {
		custom_effect_tooltip = law_unlocked
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			add_popularity = {
				ideology = fascism
				popularity = 0.008
			}
			add_popularity = {
				ideology = nationalism
				popularity = 0.008
			}
			add_popularity = {
				ideology = monarchism
				popularity = 0.008
			}
			add_popularity = {
				ideology = neutrality
				popularity = 0.008
			}
			improve_liquidity_crisis = yes
		}
		custom_effect_tooltip = FRA_deflation_law_tt
	}
}

shared_focus = {
	id = FRA_reduce_military_budget
	icon = GFX_focus_Reduce_Military_Budget
	cost = 5.8
	prerequisite = { focus = FRA_deflationary_policies }
	relative_position_id = FRA_deflationary_policies
	x = 2
	y = 1

	completion_reward = {
		custom_effect_tooltip = law_unlocked
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			add_timed_idea = {
				idea = idea_reduced_mil_spending
				days = 30
			}
			improve_liquidity_crisis = yes
		}
		custom_effect_tooltip = FRA_deflation_law_tt
	}
}

shared_focus = {
	id = FRA_encourage_wage_reductions
	icon = GFX_focus_Wage_Reduction
	cost = 5.8
	prerequisite = { focus = FRA_deflationary_policies }
	relative_position_id = FRA_deflationary_policies
	x = 1
	y = 2

	completion_reward = {
		custom_effect_tooltip = law_unlocked
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			improve_liquidity_crisis = yes
			worsen_social_crisis = yes
		}
		custom_effect_tooltip = FRA_deflation_law_tt
	}
}

shared_focus = {
	id = FRA_create_airfrance
	icon = GFX_focus_Airports
	cost = 5.8
	prerequisite = { focus = FRA_deflationary_policies }
	relative_position_id = FRA_deflationary_policies
	x = 4
	y = 1

	completion_reward = {
		16 = {
			add_building_construction = {
				type = air_base
				level = 1
				instant_build = yes
			}
		}
		custom_effect_tooltip = unlocks_idea_ind_comp_tt
		show_ideas_tooltip = airfrance
	}
}

shared_focus = {
	id = FRA_plan_marquet
	icon = GFX_focus_Tax_Incentives
	cost = 5.8
	available = { has_country_flag = FRA_reform_government }
	prerequisite = { focus = FRA_create_airfrance }
	relative_position_id = FRA_deflationary_policies
	x = 4
	y = 2

	completion_reward = {
		improve_economic_downturn = yes
		random_owned_state = {
			limit = {
				is_core_of = ROOT
				is_in_home_area = yes
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		random_owned_state = {
			limit = {
				is_core_of = ROOT
				is_in_home_area = yes
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		random_owned_state = {
			limit = {
				is_core_of = ROOT
				is_in_home_area = yes
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
	}
}

shared_focus = {
	id = FRA_relax_agricultural_prices
	icon = GFX_focus_Price_Reductions
	cost = 5.8
	available = {
		has_country_flag = FRA_reform_end_goverment
	}
	prerequisite = { focus = FRA_plan_marquet }
	relative_position_id = FRA_deflationary_policies
	x = 4
	y = 3

	completion_reward = {
		add_stability = -0.05
		improve_liquidity_crisis = yes
	}
}

shared_focus = {
	id = FRA_fix_agricultural_prices
	icon = GFX_focus_Set_Agricultural_Prices
	cost = 5.8
	available = {
		has_government = social_liberalism
	}
	prerequisite = { focus = FRA_deflationary_policies }
	relative_position_id = FRA_deflationary_policies
	x = -4
	y = 1

	completion_reward = {
		improve_agricultural_crisis = yes
	}
}

shared_focus = {
	id = FRA_fix_buy_surplus
	icon = GFX_focus_Buy_Surplus_Produce
	cost = 5.8
	available = {
		has_government = social_liberalism
	}
	prerequisite = { focus = FRA_fix_agricultural_prices }
	relative_position_id = FRA_deflationary_policies
	x = -5
	y = 2

	completion_reward = {
		improve_agricultural_crisis = yes
		worsen_liquidity_crisis = yes
	}
}

shared_focus = {
	id = FRA_combat_black_market
	icon = GFX_focus_Combat_Black_Market
	cost = 5.8
	available = {
		has_government = social_liberalism
	}
	prerequisite = { focus = FRA_fix_agricultural_prices }
	relative_position_id = FRA_deflationary_policies
	x = -3
	y = 2

	completion_reward = {
		add_political_power = -75
		improve_agricultural_crisis = yes
	}
}

## Army ##
# 28 focuses

shared_focus = {
	id = FRA_lessons_great_war
	icon = GFX_focus_Lessons_Great_War
	cost = 5
	available = {
		date > 1934.1.1
	}
	x = 35
	y = 0

	completion_reward = {
		army_experience = 15
	}

	offset = {
		x = 5
		y = 0
		trigger = {
			has_focus_tree = CBTS_France_DA
		}
	}

	offset = {
		x = -3
		y = 0
		trigger = {
			has_focus_tree = CBTS_France_popular_front
		}
	}

	ai_will_do = {
		base = 0.5
	}
}

shared_focus = {
	id = FRA_invest_new_equipment
	icon = GFX_focus_Renewed_Arms
	cost = 5.8
	prerequisite = { focus = FRA_new_type_of_war focus = FRA_guerre_de_longue_duree }
	relative_position_id = FRA_lessons_great_war
	x = 4
	y = 2

	completion_reward = {
		add_tech_bonus = {
			bonus = 1
			uses = 1
			category = infantry_weapons
		}
	}
}

shared_focus = {
	id = FRA_support_companies
	icon = GFX_focus_Support_Equipment
	cost = 5.8
	prerequisite = { focus = FRA_invest_new_equipment }
	relative_position_id = FRA_lessons_great_war
	x = 4
	y = 3

	completion_reward = {
		add_tech_bonus = {
			bonus = 0.5
			uses = 3
			category = support_tech
		}
	}
}

shared_focus = {
	id = FRA_montanier_units
	icon = GFX_focus_Mountain_Troops
	cost = 5.8
	prerequisite = { focus = FRA_support_companies }
	relative_position_id = FRA_lessons_great_war
	x = 4
	y = 4

	completion_reward = {
		add_tech_bonus = {
			bonus = 0.5
			uses = 2
			category = mountaineers_tech
		}
	}
}

shared_focus = {
	id = FRA_invest_artillery
	icon = GFX_goal_generic_artillery
	cost = 5.8
	prerequisite = { focus = FRA_support_companies }
	relative_position_id = FRA_lessons_great_war
	x = 6
	y = 4

	completion_reward = {
		add_tech_bonus = {
			bonus = 0.5
			uses = 2
			category = artillery
		}
	}
}

shared_focus = {
	id = FRA_invest_tank_motors
	icon = GFX_focus_Engine_Tech
	cost = 5.8
	prerequisite = { focus = FRA_invest_new_equipment }
	relative_position_id = FRA_lessons_great_war
	x = 2
	y = 3

	completion_reward = {
		add_tech_bonus = {
			bonus = 1
			uses = 1
			category = tank_engines
		}
	}
}

shared_focus = {
	id = FRA_new_tank_designs
	icon = GFX_focus_French_Tank
	cost = 5.8
	prerequisite = { focus = FRA_invest_tank_motors }
	relative_position_id = FRA_lessons_great_war
	x = 2
	y = 4

	completion_reward = {
		add_political_power = 50
		custom_effect_tooltip = FRA_unlcok_design_tank_tt
	}
}

shared_focus = {
	id = FRA_light_tanks_focus
	icon = GFX_focus_Light_Tanks
	cost = 5.8
	mutually_exclusive = { focus = FRA_medium_tanks_focus focus = FRA_heavy_tanks_focus }
	prerequisite = { focus = FRA_invest_armored_units }
	prerequisite = { focus = FRA_new_tank_designs }
	relative_position_id = FRA_lessons_great_war
	x = -1
	y = 5

	completion_reward = {
		add_tech_bonus = {
			ahead_reduction = 1
			uses = 2
			category = cat_light_armor
		}
	}
}

shared_focus = {
	id = FRA_medium_tanks_focus
	icon = GFX_focus_Medium_Tanks
	cost = 5.8
	mutually_exclusive = { focus = FRA_heavy_tanks_focus focus = FRA_light_tanks_focus }
	prerequisite = { focus = FRA_invest_armored_units }
	prerequisite = { focus = FRA_new_tank_designs }
	relative_position_id = FRA_lessons_great_war
	x = 1
	y = 5

	completion_reward = {
		add_tech_bonus = {
			ahead_reduction = 1
			uses = 2
			category = cat_medium_armor
		}
	}
}

shared_focus = {
	id = FRA_heavy_tanks_focus
	icon = GFX_focus_Heavy_Tanks
	cost = 5.8
	mutually_exclusive = { focus = FRA_medium_tanks_focus focus = FRA_light_tanks_focus }
	prerequisite = { focus = FRA_invest_armored_units }
	prerequisite = { focus = FRA_new_tank_designs }
	relative_position_id = FRA_lessons_great_war
	x = 3
	y = 5

	completion_reward = {
		add_tech_bonus = {
			ahead_reduction = 1
			uses = 2
			category = cat_heavy_armor
		}
	}
}

shared_focus = {
	id = FRA_new_type_of_war
	icon = GFX_focus_Officers_Command
	cost = 7.2
	cancelable = no
	cancel_if_invalid = no
	continue_if_invalid = yes
	available = {
		NOT = {
			has_country_flag = law_passing
			has_country_flag = failed_pass_reform
		}
	}
	mutually_exclusive = { focus = FRA_guerre_de_longue_duree }
	prerequisite = { focus = FRA_lessons_great_war }
	relative_position_id = FRA_lessons_great_war
	x = -6
	y = 1

	select_effect = {
		set_country_flag = FRA_reform_army
		activate_decision = FRA_army_reforms_act
	}

	completion_reward = {
		custom_effect_tooltip = FRA_trigger_debate_tt
		custom_effect_tooltip = will_vote
		add_political_power = 50
		add_tech_bonus = {
			bonus = 1
			uses = 1
			category = land_doctrine
		}
		custom_effect_tooltip = FRA_unlock_branch
	}

	cancel = { has_country_flag = failed_pass_reform }

	ai_will_do = {
		base = 0.1

		modifier = {
			has_political_power > 100
			factor = 2
		}
		modifier = {
			has_government = social_liberalism
			factor = 3
		}
		modifier = {
			has_idea = paulreynaudhog
			factor = 5
		}
	}
}

shared_focus = {
	id = FRA_promote_new_blood
	icon = GFX_focus_Call_Old_Generals
	cost = 5.8
	prerequisite = { focus = FRA_new_type_of_war }
	relative_position_id = FRA_lessons_great_war
	x = -7
	y = 2

	completion_reward = {
		set_country_flag = FRA_promote_new_blood
		add_political_power = 50
		custom_effect_tooltip = FRA_unlock_new_blood_tt
	}
}

shared_focus = {
	id = FRA_invest_armored_units
	icon = GFX_focus_Blitzkrieg
	cost = 5.8
	prerequisite = { focus = FRA_new_type_of_war focus = FRA_guerre_de_longue_duree }
	mutually_exclusive = { focus = FRA_invest_in_maginot }
	relative_position_id = FRA_lessons_great_war
	x = 0
	y = 4

	completion_reward = {
		add_tech_bonus = {
			bonus = 0.5
			uses = 1
			category = armor
		}
		add_tech_bonus = {
			bonus = 0.25
			uses = 1
			category = armoured_car_research
		}
		division_template = {
			name = "Brigade de Chars de Combat" 	# Brigade de Chars de Combat, independent armor brigades of 2x Rgts., 2 Bns. each
			division_names_group = FRA_ARM_01
			regiments = {
				light_armor = { x = 0 y = 0 }
				light_armor = { x = 0 y = 1 }
				light_armor = { x = 1 y = 0 }
				light_armor = { x = 1 y = 1 }
			}
		}
		custom_effect_tooltip = FRA_char_unit_tt
		hidden_effect = {
			load_oob = "FRA_chars_combat"
		}
	}
}

shared_focus = {
	id = FRA_introduce_army_reforms
	icon = GFX_focus_Army_Reform
	cost = 5.8
	prerequisite = { focus = FRA_new_type_of_war }
	relative_position_id = FRA_lessons_great_war
	x = -5
	y = 2

	completion_reward = {
		remove_ideas = FRA_The_Great_War
		add_ideas = FRA_army_reformed1
	}
}

shared_focus = {
	id = FRA_flexible_chain_of_command
	icon = GFX_focus_Flexible_chain_of_command
	cost = 5.8
	prerequisite = { focus = FRA_introduce_army_reforms }
	prerequisite = { focus = FRA_promote_new_blood }
	relative_position_id = FRA_lessons_great_war
	x = -8
	y = 3

	completion_reward = {
		swap_ideas = {
			remove_idea = FRA_army_reformed1
			add_idea = FRA_army_reformed2
		}
	}
}

shared_focus = {
	id = FRA_reform_intelligence_service
	icon = GFX_focus_Espionage
	cost = 5.8
	prerequisite = { focus = FRA_flexible_chain_of_command }
	relative_position_id = FRA_lessons_great_war
	x = -8
	y = 4

	completion_reward = {
		if = {
			limit = {
				has_dlc = "La Resistance"
				has_intelligence_agency = no
			}
			create_intelligence_agency = yes
		}
		else_if = {
			limit = {
				has_dlc = "La Resistance"
				NOT = { has_done_agency_upgrade = upgrade_economy_civilian }
			}
			upgrade_intelligence_agency = upgrade_economy_civilian
		}
		else_if = {
			limit = {
				has_dlc = "La Resistance"
				NOT = { has_done_agency_upgrade = upgrade_army_department }
			}
			upgrade_intelligence_agency = upgrade_army_department
		}
		else_if = {
			limit = {
				has_dlc = "La Resistance"
				NOT = { has_done_agency_upgrade = upgrade_naval_department }
			}
			upgrade_intelligence_agency = upgrade_naval_department
		}
		else_if = {
			limit = {
				has_dlc = "La Resistance"
				NOT = { has_done_agency_upgrade = upgrade_airforce_department }
			}
			upgrade_intelligence_agency = upgrade_airforce_department
		}
		if = {
			limit = {
				has_dlc = "La Resistance"
			}
			else = {
			}
		}
		army_experience = 5
		navy_experience = 5
		air_experience = 5
		custom_effect_tooltip = FRA_unlock_int_min_tt
	}
}

shared_focus = {
	id = FRA_invest_mechanized
	icon = GFX_focus_Mechanized_Divisions
	cost = 5.8
	prerequisite = { focus = FRA_promote_new_blood  focus = FRA_introduce_army_reforms }
	relative_position_id = FRA_lessons_great_war
	x = -6
	y = 3

	completion_reward = {
		add_tech_bonus = {
			ahead_reduction = 4
			uses = 1
			category = cat_mechanized_equipment
		}
	}
}

shared_focus = {
	id = FRA_towards_professional_army
	icon = GFX_focus_French_Modern_Army
	cost = 5.8
	prerequisite = { focus = FRA_invest_mechanized }
	prerequisite = { focus = FRA_reform_intelligence_service }
	relative_position_id = FRA_lessons_great_war
	x = -7
	y = 5

	completion_reward = {
		add_tech_bonus = {
			bonus = 0.5
			uses = 3
			category = land_doctrine
		}
	}
}

shared_focus = {
	id = FRA_guerre_de_longue_duree
	icon = GFX_focus_Long_Planning
	cost = 5
	mutually_exclusive = { focus = FRA_new_type_of_war }
	prerequisite = { focus = FRA_lessons_great_war }
	relative_position_id = FRA_lessons_great_war
	x = 7
	y = 1

	completion_reward = {
		army_experience = 10
		add_political_power = 50
		add_tech_bonus = {
			bonus = 0.8
			uses = 1
			category = cat_grand_battle_plan
		}
	}

	ai_will_do = {
		base = 0.2

		modifier = {
			has_government = social_liberalism
			factor = 0.25
		}
		modifier = {
			has_idea = paulreynaudhog
			factor = 0.25
		}

		modifier = {
			date > 1937.1.1
			factor = 20
		}
	}
}

shared_focus = {
	id = FRA_increase_gun_production
	icon = GFX_focus_Firearms
	cost = 5.8
	prerequisite = { focus = FRA_guerre_de_longue_duree }
	relative_position_id = FRA_lessons_great_war
	x = 6
	y = 2

	available = {
		OR = {
			has_war = yes
			threat > 0.75
		}
	}

	completion_reward = {
		add_ideas = { FRA_gun_production }
	}
}

shared_focus = {
	id = FRA_move_gqg_to_paris
	icon = GFX_focus_Kepi
	cost = 5.8
	prerequisite = { focus = FRA_guerre_de_longue_duree }
	relative_position_id = FRA_lessons_great_war
	x = 8
	y = 2

	completion_reward = {
		army_experience = 15
		add_ideas = { FRA_grand_quartier_general }
	}
}

shared_focus = {
	id = FRA_promote_conscription
	icon = GFX_focus_FrenchSoldiers
	cost = 5.8
	prerequisite = { focus = FRA_increase_gun_production focus = FRA_move_gqg_to_paris }
	relative_position_id = FRA_lessons_great_war
	x = 6
	y = 3

	available = {
		OR = {
			has_war = yes
			threat > 0.75
		}
	}

	completion_reward = {
		remove_ideas = FRA_The_Great_War
		if = {
			limit = {
				OR = {
					has_idea = disarmed_nation
					has_idea = volunteer_only
				}
			}
			MOVE_UP_CONSCRIPTION = yes
		}
	}
}

shared_focus = {
	id = FRA_reform_the_gqg
	icon = GFX_focus_Military_Attache
	cost = 5.8
	prerequisite = { focus = FRA_move_gqg_to_paris }
	relative_position_id = FRA_lessons_great_war
	x = 8
	y = 3

	completion_reward = {
		army_experience = 5
		swap_ideas = {
			remove_idea = FRA_grand_quartier_general
			add_idea = FRA_grand_quartier_general2
		}
	}
}

shared_focus = {
	id = FRA_legacy_trench_warfare
	icon = GFX_focus_Trench_Warfare
	cost = 5.8
	prerequisite = { focus = FRA_reform_the_gqg }
	prerequisite = { focus = FRA_invest_artillery }
	relative_position_id = FRA_lessons_great_war
	x = 7
	y = 5

	completion_reward = {
		add_tech_bonus = {
			bonus = 0.5
			uses = 3
			category = cat_grand_battle_plan
		}
	}
}

shared_focus = {
	id = FRA_invest_in_maginot
	icon = GFX_focus_Maginot_Line
	cost = 5
	prerequisite = { focus = FRA_guerre_de_longue_duree focus = FRA_new_type_of_war }
	mutually_exclusive = { focus = FRA_invest_armored_units }
	relative_position_id = FRA_lessons_great_war
	x = -4
	y = 4

	completion_reward = {
		custom_effect_tooltip = FRA_maginot_invest_tt
		modify_timed_idea = {
			idea = FRA_maginot_line_construction
			days = 549
		}

		set_country_flag = expanded_maginot
	}
}

shared_focus = {
	id = FRA_alpine_line
	icon = GFX_focus_Mountain_Defense
	cost = 5
	available = {
		NOT = {
			has_idea = FRA_maginot_line_expand
		}
	}
	prerequisite = { focus = FRA_invest_in_maginot }
	relative_position_id = FRA_lessons_great_war
	x = -5
	y = 5

	completion_reward = {
		custom_effect_tooltip = FRA_maginot_alpine_tt
		add_timed_idea = {
			idea = FRA_maginot_line_expand
			days = 250
		}
		set_country_flag = maginot_alpine
	}
}

shared_focus = {
	id = FRA_extend_maginot
	icon = GFX_focus_Expand_Maginot
	cost = 5
	available = {
		NOT = {
			has_idea = FRA_maginot_line_expand
		}
	}
	prerequisite = { focus = FRA_invest_in_maginot }
	relative_position_id = FRA_lessons_great_war
	x = -3
	y = 5

	completion_reward = {
		custom_effect_tooltip = FRA_maginot_extend_tt
		add_timed_idea = {
			idea = FRA_maginot_line_expand
			days = 120
		}
		set_country_flag = maginot_expand
	}
}

shared_focus = {
	id = FRA_form_dlm
	icon = GFX_focus_Tanks_Across_the_Border
	cost = 5.8
	prerequisite = { focus = FRA_light_tanks_focus focus = FRA_medium_tanks_focus focus = FRA_heavy_tanks_focus }
	relative_position_id = FRA_lessons_great_war
	x = 1
	y = 6

	completion_reward = {
		if = { # Gives you the template with mechanized if you have researched it, else gives it with motorized
			limit = {
				has_tech = mechanised_infantry
			}
			division_template = {
				name = "Division Légère Mécanique" 		# Division Légère Mécanique / First appears in 1935
				division_names_group = FRA_MEC_01

				regiments = {
					light_armor = { x = 0 y = 0 }		# Bn. of Hotchkiss, then Souma (Med.) tanks
					light_armor = { x = 0 y = 1 }		# Bn. of Hotchkiss tanks
					mechanized = { x = 1 y = 0 }			# Brigade of 2x Rgts., 2 Bns. each (later 1 Rgt. of 3x Bns.)
					mechanized = { x = 1 y = 1 }
					mechanized = { x = 1 y = 2 }
					mechanized = { x = 1 y = 3 }
				}
				support = {
					mot_recon = { x = 0 y = 0 }      # Recon Group consisted of 42 ACs + motorcycles
				}
			}
		}
		else = {
			division_template = {
				name = "Division Légère Mécanique"
				division_names_group = FRA_MEC_01

				regiments = {
					light_armor = { x = 0 y = 0 }		# Bn. of Hotchkiss, then Souma (Med.) tanks
					light_armor = { x = 0 y = 1 }		# Bn. of Hotchkiss tanks
					motorized = { x = 1 y = 0 }			# Brigade of 2x Rgts., 2 Bns. each (later 1 Rgt. of 3x Bns.)
					motorized = { x = 1 y = 1 }
					motorized = { x = 1 y = 2 }
					motorized = { x = 1 y = 3 }
				}
				support = {
					mot_recon = { x = 0 y = 0 }
				}
			}
		}

		custom_effect_tooltip = FRA_dlm_tt
		hidden_effect = {
			16 = {
				create_unit = {
					division = "division_name = { is_name_ordered = yes name_order = 1 } division_template = \"Division Légère Mécanique\" start_experience_factor = 0.1 start_equipment_factor = 0.5"
					owner = FRA
					prioritize_location = 11506
				}
			}
		}
	}
}

## Bloc d'Or ##
# 9 Focuses

shared_focus = {
	id = FRA_london_conference
	icon = GFX_focus_The_London_Conference
	cost = 0
	available = { has_country_flag = london_conference }
	bypass = { has_country_flag = london_conference }
	x = 22
	y = 0

	offset = {
		x = 5
		y = 0
		trigger = {
			has_focus_tree = CBTS_France_DA
		}
	}

	offset = {
		x = -3
		y = 0
		trigger = {
			has_focus_tree = CBTS_France_popular_front
		}
	}

	completion_reward = {
		custom_effect_tooltip = GER_unlocks_below_focuses
	}
}

shared_focus = {
	id = FRA_bloc_dor
	icon = GFX_focus_Gold_Standard
	cost = 2.9
	available = {  }
	prerequisite = { focus = FRA_london_conference }
	relative_position_id = FRA_london_conference
	x = 0
	y = 1

	completion_reward = {
		set_variable = { global.num_member_bloc_dor = 0 }
		add_bloc_dor = yes
	}
}

shared_focus = {
	id = FRA_invite_benelux
	icon = GFX_focus_Gold_Benelux
	cost = 1.8
	available = {
		in_bloc_dor = yes
	}
	prerequisite = { focus = FRA_bloc_dor }
	relative_position_id = FRA_london_conference
	x = -3
	y = 2

	completion_reward = {
		BEL = {
			if = {
				limit = {
					exists = yes
					NOT = { has_country_flag = abandoned_gold_standard }
				}
				country_event = {
					id = france.31
					days = 1
				}
			}
		}
		HOL = {
			if = {
				limit = {
					exists = yes
					NOT = { has_country_flag = abandoned_gold_standard }
				}
				country_event = {
					id = france.31
					days = 1
				}
			}
		}
		LUX = {
			if = {
				limit = {
					exists = yes
					NOT = { has_country_flag = abandoned_gold_standard }
				}
				country_event = {
					id = france.31
					days = 1
				}
			}
		}
	}

	bypass = {
		BEL = {
			OR = {
				exists = no
				has_country_flag = abandoned_gold_standard
			}
		}
		HOL = {
			OR = {
				exists = no
				has_country_flag = abandoned_gold_standard
			}
		}
		LUX = {
			OR = {
				exists = no
				has_country_flag = abandoned_gold_standard
			}
		}
	}
}

shared_focus = {
	id = FRA_invite_italy
	icon = GFX_focus_Gold_Italy
	cost = 1.8
	available = {
		in_bloc_dor = yes
	}
	prerequisite = { focus = FRA_bloc_dor }
	relative_position_id = FRA_london_conference
	x = -1
	y = 2

	completion_reward = {
		ITA = {
			country_event = {
				id = france.31
				days = 1
			}
		}
	}

	bypass = {
		ITA = {
			OR = {
				exists = no
				has_country_flag = abandoned_gold_standard
			}
		}
	}
}

shared_focus = {
	id = FRA_invite_poland
	icon = GFX_focus_Gold_Poland
	cost = 1.8
	available = { in_bloc_dor = yes }
	prerequisite = { focus = FRA_bloc_dor }
	relative_position_id = FRA_london_conference
	x = 1
	y = 2

	completion_reward = {
		POL = {
			country_event = {
				id = france.31
				days = 1
			}
		}
	}

	bypass = {
		POL = {
			OR = {
				exists = no
				has_country_flag = POL_left_the_gold
			}
		}
	}
}

shared_focus = {
	id = FRA_invite_switzerland
	icon = GFX_focus_Gold_Swiss
	cost = 1.8
	available = { in_bloc_dor = yes }
	prerequisite = { focus = FRA_bloc_dor }
	relative_position_id = FRA_london_conference
	x = 3
	y = 2

	completion_reward = {
		SWI = {
			country_event = {
				id = france.31
				days = 1
			}
		}
	}

	bypass = {
		SWI = {
			OR = {
				exists = no
				has_country_flag = abandoned_gold_standard
			}
		}
	}
}

shared_focus = {
	id = FRA_invite_germany
	icon = GFX_focus_Gold_Germany
	cost = 1.8
	available = {
		in_bloc_dor = yes
		GRM = {
			exists = yes
			NOT = {
				has_country_flag = abandoned_gold_standard
				has_government = fascism
				has_government = nationalism
				has_government = monarchism
				has_government = neutrality
			}
		}
	}
	prerequisite = { focus = FRA_invite_italy }
	prerequisite = { focus = FRA_invite_switzerland }
	prerequisite = { focus = FRA_invite_poland }
	prerequisite = { focus = FRA_invite_benelux }
	relative_position_id = FRA_london_conference
	x = -2
	y = 3

	completion_reward = {
		GRM = {
			country_event = {
				id = france.31
				days = 1
			}
		}
	}
}

shared_focus = {
	id = FRA_invite_yugoslavia
	icon = GFX_focus_Gold_Yugoslavia
	cost = 1.8
	available = {
		in_bloc_dor = yes
		YUG = {
			exists = yes
			NOT = { has_country_flag = abandoned_gold_standard }
		}
	}
	prerequisite = { focus = FRA_invite_italy }
	prerequisite = { focus = FRA_invite_switzerland }
	prerequisite = { focus = FRA_invite_poland }
	prerequisite = { focus = FRA_invite_benelux }
	relative_position_id = FRA_london_conference
	x = 0
	y = 3

	completion_reward = {
		YUG = {
			country_event = {
				id = france.31
				days = 1
			}
		}
	}
}

shared_focus = {
	id = FRA_invite_czechoslovakia
	icon = GFX_focus_Gold_Czech
	cost = 1.8
	available = {
		in_bloc_dor = yes
		CZE = {
			exists = yes
			NOT = { has_country_flag = abandoned_gold_standard }
		}
	}
	prerequisite = { focus = FRA_invite_italy }
	prerequisite = { focus = FRA_invite_switzerland }
	prerequisite = { focus = FRA_invite_poland }
	prerequisite = { focus = FRA_invite_benelux }
	relative_position_id = FRA_london_conference
	x = 2
	y = 3

	completion_reward = {
		CZE = {
			country_event = {
				id = france.31
				days = 1
			}
		}
	}
}