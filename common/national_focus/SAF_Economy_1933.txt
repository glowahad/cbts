shared_focus = {
	id = SAF_Economy_Start_1933
	icon = GFX_goal_pol666
	x = 69
	y = 0
	cost = 2
	available = {
		has_country_flag = SAF_Depression_Over_flag
		has_country_flag = SAF_United_Party_formed
	}
	ai_will_do = { 
		factor = 1
	} 
	completion_reward = {
		add_political_power = 20
	}
 } 
#Focus for Agricultural Focus
shared_focus = {
	id = SAF_agriculturalfocus
	icon = GFX_focus_Agriculture
	x = -6
	y = 1
	relative_position_id = SAF_Economy_Start_1933
	cost = 3
	prerequisite = { focus = SAF_Economy_Start_1933 } 
	available = {
	}
	mutually_exclusive = { focus = SAF_industrialfocus } 
	ai_will_do = { 
		factor = 1
	} 
	completion_reward = {
		add_stability = 0.05
		custom_effect_tooltip = SAF_Agricultural_Focus
	}
 } 

#Focus for Industrial Focus
shared_focus = {
id = SAF_industrialfocus
icon = GFX_focus_Industrialization
x = 6
y = 1
relative_position_id = SAF_Economy_Start_1933
cost = 4
prerequisite = { focus = SAF_Economy_Start_1933 } 
available = {
	}
mutually_exclusive = { focus = SAF_agriculturalfocus } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		add_stability = -0.05
		custom_effect_tooltip = SAF_Industrial_Focus
	}
 } 

#Focus for Subsidize White Farming
shared_focus = {
id = SAF_subsidizewhitefarming
icon = GFX_focus_Agrarian_Reform
x = -9
y = 2
relative_position_id = SAF_Economy_Start_1933
cost = 3
available_if_capitulated = yes 
prerequisite = { focus = SAF_agriculturalfocus } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		add_ideas = SAF_Subsidized_White_Farming
	}	
 } 

#Focus for Begin Long-Term Planning
shared_focus = {
id = SAF_beginlongtermplanning
icon = GFX_focus_Economic_Stimulus
x = 9
y = 5
relative_position_id = SAF_Economy_Start_1933
cost = 4
available_if_capitulated = yes 
prerequisite = { focus = SAF_investinmajorsectors } 
prerequisite = { focus = SAF_developthecapeport } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		custom_effect_tooltip = SAF_Long_Term_Planning
		set_country_flag = SAF_Planning_Flag
	}
 } 

#Focus for An Industrializing SAF
shared_focus = {
id = SAF_anindustrializingsouthafrica
icon = GFX_focus_Industrial_South_Africa
x = 6
y = 6
relative_position_id = SAF_Economy_Start_1933
cost = 6
available_if_capitulated = yes 
prerequisite = { focus = SAF_beginlongtermplanning  } 
prerequisite = { focus = SAF_negotiatewithminerunions focus = SAF_ignoreminerunions } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		swap_ideas = {
				remove_idea = SAF_Industrializing_Nation
				add_idea = SAF_Progressed_Industrialization
			}
	}
 } 

#Focus for Expand the Western Cape Vineyards
shared_focus = {
id = SAF_expandthewesterncapevineyards
icon = GFX_focus_Expand_Cape_Vineyards
x = -9
y = 5
relative_position_id = SAF_Economy_Start_1933
cost = 3
available_if_capitulated = yes 
prerequisite = { focus = SAF_expandnatalplantations } 
prerequisite = { focus = SAF_expandeasterncaperanching } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
				681 = {
					if = {
						limit = {
							free_building_slots = {
								building = infrastructure
								size > 1
								include_locked = yes
							}
						}
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
	}
 } 

#Focus for Expand Natal Plantations
shared_focus = {
id = SAF_expandnatalplantations
icon = GFX_focus_Agrarian_Reform
x = -10
y = 4
relative_position_id = SAF_Economy_Start_1933
cost = 3
available_if_capitulated = yes 
prerequisite = { focus = SAF_expandtransvaalagriculture } 
prerequisite = { focus = SAF_expandfreestatefarming } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
				719 = {
					if = {
						limit = {
							free_building_slots = {
								building = infrastructure
								size > 1
								include_locked = yes
							}
						}
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
	}
 } 

#Focus for Expand Eastern Cape Ranching
shared_focus = {
id = SAF_expandeasterncaperanching
icon = GFX_focus_Compensate_Landowners
x = -8
y = 4
relative_position_id = SAF_Economy_Start_1933
cost = 3
available_if_capitulated = yes 
prerequisite = { focus = SAF_expandfreestatefarming } 
prerequisite = { focus = SAF_expandnorthwestagriculture } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
				1161 = {
					if = {
						limit = {
							free_building_slots = {
								building = infrastructure
								size > 1
								include_locked = yes
							}
						}
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
	}
 } 

#Focus for Ignore Miner Unions
shared_focus = {
id = SAF_ignoreminerunions
icon = GFX_focus_Anti_Communist
x = 4
y = 4
relative_position_id = SAF_Economy_Start_1933
cost = 5
available_if_capitulated = yes 
prerequisite = { focus = SAF_expandgoldmining } 
prerequisite = { focus = SAF_expanddiamondmining } 
prerequisite = { focus = SAF_expandchromiumextraction } 
mutually_exclusive = { focus = SAF_negotiatewithminerunions } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		swap_ideas = {
				remove_idea = SAF_Growing_Unionism
				add_idea = SAF_Sidelined_Unionism
			}
	}
 } 

#Focus for Nationalize Industries
shared_focus = {
id = SAF_nationalizeindustries
icon = GFX_focus_Control_the_Economy
x = -3
y = 3
relative_position_id = SAF_Economy_Start_1933
cost = 6
available_if_capitulated = yes 
prerequisite = { focus = SAF_limitminingoutput } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
				OR = {
					region = 139
					region = 232
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
				OR = {
					region = 139
					region = 232
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
	}
 } 

#Focus for Stop Industrialization
shared_focus = {
id = SAF_stopindustrialization
icon = GFX_focus_Stop_Industrialization
x = -4
y = 4
relative_position_id = SAF_Economy_Start_1933
cost = 3
available_if_capitulated = yes 
prerequisite = { focus = SAF_endindustrialsubsidies } 
prerequisite = { focus = SAF_nationalizeindustries } 
mutually_exclusive = { focus = SAF_slowindustrialization } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		custom_effect_tooltip = SAF_Industrialization_Stopping
		set_country_flag = SAF_Industrialization_Stopping_flag
	}
 } 

#Focus for End Industrial Subsidies
shared_focus = {
id = SAF_endindustrialsubsidies
icon = GFX_focus_Deregulation
x = -5
y = 3
relative_position_id = SAF_Economy_Start_1933
cost = 4
available_if_capitulated = yes 
prerequisite = { focus = SAF_limitminingoutput } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		add_stability = 0.02
		add_timed_idea = { 
			idea = SAF_Ended_Subsidies
			days = 180
			}
	}
 } 

#Focus for Develop the Railways
shared_focus = {
id = SAF_developtherailways
icon = GFX_focus_Trains
x = 9
y = 3
relative_position_id = SAF_Economy_Start_1933
cost = 8
available_if_capitulated = yes 
prerequisite = { focus = SAF_expandinfrastructureprojects } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		add_political_power = -20
		add_stability = -0.03
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
				OR = {
					region = 139
					region = 232
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
				OR = {
					region = 139
					region = 232
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
				OR = {
					region = 139
					region = 232
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
	}
 } 

#Focus for Invest in Major Sectors
shared_focus = {
id = SAF_investinmajorsectors
icon = GFX_focus_found_the_investment_bank
x = 8
y = 4
relative_position_id = SAF_Economy_Start_1933
cost = 6
available_if_capitulated = yes 
prerequisite = { focus = SAF_subsidizecivilianindustry } 
prerequisite = { focus = SAF_developtherailways } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		add_political_power = -30
		add_stability = -0.05
		add_timed_idea = {
		idea = SAF_Investment_Scheme
		days = 300
		}
	}
 } 

#Focus for Develop the Cape Port
shared_focus = {
id = SAF_developthecapeport
icon = GFX_focus_Large_Navy
x = 10
y = 4
relative_position_id = SAF_Economy_Start_1933
cost = 5
available_if_capitulated = yes 
prerequisite = { focus = SAF_developtherailways } 
prerequisite = { focus = SAF_subsidizemilitarydevelopment } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
			add_political_power = -20
				681 = {
					if = {
						limit = {
							free_building_slots = {
								building = dockyard
								size > 1
								include_locked = yes
							}
						}
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = dockyard
					level = 1
					instant_build = yes
				}
			}
		}
		681 = {
			if = {
				limit = {
					is_controlled_by = SAF
				}
				add_building_construction = {
					type = naval_base
					level = 2
					instant_build = yes
					province = 321
				}
			}
		}
	}
 } 

#Focus for Expand Chromium Extraction
shared_focus = {
id = SAF_expandchromiumextraction
icon = GFX_focus_Chromium
x = 3
y = 3
relative_position_id = SAF_Economy_Start_1933
cost = 6
available_if_capitulated = yes 
prerequisite = { focus = SAF_expandtheminingindustry } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		1158 = {
					if = {
						limit = {
							free_building_slots = {
								building = industrial_complex
								size > 1
								include_locked = yes
							}
						}
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
			}
		}
		SAF = {
			add_resource = {
				type = chromium
				amount = 12
				state = 1158
			}
		}
	}
 } 

#Focus for Expand Infrastructure Projects
shared_focus = {
id = SAF_expandinfrastructureprojects
icon = GFX_focus_Electrification
x = 9
y = 2
relative_position_id = SAF_Economy_Start_1933
cost = 5
available_if_capitulated = yes 
prerequisite = { focus = SAF_industrialfocus } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		add_political_power = -30
		add_timed_idea = { 
			idea = SAF_Expanding_Infrastructure
			days = 180
		}
	}
 } 

#Focus for Expand Northwest Agriculture
shared_focus = {
id = SAF_expandnorthwestagriculture
icon = GFX_focus_Bread_Trade
x = -7
y = 3
relative_position_id = SAF_Economy_Start_1933
cost = 3
available_if_capitulated = yes 
prerequisite = { focus = SAF_subsidizewhitefarming } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
				1158 = {
					if = {
						limit = {
							free_building_slots = {
								building = infrastructure
								size > 1
								include_locked = yes
							}
						}
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
	}
 } 

#Focus for Expand Free State Farming
shared_focus = {
id = SAF_expandfreestatefarming
icon = GFX_focus_Orange_Free_State_Farming
x = -9
y = 3
relative_position_id = SAF_Economy_Start_1933
cost = 3
available_if_capitulated = yes 
prerequisite = { focus = SAF_subsidizewhitefarming } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
				1159 = {
					if = {
						limit = {
							free_building_slots = {
								building = infrastructure
								size > 1
								include_locked = yes
							}
						}
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
	}
 } 

#Focus for Slow Industrialization
shared_focus = {
id = SAF_slowindustrialization
icon = GFX_focus_Slow_Industrialization
x = -2
y = 4
relative_position_id = SAF_Economy_Start_1933
cost = 5
available_if_capitulated = yes 
prerequisite = { focus = SAF_nationalizeindustries } 
prerequisite = { focus = SAF_restartblackmajoritymining } 
mutually_exclusive = { focus = SAF_stopindustrialization } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		custom_effect_tooltip = SAF_Industrialization_Slowing
		set_country_flag = SAF_Industrialization_Slowing_flag
	}
 } 

#Focus for Restart Black-Majority Mining
shared_focus = {
id = SAF_restartblackmajoritymining
icon = GFX_goal_Degrade_Working_Conditions
x = -1
y = 3
relative_position_id = SAF_Economy_Start_1933
cost = 4
available_if_capitulated = yes 
prerequisite = { focus = SAF_limitminingoutput } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		add_stability = -0.02
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
				OR = {
					region = 139
					region = 232
				}
			}
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
	}
 } 

#Focus for Expand Transvaal Agriculture
shared_focus = {
id = SAF_expandtransvaalagriculture
icon = GFX_focus_Set_Agricultural_Prices
x = -11
y = 3
relative_position_id = SAF_Economy_Start_1933
cost = 3
available_if_capitulated = yes 
prerequisite = { focus = SAF_subsidizewhitefarming } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
			275 = {
					if = {
						limit = {
							free_building_slots = {
								building = infrastructure
								size > 1
								include_locked = yes
							}
						}
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
	}
 } 

#Focus for Limit Mining Output
shared_focus = {
id = SAF_limitminingoutput
icon = GFX_focus_Price_Reductions
x = -3
y = 2
relative_position_id = SAF_Economy_Start_1933
cost = 4
available_if_capitulated = yes 
prerequisite = { focus = SAF_agriculturalfocus } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		add_stability = 0.03
	}
 } 

#Focus for Subsidize Civilian Industry
shared_focus = {
id = SAF_subsidizecivilianindustry
icon = GFX_focus_Mechanization
x = 7
y = 3
relative_position_id = SAF_Economy_Start_1933
cost = 6
available_if_capitulated = yes 
prerequisite = { focus = SAF_expandinfrastructureprojects } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		add_political_power = -15
		add_stability = -0.02
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
				OR = {
					region = 139
					region = 232
				}
			}
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
				OR = {
					region = 139
					region = 232
				}
			}
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
	}
 } 

#Focus for Negotiate with Miner Unions
shared_focus = {
id = SAF_negotiatewithminerunions
icon = GFX_focus_Cooperatives
x = 2
y = 4
relative_position_id = SAF_Economy_Start_1933
cost = 5
available_if_capitulated = yes 
prerequisite = { focus = SAF_expanddiamondmining } 
prerequisite = { focus = SAF_expandgoldmining } 
prerequisite = { focus = SAF_expandchromiumextraction } 
mutually_exclusive = { focus = SAF_ignoreminerunions } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		swap_ideas = {
				remove_idea = SAF_Growing_Unionism
				add_idea = SAF_Tempered_Unionism
			}
	}
 } 

#Focus for Subsidize Military Development
shared_focus = {
id = SAF_subsidizemilitarydevelopment
icon = GFX_focus_Firearms
x = 11
y = 3
relative_position_id = SAF_Economy_Start_1933
cost = 6
available_if_capitulated = yes 
prerequisite = { focus = SAF_expandinfrastructureprojects } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		add_political_power = -15
		add_stability = -0.02
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
				OR = {
					region = 139
					region = 232
				}
			}
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
				OR = {
					region = 139
					region = 232
				}
			}
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
 } 

#Focus for An Agricultural SAF
shared_focus = {
id = SAF_anagriculturalsouthafrica
icon = GFX_focus_Agricultural_South_Africa
x = -6
y = 6
relative_position_id = SAF_Economy_Start_1933
cost = 5
available_if_capitulated = yes 
prerequisite = { focus = SAF_expandthewesterncapevineyards } 
prerequisite = { focus = SAF_stopindustrialization focus = SAF_slowindustrialization } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		custom_effect_tooltip = SAF_Agri_Effects_tt
		if = {
			limit = {
				has_country_flag = SAF_Industrialization_Stopping_flag
			}
			swap_ideas = {
				remove_idea = SAF_Industrializing_Nation
				add_idea = SAF_Agri_Harsh
			}
		}
		else_if = {
			limit = {
				has_country_flag = SAF_Industrialization_Slowing_flag
			}
			swap_ideas = {
				remove_idea = SAF_Industrializing_Nation
				add_idea = SAF_Agri_Med
			}
		}
	}
 } 

#Focus for Expand the Mining Industry
shared_focus = {
id = SAF_expandtheminingindustry
icon = GFX_focus_Industrial_Council
x = 3
y = 2
relative_position_id = SAF_Economy_Start_1933
cost = 3
available_if_capitulated = yes 
prerequisite = { focus = SAF_industrialfocus } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		add_timed_idea = { 
			idea = SAF_Expanding_Mining
			days = 180
		}
	}
 } 

#Focus for Expand Gold Mining
shared_focus = {
id = SAF_expandgoldmining
icon = GFX_focus_Gold_Standard
x = 5
y = 3
relative_position_id = SAF_Economy_Start_1933
cost = 6
available_if_capitulated = yes 
prerequisite = { focus = SAF_expandtheminingindustry } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		1159 = {
					if = {
						limit = {
							free_building_slots = {
								building = industrial_complex
								size > 1
								include_locked = yes
							}
						}
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
			}
		}
	}
 } 

#Focus for Expand Diamond Mining
shared_focus = {
id = SAF_expanddiamondmining
icon = GFX_focus_Excavations
x = 1
y = 3
relative_position_id = SAF_Economy_Start_1933
cost = 6
available_if_capitulated = yes 
prerequisite = { focus = SAF_expandtheminingindustry } 
ai_will_do = { 
 factor = 1
 } 
	completion_reward = {
		1157 = {
					if = {
						limit = {
							free_building_slots = {
								building = industrial_complex
								size > 1
								include_locked = yes
							}
						}
				add_extra_state_shared_building_slots = 1
				add_building_construction = {
					type = industrial_complex
					level = 1
					instant_build = yes
				}
			}
		}
	}
 } 