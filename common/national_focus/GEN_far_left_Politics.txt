# focuses here
#s_team337

#civil service section
shared_focus = {
	id = GEN_LEFT_POL_start
	icon = GFX_focus_Revolution
	dynamic = yes
	#prerequisite = { focus = GEN_LEFT_POL_start }
	x = 7
	y = 0
	#relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_political_power = 75
	}
}
#Civil Services
shared_focus = {
	id = GEN_LEFT_POL_Great_Purge
	icon = GFX_focus_Communist_Repression
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_start
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Light_Purge
	}
	x = -7
	y = 1
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		gen_add_our_ideology = yes
		add_timed_idea = {
			idea = GEN_Recent_Great_Purges
			days = 365
		}
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Light_Purge
	icon = GFX_focus_Civillian_Purges
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_start
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Great_Purge
	}
	x = -5
	y = 1
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_government = authoritarian_socialism
	}
	bypass = {
	}
	completion_reward = {
		add_stability = 0.04
		add_timed_idea = {
			idea = GEN_Recent_Purges
			days = 180
		}
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Anti_Liberal
	icon = GFX_focus_Anti_Democracy
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Great_Purge
		focus = GEN_LEFT_POL_Light_Purge
	}
	x = -7
	y = 2
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_popularity = {
			ideology = socialism
			popularity = -0.02
		}
		add_popularity = {
			ideology = social_democracy
			popularity = -0.02
		}
		add_popularity = {
			ideology = social_liberalism
			popularity = -0.02
		}
		add_popularity = {
			ideology = market_liberalism
			popularity = -0.02
		}
		add_popularity = {
			ideology = jacobin
			popularity = -0.02
		}
		add_popularity = {
			ideology = democratic
			popularity = -0.02
		}
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Anti_Fascist
	icon = GFX_focus_Anti_Fascist
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Great_Purge
		focus = GEN_LEFT_POL_Light_Purge
	}
	x = -5
	y = 2
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_popularity = {
			ideology = neutrality
			popularity = -0.03
		}
		add_popularity = {
			ideology = monarchism
			popularity = -0.03
		}
		add_popularity = {
			ideology = nationalism
			popularity = -0.03
		}
		add_popularity = {
			ideology = fascism
			popularity = -0.03
		}
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Secret_Police
	icon = GFX_focus_Counter_Intelligence
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Anti_Liberal
	}
	prerequisite = {
		focus = GEN_LEFT_POL_Anti_Fascist
	}
	x = -6
	y = 3
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_ideas = GEN_secret_police
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Politicized_Judciary
	icon = GFX_focus_Communist_Loyalty
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Secret_Police
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Independent_Judiciary
	}
	x = -7
	y = 4
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_political_power = 40
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Independent_Judiciary
	icon = GFX_focus_Judicial_System
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Secret_Police
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Politicized_Judciary
	}
	x = -5
	y = 4
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 2
	}
	available = {
		has_government = authoritarian_socialism
	}
	bypass = {
	}
	completion_reward = {
		add_stability = 0.06
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Commisssars
	icon = GFX_focus_Oath_of_Loyalty
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Politicized_Judciary
		focus = GEN_LEFT_POL_Independent_Judiciary
	}
	x = -6
	y = 5
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 2
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_ideas = GEN_commissars
	}
}
#ideology section
shared_focus = {
	id = GEN_LEFT_POL_Leninism
	icon = GFX_focus_Devotion_to_Lenin
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_start
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Our_Own_Path
	}
	x = -1
	y = 1
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_government = communism
	}
	bypass = {
	}
	completion_reward = {
		add_popularity = {
			ideology = communism
			popularity = 0.08
		}
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Dictatorship_Proletariat
	icon = GFX_focus_Planned_Economy
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Leninism
	}
	x = -2
	y = 2
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_government = communism
	}
	bypass = {
	}
	completion_reward = {
		add_political_power = 80
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Our_Own_Path
	icon = GFX_focus_Council_Communism
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_start
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Leninism
	}
	x = 1
	y = 1
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_government = authoritarian_socialism
	}
	bypass = {
	}
	completion_reward = {
		add_popularity = {
			ideology = authoritarian_socialism
			popularity = 0.08
		}
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Centralized_Authority
	icon = GFX_focus_Manipulate_the_Poor
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Our_Own_Path
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Decentralized_Authority
	}
	x = 0
	y = 2
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_government = authoritarian_socialism
	}
	bypass = {
	}
	completion_reward = {
		add_political_power = 40
		add_stability = 0.04
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Decentralized_Authority
	icon = GFX_focus_Workers_Support
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Our_Own_Path
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Centralized_Authority
	}
	x = 2
	y = 2
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_government = authoritarian_socialism
	}
	bypass = {
	}
	completion_reward = {
		add_stability = 0.08
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Social_Conservatism
	icon = GFX_focus_Agriculture_2
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Dictatorship_Proletariat
		focus = GEN_LEFT_POL_Centralized_Authority
	}
	x = -1
	y = 3
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_ideas = GEN_traditionalism
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Social_Progressive
	icon = GFX_focus_Pride_of_The_Left
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Decentralized_Authority
	}
	x = 1
	y = 3
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_government = authoritarian_socialism
	}
	bypass = {
	}
	completion_reward = {
		add_stability = 0.04
		add_political_power = 40
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Nationalism
	icon = GFX_focus_Soviet_War_Economy
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Social_Progressive
		focus = GEN_LEFT_POL_Social_Conservatism
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Internationalism
	}
	x = -1
	y = 4
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_ideas = GEN_socialist_patriotism
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Internationalism
	icon = GFX_focus_Crush_the_Monarchy
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Social_Progressive
		focus = GEN_LEFT_POL_Social_Conservatism
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Nationalism
	}
	x = 1
	y = 4
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_government = authoritarian_socialism
	}
	bypass = {
	}
	completion_reward = {
		add_popularity = {
			ideology = authoritarian_socialism
			popularity = 0.08
		}
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Paramilitary
	icon = GFX_focus_Recruitment_Drive
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Nationalism
		focus = GEN_LEFT_POL_Internationalism
	}
	x = -1
	y = 5
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_ideas = GEN_red_guard
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Socialist_Education
	icon = GFX_focus_Communist_Scientists
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Nationalism
		focus = GEN_LEFT_POL_Internationalism
	}
	x = 1
	y = 5
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_popularity = {
			ideology = communism
			popularity = 0.02
		}
		add_popularity = {
			ideology = authoritarian_socialism
			popularity = 0.02
		}
		add_timed_idea = {
			idea = GEN_socialist_education
			days = 365
		}
	}
}
#Economics
shared_focus = {
	id = GEN_LEFT_POL_Economics
	icon = GFX_focus_Proletarian_Intelectualism
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_start
	}
	x = 6
	y = 1
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_political_power = 45
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Command_Economy
	icon = GFX_focus_Factory_Town
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Economics
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Self_Management
	}
	x = 5
	y = 2
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_ideas = GEN_command_economy
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Self_Management
	icon = GFX_focus_Cooperatives
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Economics
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Command_Economy
	}
	x = 7
	y = 2
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 10
	}
	available = {
		has_government = authoritarian_socialism
	}
	bypass = {
	}
	completion_reward = {
		add_ideas = GEN_market_socialist_economy
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Forced_Collectivization
	icon = GFX_focus_Purge_Kulaks
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Command_Economy
		focus = GEN_LEFT_POL_Self_Management
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Voluntary_Collectivization
	}
	x = 5
	y = 3
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_timed_idea = {
			idea = GEN_forced_collec
			days = 365
		}
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Voluntary_Collectivization
	icon = GFX_focus_Communist_Pride
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Command_Economy
		focus = GEN_LEFT_POL_Self_Management
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Forced_Collectivization
	}
	x = 7
	y = 3
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_government = authoritarian_socialism
	}
	bypass = {
	}
	completion_reward = {
		add_timed_idea = {
			idea = GEN_voluntary_collec
			days = 545
		}
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Provide_Employment
	icon = GFX_focus_Highways
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Forced_Collectivization
		focus = GEN_LEFT_POL_Voluntary_Collectivization
	}
	x = 6
	y = 4
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_timed_idea = {
			idea = GEN_public_works
			days = 730
		}
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Control_Unions
	icon = GFX_focus_Destroy_Trade_Unions
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Provide_Employment
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Empower_Unions
	}
	x = 5
	y = 5
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_socialist_government = yes
	}
	bypass = {
	}
	completion_reward = {
		add_political_power = 50
	}
}
shared_focus = {
	id = GEN_LEFT_POL_Empower_Unions
	icon = GFX_focus_Union_Workers
	dynamic = yes
	prerequisite = {
		focus = GEN_LEFT_POL_Provide_Employment
	}
	mutually_exclusive = {
		focus = GEN_LEFT_POL_Control_Unions
	}
	x = 7
	y = 5
	relative_position_id = GEN_LEFT_POL_start
	cost = 8
	ai_will_do = {
		factor = 16
	}
	available = {
		has_government = authoritarian_socialism
	}
	bypass = {
	}
	completion_reward = {
		add_stability = 0.075
	}
}
