shared_focus = {
	id = WGR_military_start
	icon = GFX_focus_German_Plans
	x = 58
	y = 0
	offset = {
		x = 2
		y = 0
		trigger = {
			has_focus_tree = WGR_DVP_DNVP_tree
		}
	}
	cost = 5
	available = {
		NOT = {
			has_idea = anger_against_gov
			has_idea = paramilitary_divisions
		}
		date > 1935.1.1
	}
	ai_will_do = {
		factor = 20
	}
	bypass = {
		NOT = { has_idea = versailles_treaty }
	}	
	completion_reward = {
		FRA = { country_event = { id = wgr_external.1 } }
	}
}
shared_focus = {
	id = WGR_army_amnesty_for_nazi_officers
	icon = GFX_focus_Risky_Deal
	prerequisite = {
		focus = WGR_military_start
	}
	mutually_exclusive = {
		focus = WGR_army_no_amnesty
	}
	relative_position_id = WGR_military_start
	x = -4
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_political_power = -20
		add_popularity = { ideology = fascism popularity = 0.025 }
		army_experience = 30
	}
}
shared_focus = {
	id = WGR_army_mobile_warfare
	icon = GFX_focus_GER_blitzkrieg_theory
	prerequisite = {
		focus = WGR_army_amnesty_for_nazi_officers
	}
	mutually_exclusive = {
		focus = WGR_army_grand_battleplan
	}
	relative_position_id = WGR_army_amnesty_for_nazi_officers
	x = -2
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = land_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = cat_mobile_warfare
		}
	}
}
shared_focus = {
	id = WGR_army_mobile_warfare_motorized
	icon = GFX_focus_Motorization_Effort
	prerequisite = {
		focus = WGR_army_mobile_warfare
	}
	relative_position_id = WGR_army_amnesty_for_nazi_officers
	x = -2
	y = 2
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = motorized_bonus
			bonus = 0.5
			uses = 1
			category = motorized_equipment
			category = cat_mechanized_equipment
		}
	}
}
shared_focus = {
	id = WGR_army_support_equipment
	icon = GFX_focus_Supply_Troops
	prerequisite = {
		focus = WGR_army_mobile_warfare_motorized
		focus = WGR_army_battleplan_infantry_weapons
	}
	relative_position_id = WGR_army_amnesty_for_nazi_officers
	x = 0
	y = 3
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = support_bonus
			bonus = 0.5
			uses = 1
			category = support_tech
		}
	}
}
shared_focus = {
	id = WGR_army_mobile_warfare_armor
	icon = GFX_focus_Tanks_Across_the_Border
	prerequisite = {
		focus = WGR_army_mobile_warfare_motorized
	}
	relative_position_id = WGR_army_amnesty_for_nazi_officers
	x = -2
	y = 3
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = armor_bonus
			bonus = 0.5
			uses = 1
			category = armor
		}
	}
}
shared_focus = {
	id = WGR_army_mobile_warfare_heavy_armor
	icon = GFX_focus_Heavy_Tanks
	prerequisite = {
		focus = WGR_army_mobile_warfare_armor
	}
	prerequisite = {
		focus = WGR_army_support_equipment
	}
	mutually_exclusive = {
		focus = WGR_army_mobile_warfare_light_armor
	}
	relative_position_id = WGR_army_amnesty_for_nazi_officers
	x = -3
	y = 4
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = armor_bonus
			bonus = 0.33
			uses = 1
			category = cat_heavy_armor
		}
	}
}
shared_focus = {
	id = WGR_army_mobile_warfare_light_armor
	icon = GFX_focus_Light_Tanks
	prerequisite = {
		focus = WGR_army_mobile_warfare_armor
	}
	prerequisite = {
		focus = WGR_army_support_equipment
	}
	mutually_exclusive = {
		focus = WGR_army_mobile_warfare_heavy_armor
	}
	relative_position_id = WGR_army_amnesty_for_nazi_officers
	x = -1
	y = 4
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = armor_bonus
			bonus = 0.33
			uses = 1
			category = cat_light_armor
		}
	}
}
shared_focus = {
	id = WGR_army_mobile_warfare_medium_armor
	icon = GFX_focus_Medium_Tanks
	prerequisite = {
		focus = WGR_army_mobile_warfare_light_armor
		focus = WGR_army_mobile_warfare_heavy_armor
	}
	relative_position_id = WGR_army_amnesty_for_nazi_officers
	x = -2
	y = 5
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = armor_bonus
			bonus = 0.33
			uses = 1
			category = cat_medium_armor
		}
	}
}

shared_focus = {
	id = WGR_army_grand_battleplan
	icon = GFX_focus_Long_Planning
	prerequisite = {
		focus = WGR_army_amnesty_for_nazi_officers
	}
	mutually_exclusive = {
		focus = WGR_army_mobile_warfare
	}
	relative_position_id = WGR_army_amnesty_for_nazi_officers
	x = 2
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = land_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = cat_grand_battle_plan
		}
	}
}
shared_focus = {
	id = WGR_army_battleplan_infantry_weapons
	icon = GFX_focus_Renewed_Arms
	prerequisite = {
		focus = WGR_army_grand_battleplan
	}
	relative_position_id = WGR_army_amnesty_for_nazi_officers
	x = 2
	y = 2
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = small_arms_bonus
			bonus = 0.5
			uses = 1
			category = infantry_tech
		}
	}
}
shared_focus = {
	id = WGR_army_battleplan_planning_office
	icon = GFX_focus_Flexible_chain_of_command
	prerequisite = {
		focus = WGR_army_battleplan_infantry_weapons
	}
	relative_position_id = WGR_army_amnesty_for_nazi_officers
	x = 2
	y = 3
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_ideas = WGR_decentralized_command
	}
}
shared_focus = {
	id = WGR_army_battleplan_motorized
	icon = GFX_goal_generic_army_motorized
	prerequisite = {
		focus = WGR_army_battleplan_planning_office
	}
	prerequisite = {
		focus = WGR_army_support_equipment
	}
	mutually_exclusive = {
		focus = WGR_army_battleplan_spec_ops
	}
	relative_position_id = WGR_army_amnesty_for_nazi_officers
	x = 1
	y = 4
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = motorized_bonus
			bonus = 0.5
			uses = 1
			category = motorized_equipment
			category = cat_mechanized_equipment
		}
	}
}
shared_focus = {
	id = WGR_army_battleplan_spec_ops
	icon = GFX_focus_Paratroopers
	prerequisite = {
		focus = WGR_army_battleplan_planning_office
	}
	prerequisite = {
		focus = WGR_army_support_equipment
	}
	mutually_exclusive = {
		focus = WGR_army_battleplan_motorized
	}
	relative_position_id = WGR_army_amnesty_for_nazi_officers
	x = 3
	y = 4
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = special_forces_bonus
			bonus = 0.5
			uses = 1
			category = mountaineers_tech
			category = para_tech
			category = marine_tech
		}
	}
}
shared_focus = {
	id = WGR_army_battleplan_defensive_posture
	icon = GFX_focus_Fortification_Effort
	prerequisite = {
		focus = WGR_army_battleplan_motorized
		focus = WGR_army_battleplan_spec_ops
	}
	relative_position_id = WGR_army_amnesty_for_nazi_officers
	x = 2
	y = 5
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_ideas = WGR_defense
	}
}

shared_focus = {
	id = WGR_army_no_amnesty
	icon = GFX_focus_Target_Fascism
	prerequisite = {
		focus = WGR_military_start
	}
	mutually_exclusive = {
		focus = WGR_army_amnesty_for_nazi_officers
	}
	relative_position_id = WGR_military_start
	x = 4
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		army_experience = 15
	}
}
shared_focus = {
	id = WGR_army_superior_firepower
	icon = GFX_focus_Heavy_Strike
	prerequisite = {
		focus = WGR_army_no_amnesty
	}
	mutually_exclusive = {
		focus = WGR_army_mass_assault
	}
	relative_position_id = WGR_army_no_amnesty
	x = -2
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = land_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = cat_superior_firepower
		}
	}
}
shared_focus = {
	id = WGR_army_superior_firepower_arty_effort
	icon = GFX_goal_generic_army_artillery2
	prerequisite = {
		focus = WGR_army_superior_firepower
	}
	relative_position_id = WGR_army_no_amnesty
	x = -2
	y = 2
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = artillery_bonus
			bonus = 0.5
			uses = 1
			category = artillery
		}
	}
}
shared_focus = {
	id = WGR_army_superior_firepower_offensive_posture
	icon = GFX_goal_generic_army_artillery2
	prerequisite = {
		focus = WGR_army_superior_firepower_arty_effort
		focus = WGR_army_mass_assault_recruitment_effort
	}
	relative_position_id = WGR_army_no_amnesty
	x = 0
	y = 3
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_ideas = WGR_offense
	}
}

shared_focus = {
	id = WGR_army_superior_firepower_rocket_arty_effort
	icon = GFX_focus_Bombs_Away
	prerequisite = {
		focus = WGR_army_superior_firepower_arty_effort
	}
	relative_position_id = WGR_army_no_amnesty
	x = -2
	y = 3
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = artillery_bonus
			bonus = 0.5
			uses = 1
			technology = rocket_artillery
			technology = rocket_artillery2
			technology = rocket_artillery3
			technology = rocket_artillery4
		}
	}
}
shared_focus = {
	id = WGR_army_superior_firepower_more_arty
	icon = GFX_goal_generic_artillery
	prerequisite = {
		focus = WGR_army_superior_firepower_rocket_arty_effort
	}
	prerequisite = {
		focus = WGR_army_superior_firepower_offensive_posture
	}
	mutually_exclusive = {
		focus = WGR_army_superior_firepower_infantry_weapons
	}
	relative_position_id = WGR_army_no_amnesty
	x = -2
	y = 4
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = artillery_bonus
			bonus = 0.5
			uses = 1
			category = artillery
		}
	}
}
shared_focus = {
	id = WGR_army_superior_firepower_infantry_weapons
	icon = GFX_focus_Firearms
	prerequisite = {
		focus = WGR_army_superior_firepower_rocket_arty_effort
		focus = WGR_army_mass_assault_naval_landings
	}
	prerequisite = {
		focus = WGR_army_superior_firepower_offensive_posture
	}
	mutually_exclusive = {
		focus = WGR_army_superior_firepower_more_arty
	}
	mutually_exclusive = {
		focus = WGR_army_mass_assault_motorized
	}
	relative_position_id = WGR_army_no_amnesty
	x = 0
	y = 4
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = small_arms_bonus
			bonus = 0.5
			uses = 1
			category = infantry_tech
		}
	}
}
shared_focus = {
	id = WGR_army_superior_firepower_support_equipment
	icon = GFX_focus_Support_Equipment
	prerequisite = {
		focus = WGR_army_superior_firepower_more_arty
		focus = WGR_army_superior_firepower_infantry_weapons
	}
	mutually_exclusive = { focus = WGR_army_mass_assault_spec_ops }
	relative_position_id = WGR_army_no_amnesty
	x = -1
	y = 5
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = support_bonus
			bonus = 0.5
			uses = 1
			category = support_tech
		}
	}
}

shared_focus = {
	id = WGR_army_mass_assault
	icon = GFX_focus_Prepare_the_Troops
	prerequisite = {
		focus = WGR_army_no_amnesty
	}
	mutually_exclusive = {
		focus = WGR_army_superior_firepower
	}
	relative_position_id = WGR_army_no_amnesty
	x = 2
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = land_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = cat_mass_assault
		}
	}
}
shared_focus = {
	id = WGR_army_mass_assault_recruitment_effort
	icon = GFX_focus_Recruitment_Drive
	prerequisite = {
		focus = WGR_army_mass_assault
	}
	relative_position_id = WGR_army_no_amnesty
	x = 2
	y = 2
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_ideas = WGR_recruitment_effort
	}
}
shared_focus = {
	id = WGR_army_mass_assault_naval_landings
	icon = GFX_goal_generic_amphibious_assault
	prerequisite = {
		focus = WGR_army_mass_assault_recruitment_effort
	}
	relative_position_id = WGR_army_no_amnesty
	x = 2
	y = 3
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = land_doctrine_bonus
			bonus = 0.5
			uses = 1
			technology = transport
			technology = landing_craft
			technology = tank_landing_craft
		}
	}
}
shared_focus = {
	id = WGR_army_mass_assault_motorized
	icon = GFX_focus_Combined_Arms
	prerequisite = {
		focus = WGR_army_mass_assault_naval_landings
	}
	prerequisite = {
		focus = WGR_army_superior_firepower_offensive_posture
	}
	mutually_exclusive = {
		focus = WGR_army_superior_firepower_infantry_weapons
	}
	relative_position_id = WGR_army_no_amnesty
	x = 2
	y = 4
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = motorized_bonus
			bonus = 0.5
			uses = 1
			category = motorized_equipment
			category = cat_mechanized_equipment
		}
	}
}
shared_focus = {
	id = WGR_army_mass_assault_spec_ops
	icon = GFX_focus_Commando_Operations
	prerequisite = {
		focus = WGR_army_superior_firepower_infantry_weapons
		focus = WGR_army_mass_assault_motorized
	}
	mutually_exclusive = { focus = WGR_army_superior_firepower_support_equipment }
	relative_position_id = WGR_army_no_amnesty
	x = 1
	y = 5
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = special_forces_bonus
			bonus = 0.5
			uses = 1
			category = mountaineers_tech
			category = para_tech
			category = marine_tech
		}
	}
}
shared_focus = {
	id = WGR_army_doctrines
	icon = GFX_goal_generic_army_doctrines
	prerequisite = {
		focus = WGR_army_mobile_warfare_medium_armor
		focus = WGR_army_battleplan_defensive_posture
		focus = WGR_army_superior_firepower_support_equipment
		focus = WGR_army_mass_assault_spec_ops
	}
	relative_position_id = WGR_military_start
	x = 0
	y = 7
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = land_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = land_doctrine
		}
	}
}
shared_focus = {
	id = WGR_airforce_light_bombers
	icon = GFX_focus_Close_Air_Support
	prerequisite = {
		focus = WGR_military_start
	}
	mutually_exclusive = {
		focus = WGR_airforce_heavy_bombers
	}
	relative_position_id = WGR_military_start
	x = 9
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		air_experience = 20
		add_tech_bonus = {
			name = cas_bonus
			bonus = 0.5
			uses = 1
			category = cas_bomber
		}
	}
}
shared_focus = {
	id = WGR_airforce_carrier_CAS
	icon = GFX_goal_generic_navy_carrier
	prerequisite = {
		focus = WGR_airforce_light_bombers
	}
	relative_position_id = WGR_airforce_light_bombers
	x = -1
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = cas_bonus
			bonus = 0.5
			uses = 1
			technology = cv_CAS1
			technology = cv_CAS2
			technology = cv_CAS3
		}
	}
}
shared_focus = {
	id = WGR_airforce_naval_bombers
	icon = GFX_focus_Naval_Bombers
	prerequisite = {
		focus = WGR_airforce_carrier_CAS
	}
	relative_position_id = WGR_airforce_light_bombers
	x = -1
	y = 2
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = naval_bomber_bonus
			bonus = 0.5
			uses = 1
			category = naval_bomber
		}
	}
}

shared_focus = {
	id = WGR_airforce_heavy_bombers
	icon = GFX_German_Bombers
	prerequisite = {
		focus = WGR_military_start
	}
	mutually_exclusive = {
		focus = WGR_airforce_light_bombers
	}
	relative_position_id = WGR_military_start
	x = 11
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = tactical_bomber_bonus
			bonus = 0.5
			uses = 1
			category = tactical_bomber
		}
	}
}
shared_focus = {
	id = WGR_airforce_strategic_bombers
	icon = GFX_goal_generic_air_bomber
	prerequisite = {
		focus = WGR_airforce_heavy_bombers
	}
	relative_position_id = WGR_airforce_heavy_bombers
	x = 1
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = strategic_bomber_bonus
			bonus = 0.5
			uses = 1
			category = cat_strategic_bomber
		}
	}
}
shared_focus = {
	id = WGR_airforce_heavy_fighters
	icon = GFX_focus_Aerial_Fleet
	prerequisite = {
		focus = WGR_airforce_strategic_bombers
	}
	relative_position_id = WGR_airforce_heavy_bombers
	x = 1
	y = 2
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = heavy_fighter_bonus
			bonus = 0.5
			uses = 1
			category = cat_heavy_fighter
		}
	}
}
shared_focus = {
	id = WGR_airforce_fighters
	icon = GFX_goal_generic_air_fighter
	prerequisite = {
		focus = WGR_airforce_light_bombers
		focus = WGR_airforce_heavy_bombers
	}
	relative_position_id = WGR_airforce_heavy_bombers
	x = -1
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = light_fighter_bonus
			bonus = 0.5
			uses = 1
			category = light_fighter
		}
	}
}
shared_focus = {
	id = WGR_airforce_aa_guns
	icon = GFX_Generic_Anti_Aircraft_Artillery
	prerequisite = {
		focus = WGR_airforce_fighters
	}
	relative_position_id = WGR_airforce_heavy_bombers
	x = -1
	y = 2
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_ideas = WGR_aa
	}
}
shared_focus = {
	id = WGR_airforce_rocketry
	icon = GFX_focus_rocketry
	prerequisite = {
		focus = WGR_airforce_aa_guns
	}
	prerequisite = {
		focus = WGR_airforce_heavy_fighters
		focus = WGR_airforce_naval_bombers
	}
	mutually_exclusive = {
		focus = WGR_airforce_more_fighters
	}
	relative_position_id = WGR_airforce_light_bombers
	x = 0
	y = 3
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = rocketry_bonus
			bonus = 0.5
			uses = 1
			category = rocketry
		}
	}
}
shared_focus = {
	id = WGR_airforce_more_fighters
	icon = GFX_goal_generic_build_airforce
	prerequisite = {
		focus = WGR_airforce_aa_guns
	}
	prerequisite = {
		focus = WGR_airforce_heavy_fighters
		focus = WGR_airforce_naval_bombers
	}
	mutually_exclusive = {
		focus = WGR_airforce_rocketry
	}
	relative_position_id = WGR_airforce_light_bombers
	x = 2
	y = 3
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_ideas = WGR_fighters
	}
}
shared_focus = {
	id = WGR_airforce_doctrine
	icon = GFX_focus_Bombs_Away

	prerequisite = {
		focus = WGR_airforce_more_fighters
		focus = WGR_airforce_rocketry
	}
	relative_position_id = WGR_airforce_light_bombers
	x = 1
	y = 4
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = air_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = air_doctrine
		}
	}
}
shared_focus = {
	id = WGR_navy_compete_with_britain
	icon = GFX_focus_GER_plan_Z
	prerequisite = {
		focus = WGR_military_start
	}
	mutually_exclusive = {
		focus = WGR_navy_go_around_britain
	}
	relative_position_id = WGR_military_start
	x = -14
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		navy_experience = 20
		add_tech_bonus = {
			name = naval_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = cat_fleet_in_being
		}
	}
}
shared_focus = {
	id = WGR_navy_compete_cruiser_effort
	icon = GFX_focus_generic_cruiser2
	prerequisite = {
		focus = WGR_navy_compete_with_britain
	}
	relative_position_id = WGR_navy_compete_with_britain
	x = -1
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = naval_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = cl_tech
			category = ca_tech
		}
	}
}
shared_focus = {
	id = WGR_navy_compete_battleship
	icon = GFX_focus_Battleship
	prerequisite = {
		focus = WGR_navy_compete_cruiser_effort
	}
	relative_position_id = WGR_navy_compete_with_britain
	x = -2
	y = 2
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = naval_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = bb_tech
		}
	}
}
shared_focus = {
	id = WGR_navy_compete_north_sea_dockyards
	icon = GFX_goal_generic_construct_naval_dockyard
	prerequisite = {
		focus = WGR_navy_compete_with_britain
	}
	relative_position_id = WGR_navy_compete_with_britain
	x = 0
	y = 2
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = dockyard
					size > 1
					include_locked = yes
				}
				region = 6
				is_coastal = yes
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = dockyard
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = WGR_navy_compete_north_sea_ports
	icon = GFX_focus_Naval_Effort
	prerequisite = {
		focus = WGR_navy_compete_north_sea_dockyards
	}
	relative_position_id = WGR_navy_compete_with_britain
	x = 0
	y = 3
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		NOT = {
			controls_state = 829
			controls_state = 59
		}
	}	
	completion_reward = {
		829 = {
			if = {
				limit = {
					is_controlled_by = ROOT
				}
				add_building_construction = {
					type = naval_base
					level = 2
					province = 821
					instant_build = yes
				}
			}
		}
		59 = {
			if = {
				limit = {
					is_controlled_by = ROOT
				}
				add_building_construction = {
					type = naval_base
					level = 2
					province = 6325
					instant_build = yes
				}
			}
		}
	}
}
shared_focus = {
	id = WGR_navy_compete_bb_production
	icon = GFX_focus_generic_navy_battleship2
	prerequisite = {
		focus = WGR_navy_compete_north_sea_ports
	}
	prerequisite = {
		focus = WGR_navy_compete_battleship
	}
	prerequisite = {
		focus = WGR_navy_battlecruisers
	}
	mutually_exclusive = {
		focus = WGR_navy_compete_carriers
	}
	relative_position_id = WGR_navy_compete_with_britain
	x = -1
	y = 4
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_ideas = WGR_bb_production
	}
}
shared_focus = {
	id = WGR_navy_compete_carriers
	icon = GFX_focus_Carrier_Focus
	prerequisite = {
		focus = WGR_navy_compete_north_sea_ports
	}
	prerequisite = {
		focus = WGR_navy_compete_battleship
	}
	prerequisite = {
		focus = WGR_navy_battlecruisers
	}
	mutually_exclusive = {
		focus = WGR_navy_compete_bb_production
	}
	relative_position_id = WGR_navy_compete_with_britain
	x = 1
	y = 4
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = carrier_bonus
			bonus = 0.5
			uses = 1
			category = cv_tech
		}
	}
}

shared_focus = {
	id = WGR_navy_screens
	icon = GFX_Goal_Generic_Refit_Destroyers
	prerequisite = {
		focus = WGR_navy_compete_with_britain
		focus = WGR_navy_go_around_britain
	}
	relative_position_id = WGR_navy_compete_with_britain
	x = 2
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = destroyer_bonus
			bonus = 0.5
			uses = 1
			category = dd_tech
		}
	}
}
shared_focus = {
	id = WGR_navy_battlecruisers
	icon = GFX_focus_Battlefleet
	prerequisite = {
		focus = WGR_navy_screens
	}
	relative_position_id = WGR_navy_compete_with_britain
	x = 2
	y = 2
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = battlecruiser_bonus
			bonus = 0.5
			uses = 1
			category = bc_tech
		}
	}
}

shared_focus = {
	id = WGR_navy_go_around_britain
	icon = GFX_focus_Naval_Operations
	prerequisite = {
		focus = WGR_military_start
	}
	mutually_exclusive = {
		focus = WGR_navy_compete_with_britain
	}
	relative_position_id = WGR_military_start
	x = -10
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		navy_experience = 20
		add_tech_bonus = {
			name = naval_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = cat_trade_interdiction
		}
	}
}
shared_focus = {
	id = WGR_navy_go_around_subs
	icon = GFX_goal_generic_navy_submarine
	prerequisite = {
		focus = WGR_navy_go_around_britain
	}
	relative_position_id = WGR_navy_go_around_britain
	x = 1
	y = 1
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = submarine_bonus
			bonus = 0.5
			uses = 1
			category = ss_tech
		}
	}
}
shared_focus = {
	id = WGR_navy_go_around_better_fuel_tanks
	icon = GFX_goal_generic_wolf_pack
	prerequisite = {
		focus = WGR_navy_go_around_subs
	}
	relative_position_id = WGR_navy_go_around_britain
	x = 2
	y = 2
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_ideas = WGR_fuel_tanks
	}
}
shared_focus = {
	id = WGR_navy_go_around_baltic_dockyards
	icon = GFX_focus_Small_Navy
	prerequisite = {
		focus = WGR_navy_go_around_britain
	}
	relative_position_id = WGR_navy_go_around_britain
	x = 0
	y = 2
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = dockyard
					size > 1
					include_locked = yes
				}
				is_coastal = yes
				region = 8
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = dockyard
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = WGR_navy_go_around_baltic_ports
	icon = GFX_focus_Naval_Effort
	prerequisite = {
		focus = WGR_navy_go_around_baltic_dockyards
	}
	relative_position_id = WGR_navy_go_around_britain
	x = 0
	y = 3
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		829 = {
			if = {
				limit = {
					is_controlled_by = ROOT
				}
				add_building_construction = {
					type = naval_base
					level = 2
					province = 821
					instant_build = yes
				}
			}
		}
		58 = {
			if = {
				limit = {
					is_controlled_by = ROOT
				}
				add_building_construction = {
					type = naval_base
					level = 2
					province = 6389
					instant_build = yes
				}
			}
		}
	}
}
shared_focus = {
	id = WGR_navy_go_around_cruiser_effort
	icon = GFX_Goal_Generic_Defend_Conveys
	prerequisite = {
		focus = WGR_navy_go_around_baltic_ports
	}
	prerequisite = {
		focus = WGR_navy_go_around_better_fuel_tanks
	}
	prerequisite = {
		focus = WGR_navy_battlecruisers
	}
	mutually_exclusive = {
		focus = WGR_navy_go_around_sub_production
	}
	relative_position_id = WGR_navy_go_around_britain
	x = -1
	y = 4
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = cruiser_bonus
			bonus = 0.5
			uses = 1
			category = ca_tech
			category = cl_tech
		}
	}
}
shared_focus = {
	id = WGR_navy_go_around_sub_production
	icon = GFX_goal_generic_navy_submarine
	prerequisite = {
		focus = WGR_navy_go_around_baltic_ports
	}
	prerequisite = {
		focus = WGR_navy_go_around_better_fuel_tanks
	}
	prerequisite = {
		focus = WGR_navy_battlecruisers
	}
	mutually_exclusive = {
		focus = WGR_navy_go_around_cruiser_effort
	}
	relative_position_id = WGR_navy_go_around_britain
	x = 1
	y = 4
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_ideas = WGR_sub_production
	}
}
shared_focus = {
	id = WGR_navy_doctrine
	icon = GFX_focus_Status_of_the_Admirality
	prerequisite = {
		focus = WGR_navy_go_around_sub_production
		focus = WGR_navy_go_around_cruiser_effort
		focus = WGR_navy_compete_bb_production
		focus = WGR_navy_compete_carriers
	}
	relative_position_id = WGR_navy_compete_with_britain
	x = 2
	y = 5
	cost = 5
	available = {
		NOT = {
			has_idea = versailles_treaty
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_tech_bonus = {
			name = naval_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = naval_doctrine
		}
	}
}
