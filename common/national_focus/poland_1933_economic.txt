
shared_focus = {
	id = POL_continue_measures
	icon = GFX_goal_pol666
	
	ai_will_do = { factor = 100 }
	
	x = 38
	y = 0
	cost = 2
	
	available_if_capitulated = no
	available = { has_completed_focus = POL_administration_reform }
	completion_reward = {
		add_political_power = 100
	}
}
shared_focus = {
	id = POL_begin1a
	icon = GFX_focus_Destroy_Trade_Unions
	
	ai_will_do = { factor = 100 }
	
	x = 38
	y = 2
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_begin1b }
	completion_reward = {
	add_ideas = POL_increased_work_hours
	add_political_power = -50
	}
}
shared_focus = {
	id = POL_begin1b
	icon = GFX_focus_Tax_Incentives
	
	ai_will_do = { factor = 100 }
	
	x = 38
	y = 1
	cost = 3
	
	available_if_capitulated = no
	prerequisite = { focus = POL_continue_measures }
	completion_reward = {
	POL_Move_Up_Great_Crisis = yes
	}
}
shared_focus = {
	id = POL_begin2a
	icon = GFX_focus_strengthen_small_production
	
	ai_will_do = { factor = 100 }
	
	x = 36
	y = 2
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_begin1b }
	completion_reward = {
	add_offsite_building = { type = industrial_complex level = 1 }
	}
}
shared_focus = {
	id = POL_begin2b
	icon = GFX_focus_Mechanization
	
	ai_will_do = { factor = 100 }
	
	x = 40
	y = 2
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_begin1b }
	completion_reward = {
		random_owned_state = {
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		random_owned_state = {
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = POL_gold_standard
	icon = GFX_goal_Gold_Standard_Question
	
	ai_will_do = { factor = 100 }
	
	x = 38
	y = 3
	cost = 1
	available = { has_completed_focus = POL_1933_elections }
	available_if_capitulated = no
	prerequisite = { focus = POL_begin2a }
	prerequisite = { focus = POL_begin2b }
	prerequisite = { focus = POL_begin1a }
	
	completion_reward = {
		custom_effect_tooltip = POL_gold_standard_tooltip
		country_event = pol.12
	}
}
#Chad brygada command economy
shared_focus = {
	id = POL_brygada
	icon = GFX_goal_I_Brygada_Gospodarcza2
	
	ai_will_do = { factor = 100 }
	
	x = 26
	y = 4
	cost = 1
	mutually_exclusive = { focus = POL_liberal }
	mutually_exclusive = { focus = POL_lewiatan }
	available_if_capitulated = no
	available = { has_country_flag = POL_left_the_gold }
	prerequisite = { focus = POL_gold_standard }
	
	completion_reward = {
	swap_ideas = {
		remove_idea = wladyslawzawadzki1
		add_idea = stefanstarzynski
	}
	}
}
shared_focus = {
	id = POL_money_politics
	icon = GFX_focus_Modernized_Finances
	
	ai_will_do = { factor = 100 }
	
	x = 26
	y = 5
	cost = 1
	
	available_if_capitulated = no
	prerequisite = { focus = POL_brygada }
	completion_reward = {
	add_political_power = 50
	}
}
shared_focus = {
	id = POL_unemployed_fund
	icon = GFX_focus_National_Unemployment_Policy
	
	ai_will_do = { factor = 100 }
	available = { has_political_power > 0 }
	x = 30
	y = 6
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_money_politics }
	completion_reward = {
		add_ideas = POL_unemployed_fund_idea
	}
}
shared_focus = {
	id = POL_reducing_work_time_mining
	icon = GFX_focus_Reduce_work_hour
	
	ai_will_do = { factor = 100 }
	
	x = 31
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_unemployed_fund }
	completion_reward = {
	remove_ideas  = POL_increased_work_hours
	}
}
shared_focus = {
	id = POL_public_works
	icon = GFX_goal_Public_Works
	
	ai_will_do = { factor = 100 }
	
	x = 29
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_unemployed_fund }
	
	completion_reward = {
		custom_effect_tooltip = POL_public_works_tooltip
	}
}
shared_focus = {
	id = POL_propaganda_workers
	icon = GFX_goal_ZZZ
	
	ai_will_do = { factor = 100 }
	
	x = 30
	y = 8
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_public_works }
	prerequisite = { focus = POL_reducing_work_time_mining}
	
	completion_reward = {
		add_popularity = {
				ideology = authoritarian_socialism 
				popularity = 0.02
		}
		add_popularity = {
				ideology = social_democracy
				popularity = -0.01
		}
		add_popularity = {
				ideology = socialism
				popularity = -0.01
		}
	}
}
shared_focus = {
	id = POL_socialist_support
	icon = GFX_focus_Workers_Rights
	
	ai_will_do = { factor = 100 }
	
	x = 30
	y = 9
	cost = 2
	
	available_if_capitulated = no
	prerequisite = { focus = POL_propaganda_workers }
	completion_reward = {
		add_popularity = {
			ideology = authoritarian_socialism
			popularity = 0.01
		}
		add_popularity = {
			ideology = socialism
			popularity = 0.02
		}
		add_popularity = {
			ideology = social_democracy
			popularity = 0.01
		}
		add_stability = 0.05
		add_political_power = 50
	}
}
shared_focus = {
	id = POL_deleverage_local_gov2
	icon = GFX_focus_Cancel_Debts
	
	ai_will_do = { factor = 100 }
	
	x = 25
	y = 6
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_money_politics }
	completion_reward = {
		add_timed_idea = {
			idea = POL_deleveraged
			days = 365
		}
	}
}
shared_focus = {
	id = POL_dollar_decree2
	icon = GFX_focus_mex_privatisation
	
	ai_will_do = { factor = 100 }
	
	x = 25
	y = 7
	cost = 5
	available = { date > 1934.01.30 }
	available_if_capitulated = no
	prerequisite = { focus = POL_deleverage_local_gov2 }
	completion_reward = {
		add_timed_idea = {
			idea = POL_dollar_decree_idea
			days = 1095
		}
	}
}
shared_focus = {
	id = POL_public_investment
	icon = GFX_focus_Economic_Stimulus
	
	ai_will_do = { factor = 100 }
	
	x = 25
	y = 8
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_dollar_decree2 }
	completion_reward = {
		add_ideas = POL_increased_public_investment
	}
}
shared_focus = {
	id = POL_deleverage_country
	icon = GFX_focus_Cancel_Debts
	
	ai_will_do = { factor = 100 }
	
	x = 27
	y = 6
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_money_politics }
	completion_reward = {
		add_timed_idea = {
			idea = POL_deleveraged_us
			days = 182
		}
	}
}
shared_focus = {
	id = POL_obligations
	icon = GFX_focus_Political_reform_1
	
	ai_will_do = { factor = 100 }
	
	x = 27
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_deleverage_country }
	completion_reward = {
		add_timed_idea = {
			idea = POL_issued_gov_bonds
			days = 200
		}
	}
}
shared_focus = {
	id = POL_investment_abroad2
	icon = GFX_focus_escape_from_the_market_economy
	
	ai_will_do = { factor = 100 }
	
	x = 27
	y = 8
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_obligations }
	completion_reward = {
		remove_ideas  = POL_issued_gov_bonds
		add_timed_idea = {
			idea = POL_issued_gov_bonds2
			days = 200
		}
	}
}
shared_focus = {
	id = POL_small_tax_reform
	icon = GFX_focus_Streamlined_Tax_Code
	
	ai_will_do = { factor = 100 }
	
	x = 26
	y = 9
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_investment_abroad2 }
	prerequisite = { focus = POL_public_investment }
	completion_reward = {
		POL_Move_Up_Great_Crisis = yes
		add_political_power = -50
	}
}
#
shared_focus = {
	id = POL_state_planning_ministry
	icon = GFX_focus_Planned_Economy
	
	ai_will_do = { factor = 100 }
	available = { has_political_power > 0 }
	x = 20
	y = 5
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_brygada }
	completion_reward = {
	add_ideas = POL_state_planning
	POL_Move_Up_Great_Crisis = yes
	}
}
shared_focus = {
	id = POL_parcelation_agriculture
	icon = GFX_focus_Agrarian_Reform
	
	ai_will_do = { factor = 100 }
	available = { has_political_power > 0 }
	x = 23
	y = 6
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_state_planning_ministry }
	completion_reward = {
		add_timed_idea = {
			idea = landowner_angry
			days = 180
		}
		add_political_power = -50
	}
}
shared_focus = {
	id = POL_agriculture_loans
	icon = GFX_focus_Tractors
	
	ai_will_do = { factor = 100 }
	
	x = 23
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_parcelation_agriculture }
	completion_reward = {
		add_timed_idea = {
			idea = recently_loaned
			days = 180
		}
	}
}
shared_focus = {
	id = POL_agriculture_reform2
	icon = GFX_focus_Agriculture
	
	ai_will_do = { factor = 100 }
	
	x = 23
	y = 8
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_agriculture_loans }
	completion_reward = {
		add_ideas = POL_agricultural_reform_idea
		add_political_power = -50
		POL_Move_Up_Great_Crisis = yes
	}
}
shared_focus = {
	id = POL_support_peoplesparty
	icon = GFX_focus_Popular_Sentiment
	
	ai_will_do = { factor = 100 }
	
	x = 23
	y = 9
	cost = 2
	
	available_if_capitulated = no
	prerequisite = { focus = POL_agriculture_reform2 }
	completion_reward = {
		add_popularity = {
			ideology = social_liberalism
			popularity = 0.02
		}
		add_stability = 0.05
		add_political_power = 75
	}
}
shared_focus = {
	id = POL_work_inspectorate
	icon = GFX_focus_National_Assurance_Service
	available = { has_political_power > 0 }
	ai_will_do = { factor = 100 }
	
	x = 18
	y = 6
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_state_planning_ministry }
	completion_reward = {
	add_ideas = POL_labour_inspectorate
	}
}
shared_focus = {
	id = POL_public_insurance_regulation
	icon = GFX_Relief_Plans
	
	ai_will_do = { factor = 100 }
	
	x = 17
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_work_inspectorate }
	completion_reward = {
	add_political_power = 50
	add_stability = 0.025
	}
}
shared_focus = {
	id = POL_insurance_system
	icon = GFX_Unemployeds_Help
	
	ai_will_do = { factor = 100 }
	
	x = 19
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_work_inspectorate }
	completion_reward = {
	swap_ideas = {
		remove_idea = POL_labour_inspectorate
		add_idea = POL_labour_inspectorate2
	}
	}
}
shared_focus = {
	id = POL_something
	icon = GFX_focus_Redraw_Admin_Borders
	
	ai_will_do = { factor = 100 }
	
	x = 21
	y = 6
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_state_planning_ministry }
	completion_reward = {
		swap_ideas = {
			remove_idea = POL_state_planning
			add_idea = POL_state_planning2
		}
	}
}
shared_focus = {
	id = POL_somethingV2
	icon = GFX_focus_Consensus_with_Peasants
	
	ai_will_do = { factor = 100 }
	
	x = 21
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_something }
	completion_reward = {
		add_stability = 0.05
		swap_ideas = {
			remove_idea = POL_state_planning2
			add_idea = POL_state_planning3
		}
	}
}
shared_focus = {
	id = POL_bank_nationalization
	icon = GFX_focus_Economics_School
	
	ai_will_do = { factor = 100 }
	
	x = 19
	y = 8
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_somethingV2 }
	prerequisite = { focus = POL_insurance_system }
	prerequisite = { focus = POL_public_insurance_regulation }
	completion_reward = {
	POL_Move_Up_Great_Crisis = yes
	}
}
shared_focus = {
	id = POL_industry_nationalization
	icon = GFX_focus_Control_the_Economy
	
	ai_will_do = { factor = 100 }
	
	x = 19
	y = 9
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_bank_nationalization }
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
		random_owned_state = {
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		random_owned_state = {
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = POL_price_commission
	icon = GFX_focus_Office_Blevin
	
	ai_will_do = { factor = 100 }
	available = { has_political_power > 0 }
	x = 15
	y = 6
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_state_planning_ministry }
	completion_reward = {
	add_ideas = POL_interministerial
	}
}
shared_focus = {
	id = POL_price_reglamentation
	icon = GFX_focus_hol_abandon_the_gold_standard
	
	ai_will_do = { factor = 100 }
	
	x = 15
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_price_commission }
	completion_reward = {
	swap_ideas = {
		remove_idea = POL_interministerial
		add_idea = POL_interministerial2
		}
	}
}
shared_focus = {
	id = POL_securing_supply
	icon = GFX_focus_Bread_Trade
	
	ai_will_do = { factor = 100 }
	
	x = 15
	y = 8
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_price_reglamentation }
	completion_reward = {
	swap_ideas = {
		remove_idea = POL_interministerial2
		add_idea = POL_interministerial3
		}	
	}
}
shared_focus = {
	id = POL_fight_cartels
	icon = GFX_focus_eng_concessions_to_the_trade_unions
	
	ai_will_do = { factor = 100 }
	
	x = 15
	y = 9
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_securing_supply }
	completion_reward = {
	swap_ideas = {
		remove_idea = POL_interministerial3
		add_idea = POL_interministerial4
		}
		random_owned_state = {
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		random_owned_state = {
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		add_offsite_building = { type = industrial_complex level = -1 }		
	}
}
shared_focus = {
	id = POL_way_polish_statism
	icon = GFX_focus_Common_Identity
	
	ai_will_do = { factor = 100 }
	
	x = 23
	y = 12
	cost = 2
	
	available_if_capitulated = no
	prerequisite = { focus = POL_socialist_support }
	prerequisite = { focus = POL_small_tax_reform }
	prerequisite = { focus = POL_support_peoplesparty }
	prerequisite = { focus = POL_industry_nationalization }
	prerequisite = { focus = POL_fight_cartels }
	prerequisite = { focus = POL_equal_prices }
	completion_reward = {
	add_political_power = 100
	}
}
#liberal brygada
shared_focus = {
	id = POL_reducing_imports
	icon = GFX_focus_Protectionism
	
	ai_will_do = { factor = 100 }
	
	x = 34
	y = 5
	cost = 5
	available_if_capitulated = no
	prerequisite = { focus = POL_liberal focus = POL_brygada }
	completion_reward = {
		swap_ideas = {
		remove_idea = export_focus
		add_idea = limited_exports
		}
	}
}
shared_focus = {
	id = POL_increasing_taxes
	icon = GFX_focus_Progressive_Taxation
	
	ai_will_do = { factor = 100 }
	
	x = 34
	y = 6
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_reducing_imports }
	completion_reward = {
		add_timed_idea = {
			idea = POL_taxes_raised
			days = 365
		}
	}
}
shared_focus = {
	id = POL_nationalising_failing_companies
	icon = GFX_focus_Military_Government
	
	ai_will_do = { factor = 100 }
	
	x = 33
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_increasing_taxes }
	completion_reward = {
		custom_effect_tooltip = POL_unlocks_nationalization
	}
}
shared_focus = {
	id = POL_lower_prices
	icon = GFX_focus_Business_Men
	
	ai_will_do = { factor = 100 }
	
	x = 32
	y = 9
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_nationalising_failing_companies }
	
	completion_reward = {
		add_stability = 0.05
		add_political_power = -50
	}
}
shared_focus = {
	id = POL_decreasing_production_costs
	icon = GFX_focus_Price_Reductions
	
	ai_will_do = { factor = 100 }
	
	x = 33
	y = 10
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_lower_prices }
	prerequisite = { focus = POL_promoting_polish_products }
	completion_reward = {
		POL_Move_Up_Protectionism = yes
	}
}
shared_focus = {
	id = POL_deleverage
	icon = GFX_focus_Agriculture_2
	
	ai_will_do = { factor = 100 }
	
	x = 34
	y = 8
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_increasing_taxes }
	
	completion_reward = {
		add_timed_idea = {
			idea = POL_deleveraged2
			days = 365
		}
	}
}
shared_focus = {
	id = POL_promoting_polish_products
	icon = GFX_focus_Buy_Surplus_Produce
	
	ai_will_do = { factor = 100 }
	
	x = 34
	y = 9
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_deleverage }
	completion_reward = {
		add_ideas = POL_protectionism
	}
}
shared_focus = {
	id = POL_helping_failing_companies
	icon = GFX_focus_War_Liquidation_Fund
	
	ai_will_do = { factor = 100 }
	
	x = 35
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_increasing_taxes }
	completion_reward = {
		add_timed_idea = {
			idea = POL_deleveraged3
			days = 365
		}
	}
}
shared_focus = {
	id = POL_lowering_cartel_prices
	icon = GFX_focus_Negotiate_Fair_Prices
	
	ai_will_do = { factor = 100 }
	
	x = 36
	y = 9
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_helping_failing_companies }
	completion_reward = {
		add_stability = -0.05
		add_political_power = -50
	}
}
shared_focus = {
	id = POL_increasing_product_prices
	icon = GFX_focus_Set_Agricultural_Prices
	
	ai_will_do = { factor = 100 }
	
	x = 35
	y = 10
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_lowering_cartel_prices }
	prerequisite = { focus = POL_promoting_polish_products }
	completion_reward = {
		POL_Move_Up_Protectionism = yes
	}
}
shared_focus = {
	id = POL_equal_prices
	icon = GFX_focus_Proportional_Suffrage
	
	ai_will_do = { factor = 100 }
	
	x = 34
	y = 11
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_increasing_product_prices }
	prerequisite = { focus = POL_promoting_polish_products }
	prerequisite = { focus = POL_decreasing_production_costs }
	completion_reward = {
		add_ideas = POL_agricultural_industrial_balance
	}
}

#liberal centrist economic hell
shared_focus = {
	id = POL_liberal
	icon = GFX_goal_liberalowie
	
	ai_will_do = { factor = 100 }
	
	x = 38
	y = 4
	cost = 1
	mutually_exclusive = { focus = POL_brygada }
	mutually_exclusive = { focus = POL_lewiatan }
	available = { has_country_flag = POL_stayed_with_gold }
	available_if_capitulated = no
	prerequisite = { focus = POL_gold_standard }
	
	completion_reward = {
		add_stability = 0.05
		add_political_power = 50
	}
}
shared_focus = {
	id = POL_reduce_money
	icon = GFX_focus_Grab_Money
	
	ai_will_do = { factor = 100 }
	
	x = 38
	y = 5
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_liberal focus = POL_lewiatan }
	
	completion_reward = {
	add_stability = 0.05
	add_political_power = 50
	}
}
shared_focus = {
	id = POL_deleverage_local_gov
	icon = GFX_focus_Cancel_Debts
	
	ai_will_do = { factor = 100 }
	
	x = 37
	y = 6
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_reduce_money }
	
	completion_reward = {
		add_timed_idea = {
			idea = POL_deleveraged4
			days = 365
		}
	}
}
shared_focus = {
	id = POL_investment_loans
	icon = GFX_focus_balanced_investments
	
	ai_will_do = { factor = 100 }
	
	x = 37
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_deleverage_local_gov }
	completion_reward = {
		add_ideas = POL_issued_gov_bonds
	}
}
shared_focus = {
	id = POL_dollar_decree
	icon = GFX_focus_mex_privatisation
	
	ai_will_do = { factor = 100 }
	
	x = 37
	y = 8
	cost = 5
	available = { date > 1934.01.30 }
	available_if_capitulated = no
	prerequisite = { focus = POL_investment_loans }
	
	completion_reward = {
		add_timed_idea = {
			idea = POL_dollar_decree_idea
			days = 1095
		}
	}
}

shared_focus = {
	id = POL_paying_loans
	icon = GFX_focus_Economic_Stimulus
	
	ai_will_do = { factor = 100 }
	
	x = 39
	y = 6
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_reduce_money }
	
	completion_reward = {
		add_timed_idea = {
			idea = POL_mantain_payment
			days = 730
		}
	}
}
shared_focus = {
	id = POL_taking_loans
	icon = GFX_focus_Grab_Money
	
	ai_will_do = { factor = 100 }
	
	x = 39
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_paying_loans }
	
	completion_reward = {
		remove_ideas = POL_mantain_payment
		add_timed_idea = {
			idea = POL_loan_balance
			days = 527
		}
	}
}
shared_focus = {
	id = POL_investment_abroad
	icon = GFX_focus_escape_from_the_market_economy
	
	ai_will_do = { factor = 100 }
	
	x = 39
	y = 8
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_taking_loans }
	prerequisite = { focus = POL_investment_loans }
	
	completion_reward = {
		remove_ideas  = POL_issued_gov_bonds
		add_timed_idea = {
			idea = POL_issued_gov_bonds2
			days = 365
		}
	}
}
shared_focus = {
	id = POL_customs_reform
	icon = GFX_focus_Political_reform_2
	
	ai_will_do = { factor = 100 }
	
	x = 38
	y = 9
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_investment_abroad }
	prerequisite = { focus = POL_dollar_decree }
	
	completion_reward = {
		POL_Move_Up_Great_Crisis = yes
	}
}
shared_focus = {
	id = POL_survive_storm
	icon = GFX_goal_pol666
	
	ai_will_do = { factor = 100 }
	
	x = 38
	y = 12
	cost = 2
	
	available_if_capitulated = no
	prerequisite = { focus = POL_equal_prices }
	prerequisite = { focus = POL_pulling_belt }
	prerequisite = { focus = POL_customs_reform }
	completion_reward = {
	add_political_power = 100
	}
}
#liberal and ancap explosive mix
shared_focus = {
	id = POL_export
	icon = GFX_focus_Free_Trade
	
	ai_will_do = { factor = 100 }
	
	x = 42
	y = 5
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_lewiatan focus = POL_liberal }
	completion_reward = {
		add_ideas = POL_no_barriers
	}
}
shared_focus = {
	id = POL_budget_reduction
	icon = GFX_focus_Increase_Funding
	
	ai_will_do = { factor = 100 }
	
	x = 42
	y = 6
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_export }
	completion_reward = {
		add_ideas = POL_cut_budget
	}
}
shared_focus = {
	id = POL_lowering_teacher_salaries
	icon = GFX_focus_Cut_state_workers_expenses
	
	ai_will_do = { factor = 100 }
	
	x = 41
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_budget_reduction }
	completion_reward = {
	add_stability = -0.05
	add_political_power = -25
	}
}
shared_focus = {
	id = POL_lowering_welfare
	icon = GFX_focus_Wage_Reduction	
	
	ai_will_do = { factor = 100 }
	
	x = 43
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_budget_reduction }
	completion_reward = {
		add_ideas = POL_welfare_lowered
	}
}
shared_focus = {
	id = POL_support_banks
	icon = GFX_focus_found_the_investment_bank
	
	ai_will_do = { factor = 100 }
	
	x = 45
	y = 6
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_export }
	completion_reward = {
	add_political_power = 50
	add_stability = 0.05
	}
}
shared_focus = {
	id = POL_support_thicc_capital
	icon = GFX_focus_Industrial_Council
	
	ai_will_do = { factor = 100 }
	
	x = 45
	y = 7
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_support_banks }
	completion_reward = {
	POL_Move_Up_Great_Crisis = yes
	}
}
shared_focus = {
	id = POL_search_savings
	icon = GFX_focus_Money_Bags
	
	ai_will_do = { factor = 100 }
	
	x = 43
	y = 8
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_lowering_teacher_salaries }
	prerequisite = { focus = POL_lowering_welfare }
	prerequisite = { focus = POL_support_thicc_capital }
	completion_reward = {
		add_timed_idea = {
			idea = POL_savings_found
			days = 200
		}
	}
}
shared_focus = {
	id = POL_pulling_belt
	icon = GFX_focus_Root_out_Funding
	
	ai_will_do = { factor = 100 }
	
	x = 43
	y = 9
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_search_savings }
	completion_reward = {
		remove_ideas = POL_cut_budget
		add_timed_idea = {
			idea = POL_cut_budget2
			days = 730
		}		
	}
}
#LEWIATAN ANCAPISTAN
shared_focus = {
	id = POL_lewiatan
	icon = GFX_goal_Lewiatan2
	
	ai_will_do = { factor = 100 }
	
	x = 46
	y = 4
	cost = 1
	mutually_exclusive = { focus = POL_liberal }
	mutually_exclusive = { focus = POL_brygada }
	available = { has_country_flag = POL_left_the_gold }
	available_if_capitulated = no
	prerequisite = { focus = POL_gold_standard }
	
	completion_reward = {
	swap_ideas = {
		remove_idea = wladyslawzawadzki1
		add_idea = andrzejwierzbicki
		}
	}
}
shared_focus = {
	id = POL_lower_taxes2
	icon = GFX_focus_Reduce_Employer_Contributions
	
	ai_will_do = { factor = 100 }
	
	x = 48
	y = 5
	cost = 5
	available_if_capitulated = no
	prerequisite = { focus = POL_lewiatan }
	
	completion_reward = {
		add_timed_idea = {
			idea = POL_lower_taxes
			days = 365
		}
		add_stability = 0.05
	}
}
shared_focus = {
	id = POL_liquidate_state_monopoly
	icon = GFX_focus_Lock_Them_Up
	
	ai_will_do = { factor = 100 }
	
	x = 49
	y = 6
	cost = 5
	available_if_capitulated = no
	prerequisite = { focus = POL_lower_taxes2 }
	
	completion_reward = {
		POL_Move_Up_Great_Crisis = yes
	}
}
shared_focus = {
	id = POL_privatize_state_companies
	icon = GFX_focus_Corporatism
	
	ai_will_do = { factor = 100 }
	
	x = 47
	y = 6
	cost = 5
	available_if_capitulated = no
	prerequisite = { focus = POL_lower_taxes2 }
	
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = POL_lower_tariffs
	icon = GFX_focus_Free_Trade
	
	ai_will_do = { factor = 100 }
	
	x = 49
	y = 7
	cost = 5
	available_if_capitulated = no
	prerequisite = { focus = POL_liquidate_state_monopoly }
	
	completion_reward = {
		swap_ideas = {
			remove_idea = export_focus
			add_idea = free_trade
		}
	}
}
shared_focus = {
	id = POL_degrade_conditions
	icon = GFX_goal_Degrade_Working_Conditions
	
	ai_will_do = { factor = 100 }
	
	x = 49
	y = 8
	cost = 5
	
	available_if_capitulated = no
	prerequisite = { focus = POL_lower_tariffs  }
	
	completion_reward = {
		swap_ideas = {
			remove_idea = POL_increased_work_hours
			add_idea = POL_increased_work_hours2
		}
		add_political_power = -50
	}
}
shared_focus = {
	id = POL_privatize_infrastructure
	icon = GFX_focus_Railways
	
	ai_will_do = { factor = 100 }
	
	x = 47
	y = 7
	cost = 5
	available_if_capitulated = no
	prerequisite = { focus = POL_privatize_state_companies }
	
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		add_political_power = -25
	}
}
shared_focus = {
	id = POL_privatize_healthcare
	icon = GFX_focus_Medicine
	
	ai_will_do = { factor = 100 }
	
	x = 47
	y = 8
	cost = 5
	available_if_capitulated = no
	prerequisite = { focus = POL_privatize_infrastructure }
	
	completion_reward = {
		add_ideas = POL_privatized_healthcare
		add_political_power = -25
	}
}
shared_focus = {
	id = POL_equal_prices2
	icon = GFX_focus_Proportional_Suffrage
	
	ai_will_do = { factor = 100 }
	
	x = 48
	y = 9
	cost = 5
	available_if_capitulated = no
	prerequisite = { focus = POL_privatize_healthcare }
	prerequisite = { focus = POL_degrade_conditions }
	
	completion_reward = {
		add_ideas = POL_agricultural_industrial_balance
	}
}
shared_focus = {
	id = POL_polish_capitalism
	icon = GFX_focus_Capitalist_Systems
	
	ai_will_do = { factor = 100 }
	
	x = 46
	y = 12
	cost = 2
	available_if_capitulated = no
	prerequisite = { focus = POL_equal_prices2 }
	prerequisite = { focus = POL_pulling_belt }
	prerequisite = { focus = POL_customs_reform }	
	completion_reward = {
	add_political_power = 100
	}
}
##to use:





#shared_focus = {
#	id = POL_overdue_taxes
#	icon = GFX_focus_Cancel_Debts
#	
#	ai_will_do = { factor = 100 }
#	
#	x = 
#	y = 
#	cost = 5
#	
#	available_if_capitulated = no
#	prerequisite = { focus = POL_ }
#	
#	completion_reward = {
#		swap_ideas = {
#		remove_idea = POL_great_crisis
#		add_idea = POL_great_crisis2
#		}
#	}
#}




