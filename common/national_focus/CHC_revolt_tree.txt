# focuses here
#s_team337
shared_focus = {
	id = CHC_Revolt_Start
	icon = GFX_focus_Chechnya
	#prerequisite = {
	#	focus = CHC_Revolt_Start
	#}
	x = 4
	y = 0
	#relative_position_id = CHC_Revolt_Start
	cost = 0
	ai_will_do = {
		factor = 100
	}
	available = {
		has_country_flag = CHC_Independence_War
	}
	bypass = {
		has_country_flag = CHC_Independence_War
	}
	completion_reward = {
		
	}
}
shared_focus = {
	id = CHC_Revolt_Military_Affairs
	icon = GFX_focus_Recruitment_Drive
	prerequisite = {
		focus = CHC_Revolt_Start
	}
	x = -2
	y = 1
	relative_position_id = CHC_Revolt_Start
	cost = 2.86
	ai_will_do = {
		factor = 20
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		army_experience = 20
		add_ideas = CHC_cos_Maribek_Sheripov
	}
}
shared_focus = {
	id = CHC_Revolt_Guerilla_Training
	icon = GFX_focus_Guerilla_Warfare
	prerequisite = {
		focus = CHC_Revolt_Military_Affairs
	}
	x = -3
	y = 2
	relative_position_id = CHC_Revolt_Start
	cost = 5
	ai_will_do = {
		factor = 30
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_ideas = CHC_Guerilla
	}
}
shared_focus = {
	id = CHC_Revolt_Cavalry
	icon = GFX_focus_Cavalry
	prerequisite = {
		focus = CHC_Revolt_Guerilla_Training
	}
	x = -4
	y = 3
	relative_position_id = CHC_Revolt_Start
	cost = 6.43
	ai_will_do = {
		factor = 18
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		hidden_effect = {
			load_oob = "CHC_cavalry"
		}
		custom_effect_tooltip = CHC_cav_tt
	}
}
shared_focus = {
	id = CHC_Revolt_Chain_of_Command
	icon = GFX_focus_Officers_Command
	prerequisite = {
		focus = CHC_Revolt_Military_Affairs
	}
	prerequisite = {
		focus = CHC_Revolt_Political_Affairs
	}
	x = -1
	y = 2
	relative_position_id = CHC_Revolt_Start
	cost = 4.29
	ai_will_do = {
		factor = 30
	}
	available = {
	}
	bypass = {
		NOT = {
			has_idea = CHC_Disorganized_Army
		}
	}
	completion_reward = {
		swap_ideas = {
			remove_idea = CHC_Disorganized_Army
			add_idea = CHC_Disorganized_Army2
		}
	}
}
shared_focus = {
	id = CHC_Revolt_Oath
	icon = GFX_focus_Oath_of_Loyalty
	prerequisite = {
		focus = CHC_Revolt_Guerilla_Training
	}
	prerequisite = {
		focus = CHC_Revolt_Chain_of_Command
	}
	x = -2
	y = 3
	relative_position_id = CHC_Revolt_Start
	cost = 2
	ai_will_do = {
		factor = 15
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_popularity = {
			ideology = nationalism
			popularity = 0.05
		}
	}
}
shared_focus = {
	id = CHC_Revolt_Doctrine_Effort
	icon = GFX_goal_generic_army_doctrines
	prerequisite = {
		focus = CHC_Revolt_Cavalry
	}
	prerequisite = {
		focus = CHC_Revolt_Oath
	}
	x = -3
	y = 4
	relative_position_id = CHC_Revolt_Start
	cost = 5
	ai_will_do = {
		factor = 15
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = land_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = land_doctrine
		}
	}
}
shared_focus = {
	id = CHC_Revolt_Dagestan_Units
	icon = GFX_focus_Rally
	prerequisite = {
		focus = CHC_Revolt_Oath
	}
	x = -1
	y = 4
	relative_position_id = CHC_Revolt_Start
	cost = 7.15
	ai_will_do = {
		factor = 17
	}
	available = {
		controls_state = 232
	}
	bypass = {
	}
	completion_reward = {
		custom_effect_tooltip = CHC_dag_tt
		hidden_effect = { load_oob = "CHC_dag_oob" }
	}
}
shared_focus = {
	id = CHC_Revolt_First_Pilots
	icon = GFX_focus_Aerial_Offensive
	prerequisite = {
		focus = CHC_Revolt_Doctrine_Effort
		focus = CHC_Revolt_Dagestan_Units
	}
	x = -2
	y = 5
	relative_position_id = CHC_Revolt_Start
	cost = 5
	ai_will_do = {
		factor = 12
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		air_experience = 20
	}
}
shared_focus = {
	id = CHC_Revolt_Recognize_Officers
	icon = GFX_focus_Smoking_Guns
	prerequisite = {
		focus = CHC_Revolt_First_Pilots
	}
	x = -2
	y = 6
	relative_position_id = CHC_Revolt_Start
	cost = 4
	ai_will_do = {
		factor = 8
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_command_power = 25
	}
}
shared_focus = {
	id = CHC_Revolt_Political_Affairs
	icon = GFX_focus_Civil_Unrest
	prerequisite = {
		focus = CHC_Revolt_Start
	}
	x = 0
	y = 1
	relative_position_id = CHC_Revolt_Start
	cost = 3.58
	ai_will_do = {
		factor = 20
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_political_power = 30
		hidden_effect = {
			country_event = {
				id = chc_internal.1
				days = 58
			}
		}
	}
}
shared_focus = {
	id = CHC_Revolt_Foreign_Recognition
	icon = GFX_focus_Foreign_Diplomacy
	prerequisite = {
		focus = CHC_Revolt_Political_Affairs
	}
	x = 0
	y = 3
	relative_position_id = CHC_Revolt_Start
	cost = 4
	ai_will_do = {
		factor = 10
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		every_country = {
			limit = {
				OR = {
					is_major = yes
					num_of_factories > 30
				}
				NOT = {
					has_war_with = CHC
				}
			}
			CHC = {
				add_opinion_modifier = {
					target = PREV
					modifier = CHC_recognition
				}
			}
		}
	}
}
shared_focus = {
	id = CHC_Revolt_Economic_Affairs
	icon = GFX_focus_Money_Bags
	prerequisite = {
		focus = CHC_Revolt_Start
	}
	x = 2
	y = 1
	relative_position_id = CHC_Revolt_Start
	cost = 3
	ai_will_do = {
		factor = 20
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = industrial_bonus
			bonus = 0.25
			uses = 1
			category = industry
		}
	}
}
shared_focus = {
	id = CHC_Revolt_Decollectivization
	icon = GFX_focus_Agriculture
	prerequisite = {
		focus = CHC_Revolt_Political_Affairs
	}
	prerequisite = {
		focus = CHC_Revolt_Economic_Affairs
	}
	x = 1
	y = 2
	relative_position_id = CHC_Revolt_Start
	cost = 5.72
	ai_will_do = {
		factor = 20
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_ideas = CHC_decollect
		add_popularity = {
			ideology = neutrality
			popularity = 0.05
		}
		hidden_effect = {
			country_event = {
				id = chc_internal.2
				days = 99
			}
		}
	}
}
shared_focus = {
	id = CHC_Revolt_Convert_Manufactory
	icon = GFX_goal_generic_construct_mil_factory
	prerequisite = {
		focus = CHC_Revolt_Economic_Affairs
	}
	x = 3
	y = 2
	relative_position_id = CHC_Revolt_Start
	cost = 5.72
	ai_will_do = {
		factor = 20
	}
	available = {
	}
	bypass = {
		NOT = {
			1080 = { industrial_complex > 0 }
		}
	}
	completion_reward = {
		1080 = {
			remove_building = {
				type = industrial_complex
				level = 1
			}
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}			
}
shared_focus = {
	id = CHC_Revolt_Defence_Industry
	icon = GFX_goal_generic_construct_military
	prerequisite = {
		focus = CHC_Revolt_Convert_Manufactory
	}
	x = 4
	y = 3
	relative_position_id = CHC_Revolt_Start
	cost = 10
	ai_will_do = {
		factor = 20
	}
	available = {
	}
	bypass = {

	}
	completion_reward = {
		1080 = {
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}			
}
shared_focus = {
	id = CHC_Revolt_Open_for_Business
	icon = GFX_goal_generic_positive_trade_relations
	prerequisite = {
		focus = CHC_Revolt_Decollectivization
	}
	prerequisite = {
		focus = CHC_Revolt_Convert_Manufactory
	}
	x = 2
	y = 3
	relative_position_id = CHC_Revolt_Start
	cost = 2.14
	ai_will_do = {
		factor = 14
	}
	available = {
	}
	bypass = {

	}
	completion_reward = {
		add_ideas = CHC_open
	}			
}
shared_focus = {
	id = CHC_Revolt_Import_Foreign_Weapons
	icon = GFX_focus_Buy_Foreign_Weapons
	prerequisite = {
		focus = CHC_Revolt_Foreign_Recognition
	}
	prerequisite = {
		focus = CHC_Revolt_Open_for_Business
	}
	x = 1
	y = 4
	relative_position_id = CHC_Revolt_Start
	cost = 8
	ai_will_do = {
		factor = 25
	}
	available = {
	}
	bypass = {

	}
	completion_reward = {
		add_equipment_to_stockpile = {
			type = infantry_equipment_2
			amount = 250
			producer = CHC
		}
	}			
}
shared_focus = {
	id = CHC_Revolt_Improve_Oil
	icon = GFX_focus_Oil_Industry
	prerequisite = {
		focus = CHC_Revolt_Open_for_Business
	}
	prerequisite = {
		focus = CHC_Revolt_Defence_Industry
	}
	x = 3
	y = 4
	relative_position_id = CHC_Revolt_Start
	cost = 5.72
	ai_will_do = {
		factor = 25
	}
	available = {
	}
	bypass = {

	}
	completion_reward = {
		1080 = {
			add_resource = {
				type = oil
				amount = 2
			}
		}
	}			
}
shared_focus = {
	id = CHC_Private_Initiative
	icon = GFX_focus_Business_Men
	prerequisite = {
		focus = CHC_Revolt_Import_Foreign_Weapons
	}
	prerequisite = {
		focus = CHC_Revolt_Improve_Oil
	}
	mutually_exclusive = {
		focus = CHC_Mixed_Systems
	}
	x = 0
	y = 5
	relative_position_id = CHC_Revolt_Start
	cost = 3.58
	ai_will_do = {
		factor = 10
	}
	available = {
	}
	bypass = {

	}
	completion_reward = {
		add_ideas = CHC_private_initiative
	}			
}
shared_focus = {
	id = CHC_Lower_Trade_Barriers
	icon = GFX_focus_Free_Trade
	prerequisite = {
		focus = CHC_Private_Initiative
		focus = CHC_Mixed_Systems
	}
	mutually_exclusive = {
		focus = CHC_Raise_Trade_Barriers
	}
	x = 0
	y = 6
	relative_position_id = CHC_Revolt_Start
	cost = 3
	ai_will_do = {
		factor = 10
	}
	available = {
	}
	bypass = {

	}
	completion_reward = {
		if = {
			limit = {
				has_idea = closed_economy
			}
			swap_ideas = {
				remove_idea = closed_economy
				add_idea = limited_exports
			}
		}
		else_if = {
			limit = {
				has_idea = limited_exports
			}
			swap_ideas = {
				remove_idea = limited_exports
				add_idea = export_focus
			}
		}
		else_if = {
			limit = {
				has_idea = export_focus
			}
			swap_ideas = {
				remove_idea = export_focus
				add_idea = free_trade
			}
		}
		else = {
			add_political_power = 30
		}
	}			
}
shared_focus = {
	id = CHC_Raise_Trade_Barriers
	icon = GFX_focus_Fortification_Effort
	prerequisite = {
		focus = CHC_Private_Initiative
		focus = CHC_Mixed_Systems
	}
	mutually_exclusive = {
		focus = CHC_Lower_Trade_Barriers
	}
	x = 2
	y = 6
	relative_position_id = CHC_Revolt_Start
	cost = 3
	ai_will_do = {
		factor = 10
	}
	available = {
	}
	bypass = {

	}
	completion_reward = {
		if = {
			limit = {
				has_idea = limited_exports
			}
			swap_ideas = {
				remove_idea = limited_exports
				add_idea = closed_economy
			}
		}
		else_if = {
			limit = {
				has_idea = export_focus
			}
			swap_ideas = {
				remove_idea = export_focus
				add_idea = limited_exports
			}
		}
		else_if = {
			limit = {
				has_idea = free_trade
			}
			swap_ideas = {
				remove_idea = free_trade
				add_idea = export_focus
			}
		}
		else = {
			add_political_power = 30
		}
	}			
}
shared_focus = {
	id = CHC_Mixed_Systems
	icon = GFX_goal_generic_neutrality_focus
	prerequisite = {
		focus = CHC_Revolt_Import_Foreign_Weapons
	}
	prerequisite = {
		focus = CHC_Revolt_Improve_Oil
	}
	mutually_exclusive = {
		focus = CHC_Private_Initiative
	}
	x = 2
	y = 5
	relative_position_id = CHC_Revolt_Start
	cost = 3.58
	ai_will_do = {
		factor = 10
	}
	available = {
	}
	bypass = {

	}
	completion_reward = {
		add_ideas = CHC_mixed_systems
	}			
}
