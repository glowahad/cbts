shared_focus = {
	id = WGR_SPD_start
	icon = GFX_focus_GER_SPD
	x = 5
	y = 0
	cost = 1.43
	#prerequisite = {
	#	focus = WGR_SPD_start
	#}
	#relative_position_id = WGR_SPD_start
	available = {
		WGR_has_socialist_government = yes
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		
	}	
	completion_reward = {
		add_political_power = 15
	}
}
shared_focus = {
	id = WGR_SPD_Left
	icon = GFX_focus_Pride_of_The_Left
	prerequisite = {
		focus = WGR_SPD_start
	}
	x = -3
	y = 1
	cost = 1.43
	relative_position_id = WGR_SPD_start
	available = {
		WGR_has_socialist_government = yes
		NOT = {
			custom_trigger_tooltip = {
				tooltip = WGR_not_dstp_in_coalition
				SocLib_in_Coalition = yes
			}
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		
	}	
	completion_reward = {
		add_political_power = 30
		add_stability = -0.03
	}
}
shared_focus = {
	id = WGR_SPD_Banking_Regulations
	icon = GFX_focus_found_the_investment_bank
	prerequisite = {
		focus = WGR_SPD_Left
	}
	x = 1
	y = 1
	cost = 3.33
	relative_position_id = WGR_SPD_Left
	available = {
		WGR_has_socialist_government = yes
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_add_banking_regulations = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Pressure_Junkers
	icon = GFX_focus_Lock_Them_Up
	prerequisite = {
		focus = WGR_SPD_Left
	}
	x = -1
	y = 1
	cost = 2.15
	relative_position_id = WGR_SPD_Left
	available = {
		WGR_has_socialist_government = yes
		NOT = {
			custom_trigger_tooltip = {
				tooltip = WGR_not_dstp_in_coalition
				SocLib_in_Coalition = yes
			}
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		
	}	
	completion_reward = {
		add_popularity = {
			ideology = socialism
			popularity = 0.02
		}
		add_popularity = {
			ideology = social_democracy
			popularity = 0.01
		}
	}
}
shared_focus = {
	id = WGR_SPD_Encroach_Land
	icon = GFX_focus_Agriculture
	prerequisite = {
		focus = WGR_SPD_Pressure_Junkers
	}
	x = -2
	y = 2
	cost = 4.29
	relative_position_id = WGR_SPD_Left
	available = {
		WGR_has_socialist_government = yes
		NOT = {
			custom_trigger_tooltip = {
				tooltip = WGR_not_dstp_in_coalition
				SocLib_in_Coalition = yes
			}
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_encroach_land_effect = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Education_Reform
	icon = GFX_focus_Education_Reform
	prerequisite = {
		focus = WGR_SPD_Encroach_Land
	}
	x = -2
	y = 3
	cost = 3.58
	relative_position_id = WGR_SPD_Left
	available = {
		WGR_has_socialist_government = yes
		NOT = {
			custom_trigger_tooltip = {
				tooltip = WGR_not_dstp_in_coalition
				SocLib_in_Coalition = yes
			}
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		OR = {
			has_completed_focus = WGR_SPD_University_Expansion
			has_completed_focus = WGR_DStP_Education_Budget
			has_completed_focus = WGR_DVP_Expand_Education_Programs
		}
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_add_small_education_spending = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Raise_Taxes
	icon = GFX_focus_Progressive_Taxation
	prerequisite = {
		focus = WGR_SPD_Pressure_Junkers
	}
	x = 0
	y = 2
	cost = 3
	relative_position_id = WGR_SPD_Left
	available = {
		WGR_has_socialist_government = yes
		NOT = {
			custom_trigger_tooltip = {
				tooltip = WGR_not_dstp_in_coalition
				SocLib_in_Coalition = yes
			}
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		OR = {
			has_completed_focus = WGR_SPD_Income_Taxes1
			has_completed_focus = WGR_DStP_Income_Tax
		}
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_raise_taxes_small = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Government_Mediation
	icon = GFX_focus_Workers_Rights
	prerequisite = {
		focus = WGR_SPD_Raise_Taxes
	}
	x = 0
	y = 3
	cost = 4.15
	relative_position_id = WGR_SPD_Left
	available = {
		WGR_has_socialist_government = yes
		NOT = {
			custom_trigger_tooltip = {
				tooltip = WGR_not_dstp_in_coalition
				SocLib_in_Coalition = yes
			}
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_stability = 0.04
		add_popularity = {
			ideology = socialism
			popularity = 0.02
		}
		add_popularity = {
			ideology = social_democracy
			popularity = 0.01
		}
		add_popularity = {
			ideology = jacobin
			popularity = -0.01
		}
		add_popularity = {
			ideology = neutrality
			popularity = -0.01
		}
		add_popularity = {
			ideology = monarchism
			popularity = -0.01
		}
	}
}
shared_focus = {
	id = WGR_SPD_Nationalize_Mining
	icon = GFX_focus_Excavations
	prerequisite = {
		focus = WGR_SPD_Education_Reform
	}
	prerequisite = {
		focus = WGR_SPD_Government_Mediation
	}
	x = -1
	y = 4
	cost = 5
	relative_position_id = WGR_SPD_Left
	available = {
		WGR_has_socialist_government = yes
		NOT = {
			custom_trigger_tooltip = {
				tooltip = WGR_not_dstp_in_coalition
				SocLib_in_Coalition = yes
			}
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_mining_nationalized = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Nationalize_Military_Industries
	icon = GFX_focus_Planned_Economy
	prerequisite = {
		focus = WGR_SPD_Nationalize_Mining
	}
	mutually_exclusive = {
		focus = WGR_SPD_Break_Up_Conglomerates
	}
	x = -2
	y = 5
	cost = 5.72
	relative_position_id = WGR_SPD_Left
	available = {
		WGR_has_socialist_government = yes
		NOT = {
			custom_trigger_tooltip = {
				tooltip = WGR_not_dstp_in_coalition
				SocLib_in_Coalition = yes
			}
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_defense_nationalized = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Break_Up_Conglomerates
	icon = GFX_focus_intervene_in_wage_market
	prerequisite = {
		focus = WGR_SPD_Nationalize_Mining
	}
	mutually_exclusive = {
		focus = WGR_SPD_Nationalize_Military_Industries
	}
	x = 0
	y = 5
	cost = 5.72
	relative_position_id = WGR_SPD_Left
	available = {
		WGR_has_socialist_government = yes
		NOT = {
			custom_trigger_tooltip = {
				tooltip = WGR_not_dstp_in_coalition
				SocLib_in_Coalition = yes
			}
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		has_completed_focus = WGR_radical_reform_fair_competition_law
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_break_up_conglomerates = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Organized_Capitalism
	icon = GFX_focus_Control_the_Economy
	prerequisite = {
		focus = WGR_SPD_Nationalize_Military_Industries
		focus = WGR_SPD_Break_Up_Conglomerates
	}
	x = -1
	y = 6
	cost = 4.29
	relative_position_id = WGR_SPD_Left
	available = {
		has_government = socialism
		NOT = {
			custom_trigger_tooltip = {
				tooltip = WGR_not_dstp_in_coalition
				SocLib_in_Coalition = yes
			}
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_popularity = {
			ideology = socialism
			popularity = 0.04
		}
		add_popularity = {
			ideology = authoritarian_socialism
			popularity = 0.02
		}
		add_political_power = 50
	}
}
shared_focus = {
	id = WGR_SPD_Right
	icon = GFX_focus_Social_Liberalism
	prerequisite = {
		focus = WGR_SPD_start
	}
	x = 3
	y = 1
	cost = 1.43
	relative_position_id = WGR_SPD_start
	available = {
		WGR_has_socialist_government = yes
		custom_trigger_tooltip = {
			tooltip = WGR_dstp_in_coalition
			SocLib_in_Coalition = yes
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		
	}	
	completion_reward = {
		add_political_power = 15
		add_stability = 0.03
	}
}
shared_focus = {
	id = WGR_SPD_University_Expansion
	icon = GFX_focus_Brainpower
	prerequisite = {
		focus = WGR_SPD_Right
	}
	x = -1
	y = 1
	cost = 2.86
	relative_position_id = WGR_SPD_Right
	available = {
		WGR_has_socialist_government = yes
		custom_trigger_tooltip = {
			tooltip = WGR_dstp_in_coalition
			SocLib_in_Coalition = yes
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		OR = {
			has_completed_focus = WGR_SPD_Education_Reform
			has_completed_focus = WGR_DStP_Education_Budget
			has_completed_focus = WGR_DVP_Expand_Education_Programs
		}
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_add_small_education_spending = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Progressive_Tent
	icon = GFX_focus_Compassionaite_Politics
	prerequisite = {
		focus = WGR_SPD_Right
	}
	x = 1
	y = 1
	cost = 2.15
	relative_position_id = WGR_SPD_Right
	available = {
		WGR_has_socialist_government = yes
		custom_trigger_tooltip = {
			tooltip = WGR_dstp_in_coalition
			SocLib_in_Coalition = yes
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		
	}	
	completion_reward = {
		add_political_power = 20
		add_stability = 0.02
		add_popularity = {
			ideology = socialism
			popularity = 0.015
		}
		add_popularity = {
			ideology = social_democracy
			popularity = 0.01
		}
		add_popularity = {
			ideology = social_liberalism
			popularity = 0.015
		}		
	}
}
shared_focus = {
	id = WGR_SPD_Tone_down_Marxism
	icon = GFX_focus_Anti_Communist
	prerequisite = {
		focus = WGR_SPD_Progressive_Tent
	}
	x = 0
	y = 2
	cost = 2.86
	relative_position_id = WGR_SPD_Right
	available = {
		WGR_has_socialist_government = yes
		custom_trigger_tooltip = {
			tooltip = WGR_dstp_in_coalition
			SocLib_in_Coalition = yes
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		has_completed_focus = WGR_SPD_Nationalize_Mining
	}	
	completion_reward = {
		add_stability = 0.03
		add_popularity = {
			ideology = socialism
			popularity = -0.03
		}
		add_popularity = {
			ideology = social_democracy
			popularity = 0.03
		}
	}
}
shared_focus = {
	id = WGR_SPD_Lower_Retirement_Age
	icon = GFX_Unemployeds_Help
	prerequisite = {
		focus = WGR_SPD_Tone_down_Marxism
	}
	x = 0
	y = 3
	cost = 3.28
	relative_position_id = WGR_SPD_Right
	available = {
		WGR_has_socialist_government = yes
		custom_trigger_tooltip = {
			tooltip = WGR_dstp_in_coalition
			SocLib_in_Coalition = yes
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_lower_retirement_age = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Introduce_Regulations
	icon = GFX_focus_strengthen_small_production
	prerequisite = {
		focus = WGR_SPD_Progressive_Tent
	}
	x = 2
	y = 2
	cost = 4.29
	relative_position_id = WGR_SPD_Right
	available = {
		WGR_has_socialist_government = yes
		custom_trigger_tooltip = {
			tooltip = WGR_dstp_in_coalition
			SocLib_in_Coalition = yes
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_regulations = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Improve_Worker_Protections
	icon = GFX_focus_Workers_Support
	prerequisite = {
		focus = WGR_SPD_Introduce_Regulations
	}
	x = 2
	y = 3
	cost = 4.86
	relative_position_id = WGR_SPD_Right
	available = {
		WGR_has_socialist_government = yes
		custom_trigger_tooltip = {
			tooltip = WGR_dstp_in_coalition
			SocLib_in_Coalition = yes
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_worker_protections = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Anti_Nationalism
	icon = GFX_focus_Target_Fascism
	prerequisite = {
		focus = WGR_SPD_Lower_Retirement_Age
	}
	prerequisite = {
		focus = WGR_SPD_Improve_Worker_Protections
	}
	x = 1
	y = 4
	cost = 3
	relative_position_id = WGR_SPD_Right
	available = {
		WGR_has_socialist_government = yes
		custom_trigger_tooltip = {
			tooltip = WGR_dstp_in_coalition
			SocLib_in_Coalition = yes
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		
	}	
	completion_reward = {
		add_political_power = -20
		add_popularity = {
			ideology = neutrality
			popularity = -0.02
		}
		add_popularity = {
			ideology = monarchism
			popularity = -0.02
		}
		add_popularity = {
			ideology = nationalism
			popularity = -0.02
		}
		add_popularity = {
			ideology = fascism
			popularity = -0.02
		}
	}
}
shared_focus = {
	id = WGR_SPD_Increase_Land_Taxes
	icon = GFX_focus_Agriculture_2
	prerequisite = {
		focus = WGR_SPD_Anti_Nationalism
	}
	mutually_exclusive = {
		focus = WGR_SPD_Income_Taxes1
	}
	x = 0
	y = 5
	cost = 2.86
	relative_position_id = WGR_SPD_Right
	available = {
		WGR_has_socialist_government = yes
		custom_trigger_tooltip = {
			tooltip = WGR_dstp_in_coalition
			SocLib_in_Coalition = yes
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		
	}	
	completion_reward = {
		add_stability = -0.04
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_raise_taxes_small = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Income_Taxes1
	icon = GFX_focus_Capitalist_Systems
	prerequisite = {
		focus = WGR_SPD_Anti_Nationalism
	}
	mutually_exclusive = {
		focus = WGR_SPD_Increase_Land_Taxes
	}
	x = 2
	y = 5
	cost = 2.86
	relative_position_id = WGR_SPD_Right
	available = {
		WGR_has_socialist_government = yes
		custom_trigger_tooltip = {
			tooltip = WGR_dstp_in_coalition
			SocLib_in_Coalition = yes
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		OR = {
			has_completed_focus = WGR_SPD_Raise_Taxes
			has_completed_focus = WGR_DStP_Income_Tax
		}
	}	
	completion_reward = {
		add_popularity = {
			ideology = socialism
			popularity = -0.02
		}
		add_popularity = {
			ideology = social_democracy
			popularity = -0.02
		}
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_raise_taxes_small = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Welfare_Capitalism
	icon = GFX_focus_Medicine
	prerequisite = {
		focus = WGR_SPD_Income_Taxes1
		focus = WGR_SPD_Increase_Land_Taxes
	}
	x = 1
	y = 6
	cost = 2.86
	relative_position_id = WGR_SPD_Right
	available = {
		WGR_has_socialist_government = yes
		custom_trigger_tooltip = {
			tooltip = WGR_dstp_in_coalition
			SocLib_in_Coalition = yes
		}
		custom_trigger_tooltip = {
			tooltip = WGR_enough_welfare_spending
			check_variable = {
				WGR_welfare_spending < -0.19
			}
		}
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_popularity = {
			ideology = social_democracy
			popularity = 0.04
		}
		add_popularity = {
			ideology = social_liberalism
			popularity = 0.02
		}
		add_political_power = 40
	}
}
shared_focus = {
	id = WGR_SPD_Welfare_Budget
	icon = GFX_focus_Consensus_with_Peasants
	prerequisite = {
		focus = WGR_SPD_Left
		focus = WGR_SPD_Right
	}
	x = 0
	y = 2
	cost = 3
	relative_position_id = WGR_SPD_start
	available = {
		WGR_has_socialist_government = yes
	}
	ai_will_do = {
		factor = 30
	}
	bypass = {
		OR = {
			has_completed_focus = WGR_DStP_Expand_Welfare_Programs
			has_completed_focus = WGR_Expand_Welfare
			has_completed_focus = WGR_Zentrum_Expand_Welfare_Programs
		}
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_add_medium_welfare_spending = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Agricultural_Subsidies
	icon = GFX_focus_Tractors
	prerequisite = {
		focus = WGR_SPD_Banking_Regulations
		focus = WGR_SPD_University_Expansion
	}
	prerequisite = {
		focus = WGR_SPD_Welfare_Budget
	}
	x = -1
	y = 1
	cost = 2.86
	relative_position_id = WGR_SPD_Welfare_Budget
	available = {
		WGR_has_socialist_government = yes
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		has_completed_focus = WGR_DNVP_Agricultural_Subsidies
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_agro_subsidies = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Redraw_Provinces
	icon = GFX_focus_Redraw_Admin_Borders
	prerequisite = {
		focus = WGR_SPD_Agricultural_Subsidies
	}
	x = -1
	y = 2
	cost = 5
	relative_position_id = WGR_SPD_Welfare_Budget
	available = {
		WGR_has_socialist_government = yes
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_redraw_provinces_SPD = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_Encourage_SPD_Unions
	icon = GFX_focus_Workers_on_Strike
	prerequisite = {
		focus = WGR_SPD_Redraw_Provinces
	}
	mutually_exclusive = {
		focus = WGR_SPD_Encourage_All_Unions
	}
	x = -2
	y = 3
	cost = 2.15
	relative_position_id = WGR_SPD_Welfare_Budget
	available = {
		WGR_has_socialist_government = yes
	}
	ai_will_do = {
		factor = 30
	}
	bypass = {
	}	
	completion_reward = {
		add_stability = 0.015
		add_popularity = {
			ideology = socialism
			popularity = 0.03
		}
		add_popularity = {
			ideology = social_democracy
			popularity = 0.015
		}
	}
}
shared_focus = {
	id = WGR_SPD_Encourage_All_Unions
	icon = GFX_focus_Union_Workers
	prerequisite = {
		focus = WGR_SPD_Redraw_Provinces
	}
	mutually_exclusive = {
		focus = WGR_SPD_Encourage_SPD_Unions
	}
	x = 0
	y = 3
	cost = 2.86
	relative_position_id = WGR_SPD_Welfare_Budget
	available = {
		WGR_has_socialist_government = yes
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		has_completed_focus = WGR_Expand_Union_Activity
	}	
	completion_reward = {
		add_stability = 0.03
		add_popularity = {
			ideology = socialism
			popularity = 0.01
		}
		add_popularity = {
			ideology = social_democracy
			popularity = 0.01
		}
		add_popularity = {
			ideology = social_liberalism
			popularity = 0.01
		}
		add_popularity = {
			ideology = democratic
			popularity = 0.01
		}
	}
}
shared_focus = {
	id = WGR_SPD_Expand_Womens_Rights
	icon = GFX_focus_Womens_Rights
	prerequisite = {
		focus = WGR_SPD_Banking_Regulations
		focus = WGR_SPD_University_Expansion
	}
	prerequisite = {
		focus = WGR_SPD_Welfare_Budget
	}
	x = 1
	y = 1
	cost = 1.58
	relative_position_id = WGR_SPD_Welfare_Budget
	available = {
		WGR_has_socialist_government = yes
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		custom_effect_tooltip = have_to_vote
		custom_effect_tooltip = will_vote
		effect_tooltip = {
			WGR_contraceptives = yes
		}
	}
}
shared_focus = {
	id = WGR_SPD_No_Stab
	icon = GFX_focus_Equality_for_Jews
	prerequisite = {
		focus = WGR_SPD_Expand_Womens_Rights
	}
	x = 1
	y = 2
	cost = 3.58
	relative_position_id = WGR_SPD_Welfare_Budget
	available = {
		WGR_has_socialist_government = yes
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
	}	
	completion_reward = {
		add_popularity = {
			ideology = communism
			popularity = -0.025
		}
		add_popularity = {
			ideology = fascism
			popularity = -0.025
		}
		add_popularity = {
			ideology = nationalism
			popularity = -0.025
		}
	}
}
shared_focus = {
	id = WGR_SPD_Protect_Minorities
	icon = GFX_focus_Minority_rights
	prerequisite = {
		focus = WGR_SPD_No_Stab
	}
	x = 2
	y = 3
	cost = 4.29
	relative_position_id = WGR_SPD_Welfare_Budget
	available = {
		WGR_has_socialist_government = yes
	}
	ai_will_do = {
		factor = 10
	}
	bypass = {
		has_completed_focus = WGR_DStP_Minorities
	}	
	completion_reward = {
		add_stability = 0.04
		add_political_power = -20
	}
}
