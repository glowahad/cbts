### search_filters = {FOCUS_FILTER_POLITICAL}
### search_filters = {FOCUS_FILTER_RESEARCH}
### search_filters = {FOCUS_FILTER_INDUSTRY}
### search_filters = {FOCUS_FILTER_STABILITY}
### search_filters = {FOCUS_FILTER_WAR_SUPPORT}
### search_filters = {FOCUS_FILTER_MANPOWER}
### search_filters = {FOCUS_FILTER_USA_CONGRESS_ACT}
### search_filters = {FOCUS_FILTER_USA_DEPRESSION_RECOVERY}
### search_filters = {FOCUS_FILTER_USA_SLUM_CLEARANCE}
### search_filters = {FOCUS_FILTER_AGGRESIVE_DIPLO}
### search_filters = {FOCUS_FILTER_PACIFISM_DIPLO}



focus_tree = {
	id = USA_USA_focus
	
	country = {
		factor = 0
		
		modifier = {
			add = 10
			tag = USA
		}

	}
	default = no
	continuous_focus_position = { x = 80 y = 3000 }
    shared_focus = USA_reviewthemilitary
	shared_focus = USA_new_foreign
	shared_focus = USA_inaugurationday
}