# focuses here
#s_team337
#s_team337 and Focus Tree
shared_focus = {
	id = GEO_2RCW_Economics_Start
	icon = GFX_focus_Capitalist_Systems
	#prerequisite = { focus = GEO_2RCW_Economics_Start }
	x = 5
	y = 0
	x = 15
	y = 1
	#relative_position_id = GEO_2RCW_Economics_Start
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		add_political_power = 70
	}
}

shared_focus = {
	id = GEO_kickstart_the_economy
	icon = GFX_focus_Economic_Stimulus
	x = 0
	y = 1
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_2RCW_Economics_Start
	}
	available = { 
		 
	}
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		if = {
			limit = {
				has_idea = tot_economic_mobilisation
			}
			add_political_power = 65
		}
		else_if = {
			limit = {
				has_idea = war_economy
			}
			swap_ideas = {
				remove_idea = war_economy
				add_idea = tot_economic_mobilisation
			}
		}
		else_if = {
			limit = {
				has_idea = partial_economic_mobilisation
			}
			swap_ideas = {
				remove_idea = partial_economic_mobilisation
				add_idea = war_economy
			}
		}
		else_if = {
			limit = {
				has_idea = low_economic_mobilisation
			}
			swap_ideas = {
				remove_idea = low_economic_mobilisation
				add_idea = partial_economic_mobilisation
			}
		}
		else_if = {
			limit = {
				has_idea = civilian_economy
			}
			swap_ideas = {
				remove_idea = civilian_economy
				add_idea = low_economic_mobilisation
			}
		}
	}
}

shared_focus = {
	id = GEO_begin_decollectivization_of_the_nation
	icon = GFX_focus_Agriculture
	x = 0
	y = 2
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_kickstart_the_economy
	}
	available = { 
		 
	}
	cost = 3
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		add_timed_idea = {
		idea = RUS_decollectivize
		days = 730
		}
	}
}

shared_focus = {
	id = GEO_emergency_factory_conversion
	icon = GFX_focus_strengthen_small_production
	x = -1
	y = 3
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_begin_decollectivization_of_the_nation
	}
	available = { 
		 
	}
	cost = 3
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		random_owned_controlled_state = {
		limit = {
			is_controlled_by = ROOT
			industrial_complex > 0
		}
		remove_building = {
			type = industrial_complex
			level = 1
		}
		add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}	
	}
}

shared_focus = {
	id = GEO_take_loans
	icon = GFX_focus_Economic_Stimulus
	x = 1
	y = 3
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_begin_decollectivization_of_the_nation
	}
	available = { 
		 
	}
	cost = 3
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		add_political_power = 70
	}
}

shared_focus = {
	id = GEO_choosing_an_economy_minister
	icon = GFX_focus_Shady_Deals
	x = 0
	y = 4
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_emergency_factory_conversion
	}
	prerequisite = {
		focus = GEO_take_loans
	}
	available = { 
		 
	}
	cost = 3
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		country_event = {
			id = geo_internal.2
			days = 1
		}
	}
}

shared_focus = {
	id = GEO_socialist_minister
	icon = GFX_focus_Social_Democracy
	x = -2
	y = 5
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_choosing_an_economy_minister
	}
	mutually_exclusive = {
		focus = GEO_conservative_minister
	}
	available = { 
		 
		always = no
	}
	cost = 0
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		 #bypass
	}
}

shared_focus = {
	id = GEO_conservative_minister
	icon = GFX_focus_Give_Democracy
	x = 2
	y = 5
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_choosing_an_economy_minister
	}
	mutually_exclusive = {
		focus = GEO_socialist_minister
	}
	available = { 
		 
		always = no
	}
	cost = 0 #will probably be bypassed
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		#bypass
	}
}

shared_focus = {
	id = GEO_institute_price_controls
	icon = GFX_focus_Grab_Money
	x = -2
	y = 6
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_socialist_minister
	}
	available = { 
		 
	}
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		add_political_power = -30
		add_stability = 0.03
	}
}

shared_focus = {
	id = GEO_tax_reform #welfare is expensive
	icon = GFX_focus_Progressive_Taxation
	x = -2
	y = 7
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_institute_price_controls
	}
	available = { 
		 
	}
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		add_ideas = {
			GEO_soc_tax
		}
	}
}

shared_focus = {
	id = GEO_encourage_unionism
	icon = GFX_focus_Union_Workers
	x = -3
	y = 8
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_tax_reform
	}
	available = { 
		 
	}
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		add_ideas = {
			GEO_trade_unions
		}
	}
}

shared_focus = {
	id = GEO_create_cooperatives
	icon = GFX_focus_Cooperatives
	x = -3
	y = 9
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_encourage_unionism
	}
	available = { 
		 
	}
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		country_event = {
			id = geo_internal.3
			days = 1
		}
	}
}

shared_focus = {
	id = GEO_national_work_programs
	icon = GFX_focus_Planned_Economy
	x = -3
	y = 10
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_create_cooperatives
	}
	available = { 
		 
	} 
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		swap_ideas = {
			remove_idea = GEO_trade_unions
			add_idea = GEO_nat_work
		} 
	}
}

shared_focus = {
	id = GEO_allow_private_investment
	icon = GFX_focus_Business_Men
	x = 0
	y = 6
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_socialist_minister
		focus = GEO_conservative_minister
	}
	available = { 
		 
	} #note that all focuses under this branch will require having a certain amount of pp
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		add_political_power = 60 
	}
}

shared_focus = {
	id = GEO_railway_expansion
	icon = GFX_focus_Trains
	x = 0
	y = 7
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_allow_private_investment
	}
	available = { 
		has_political_power > 30
		
	} 
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		hidden_effect = { add_political_power = -30 }
	}
}

shared_focus = {
	id = GEO_expand_the_industry
	icon = GFX_focus_Industrialization
	x = -1
	y = 8
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_railway_expansion
	}
	available = { 
		has_political_power > 30
		
	} 
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		hidden_effect = { add_political_power = -30 }
	}
}

shared_focus = {
	id = GEO_build_arms_factories
	icon = GFX_focus_Military_Intervention
	x = -1
	y = 9
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_expand_the_industry
	}
	available = { 
		has_political_power > 30
		
	} 
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
		hidden_effect = { add_political_power = -30 }
	}
}

shared_focus = {
	id = GEO_exploit_mineral_wealth
	icon = GFX_focus_generic_tungsten
	x = -1
	y = 10
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_build_arms_factories
	}
	available = {
		has_political_power > 20
		
	} 
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		add_resource = {
			type = steel
			amount = 8
			state = 231
		}
		hidden_effect = { add_political_power = -20 }
	}
}

shared_focus = {
	id = GEO_implement_modern_agricultural_methods
	icon = GFX_focus_Bread_Trade
	x = 1
	y = 8
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_railway_expansion
	}
	available = {
		has_political_power > 30
		
	} 
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		add_ideas = {
			GEO_modern_farming
		}
		hidden_effect = { add_political_power = -30 }
	}
}

shared_focus = {
	id = GEO_purchase_farm_equipment
	icon = GFX_focus_Tractors
	x = 1
	y = 9
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_implement_modern_agricultural_methods
	}
	available = {
		has_political_power > 20
		
	} 
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		swap_ideas = {
			remove_idea = GEO_modern_farming
			add_idea = GEO_modern_farming2
		}
		hidden_effect = { add_political_power = -20 }
	}
}

shared_focus = {
	id = GEO_tbilisi_senaki_highway
	icon = GFX_focus_Highways
	x = 0
	y = 11
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_purchase_farm_equipment
	}
	prerequisite = {
		focus = GEO_exploit_mineral_wealth
	}
	available = { 
		has_political_power > 40
		
	} 
	cost = 6
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 2
				instant_build = yes
			}
		}
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 2
				instant_build = yes
			}
		}
		hidden_effect = { add_political_power = -40 }
	}
}

shared_focus = {
	id = GEO_loosen_trade_barriers 
	icon = GFX_focus_Free_Trade
	x = 2
	y = 6
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_conservative_minister
	}
	available = { 
		
	}
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		add_ideas = {
			GEO_freer_trade
		}
	}
}

shared_focus = {
	id = GEO_attract_businesses
	icon = GFX_focus_Extravagance
	x = 2
	y = 7
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_loosen_trade_barriers
	}
	available = { 
		
	}
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		add_ideas = {
			GEO_encourage_business
		} 
	}
}

shared_focus = {
	id = GEO_destroy_the_vestiges_of_bolshevism
	icon = GFX_goal_anti_comintern_pact
	x = 3
	y = 8
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_attract_businesses
	}
	available = { 
		
	}
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		country_event = {
			id = geo_internal.4
			days = 1
		}
	}
}

shared_focus = {
	id = GEO_foreign_investments
	icon = GFX_focus_Fundraising
	x = 3
	y = 9
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_destroy_the_vestiges_of_bolshevism
	}
	available = { 
		
	}
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		add_offsite_building = { type = industrial_complex level = 1 }
	}
}

shared_focus = {
	id = GEO_a_balanced_budget
	icon = GFX_focus_Capitalist_Rule
	x = 3
	y = 10
	relative_position_id = GEO_2RCW_Economics_Start
	prerequisite = {
		focus = GEO_foreign_investments
	}
	available = { 
		
	}
	cost = 5
	ai_will_do = {
		factor = 16
	}
	completion_reward = {
		swap_ideas = {
		remove_idea = GEO_encourage_business
		add_idea = GEO_balanced_budget
		}  
	}
}