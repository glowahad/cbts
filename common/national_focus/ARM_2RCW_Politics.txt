#11 focuses here
#s_team337
shared_focus = {
	id = ARM_2RCW_Politics_Start
	icon = GFX_focus_Armenia
	#prerequisite = { focus = ARM_2RCW_Politics_Start }
	x = 2
	y = 0
	#relative_position_id = ARM_2RCW_Politics_Start
	cost = 4.29
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_political_power = 50
	}
}
shared_focus = {
	id = ARM_2RCW_National_Revolution
	icon = GFX_focus_Triumphant_Will
	prerequisite = {
		focus = ARM_2RCW_Politics_Start
	}
	x = -1
	y = 1
	relative_position_id = ARM_2RCW_Politics_Start
	cost = 3
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_war_support = 0.075
	}
}
shared_focus = {
	id = ARM_2RCW_Churches
	icon = GFX_focus_Armenian_Church
	prerequisite = {
		focus = ARM_2RCW_National_Revolution
	}
	x = -2
	y = 2
	relative_position_id = ARM_2RCW_Politics_Start
	cost = 3.58
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_popularity = {
			ideology = neutrality
			popularity = 0.05
		}
		add_popularity = {
			ideology = nationalism
			popularity = 0.02
		}
	}
}
shared_focus = {
	id = ARM_2RCW_Social_Revolution
	icon = GFX_focus_Pride_of_The_Left
	prerequisite = {
		focus = ARM_2RCW_Politics_Start
	}
	x = 1
	y = 1
	relative_position_id = ARM_2RCW_Politics_Start
	cost = 3
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_stability = 0.075
	}
}
shared_focus = {
	id = ARM_2RCW_Speech
	icon = GFX_focus_Freedom_of_Speech
	prerequisite = {
		focus = ARM_2RCW_National_Revolution
	}
	prerequisite = {
		focus = ARM_2RCW_Social_Revolution
	}
	x = 0
	y = 2
	relative_position_id = ARM_2RCW_Politics_Start
	cost = 1.43
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_stability = -0.03
		add_war_support = 0.03
		hidden_effect = {
			country_event = {
				id = arm_internal.3
				days = 15
			}
		}
	}
}
shared_focus = {
	id = ARM_2RCW_Enemies_on_All_Sides
	icon = GFX_focus_Fortification_Effort
	prerequisite = {
		focus = ARM_2RCW_Churches
	}
	prerequisite = {
		focus = ARM_2RCW_Speech
	}
	x = -1
	y = 3
	relative_position_id = ARM_2RCW_Politics_Start
	cost = 4.72
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		swap_ideas = {
			remove_idea = ARM_dreams_of_ararat
			add_idea = ARM_dreams_of_ararat2
		}
		every_country = {
			limit = {
				is_neighbor_of = ROOT
			}
			add_opinion_modifier = {
				target = ROOT
				modifier = ARM_antagonize
			}
		}
	}
}
shared_focus = {
	id = ARM_2RCW_Democratic_Commitments
	icon = GFX_focus_Give_Democracy
	prerequisite = {
		focus = ARM_2RCW_Social_Revolution
	}
	x = 1
	y = 3
	relative_position_id = ARM_2RCW_Politics_Start
	cost = 2.57
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_popularity = {
			ideology = authoritarian_socialism
			popularity = 0.03
		}
		add_popularity = {
			ideology = socialism
			popularity = 0.03
		}
		custom_effect_tooltip = ARM_democratic_expections
	}
}
shared_focus = {
	id = ARM_2RCW_Ethnic_Equality
	icon = GFX_focus_Minority_rights
	prerequisite = {
		focus = ARM_2RCW_Social_Revolution
	}
	x = 2
	y = 2
	relative_position_id = ARM_2RCW_Politics_Start
	cost = 2.57
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		swap_ideas = {
			remove_idea = ARM_ethnic_tensions
			add_idea = ARM_ethnic_tensions2
		}
		hidden_effect = {
			country_event = {
				id = arm_internal.1
				days = 153
			}
		}
	}
}
shared_focus = {
	id = ARM_2RCW_Red_Guard
	icon = GFX_focus_Communist_Pride
	prerequisite = {
		focus = ARM_2RCW_Ethnic_Equality
	}
	x = 3
	y = 3
	relative_position_id = ARM_2RCW_Politics_Start
	cost = 5
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_ideas = ARM_red_guard
	}
}
shared_focus = {
	id = ARM_2RCW_Top_Down
	icon = GFX_focus_Manipulate_the_Poor
	prerequisite = {
		focus = ARM_2RCW_Enemies_on_All_Sides
	}
	prerequisite = {
		focus = ARM_2RCW_Democratic_Commitments
	}
	mutually_exclusive = {
		focus = ARM_2RCW_Bottom_Up
	}
	x = 0
	y = 4
	relative_position_id = ARM_2RCW_Politics_Start
	cost = 4.43
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_popularity = {
			ideology = neutrality
			popularity = 0.03
		}
		add_political_power = 30
	}
}
shared_focus = {
	id = ARM_2RCW_Bottom_Up
	icon = GFX_focus_Workers_Support
	prerequisite = {
		focus = ARM_2RCW_Democratic_Commitments
	}
	prerequisite = {
		focus = ARM_2RCW_Red_Guard
	}
	mutually_exclusive = {
		focus = ARM_2RCW_Top_Down
	}
	x = 2
	y = 4
	relative_position_id = ARM_2RCW_Politics_Start
	cost = 4.43
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_popularity = {
			ideology = authoritarian_socialism
			popularity = 0.03
		}
		add_stability = 0.03
	}
}
