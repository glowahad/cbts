shared_focus = { #I have legit no idea where this tree actually goes. Probably legacy - s_team
	id = GER_renounce_versailles_treaty
	icon = GFX_focus_Renewed_Arms
	x = 50
	y = 0
	offset = {
		x = -36
		y = 0
		trigger = {
			has_focus_tree = german_focus_default
		}
	}
	cost = 1
	ai_will_do = {
		factor = 50
		modifier = {
			factor = 3
			date > 1935.3.1
		}
		modifier = {
			factor = 0
			date < 1935.3.1
			is_historical_focus_on = yes
		}
	}
	available = {
		date > 1935.1.1
		has_idea = versailles_treaty
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		remove_ideas = versailles_treaty
		if = {
			limit = { has_idea = disarmed_nation }
			swap_ideas = { remove_idea = disarmed_nation add_idea = limited_conscription }
		}
		if = {
			limit = { has_idea = volunteer_only }
			swap_ideas = { remove_idea = volunteer_only add_idea = limited_conscription }
		}
	}
}

shared_focus = {
	id = GER_army_start
	icon = GFX_focus_Prussian_Discipline
	prerequisite = { focus = GER_renounce_versailles_treaty }
	x = -8
	y = 1
	relative_position_id = GER_renounce_versailles_treaty
	cost = 5
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		army_experience = 25
	}
}

shared_focus = {
	id = GER_blitzkrieg
	icon = GFX_focus_GER_blitzkrieg_theory
	prerequisite = { focus = GER_army_start }
	mutually_exclusive = { focus = GER_firepower }
	mutually_exclusive = { focus = GER_battleplan }
	mutually_exclusive = { focus = GER_mass_assault }
	x = -3
	y = 1
	relative_position_id = GER_army_start
	cost = 10
	ai_will_do  = {
		factor = 12
		modifier = {
			factor = 0.001
			date < 1935.1.1
		}
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = land_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = cat_mobile_warfare
		}
	}
}
shared_focus = {
	id = GER_firepower
	icon = GFX_goal_generic_army_artillery2
	prerequisite = { focus = GER_army_start }
	mutually_exclusive = { focus = GER_blitzkrieg }
	mutually_exclusive = { focus = GER_battleplan }
	mutually_exclusive = { focus = GER_mass_assault }
	x = -1
	y = 1
	relative_position_id = GER_army_start
	cost = 10
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	ai_will_do  = {
		factor = 8
		modifier = {
			factor = 0
			is_historical_focus_on = yes
		}
		modifier = {
			factor = 0.001
			date < 1935.1.1
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = land_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = cat_superior_firepower
		}
	}
}
shared_focus = {
	id = GER_battleplan
	icon = GFX_focus_Officers_Command
	prerequisite = { focus = GER_army_start }
	mutually_exclusive = { focus = GER_blitzkrieg }
	mutually_exclusive = { focus = GER_firepower }
	mutually_exclusive = { focus = GER_mass_assault }
	x = 1
	y = 1
	relative_position_id = GER_army_start
	cost = 10
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	ai_will_do  = {
		factor = 8
		modifier = {
			factor = 0
			is_historical_focus_on = yes
		}
		modifier = {
			factor = 0.001
			date < 1935.1.1
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = land_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = cat_grand_battle_plan
		}
	}
}

shared_focus = {
	id = GER_mass_assault
	icon = GFX_goal_generic_special_forces
	prerequisite = { focus = GER_army_start }
	mutually_exclusive = { focus = GER_blitzkrieg }
	mutually_exclusive = { focus = GER_firepower }
	mutually_exclusive = { focus = GER_battleplan }
	x = 3
	y = 1
	relative_position_id = GER_army_start
	cost = 10
	ai_will_do  = {
		factor = 7
		modifier = {
			factor = 0
			is_historical_focus_on = yes
		}
		modifier = {
			factor = 0.001
			date < 1935.1.1
		}
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = land_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = cat_mass_assault
		}
	}
}

shared_focus = {
	id = GER_infantry_weapons
	icon = GFX_goal_generic_small_arms
	prerequisite = {
		focus = GER_mass_assault
		focus = GER_blitzkrieg
		focus = GER_firepower
		focus = GER_battleplan
	}
	x = -2
	y = 2
	relative_position_id = GER_army_start
	cost = 10
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = small_arms_bonus
			bonus = 0.5
			uses = 1
			category = infantry_weapons
		}
	}
}
shared_focus = {
	id = GER_motorized
	icon = GFX_goal_generic_army_motorized
	prerequisite = {
		focus = GER_mass_assault
		focus = GER_blitzkrieg
		focus = GER_firepower
		focus = GER_battleplan
	}
	
	x = 0
	y = 2
	relative_position_id = GER_army_start
	cost = 5
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = motorized_bonus
			bonus = 0.5
			uses = 1
			category = motorized_equipment
		}
	}
}
shared_focus = {
	id = GER_support_equipment
	icon = GFX_goal_generic_military_deal
	prerequisite = {
		focus = GER_mass_assault
		focus = GER_blitzkrieg
		focus = GER_firepower
		focus = GER_battleplan
	}
	
	x = 2
	y = 2
	relative_position_id = GER_army_start
	cost = 10
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = support_bonus
			bonus = 0.5
			uses = 1
			category = support_tech
		}
	}
}
shared_focus = {
	id = GER_tank_research
	icon = GFX_focus_Tanks_Across_the_Border
	prerequisite = {
		focus = GER_blitzkrieg
	}
	prerequisite = {
		focus = GER_infantry_weapons
		focus = GER_support_equipment
		focus = GER_motorized
	}
	x = -3
	y = 3
	relative_position_id = GER_army_start
	cost = 10
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = armor_bonus
			bonus = 0.5
			uses = 1
			category = armor
		}
	}
}
shared_focus = {
	id = GER_artillery_research
	icon = GFX_goal_generic_army_artillery
	prerequisite = {
		focus = GER_firepower
	}
	prerequisite = {
		focus = GER_infantry_weapons
		focus = GER_support_equipment
		focus = GER_motorized
	}
	x = -1
	y = 3
	relative_position_id = GER_army_start
	cost = 10
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = artillery_bonus
			bonus = 0.5
			uses = 1
			category = artillery
		}
	}
}

shared_focus = {
	id = GER_spec_ops
	icon = GFX_focus_Into_the_Flames
	prerequisite = {
		focus = GER_battleplan
	}
	prerequisite = {
		focus = GER_infantry_weapons
		focus = GER_support_equipment
		focus = GER_motorized
	}
	x = 1
	y = 3
	relative_position_id = GER_army_start
	cost = 10
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = special_forces_bonus
			bonus = 0.5
			uses = 1
			category = marine_tech
			category = para_tech
			category = mountaineers_tech
		}
	}
}

shared_focus = {
	id = GER_recruitment_tactics
	icon = GFX_focus_Recruitment
	prerequisite = {
		focus = GER_mass_assault
	}
	prerequisite = {
		focus = GER_infantry_weapons
		focus = GER_support_equipment
		focus = GER_motorized
	}
	x = 3
	y = 3
	relative_position_id = GER_army_start
	cost = 10
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		MOVE_UP_CONSCRIPTION = yes
	}
}

shared_focus = {
	id = GER_tank_innovations
	icon = GFX_focus_NZL_bob_semple_tank
	prerequisite = {
		focus = GER_tank_research
		focus = GER_artillery_research
		focus = GER_spec_ops
		focus = GER_recruitment_tactics
	}
	x = -1
	y = 4
	relative_position_id = GER_army_start
	cost = 5
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = armor_bonus
			bonus = 0.25
			uses = 1
			category = armor
		}
	}
}

shared_focus = {
	id = GER_mechanized_research
	icon = GFX_goal_generic_army_motorized
	prerequisite = {
		focus = GER_tank_research
		focus = GER_artillery_research
		focus = GER_spec_ops
		focus = GER_recruitment_tactics
	}
	x = 1
	y = 4
	relative_position_id = GER_army_start
	cost = 10
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = mechanized_bonus
			bonus = 0.5
			uses = 1
			category = cat_mechanized_equipment
		}
	}
}
shared_focus = {
	id = GER_doctrines
	icon = GFX_goal_generic_army_doctrines
	prerequisite = {
		focus = GER_mechanized_research
	}
	prerequisite = {
		focus = GER_tank_innovations
	}
	x = 0
	y = 5
	relative_position_id = GER_army_start
	cost = 10
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = land_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = land_doctrine
		}
	}
}
shared_focus = {
	id = GER_navy_start
	icon = GFX_focus_Status_of_the_Admirality
	prerequisite = { focus = GER_renounce_versailles_treaty }
	x = 0
	y = 1
	relative_position_id = GER_renounce_versailles_treaty
	cost = 5
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		navy_experience = 25
	}
}
shared_focus = {
	id = GER_dockyard_1
	icon = GFX_focus_Naval_Operations
	prerequisite = { focus = GER_navy_start }
	x = 0
	y = 1
	relative_position_id = GER_navy_start
	cost = 7
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_coastal = yes
				free_building_slots = {
					building = dockyard
					size > 2
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = dockyard
				level = 2
				instant_build = yes
			}
		}
		random_owned_controlled_state = {
			limit = {
				is_coastal = yes
				free_building_slots = {
					building = dockyard
					size > 2
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = dockyard
				level = 2
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = GER_dockyard_2
	icon = GFX_focus_Naval_Operations
	prerequisite = { focus = GER_dockyard_1 }
	x = 0
	y = 2
	relative_position_id = GER_navy_start
	cost = 7
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_coastal = yes
				free_building_slots = {
					building = dockyard
					size > 2
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = dockyard
				level = 2
				instant_build = yes
			}
		}
		random_owned_controlled_state = {
			limit = {
				is_coastal = yes
				free_building_slots = {
					building = dockyard
					size > 2
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = dockyard
				level = 2
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = GER_sub_research
	icon = GFX_goal_generic_navy_submarine
	prerequisite = { focus = GER_dockyard_2 }
	x = -1
	y = 3
	relative_position_id = GER_navy_start
	cost = 4
	ai_will_do  = {
		factor = 7
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
	}
	completion_reward = {
		add_tech_bonus = {
			name = submarine_bonus
			bonus = 0.4
			uses = 1
			category = ss_tech
		}
	}
}
shared_focus = {
	id = GER_destroyer_research
	icon = GFX_goal_generic_navy_anti_submarine
	prerequisite = { focus = GER_dockyard_2 }
	x = 1
	y = 3
	relative_position_id = GER_navy_start
	cost = 4
	ai_will_do  = {
		factor = 7
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
	}
	completion_reward = {
		add_tech_bonus = {
			name = destroyer_bonus
			bonus = 0.4
			uses = 1
			category = dd_tech
		}
	}
}

shared_focus = {
	id = GER_plan_z_start
	icon = GFX_focus_GER_plan_Z
	prerequisite = { focus = GER_destroyer_research focus = GER_sub_research }
	x = 0
	y = 4
	relative_position_id = GER_navy_start
	cost = 10
	ai_will_do  = {
		factor = 8
		modifier = {
			factor = 0.5
			date < 1939.1.1
		}
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
	}
	completion_reward = {
		add_tech_bonus = {
			name = naval_doctrine_bonus
			bonus = 0.5
			uses = 3
			category = naval_doctrine
		}
	}
}
shared_focus = {
	id = GER_plan_z_submarines
	icon = GFX_goal_generic_navy_submarine
	prerequisite = { focus = GER_plan_z_start }
	x = -3
	y = 5
	relative_position_id = GER_navy_start
	cost = 4
	ai_will_do  = {
		factor = 8
		modifier = {
			factor = 0.5
			date < 1939.1.1
		}
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
	}
	completion_reward = {
		add_tech_bonus = {
			name = submarine_bonus
			bonus = 0.4
			uses = 1
			category = ss_tech
		}
	}
}
shared_focus = {
	id = GER_plan_z_cruisers
	icon = GFX_goal_generic_navy_cruiser
	prerequisite = { focus = GER_plan_z_start }
	x = -1
	y = 5
	relative_position_id = GER_navy_start
	cost = 8
	ai_will_do  = {
		factor = 8
		modifier = {
			factor = 0.5
			date < 1939.1.1
		}
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
	}
	completion_reward = {
		add_tech_bonus = {
			name = light_cruiser_bonus
			bonus = 0.4
			uses = 1
			category = cl_tech
		}
		add_tech_bonus = {
			name = heavy_cruiser_bonus
			bonus = 0.4
			uses = 1
			category = ca_tech
		}
	}
}
shared_focus = {
	id = GER_plan_z_battleship
	icon = GFX_focus_Battleship
	prerequisite = { focus = GER_plan_z_start }
	x = 1
	y = 5
	relative_position_id = GER_navy_start
	cost = 4
	ai_will_do  = {
		factor = 8
		modifier = {
			factor = 0.5
			date < 1939.1.1
		}
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
	}
	completion_reward = {
		add_tech_bonus = {
			name = battleship_bonus
			bonus = 0.4
			uses = 1
			category = bb_tech
			category = bc_tech
			category = shbb_tech
		}
	}
}

shared_focus = {
	id = GER_plan_z_carriers
	icon = GFX_goal_generic_navy_carrier
	prerequisite = { focus = GER_plan_z_start }
	x = 3
	y = 5
	relative_position_id = GER_navy_start
	cost = 4
	ai_will_do  = {
		factor = 8
		modifier = {
			factor = 0.5
			date < 1939.1.1
		}
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
	}
	completion_reward = {
		add_tech_bonus = {
			name = carrier_bonus
			bonus = 0.4
			uses = 1
			category = cv_tech
		}
	}
}

shared_focus = {
	id = GER_sub_primacy
	icon = GFX_focus_Small_Navy
	prerequisite = { focus = GER_plan_z_submarines }
	mutually_exclusive = { focus = GER_cruiser_primacy }
	mutually_exclusive = { focus = GER_battleship_primacy }
	mutually_exclusive = { focus = GER_carrier_primacy }
	x = -3
	y = 6
	relative_position_id = GER_navy_start
	cost = 2
	ai_will_do  = {
		factor = 8
		modifier = {
			factor = 0.5
			date < 1939.1.1
		}
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
	}
	completion_reward = {
		add_ideas = GER_sub_primacy_ideas
	}
}
shared_focus = {
	id = GER_cruiser_primacy
	icon = GFX_focus_Battlefleet
	prerequisite = { focus = GER_plan_z_cruisers }
	mutually_exclusive = { focus = GER_sub_primacy }
	mutually_exclusive = { focus = GER_battleship_primacy }
	mutually_exclusive = { focus = GER_carrier_primacy }
	x = -1
	y = 6
	relative_position_id = GER_navy_start
	cost = 2
	ai_will_do  = {
		factor = 8
		modifier = {
			factor = 0.5
			date < 1939.1.1
		}
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
	}
	completion_reward = {
		add_ideas = GER_cruiser_primacy_ideas
	}
}
shared_focus = {
	id = GER_battleship_primacy
	icon = GFX_focus_Large_Navy
	prerequisite = { focus = GER_plan_z_battleship }
	mutually_exclusive = { focus = GER_sub_primacy }
	mutually_exclusive = { focus = GER_cruiser_primacy }
	mutually_exclusive = { focus = GER_carrier_primacy }
	x = 1
	y = 6
	relative_position_id = GER_navy_start
	cost = 2
	ai_will_do  = {
		factor = 8
		modifier = {
			factor = 0.5
			date < 1939.1.1
		}
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
	}
	completion_reward = {
		add_ideas = GER_battleship_primacy_ideas
	}
}
shared_focus = {
	id = GER_carrier_primacy
	icon = GFX_focus_Carrier_Focus
	prerequisite = { focus = GER_plan_z_carriers }
	mutually_exclusive = { focus = GER_sub_primacy }
	mutually_exclusive = { focus = GER_cruiser_primacy }
	mutually_exclusive = { focus = GER_battleship_primacy }
	x = 3
	y = 6
	relative_position_id = GER_navy_start
	cost = 2
	ai_will_do  = {
		factor = 8
		modifier = {
			factor = 0.5
			date < 1939.1.1
		}
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
	}
	completion_reward = {
		add_ideas = GER_carrier_primacy_ideas
	}
}

shared_focus = {
	id = GER_dockyard_3
	icon = GFX_focus_Naval_Operations
	prerequisite = {
		focus = GER_sub_primacy
		focus = GER_cruiser_primacy
		focus = GER_battleship_primacy
		focus = GER_carrier_primacy
	}
	x = 0
	y = 7
	relative_position_id = GER_navy_start
	cost = 4
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_coastal = yes
				free_building_slots = {
					building = dockyard
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = dockyard
				level = 1
				instant_build = yes
			}
		}
		random_owned_controlled_state = {
			limit = {
				is_coastal = yes
				free_building_slots = {
					building = dockyard
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = dockyard
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = GER_plan_z_end
	icon = GFX_focus_Bluewater_Navy
	prerequisite = { focus = GER_dockyard_3 }
	x = 0
	y = 8
	relative_position_id = GER_navy_start
	cost = 10
	ai_will_do  = {
		factor = 8
		modifier = {
			factor = 0.5
			date < 1939.1.1
		}
		modifier = {
			factor = 2
			has_navy_size = { size > 74 }
		}
		modifier = {
			factor = 1.2
			has_navy_size = { size > 99 }
		}
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		any_owned_state = { is_coastal = yes }
		has_navy_size = { size > 49 }
	}
	completion_reward = {
		add_tech_bonus = {
			name = naval_doctrine_bonus
			bonus = 0.5
			uses = 3
			category = naval_doctrine
		}
	}
}
shared_focus = {
	id = GER_air_start
	icon = GFX_focus_Aerial_Fleet
	prerequisite = { focus = GER_renounce_versailles_treaty }
	x = 8
	y = 1
	relative_position_id = GER_renounce_versailles_treaty
	cost = 5
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		air_experience = 25
	}
}
shared_focus = {
	id = GER_air_force_doctrines
	icon = GFX_goal_generic_air_doctrine
	prerequisite = { focus = GER_air_start }
	x = 0
	y = 1
	relative_position_id = GER_air_start
	cost = 10
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = air_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = air_doctrine
		}
	}
}
shared_focus = {
	id = GER_fighter_innovations
	icon = GFX_goal_generic_air_fighter
	prerequisite = { focus = GER_air_force_doctrines }
	x = -1
	y = 2
	relative_position_id = GER_air_start
	cost = 5
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = fighter_bonus
			bonus = 0.4
			uses = 1
			category = light_air
		}
	}
}
shared_focus = {
	id = GER_heavy_innovations
	icon = GFX_goal_generic_air_bomber
	prerequisite = { focus = GER_air_force_doctrines }
	x = 1
	y = 2
	relative_position_id = GER_air_start
	cost = 5
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = bomber_bonus
			bonus = 0.4
			uses = 1
			category = medium_air
			category = heavy_air
		}
	}
}
shared_focus = {
	id = GER_fighter_research
	icon = GFX_focus_Aerial_Offensive
	prerequisite = { focus = GER_fighter_innovations }
	mutually_exclusive = { focus = GER_bomber_research }
	x = -1
	y = 3
	relative_position_id = GER_air_start
	cost = 5
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = fighter_bonus
			bonus = 0.4
			uses = 1
			category = light_fighter
			category = cat_heavy_fighter
		}
	}
}

shared_focus = {
	id = GER_CAS_research
	icon = GFX_focus_Red_Baron
	prerequisite = { focus = GER_fighter_research }
	x = -1
	y = 4
	relative_position_id = GER_air_start
	cost = 5
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = CAS_bonus
			bonus = 0.4
			uses = 1
			category = cas_bomber
		}
	}
}

shared_focus = {
	id = GER_naval_bomber_research
	icon = GFX_goal_generic_air_naval_bomber
	prerequisite = { focus = GER_CAS_research }
	x = -1
	y = 5
	relative_position_id = GER_air_start
	cost = 5
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = naval_bomber_bonus
			bonus = 0.4
			uses = 1
			category = naval_bomber
		}
	}
}

shared_focus = {
	id = GER_bomber_research
	icon = GFX_focus_Bombs_Away
	prerequisite = { focus = GER_heavy_innovations }
	mutually_exclusive = { focus = GER_fighter_research }
	x = 1
	y = 3
	relative_position_id = GER_air_start
	cost = 5
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = bomber_bonus
			bonus = 0.4
			uses = 1
			category = tactical_bomber
			category = cat_strategic_bomber
		}
	}
}

shared_focus = {
	id = GER_heavy_fighter_research
	icon = GFX_goal_generic_build_airforce
	prerequisite = { focus = GER_bomber_research }
	x = 1
	y = 4
	relative_position_id = GER_air_start
	cost = 5
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = heavy_fighter_bonus
			bonus = 0.4
			uses = 1
			category = cat_heavy_fighter
		}
	}
}
shared_focus = {
	id = GER_amerika_bomber
	icon = GFX_focus_Aerial_Fleet_2
	prerequisite = { focus = GER_heavy_fighter_research }
	x = 1
	y = 5
	relative_position_id = GER_air_start
	cost = 5
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = strategic_bomber_bonus
			bonus = 0.4
			uses = 1
			category = cat_strategic_bomber
		}
	}
}
shared_focus = {
	id = GER_air_force_doctrines2
	icon = GFX_goal_generic_air_doctrine
	prerequisite = { focus = GER_amerika_bomber focus = GER_naval_bomber_research }
	x = 0
	y = 6
	relative_position_id = GER_air_start
	cost = 10
	ai_will_do  = {
		factor = 8
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = air_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = air_doctrine
		}
	}
}
shared_focus = {
	id = GER_rocketry_research
	icon = GFX_focus_rocketry
	prerequisite = { focus = GER_air_force_doctrines2 }
	x = 0
	y = 7
	relative_position_id = GER_air_start
	cost = 3
	ai_will_do  = {
		factor = 8
		modifier = {
			factor = 1.5
			is_historical_focus_on = yes
		}
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		date > 1941.12.31
	}
	completion_reward = {
		add_tech_bonus = {
			name = rocketry_bonus
			bonus = 0.25
			uses = 1
			category = rocketry
		}
	}
}

shared_focus = {
	id = GER_electronics_research
	icon = GFX_focus_research
	prerequisite = { focus = GER_renounce_versailles_treaty }
	x = -14
	y = 1
	relative_position_id = GER_renounce_versailles_treaty
	cost = 10
	ai_will_do  = {
		factor = 9
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = electronics_bonus
			bonus = 0.5
			uses = 1
			category = computing_tech
		}
	}
}

shared_focus = {
	id = GER_fund_research
	icon = GFX_focus_Brainpower
	prerequisite = { focus = GER_electronics_research }
	x = 0
	y = 1
	relative_position_id = GER_electronics_research
	cost = 6
	ai_will_do  = {
		factor = 9
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		army_experience = 20
	}
}

shared_focus = {
	id = GER_radar_effort
	icon = GFX_goal_generic_radar
	prerequisite = { focus = GER_fund_research }
	x = 0
	y = 2
	relative_position_id = GER_electronics_research
	cost = 10
	ai_will_do  = {
		factor = 9
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
	}
	completion_reward = {
		add_tech_bonus = {
			name = radar_bonus
			bonus = 0.5
			uses = 1
			category = radar_tech
		}
	}
}

shared_focus = {
	id = GER_rocketry_effort
	icon = GFX_focus_rocketry
	prerequisite = { focus = GER_radar_effort }
	mutually_exclusive = { focus = GER_nuclear_effort }
	x = -1
	y = 3
	relative_position_id = GER_electronics_research
	cost = 5
	ai_will_do  = {
		factor = 9
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		date > 1940.1.1
	}
	completion_reward = {
		add_tech_bonus = {
			name = rocketry_bonus
			bonus = 0.5
			uses = 1
			category = rocketry
		}
	}
}

shared_focus = {
	id = GER_nuclear_effort
	icon = GFX_focus_wonderweapons
	prerequisite = { focus = GER_radar_effort }
	mutually_exclusive = { focus = GER_rocketry_effort }
	x = 1
	y = 3
	relative_position_id = GER_electronics_research
	cost = 5
	ai_will_do  = {
		factor = 9
		modifier = {
			factor = 0
			is_historical_focus_on = yes
		}
	}
	available = {
		OR = {
			NOT = { has_country_flag = GER_civil_war }
			has_country_flag = GER_no_civil_war
			has_country_flag = GER_civil_war_over
		}
		date > 1940.1.1
	}
	completion_reward = {
		add_tech_bonus = {
			name = nuclear_bonus
			bonus = 0.5
			uses = 1
			category = nuclear
		}
	}
}