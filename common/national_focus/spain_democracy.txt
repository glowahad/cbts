    shared_focus = {
		id = SPR_democracy_triumphant
		icon = GFX_focus_Freedom_of_Speech
		cost = 8
		available = {
		    OR ={
			    has_completed_focus = SPR_march_forward
				has_completed_focus = SPR_reform_con_lib
				has_completed_focus = SPR_limited_reform
				has_completed_focus = SPR_third_rep
			}
		}
		prerequisite = { focus = SPR_march_forward focus = SPR_reform_con_lib focus = SPR_limited_reform focus = SPR_third_rep }
		completion_reward = { 
		    add_stability = 0.07
		}
		x = 5
		y = 7
		offset = {
		    x = -1
	        y = -1
			trigger = {
			    OR = {
		            has_focus_tree = spain_socdem
					has_focus_tree = spain_liberal
				}
	        }
		}
		offset = {
		    x = -3
	        y = 0
			trigger = {
			    OR = {
					has_focus_tree = spain_ceda
				}
	        }
		}
	}
	shared_focus = {
		id = SPR_end_protectionism
		icon = GFX_focus_Free_Trade
		cost = 8
		prerequisite = {
			focus = SPR_democracy_triumphant
		}
		completion_reward = {
		    add_ideas = free_trade
		}
		x = 3
		y = 8
		offset = {
		    x = -1
	        y = -1
			trigger = {
			    OR = {
		            has_focus_tree = spain_socdem
					has_focus_tree = spain_liberal
				}
	        }
		}
		offset = {
		    x = -3
	        y = 0
			trigger = {
			    OR = {
					has_focus_tree = spain_ceda
				}
	        }
		}
	}
	shared_focus = {
		id = SPR_healthcare_all
		icon = GFX_focus_Medicine
		cost = 8
		prerequisite = {
			focus = SPR_end_protectionism
		}
		completion_reward = {
		    add_ideas = SPR_seg_social
		}
		x = 2
		y = 9
		offset = {
		    x = -1
	        y = -1
			trigger = {
			    OR = {
		            has_focus_tree = spain_socdem
					has_focus_tree = spain_liberal
				}
	        }
		}
		offset = {
		    x = -3
	        y = 0
			trigger = {
			    OR = {
					has_focus_tree = spain_ceda
				}
	        }
		}
	}
	shared_focus = {
		id = SPR_tourism_boom
		icon = GFX_focus_Foreign_Diplomacy
		cost = 8
		prerequisite = {
			focus = SPR_healthcare_all
		}
		completion_reward = {
		    add_ideas = SPR_tourism_boom
		}
		x = 3
		y = 10
		offset = {
		    x = -1
	        y = -1
			trigger = {
			    OR = {
		            has_focus_tree = spain_socdem
					has_focus_tree = spain_liberal
				}
	        }
		}
		offset = {
		    x = -3
	        y = 0
			trigger = {
			    OR = {
					has_focus_tree = spain_ceda
				}
	        }
		}
	}
	shared_focus = {
		id = SPR_constitution_ed
		icon = GFX_focus_Social_Democracy
		cost = 8
		prerequisite = {
			focus = SPR_democracy_triumphant
		}
		completion_reward = {
		    add_ideas = SPR_constitution_ed 
		}
		x = 5
		y = 8
		offset = {
		    x = -1
	        y = -1
			trigger = {
			    OR = {
		            has_focus_tree = spain_socdem
					has_focus_tree = spain_liberal
				}
	        }
		}
		offset = {
		    x = -3
	        y = 0
			trigger = {
			    OR = {
					has_focus_tree = spain_ceda
				}
	        }
		}
	}
	shared_focus = {
		id = SPR_liberal_patriot
		icon = GFX_focus_Spanish_Republicanism
		cost = 8
		prerequisite = {
			focus = SPR_constitution_ed
		}
		completion_reward = {
		    add_popularity = { ideology = social_democracy popularity = 0.1 }
			add_popularity = { ideology = social_liberalism popularity = 0.1 }
			add_popularity = { ideology = market_liberalism popularity = 0.1 }
			add_popularity = { ideology = democratic popularity = 0.1 }
		}
		x = 6
		y = 9
		offset = {
		    x = -1
	        y = -1
			trigger = {
			    OR = {
		            has_focus_tree = spain_socdem
					has_focus_tree = spain_liberal
				}
	        }
		}
		offset = {
		    x = -3
	        y = 0
			trigger = {
			    OR = {
					has_focus_tree = spain_ceda
				}
	        }
		}
	}
	shared_focus = {
		id = SPR_artists
		icon = GFX_focus_Support_Spanish_Republic
		cost = 8
		prerequisite = {
			focus = SPR_constitution_ed
		}
		completion_reward = {
		    add_stability = 0.05
			add_war_support = 0.05
		}
		x = 4
		y = 9
		offset = {
		    x = -1
	        y = -1
			trigger = {
			    OR = {
		            has_focus_tree = spain_socdem
					has_focus_tree = spain_liberal
				}
	        }
		}
		offset = {
		    x = -3
	        y = 0
			trigger = {
			    OR = {
					has_focus_tree = spain_ceda
				}
	        }
		}
	}
	shared_focus = {
		id = SPR_tolerant_society
		icon = GFX_focus_Unite_Spain
		cost = 8
		prerequisite = {
			focus = SPR_artists
		}
		prerequisite = {
			focus = SPR_liberal_patriot
		}
		completion_reward = {
		    add_ideas = SPR_tolerance
		}
		x = 5
		y = 10
		offset = {
		    x = -1
	        y = -1
			trigger = {
			    OR = {
		            has_focus_tree = spain_socdem
					has_focus_tree = spain_liberal
				}
	        }
		}
		offset = {
		    x = -3
	        y = 0
			trigger = {
			    OR = {
					has_focus_tree = spain_ceda
				}
	        }
		}
	}
	shared_focus = {
		id = SPR_language_recognition
		icon = GFX_focus_Statutes_Autonomy
		cost = 8
		prerequisite = {
			focus = SPR_democracy_triumphant
		}
		completion_reward = {
		    add_ideas = SPR_language_rec
		}
		x = 7
		y = 8
		offset = {
		    x = -1
	        y = -1
			trigger = {
			    OR = {
		            has_focus_tree = spain_socdem
					has_focus_tree = spain_liberal
				}
	        }
		}
		offset = {
		    x = -3
	        y = 0
			trigger = {
			    OR = {
					has_focus_tree = spain_ceda
				}
	        }
		}
	}
	shared_focus = {
		id = SPR_judiciary
		icon = GFX_focus_Fair_Trials
		cost = 8
		prerequisite = {
			focus = SPR_language_recognition
		}
		completion_reward = {
		    add_political_power = 100
		}
		x = 8
		y = 9
		offset = {
		    x = -1
	        y = -1
			trigger = {
			    OR = {
		            has_focus_tree = spain_socdem
					has_focus_tree = spain_liberal
				}
	        }
		}
		offset = {
		    x = -3
	        y = 0
			trigger = {
			    OR = {
					has_focus_tree = spain_ceda
				}
	        }
		}
	}
	shared_focus = {
		id = SPR_end_corruption
		icon = GFX_focus_Root_out_Funding
		cost = 8
		prerequisite = {
			focus = SPR_judiciary
		}
		completion_reward = {
		    add_stability = 0.05
			add_war_support = 0.05
			add_political_power = 50
		}
		x = 7
		y = 10
		offset = {
		    x = -1
	        y = -1
			trigger = {
			    OR = {
		            has_focus_tree = spain_socdem
					has_focus_tree = spain_liberal
				}
	        }
		}
		offset = {
		    x = -3
	        y = 0
			trigger = {
			    OR = {
					has_focus_tree = spain_ceda
				}
	        }
		}
	}
	shared_focus = {
		id = SPR_shining_light
		icon = GFX_goal_support_democracy
		cost = 8
		prerequisite = {
			focus = SPR_end_corruption
		}
		prerequisite = {
			focus = SPR_tolerant_society
		}
		prerequisite = {
			focus = SPR_tourism_boom
		}
		completion_reward = {
		    add_stability = 0.08
			add_war_support = 0.08
		}
		x = 5
		y = 11
		offset = {
		    x = -1
	        y = -1
			trigger = {
			    OR = {
		            has_focus_tree = spain_socdem
					has_focus_tree = spain_liberal
				}
	        }
		}
		offset = {
		    x = -3
	        y = 0
			trigger = {
			    OR = {
					has_focus_tree = spain_ceda
				}
	        }
		}
	}