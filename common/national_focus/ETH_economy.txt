shared_focus = {
	id = ETH_ethiopian_economy
	icon = GFX_focus_Economics_School
	available = {
		has_completed_focus = ETH_strengthen_the_empire
	}
	x = 52 #19
	y = 0
	cost = 3
	prerequisite = {
	}
	available_if_capitulated = yes
	completion_reward = {
	   	add_political_power = 10
	}
	ai_will_do = {
		factor = 1
	}
}

shared_focus = {
	id = ETH_advantage
	icon = GFX_focus_emperors_party
	available = {
		GRM = {
			has_war = yes
		}
		OR = {
			ENG ={
				has_war = yes
			}
			USA = {
				has_war = yes
			}
		}
		OR = {
			FRA = {
				has_war = yes
			}
			country_exists = VIC
		}
	}
	relative_position_id = ETH_ethiopian_economy
	x = 2
	y = 1
	cost = 3
	prerequisite = {
		focus = ETH_ethiopian_economy
	}
	available_if_capitulated = yes
	completion_reward = {
	   	add_ideas = ETH_advantage_idea
	}
	ai_will_do = {
		factor = 2
	}
}

shared_focus = {
	id = ETH_coffee_export
	icon = wip
	available = {
	}
	relative_position_id = ETH_ethiopian_economy
	x = 1
	y = 2
	cost = 3
	prerequisite = {
		focus = ETH_advantage
	}
	available_if_capitulated = yes
	completion_reward = {
		add_political_power = 25
	}
	ai_will_do = {
		factor = 2
	}
}

shared_focus = {
	id = ETH_grain_export
	icon = wip
	available = {
	}
	relative_position_id = ETH_ethiopian_economy
	x = 3
	y = 2
	cost = 3
	prerequisite = {
		focus = ETH_advantage
	}
	available_if_capitulated = yes
	completion_reward = {
		add_political_power = 25
	}
	ai_will_do = {
		factor = 2
	}
}

shared_focus = {
	id = ETH_agriculture
	icon = GFX_focus_Agriculture_2
	available = {
	}
	relative_position_id = ETH_ethiopian_economy
	x = 2
	y = 3
	cost = 3
	prerequisite = {
		focus = ETH_grain_export
	}
	prerequisite = {
		focus = ETH_coffee_export
	}
	available_if_capitulated = yes
	completion_reward = {
		add_ideas = {
			ETH_agricultue_idea
		}
	}
	ai_will_do = {
		factor = 1
	}
}

shared_focus = {
	id = ETH_infrastructure
	icon = GFX_focus_Industrialization
	available = {
	}
	relative_position_id = ETH_ethiopian_economy
	x = -2
	y = 1
	cost = 3
	prerequisite = {
		focus = ETH_ethiopian_economy
	}
	available_if_capitulated = yes
	completion_reward = {
		add_ideas = ETH_construction_idea
	}
	ai_will_do = {
		factor = 1
	}
}

shared_focus = {
	id = ETH_road
	icon = GFX_focus_Roads
	available = {
	}
	relative_position_id = ETH_ethiopian_economy
	x = -3
	y = 2
	cost = 3
	prerequisite = {
		focus = ETH_infrastructure
	}
	available_if_capitulated = yes
	completion_reward = {
		add_ideas = ETH_infrastructure_idea
	}
	ai_will_do = {
		factor = 1
	}
}

shared_focus = {
	id = ETH_railway
	icon = GFX_focus_Industrialization
	available = {
	}
	relative_position_id = ETH_ethiopian_economy
	x = -3
	y = 3
	cost = 3
	prerequisite = {
		focus = ETH_road
	}
	available_if_capitulated = yes
	completion_reward = {
		random_owned_state = {
			add_building_construction = {
				type = infrastructure
				level = 2
				instant_build = yes
			}
		}
		random_owned_state = {
			add_building_construction = {
				type = infrastructure
				level = 2
				instant_build = yes
			}
		}
		random_owned_state = {
			add_building_construction = {
				type = infrastructure
				level = 2
				instant_build = yes
			}
		}
		random_owned_state = {
			add_building_construction = {
				type = infrastructure
				level = 2
				instant_build = yes
			}
		}
	}
	ai_will_do = {
		factor = 1
	}
}

shared_focus = {
	id = ETH_eritrean_railways
	icon = GFX_focus_Railways
	available = {
	 	ETH = {
	 		owns_state = 550
	 	}
	}
	relative_position_id = ETH_ethiopian_economy
	x = -1
	y = 2
	cost = 3
	prerequisite = {
		focus = ETH_infrastructure
	}
	available_if_capitulated = yes
	completion_reward = {
		550 = {
			add_building_construction = {
				type = infrastructure
				level = 2
				instant_build = yes
			}
		}
		if = {
			limit = {
				ETH = {
					owns_state = 1001
				}
			}
			1001 = {
				add_building_construction = {
					type = infrastructure
					level = 1
					instant_build = yes
				}
			}
		}
		
	}
	ai_will_do = {
		factor = 1
	}
}

shared_focus = {
	id = ETH_eritrean_ports
	icon = GFX_focus_Naval_Operations
	available = {
		ETH = {
			owns_state = 1001
		}
	}
	relative_position_id = ETH_ethiopian_economy
	x = -1
	y = 3
	cost = 3
	prerequisite = {
		focus = ETH_eritrean_railways
	}
	available_if_capitulated = yes
	completion_reward = {
		1001 = {
			add_building_construction = {
				type = naval_base
				level = 3
				instant_build = yes
				province = 12766
			}
		}
	}
	ai_will_do = {
		factor = 2
	}
}

shared_focus = {
	id = ETH_factories
	icon = GFX_focus_Industrialization
	available = {
	}
	relative_position_id = ETH_ethiopian_economy
	x = -2
	y = 4
	cost = 3
	prerequisite = {
		focus = ETH_railway
	}
	available_if_capitulated = yes
	completion_reward = {
		random_owned_controlled_state = {
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
		random_owned_controlled_state = {
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
	}
	ai_will_do = {
		factor = 2
	}
}

