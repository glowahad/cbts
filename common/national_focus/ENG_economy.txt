# focuses here
#s_team337
shared_focus = {
	id = ENG_Rearm_Invest_NI
	icon = GFX_focus_Northern_Irish_Industry
	#prerequisite = { focus = ENG_Rearm_Invest_NI }
	x = 17
	y = 0
	#relative_position_id = ENG_Rearm_Invest_NI
	cost = 5
	ai_will_do = {
		factor = 70
		modifier = {
			date > 1935.4.1
			factor = 5
		}
	}
	available = {
		has_country_flag = ENG_depression_recovering
	}
	bypass = {
		custom_trigger_tooltip = {
			tooltip = ENG_owns_belfast
			all_owned_state = {
				OR = {
					NOT = {
						region = 4
					}
					AND = {
						region = 4
						NOT = {
							is_controlled_by = ROOT
						}
					}
				}
			}
		}
	}
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
				OR = {
					region = 4
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		ENG_micro_depression_recovery = yes
	}
}
shared_focus = {
	id = ENG_Rearm_Invest_Scotland
	icon = GFX_focus_Scottish_Industry
	#prerequisite = { focus = ENG_Rearm_Invest_Scotland }
	x = 20
	y = 0
	#relative_position_id = ENG_Rearm_Invest_Scotland
	cost = 5.72
	ai_will_do = {
		factor = 120
		modifier = {
			date > 1935.4.1
			factor = 5
		}
	}
	available = {
		has_country_flag = ENG_depression_recovering
	}
	bypass = {
		custom_trigger_tooltip = {
			tooltip = ENG_owns_scotland
			all_owned_state = {
				OR = {
					NOT = {
						region = 3
					}
					AND = {
						region = 3
						NOT = {
							is_controlled_by = ROOT
						}
					}
				}
			}
		}
	}
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
				OR = {
					region = 3
				}
				OR = {
					has_state_category = city
					has_state_category = large_city
					has_state_category = large_town
					has_state_category = megalopolis
					has_state_category = metropolis
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		ENG_micro_depression_recovery = yes
	}
}
shared_focus = {
	id = ENG_Rearm_Invest_Northern_England
	icon = GFX_focus_Northern_English_Industry
	#prerequisite = { focus = ENG_Rearm_Invest_Northern_England }
	x = 23
	y = 0
	#relative_position_id = ENG_Rearm_Invest_Northern_England
	cost = 5.72
	ai_will_do = {
		factor = 120
		modifier = {
			date > 1935.4.1
			factor = 5
		}
	}
	available = {
		has_country_flag = ENG_depression_recovering
	}
	bypass = {
		custom_trigger_tooltip = {
			tooltip = ENG_owns_NE
			all_owned_state = {
				OR = {
					NOT = {
						region = 2
					}
					AND = {
						region = 2
						NOT = {
							is_controlled_by = ROOT
						}
					}
				}
			}
		}
	}
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
				OR = {
					region = 2
				}
				NOT = {
					state = 1177
				}
				OR = {
					has_state_category = city
					has_state_category = large_city
					has_state_category = large_town
					has_state_category = megalopolis
					has_state_category = metropolis
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		ENG_micro_depression_recovery = yes
	}
}
shared_focus = {
	id = ENG_Rearm_Invest_Wales
	icon = GFX_focus_Welsh_Industry
	#prerequisite = { focus = ENG_Rearm_Invest_Northern_England }
	x = 26
	y = 0
	#relative_position_id = ENG_Rearm_Invest_Northern_England
	cost = 5
	ai_will_do = {
		factor = 90
		modifier = {
			date > 1935.4.1
			factor = 5
		}
	}
	available = {
		has_country_flag = ENG_depression_recovering
	}
	bypass = {
		custom_trigger_tooltip = {
			tooltip = ENG_owns_Wales
			NOT = {
				122 = {
					is_controlled_by = ROOT
				}
			}
		}
	}
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
				OR = {
					state = 122
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		ENG_micro_depression_recovery = yes
	}
}
shared_focus = {
	id = ENG_Rearm_Invest_Southern_England
	icon = GFX_focus_Southern_English_Industry
	#prerequisite = { focus = ENG_Rearm_Invest_Northern_England }
	x = 29
	y = 0
	#relative_position_id = ENG_Rearm_Invest_Northern_England
	cost = 5.72
	ai_will_do = {
		factor = 125
		modifier = {
			date > 1935.4.1
			factor = 5
		}
	}
	available = {
		has_country_flag = ENG_depression_recovering
	}
	bypass = {
		custom_trigger_tooltip = {
			tooltip = ENG_owns_SE
			all_owned_state = {
				OR = {
					NOT = {
						region = 1
					}
					AND = {
						region = 1
						NOT = {
							is_controlled_by = ROOT
						}
					}
				}
			}
		}
	}
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
				}
				OR = {
					has_state_category = city
					has_state_category = large_city
					has_state_category = large_town
					has_state_category = megalopolis
					has_state_category = metropolis
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		ENG_micro_depression_recovery = yes
	}
}
shared_focus = {
	id = ENG_Rearm_Civilian_Development
	icon = GFX_Relief_Plans
	prerequisite = {
		focus = ENG_Rearm_Invest_NI
	}
	prerequisite = {
		focus = ENG_Rearm_Invest_Scotland
	}
	prerequisite = {
		focus = ENG_Rearm_Invest_Wales
	}
	prerequisite = {
		focus = ENG_Rearm_Invest_Southern_England
	}
	prerequisite = {
		focus = ENG_Rearm_Invest_Northern_England
	}
	mutually_exclusive = {
		focus = ENG_Rearm_Shadow_Scheme
	}
	mutually_exclusive = {
		focus = ENG_Rearm_Direct
	}
	x = -3
	y = 1
	relative_position_id = ENG_Rearm_Invest_NI
	cost = 2.15
	ai_will_do = {
		factor = 100
	}
	available = {
		has_government = socialism
		has_idea = ENG_hog_George_Lansbury
		has_war = no
	}
	bypass = {
	}
	completion_reward = {
		add_political_power = 25
		add_tech_bonus = {
			name = industrial_bonus
			bonus = 0.5
			uses = 2
			category = industry
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Industrial_Planning_Board
	icon = GFX_focus_Planned_Economy
	prerequisite = {
		focus = ENG_Rearm_Civilian_Development
	}
	x = -1
	y = 1
	relative_position_id = ENG_Rearm_Civilian_Development
	cost = 4.29
	ai_will_do = {
		factor = 180
	}
	available = {
		has_government = socialism
		has_idea = ENG_hog_George_Lansbury
		has_war = no
	}
	bypass = {
	}
	completion_reward = {
		add_ideas = ENG_pacifist_development
	}
}
shared_focus = {
	id = ENG_Rearm_Take_Money_From_Defense
	icon = GFX_focus_Economic_Self_Reliance
	prerequisite = {
		focus = ENG_Rearm_Industrial_Planning_Board
	}
	x = -3
	y = 2
	relative_position_id = ENG_Rearm_Civilian_Development
	cost = 4
	ai_will_do = {
		factor = 180
	}
	available = {
		has_government = socialism
		has_idea = ENG_hog_George_Lansbury
		has_war = no
	}
	bypass = {
	}
	completion_reward = {
		ENG_add_medium_public_works = yes
		ENG_subtract_military_spending = yes
	}
}
shared_focus = {
	id = ENG_Rearm_Peace_Balance_Industry
	icon = GFX_focus_Industrialization
	prerequisite = {
		focus = ENG_Rearm_Take_Money_From_Defense
	}
	x = -2
	y = 3
	relative_position_id = ENG_Rearm_Civilian_Development
	cost = 6.43
	ai_will_do = {
		factor = 180
	}
	available = {
		has_government = socialism
		has_idea = ENG_hog_George_Lansbury
		has_war = no
	}
	bypass = {
	}
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 3
					region = 4
					region = 238
					region = 241
				}
				NOT = {
					state = 1177
					has_state_category = pastoral
					has_state_category = small_island
					has_state_category = wasteland
					has_state_category = enclave
					has_state_category = tiny_island
					has_state_category = large_city
					has_state_category = megalopolis
					has_state_category = metropolis
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 3
					region = 4
					region = 238
					region = 241
				}
				NOT = {
					has_state_category = pastoral
					has_state_category = small_island
					has_state_category = wasteland
					has_state_category = enclave
					has_state_category = tiny_island
					has_state_category = large_city
					has_state_category = megalopolis
					has_state_category = metropolis
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Peace_Railways
	icon = GFX_focus_Trains
	prerequisite = {
		focus = ENG_Rearm_Civilian_Development
	}
	x = 1
	y = 1
	relative_position_id = ENG_Rearm_Civilian_Development
	cost = 6
	ai_will_do = {
		factor = 145
	}
	available = {
		has_government = socialism
		has_idea = ENG_hog_George_Lansbury
		has_war = no
	}
	bypass = {
	}
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 3
					region = 4
					region = 238
					region = 241
				}
				NOT = {
					state = 1177
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 3
					region = 4
					region = 238
					region = 241
				}
				NOT = {
					state = 1177
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Modern_Roads
	icon = GFX_focus_Highways
	prerequisite = {
		focus = ENG_Rearm_Peace_Railways
	}
	x = 3
	y = 2
	relative_position_id = ENG_Rearm_Civilian_Development
	cost = 6
	ai_will_do = {
		factor = 145
	}
	available = {
		has_government = socialism
		has_idea = ENG_hog_George_Lansbury
		has_war = no
	}
	bypass = {
	}
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 3
					region = 4
					region = 238
					region = 241
				}
				NOT = {
					state = 1177
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 3
					region = 4
					region = 238
					region = 241
				}
				NOT = {
					state = 1177
				}
			}
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Modern_Industry
	icon = GFX_goal_generic_production
	prerequisite = {
		focus = ENG_Rearm_Modern_Roads
	}
	x = 2
	y = 3
	relative_position_id = ENG_Rearm_Civilian_Development
	cost = 5
	ai_will_do = {
		factor = 145
	}
	available = {
		has_government = socialism
		has_idea = ENG_hog_George_Lansbury
		has_war = no
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = industrial_bonus
			bonus = 0.5
			uses = 1
			category = industry
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Command_Planning
	icon = GFX_focus_Mechanization
	prerequisite = {
		focus = ENG_Rearm_Industrial_Planning_Board
	}
	prerequisite = {
		focus = ENG_Rearm_Peace_Railways
	}
	mutually_exclusive = {
		focus = ENG_Rearm_Indicative_Planning
	}
	x = -1
	y = 2
	relative_position_id = ENG_Rearm_Civilian_Development
	cost = 7.15
	ai_will_do = {
		factor = 180
	}
	available = {
		has_government = socialism
		has_idea = ENG_hog_George_Lansbury
		has_war = no
	}
	bypass = {
	}
	completion_reward = {
		add_political_power = -50
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 3
					region = 4
					region = 238
					region = 241
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = industrial_complex
				level = 2
				instant_build = yes
			}
		}
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 3
					region = 4
					region = 238
					region = 241
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = industrial_complex
				level = 2
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Indicative_Planning
	icon = GFX_focus_Control_the_Economy
	prerequisite = {
		focus = ENG_Rearm_Industrial_Planning_Board
	}
	prerequisite = {
		focus = ENG_Rearm_Peace_Railways
	}
	mutually_exclusive = {
		focus = ENG_Rearm_Command_Planning
	}
	x = 1
	y = 2
	relative_position_id = ENG_Rearm_Civilian_Development
	cost = 7.15
	ai_will_do = {
		factor = 80
	}
	available = {
		has_government = socialism
		has_idea = ENG_hog_George_Lansbury
		has_war = no
	}
	bypass = {
	}
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 3
					region = 4
					region = 238
					region = 241
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 3
					region = 4
					region = 238
					region = 241
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Planning_Network
	icon = GFX_focus_Redraw_Admin_Borders
	prerequisite = {
		focus = ENG_Rearm_Command_Planning
		focus = ENG_Rearm_Indicative_Planning
	}
	x = 0
	y = 3
	relative_position_id = ENG_Rearm_Civilian_Development
	cost = 5.15
	ai_will_do = {
		factor = 200
	}
	available = {
		has_government = socialism
		has_idea = ENG_hog_George_Lansbury
		has_war = no
	}
	bypass = {
	}
	completion_reward = {
		swap_ideas = {
			remove_idea = ENG_pacifist_development
			add_idea = ENG_pacifist_development2
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Rely_on_Regional_Boards
	icon = GFX_focus_strengthen_small_production
	prerequisite = {
		focus = ENG_Rearm_Planning_Network
	}
	prerequisite = {
		focus = ENG_Rearm_Indicative_Planning
	}
	x = 1
	y = 4
	relative_position_id = ENG_Rearm_Civilian_Development
	cost = 8
	ai_will_do = {
		factor = 150
	}
	available = {
		has_government = socialism
		has_idea = ENG_hog_George_Lansbury
		has_war = no
	}
	bypass = {
	}
	completion_reward = {
		swap_ideas = {
			remove_idea = ENG_pacifist_development2
			add_idea = ENG_pacifist_development2b
		}
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 3
					region = 4
					region = 238
					region = 241
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
		ENG_fix_depression = yes
	}
}
shared_focus = {
	id = ENG_Rearm_Learn_from_Experience
	icon = GFX_focus_Cooperatives
	prerequisite = {
		focus = ENG_Rearm_Planning_Network
	}
	prerequisite = {
		focus = ENG_Rearm_Command_Planning
	}
	x = -1
	y = 4
	relative_position_id = ENG_Rearm_Civilian_Development
	cost = 4.29
	ai_will_do = {
		factor = 150
	}
	available = {
		has_government = socialism
		has_idea = ENG_hog_George_Lansbury
		has_war = no
	}
	bypass = {
	}
	completion_reward = {
		add_political_power = 30
		swap_ideas = {
			remove_idea = ENG_pacifist_development2
			add_idea = ENG_pacifist_development2a
		}
		ENG_fix_depression = yes
	}
}
shared_focus = {
	id = ENG_Rearm_Shadow_Scheme
	icon = GFX_focus_The_Shadow_Scheme
	prerequisite = {
		focus = ENG_Rearm_Invest_NI
	}
	prerequisite = {
		focus = ENG_Rearm_Invest_Scotland
	}
	prerequisite = {
		focus = ENG_Rearm_Invest_Wales
	}
	prerequisite = {
		focus = ENG_Rearm_Invest_Southern_England
	}
	prerequisite = {
		focus = ENG_Rearm_Invest_Northern_England
	}
	mutually_exclusive = {
		focus = ENG_Rearm_Civilian_Development
	}
	mutually_exclusive = {
		focus = ENG_Rearm_Direct
	}
	x = 4
	y = 1
	relative_position_id = ENG_Rearm_Invest_NI
	cost = 2.15
	ai_will_do = {
		factor = 180
	}
	available = {
		has_government = democratic
		date > 1936.1.1
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = industrial_bonus
			bonus = 0.5
			uses = 1
			category = dispersed_industry_category
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Contact_Private_Companies
	icon = GFX_focus_Corporatism
	prerequisite = {
		focus = ENG_Rearm_Shadow_Scheme
	}
	x = -1
	y = 1
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5.75
	ai_will_do = {
		factor = 180
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_ideas = ENG_shadow_scheme
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 238
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Dispersion
	icon = GFX_goal_generic_construct_mil_factory
	prerequisite = {
		focus = ENG_Rearm_Contact_Private_Companies
	}
	x = 0
	y = 2
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 6
	ai_will_do = {
		factor = 220
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		swap_ideas = {
			remove_idea = ENG_shadow_scheme
			add_idea = ENG_shadow_scheme2
		}
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 238
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Shadow_Factories
	icon = GFX_focus_Combined_Arms
	prerequisite = {
		focus = ENG_Rearm_Contact_Private_Companies
	}
	x = -2
	y = 2
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 6.45
	ai_will_do = {
		factor = 180
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 238
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Factory_Scotland
	icon = GFX_focus_Scotland
	prerequisite = {
		focus = ENG_Rearm_Dispersion
	}
	prerequisite = {
		focus = ENG_Rearm_Shadow_Factories
	}
	x = 1
	y = 3
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5.45
	ai_will_do = {
		factor = 150
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
				region = 3
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Shadow_Switching
	icon = GFX_focus_Union_Workers
	prerequisite = {
		focus = ENG_Rearm_Shadow_Factories
	}
	prerequisite = {
		focus = ENG_Rearm_Dispersion
	}
	x = -1
	y = 3
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5.15
	ai_will_do = {
		factor = 250
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		swap_ideas = {
			remove_idea = ENG_shadow_scheme2
			add_idea = ENG_shadow_scheme3
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Expand_Capability
	icon = GFX_focus_Trench_Warfare
	prerequisite = {
		focus = ENG_Rearm_Shadow_Switching
	}
	prerequisite = {
		focus = ENG_Rearm_Factory_Scotland
	}
	x = 0
	y = 4
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5.15
	ai_will_do = {
		factor = 240
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 238
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
		ENG_fix_depression = yes
	}
}
shared_focus = {
	id = ENG_Rearm_Direct
	icon = GFX_focus_Renewed_Arms
	prerequisite = {
		focus = ENG_Rearm_Invest_NI
	}
	prerequisite = {
		focus = ENG_Rearm_Invest_Scotland
	}
	prerequisite = {
		focus = ENG_Rearm_Invest_Wales
	}
	prerequisite = {
		focus = ENG_Rearm_Invest_Southern_England
	}
	prerequisite = {
		focus = ENG_Rearm_Invest_Northern_England
	}
	mutually_exclusive = {
		focus = ENG_Rearm_Civilian_Development
	}
	mutually_exclusive = {
		focus = ENG_Rearm_Shadow_Scheme
	}
	x = 14
	y = 1
	relative_position_id = ENG_Rearm_Invest_NI
	cost = 2.15
	ai_will_do = {
		factor = 180
	}
	available = {
		has_government = socialism
		date > 1936.1.1
		NOT = {
			has_idea = ENG_hog_George_Lansbury
		}
	}
	bypass = {
	}
	completion_reward = {
		add_stability = -0.04
		add_popularity = {
			ideology = socialism
			popularity = -0.04
		}
		add_tech_bonus = {
			name = industrial_bonus
			bonus = 0.5
			uses = 1
			category = concentrated_industry_category
		}
	}
}
shared_focus = {
	id = ENG_Rearm_National_Rearmament_Board
	icon = GFX_focus_Industrial_Council
	prerequisite = {
		focus = ENG_Rearm_Direct
	}
	x = 1
	y = 1
	relative_position_id = ENG_Rearm_Direct
	cost = 5.45
	ai_will_do = {
		factor = 250
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_ideas = ENG_labour_rearmament
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Peace_Crusade
	icon = GFX_focus_Swing_the_Sword
	prerequisite = {
		focus = ENG_Rearm_National_Rearmament_Board
	}
	x = 0
	y = 2
	relative_position_id = ENG_Rearm_Direct
	cost = 6.45
	ai_will_do = {
		factor = 180
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_stability = -0.02
		add_war_support = 0.02
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Factories
	icon = GFX_goal_generic_small_arms_shine
	prerequisite = {
		focus = ENG_Rearm_National_Rearmament_Board
	}
	x = 2
	y = 2
	relative_position_id = ENG_Rearm_Direct
	cost = 8.58
	ai_will_do = {
		factor = 180
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_stability = -0.04
		lose_2_percent_pop = yes
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = arms_factory
					size > 2
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = arms_factory
				level = 2
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Belfast_Arms_Factory
	icon = GFX_focus_Belfast_Rifle_Factory
	prerequisite = {
		focus = ENG_Peace_Crusade
	}
	prerequisite = {
		focus = ENG_Rearm_Factories
	}
	x = -1
	y = 3
	relative_position_id = ENG_Rearm_Direct
	cost = 6.45
	ai_will_do = {
		factor = 180
	}
	available = {
	}
	bypass = {
		119 = {
			NOT = {
				is_controlled_by = ROOT
			}
		}
	}
	completion_reward = {
		119 = {
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Continue_Preparations
	icon = GFX_focus_Military_Attache
	prerequisite = {
		focus = ENG_Peace_Crusade
	}
	prerequisite = {
		focus = ENG_Rearm_Factories
	}
	x = 1
	y = 3
	relative_position_id = ENG_Rearm_Direct
	cost = 7.15
	ai_will_do = {
		factor = 220
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		swap_ideas = {
			remove_idea = ENG_labour_rearmament
			add_idea = ENG_labour_rearmament2
		}
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = arms_factory
					size > 2
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 2
			add_building_construction = {
				type = arms_factory
				level = 2
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Labour_Private
	icon = GFX_focus_Military_Intervention
	prerequisite = {
		focus = ENG_Rearm_Belfast_Arms_Factory
	}
	prerequisite = {
		focus = ENG_Rearm_Continue_Preparations
	}
	x = 0
	y = 4
	relative_position_id = ENG_Rearm_Direct
	cost = 6
	ai_will_do = {
		factor = 220
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_political_power = 20
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
		ENG_fix_depression = yes
	}
}
shared_focus = {
	id = ENG_Rearm_Infantry_Equipment
	icon = GFX_focus_British_Soldiers
	prerequisite = {
		focus = ENG_Rearm_Shadow_Scheme
		focus = ENG_Rearm_Direct
	}
	x = 1
	y = 1
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5
	ai_will_do = {
		factor = 120
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = infantry_bonus
			bonus = 0.5
			uses = 1
			category = infantry_weapons
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Support_Equipment
	icon = GFX_focus_Support_Equipment
	prerequisite = {
		focus = ENG_Rearm_Shadow_Scheme
		focus = ENG_Rearm_Direct
	}
	x = 3
	y = 1
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5
	ai_will_do = {
		factor = 120
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = support_bonus
			bonus = 0.5
			uses = 1
			category = support_tech
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Continue_Mechanization
	icon = GFX_focus_Mechanized_Divisions
	prerequisite = {
		focus = ENG_Rearm_Infantry_Equipment
	}
	prerequisite = {
		focus = ENG_Rearm_Support_Equipment
	}
	x = 2
	y = 2
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5
	ai_will_do = {
		factor = 180
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = mechanized_bonus
			bonus = 0.5
			uses = 2
			category = motorized_equipment
			category = cat_mechanized_equipment
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Army_Doctrine
	icon = GFX_focus_Army_Reform
	prerequisite = {
		focus = ENG_Rearm_Continue_Mechanization
	}
	x = 3
	y = 3
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5
	ai_will_do = {
		factor = 135
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = grand_battleplan_bonus
			bonus = 0.5
			uses = 1
			category = cat_grand_battle_plan
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Conscription
	icon = GFX_focus_Recruitment_Drive
	prerequisite = {
		focus = ENG_Rearm_Army_Doctrine
	}
	x = 2
	y = 4
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5
	ai_will_do = {
		factor = 335
		modifier = {
			factor = 0
			date < 1939.3.1
		}
	}
	available = {
		OR = {
			threat > 0.30
			date > 1939.6.1
		}
	}
	bypass = {
	}
	completion_reward = {
		if = {
			limit = {
				OR = {
					has_idea = disarmed_nation
					has_idea = volunteer_only
				}
			}
			add_ideas = limited_conscription
		}
		else = {
			add_manpower = 10000
			add_war_support = 0.10
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Tanks
	icon = GFX_focus_Tanks_Across_the_Border
	prerequisite = {
		focus = ENG_Rearm_Army_Doctrine
	}
	x = 4
	y = 4
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5
	ai_will_do = {
		factor = 150
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = armor_bonus
			bonus = 0.5
			uses = 1
			category = armor
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Air_Panic
	icon = GFX_goal_generic_build_airforce
	prerequisite = {
		focus = ENG_Rearm_Shadow_Scheme
		focus = ENG_Rearm_Direct
	}
	x = 5
	y = 1
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 4.29
	ai_will_do = {
		factor = 300
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_ideas = ENG_air_panic
	}
}
shared_focus = {
	id = ENG_Rearm_Air_Engines
	icon = GFX_focus_Engine_Tech
	prerequisite = {
		focus = ENG_Rearm_Air_Panic
	}
	x = -1
	y = 1
	relative_position_id = ENG_Rearm_Air_Panic
	cost = 5
	ai_will_do = {
		factor = 300
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = fighter_bonus
			bonus = 0.5
			uses = 2
			category = plane_engines
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Air_Command
	icon = GFX_focus_Air_Superiority_Doctrine
	prerequisite = {
		focus = ENG_Rearm_Air_Panic
	}
	x = 1
	y = 1
	relative_position_id = ENG_Rearm_Air_Panic
	cost = 5
	ai_will_do = {
		factor = 185
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = air_doctrine_bonus
			bonus = 0.5
			uses = 1
			category = cat_strategic_destruction
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Naval_Air_Arm
	icon = GFX_focus_Naval_Bombers
	prerequisite = {
		focus = ENG_Rearm_Air_Engines
	}
	prerequisite = {
		focus = ENG_Rearm_Air_Command
	}
	x = 0
	y = 2
	relative_position_id = ENG_Rearm_Air_Panic
	cost = 5
	ai_will_do = {
		factor = 185
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = naval_bomber_bonus
			bonus = 0.5
			uses = 2
			category = naval_air
			category = naval_bomber
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Strategic_Bombers
	icon = GFX_goal_generic_air_bomber
	prerequisite = {
		focus = ENG_Rearm_Naval_Air_Arm
	}
	x = 0
	y = 4
	relative_position_id = ENG_Rearm_Air_Panic
	cost = 5
	ai_will_do = {
		factor = 185
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = strategic_bomber_bonus
			bonus = 0.5
			uses = 1
			category = cat_strategic_bomber
		}
	}
}
shared_focus = {
	id = ENG_Consult_Admirality
	icon = GFX_goal_generic_navy_doctrines_tactics
	prerequisite = {
		focus = ENG_Rearm_Shadow_Scheme
		focus = ENG_Rearm_Direct
	}
	x = 7
	y = 1
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5
	ai_will_do = {
		factor = 180
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		navy_experience = 30
		add_tech_bonus = {
			name = naval_bonus
			bonus = 0.5
			uses = 2
			category = fleet_in_being_tree
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Dockyard
	icon = GFX_Focus_British_Navy
	prerequisite = {
		focus = ENG_Rearm_Shadow_Scheme
		focus = ENG_Rearm_Direct
	}
	x = 9
	y = 1
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 6
	ai_will_do = {
		factor = 180
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = dockyard
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 238
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = dockyard
				level = 1
				instant_build = yes
			}
		}
		random_owned_state = {
			limit = {
				free_building_slots = {
					building = dockyard
					size > 1
					include_locked = yes
				}
				OR = {
					region = 1
					region = 2
					region = 238
				}
				NOT = {
					state = 1177
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = dockyard
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Ship_Models
	icon = GFX_focus_Battleship
	prerequisite = {
		focus = ENG_Consult_Admirality
		focus = ENG_Rearm_Dockyard
	}
	x = 8
	y = 2
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5
	ai_will_do = {
		factor = 180
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = naval_bonus
			bonus = 0.5
			uses = 1
			category = bb_tech
			category = ca_tech
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Cruisers
	icon = GFX_focus_Small_Navy
	prerequisite = {
		focus = ENG_Rearm_Ship_Models
	}
	x = 7
	y = 3
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5
	ai_will_do = {
		factor = 180
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = naval_bonus
			bonus = 0.5
			uses = 1
			category = cl_tech
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Destroyers
	icon = GFX_Goal_Generic_Refit_Destroyers
	prerequisite = {
		focus = ENG_Rearm_Cruisers
	}
	x = 6
	y = 4
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5
	ai_will_do = {
		factor = 180
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = naval_bonus
			bonus = 0.5
			uses = 1
			category = dd_tech
		}
	}
}
shared_focus = {
	id = ENG_Rearm_Carriers
	icon = GFX_focus_Carrier_Focus
	prerequisite = {
		focus = ENG_Rearm_Cruisers
	}
	x = 8
	y = 4
	relative_position_id = ENG_Rearm_Shadow_Scheme
	cost = 5
	ai_will_do = {
		factor = 180
	}
	available = {
	}
	bypass = {
	}
	completion_reward = {
		add_tech_bonus = {
			name = naval_bonus
			bonus = 0.5
			uses = 1
			category = cv_tech
		}
	}
}
