#####################################
## L'arbre de focalisation initial ##
#####################################
# 25 focuses

focus_tree = {
	id = CBTS_France_1933
	country = {
		factor = 0
		modifier = {
			add = 10
			tag = FRA
		}
	}
	shared_focus = FRA_lessons_great_war
	shared_focus = FRA_zay_auriol_plan
	shared_focus = FRA_deflationary_policies
	shared_focus = FRA_london_conference
	default = no
	continuous_focus_position = { x = 50 y = 1475 }


}