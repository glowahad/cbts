#13 focuses here
#s_team337
shared_focus = {
	id = RFP_2RCW_Economics_Start
	icon = GFX_focus_Tiger_Economy
	#prerequisite = { focus = RFP_2RCW_Economics_Start }
	x = 10
	y = 0
	#relative_position_id = RFP_2RCW_Economics_Start
	cost = 4.72
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_tech_bonus = {
			name = industry_bonus
			uses = 1
			bonus = 0.33
			category = industry
		}
	}
}
shared_focus = {
	id = RFP_2RCW_Economics_Mixed_Economy
	icon = GFX_focus_strengthen_small_production
	prerequisite = {
		focus = RFP_2RCW_Economics_Start
	}
	x = -1
	y = 1
	relative_position_id = RFP_2RCW_Economics_Start
	cost = 5
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_ideas = RFP_mixed_economy
	}
}
shared_focus = {
	id = RFP_2RCW_Economics_National_Unions
	icon = GFX_focus_Fascist_Trade_Union
	prerequisite = {
		focus = RFP_2RCW_Economics_Mixed_Economy
	}
	x = -1
	y = 2
	relative_position_id = RFP_2RCW_Economics_Start
	cost = 5.72
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
		custom_trigger_tooltip = {
			tooltip = RFP_zone_1
			OR = {
				563 = {
					is_core_of = ROOT
				}
				408 = {
					is_core_of = ROOT
				}
			}
		}
	}
	bypass = {
		
	}
	completion_reward = {
		swap_ideas = {
			remove_idea = RFP_mixed_economy
			add_idea = RFP_corporate_economy
		}
	}
}
shared_focus = {
	id = RFP_2RCW_Economics_State_Arbitration
	icon = GFX_focus_Fascist_Bills
	prerequisite = {
		focus = RFP_2RCW_Economics_National_Unions
	}
	x = -1
	y = 3
	relative_position_id = RFP_2RCW_Economics_Start
	cost = 4.29
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
		custom_trigger_tooltip = {
			tooltip = RFP_zone_2
			OR = {
				has_country_flag = RFP_cored_amur
				has_country_flag = RFP_cored_baikal
			}
		}
	}
	bypass = {
		
	}
	completion_reward = {
		add_political_power = 30
		add_popularity = {
			ideology = fascism
			popularity = 0.03
		}
	}
}
shared_focus = {
	id = RFP_2RCW_Economics_Decollectivization
	icon = GFX_focus_Agriculture
	prerequisite = {
		focus = RFP_2RCW_Economics_Start
	}
	x = 1
	y = 1
	relative_position_id = RFP_2RCW_Economics_Start
	cost = 2.15
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
		custom_trigger_tooltip = {
			tooltip = RFP_zone_1
			OR = {
				563 = {
					is_core_of = ROOT
				}
				408 = {
					is_core_of = ROOT
				}
			}
		}
	}
	bypass = {
		
	}
	completion_reward = {
		add_timed_idea = {
			idea = RUS_decollectivize
			days = 730
		}
	}
}
shared_focus = {
	id = RFP_2RCW_Economics_Creative_Labour
	icon = GFX_focus_Progressive_Taxation
	prerequisite = {
		focus = RFP_2RCW_Economics_Decollectivization
	}
	x = 1
	y = 2
	relative_position_id = RFP_2RCW_Economics_Start
	cost = 3
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
		custom_trigger_tooltip = {
			tooltip = RFP_zone_1
			OR = {
				563 = {
					is_core_of = ROOT
				}
				408 = {
					is_core_of = ROOT
				}
			}
		}
	}
	bypass = {
		
	}
	completion_reward = {
		add_ideas = RFP_creative_labour
	}
}
shared_focus = {
	id = RFP_2RCW_Economics_Resources
	icon = GFX_focus_Excavations
	prerequisite = {
		focus = RFP_2RCW_Economics_Creative_Labour
	}
	x = 1
	y = 3
	relative_position_id = RFP_2RCW_Economics_Start
	cost = 3
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
		custom_trigger_tooltip = {
			tooltip = RFP_zone_2
			OR = {
				has_country_flag = RFP_cored_amur
				has_country_flag = RFP_cored_baikal
			}
		}
	}
	bypass = {
		
	}
	completion_reward = {
		swap_ideas = {
			remove_idea = RFP_creative_labour
			add_idea = RFP_creative_labour_resources
		}
	}
}
shared_focus = {
	id = RFP_2RCW_Economics_Civ_Factory
	icon = GFX_focus_Industrialization
	prerequisite = {
		focus = RFP_2RCW_Economics_State_Arbitration
	}
	prerequisite = {
		focus = RFP_2RCW_Economics_Resources
	}
	x = -2
	y = 4
	relative_position_id = RFP_2RCW_Economics_Start
	cost = 7
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
		custom_trigger_tooltip = {
			tooltip = RFP_zone_2
			OR = {
				has_country_flag = RFP_cored_amur
				has_country_flag = RFP_cored_baikal
			}
		}
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = RFP_2RCW_Economics_Infrastructure
	icon = GFX_focus_Trains
	prerequisite = {
		focus = RFP_2RCW_Economics_State_Arbitration
	}
	prerequisite = {
		focus = RFP_2RCW_Economics_Resources
	}
	x = 0
	y = 4
	relative_position_id = RFP_2RCW_Economics_Start
	cost = 5.28
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
		custom_trigger_tooltip = {
			tooltip = RFP_zone_2
			OR = {
				has_country_flag = RFP_cored_amur
				has_country_flag = RFP_cored_baikal
			}
		}
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
			}
			#add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = RFP_2RCW_Economics_Arms_Factory
	icon = GFX_focus_Renewed_Arms
	prerequisite = {
		focus = RFP_2RCW_Economics_State_Arbitration
	}
	prerequisite = {
		focus = RFP_2RCW_Economics_Resources
	}
	x = 2
	y = 4
	relative_position_id = RFP_2RCW_Economics_Start
	cost = 6
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
		custom_trigger_tooltip = {
			tooltip = RFP_zone_2
			OR = {
				has_country_flag = RFP_cored_amur
				has_country_flag = RFP_cored_baikal
			}
		}
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = RFP_2RCW_Economics_Industry2
	icon = GFX_focus_Economic_Stimulus
	prerequisite = {
		focus = RFP_2RCW_Economics_Civ_Factory
	}
	prerequisite = {
		focus = RFP_2RCW_Economics_Infrastructure
	}
	mutually_exclusive = {
		focus = RFP_2RCW_Economics_Arms_Factory2
	}
	x = -1
	y = 5
	relative_position_id = RFP_2RCW_Economics_Start
	cost = 5.72
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
		custom_trigger_tooltip = {
			tooltip = RFP_zone_2
			OR = {
				has_country_flag = RFP_cored_amur
				has_country_flag = RFP_cored_baikal
			}
		}
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = RFP_2RCW_Economics_Arms_Factory2
	icon = GFX_focus_Firearms
	prerequisite = {
		focus = RFP_2RCW_Economics_Infrastructure
	}
	prerequisite = {
		focus = RFP_2RCW_Economics_Arms_Factory
	}
	mutually_exclusive = {
		focus = RFP_2RCW_Economics_Industry2
	}
	x = 1
	y = 5
	relative_position_id = RFP_2RCW_Economics_Start
	cost = 5.72
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
		custom_trigger_tooltip = {
			tooltip = RFP_zone_2
			OR = {
				has_country_flag = RFP_cored_amur
				has_country_flag = RFP_cored_baikal
			}
		}
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = RFP_2RCW_Economics_Arms_Factory3
	icon = GFX_focus_Urban_Warfare_HOI4
	prerequisite = {
		focus = RFP_2RCW_Economics_Industry2
		focus = RFP_2RCW_Economics_Arms_Factory2
	}
	x = 0
	y = 6
	relative_position_id = RFP_2RCW_Economics_Start
	cost = 5.72
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
		custom_trigger_tooltip = {
			tooltip = RFP_zone_3
			563 = {
				is_core_of = ROOT
			}
			408 = {
				is_core_of = ROOT
			}
			has_country_flag = RFP_cored_amur
			has_country_flag = RFP_cored_baikal
		}
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
