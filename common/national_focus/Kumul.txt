########################
# Kumul Tree           #
# By Roniius           #
########################

focus_tree = {
	id = kumul_focus

	country = {
		factor = 0

		modifier = {
			add = 10
			tag = KML
		}
	}

  focus = {
		id = KML_The_Kumul_Rebellion
		icon = GFX_focus_Civil_Unrest
		cost = 2

		completion_reward = {
		    add_political_power = 50
		}

		x = 8
		y = 0
	}

  focus = {
		id = KML_Fortify_Hami
		icon = GFX_focus_Fortification_Effort
		cost = 2

    prerequisite = {
      focus = KML_The_Kumul_Rebellion
    }

		completion_reward = {
		    #
		}

    relative_position_id = KML_The_Kumul_Rebellion
		x = -1
		y = 1
	}

  focus = {
		id = KML_Focus_on_the_Siege
		icon = GFX_focus_Urban_Warfare
		cost = 2

    prerequisite = {
      focus = KML_The_Kumul_Rebellion
    }

		completion_reward = {
		    #
		}

    relative_position_id = KML_The_Kumul_Rebellion
		x = 1
		y = 1
	}

  focus = {
		id = KML_Raise_Uyghur_Militia
		icon = GFX_focus_Military_Attache
		cost = 2

    prerequisite = {
      focus = KML_The_Kumul_Rebellion
    }

		completion_reward = {
		    #
		}

    relative_position_id = KML_The_Kumul_Rebellion
		x = 0
		y = 2
	}

  focus = {
		id = KML_Improve_Rebel_Morale
		icon = GFX_focus_hol_war_on_pacifism
		cost = 2

    prerequisite = {
      focus = KML_The_Kumul_Rebellion
    }

		completion_reward = {
		    add_war_support = 0.2
		}

    relative_position_id = KML_The_Kumul_Rebellion
		x = 3
		y = 1
	}

  focus = {
		id = KML_Riders_from_Gansu
		icon = GFX_focus_Cavalry
		cost = 2

    prerequisite = {
      focus = KML_Raise_Uyghur_Militia
    }

		completion_reward = {
		    #
		}

    relative_position_id = KML_Raise_Uyghur_Militia
		x = -1
		y = 1
	}

  focus = {
		id = KML_Strike_to_the_North
		icon = GFX_focus_Heavy_Strike
		cost = 2

    prerequisite = {
      focus = KML_Focus_on_the_Siege
    }

		completion_reward = {
		    #
		}

    relative_position_id = KML_Raise_Uyghur_Militia
		x = 1
		y = 1
	}
}
