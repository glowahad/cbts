#12 focuses here
#s_team337
shared_focus = {
	id = ARM_2RCW_Economics_Start
	icon = GFX_focus_Urban_Warfare_HOI4
	#prerequisite = { focus = ARM_2RCW_Economics_Start }
	x = 10
	y = 0
	#relative_position_id = ARM_2RCW_Economics_Start
	cost = 5.72
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		MOVE_UP_WAR_ECONOMY = yes
	}
}
shared_focus = {
	id = ARM_2RCW_Reform_Collective_Farms
	icon = GFX_focus_Bread_Trade
	prerequisite = {
		focus = ARM_2RCW_Economics_Start
	}
	mutually_exclusive = {
		focus = ARM_2RCW_Decollectivize
	}
	x = -1
	y = 1
	relative_position_id = ARM_2RCW_Economics_Start
	cost = 2.15
	ai_will_do = {
		factor = 4
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_timed_idea = {
			idea = ARM_collective_reform
			days = 548
		}
	}
}
shared_focus = {
	id = ARM_2RCW_Decollectivize
	icon = GFX_focus_Agriculture
	prerequisite = {
		focus = ARM_2RCW_Economics_Start
	}
	mutually_exclusive = {
		focus = ARM_2RCW_Reform_Collective_Farms
	}
	x = 1
	y = 1
	relative_position_id = ARM_2RCW_Economics_Start
	cost = 2.15
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_timed_idea = {
			idea = RUS_decollectivize
			days = 730
		}
	}
}
shared_focus = {
	id = ARM_Disband_State_Farms
	icon = GFX_focus_Agriculture_2
	prerequisite = {
		focus = ARM_2RCW_Reform_Collective_Farms
		focus = ARM_2RCW_Decollectivize
	}
	x = -1
	y = 2
	relative_position_id = ARM_2RCW_Economics_Start
	cost = 3
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
		controls_state = 230
	}
	bypass = {
		
	}
	completion_reward = {
		add_political_power = 30
		230 = {
			add_extra_state_shared_building_slots = 1
		}
	}
}
shared_focus = {
	id = ARM_Mixed_Economy
	icon = GFX_focus_Fundraising
	prerequisite = {
		focus = ARM_2RCW_Reform_Collective_Farms
		focus = ARM_2RCW_Decollectivize
	}
	x = 1
	y = 2
	relative_position_id = ARM_2RCW_Economics_Start
	cost = 3
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_ideas = ARM_people_market
		hidden_effect = {
			country_event = {
				id = arm_internal.4
				days = 186
			}
		}
	}
}
shared_focus = {
	id = ARM_Foreign_Loans
	icon = GFX_focus_Money_Bags
	prerequisite = {
		focus = ARM_2RCW_Reform_Collective_Farms
		focus = ARM_2RCW_Decollectivize
	}
	x = 0
	y = 3
	relative_position_id = ARM_2RCW_Economics_Start
	cost = 4.29
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_political_power = 60
	}
}
shared_focus = {
	id = ARM_Foreign_Weapons
	icon = GFX_focus_Firearms
	prerequisite = {
		focus = ARM_Foreign_Loans
	}
	x = 0
	y = 4
	relative_position_id = ARM_2RCW_Economics_Start
	cost = 4
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_political_power = -40
		add_equipment_to_stockpile = {
			type = infantry_equipment
			amount = 120
			producer = ARM
		}
	}
}
shared_focus = {
	id = ARM_Mil_Factory
	icon = GFX_focus_Mechanization
	prerequisite = {
		focus = ARM_Disband_State_Farms
	}
	x = -2
	y = 3
	relative_position_id = ARM_2RCW_Economics_Start
	cost = 6
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = arms_factory
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = arms_factory
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ARM_Steel
	icon = GFX_focus_generic_steel
	prerequisite = {
		focus = ARM_Mil_Factory
	}
	x = -2
	y = 4
	relative_position_id = ARM_2RCW_Economics_Start
	cost = 5.72
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
		controls_state = 230
	}
	bypass = {
		
	}
	completion_reward = {
		230 = {
			add_resource = {
				type = steel
				amount = 4
			}
		}
	}
}
shared_focus = {
	id = ARM_Infrastructure
	icon = GFX_focus_Trains
	prerequisite = {
		focus = ARM_Mixed_Economy
	}
	x = 2
	y = 3
	relative_position_id = ARM_2RCW_Economics_Start
	cost = 5.72
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = infrastructure
					size > 1
					include_locked = yes
				}
			}
			#add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = infrastructure
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ARM_Civilian_Industry
	icon = GFX_focus_Industrialization
	prerequisite = {
		focus = ARM_Infrastructure
	}
	x = 2
	y = 4
	relative_position_id = ARM_2RCW_Economics_Start
	cost = 7
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		random_owned_controlled_state = {
			limit = {
				is_controlled_by = ROOT
				free_building_slots = {
					building = industrial_complex
					size > 1
					include_locked = yes
				}
			}
			add_extra_state_shared_building_slots = 1
			add_building_construction = {
				type = industrial_complex
				level = 1
				instant_build = yes
			}
		}
	}
}
shared_focus = {
	id = ARM_Industrial_Tech
	icon = GFX_focus_Economic_Self_Reliance
	prerequisite = {
		focus = ARM_Steel
	}
	prerequisite = {
		focus = ARM_Foreign_Weapons
	}
	prerequisite = {
		focus = ARM_Civilian_Industry
	}
	x = 0
	y = 5
	relative_position_id = ARM_2RCW_Economics_Start
	cost = 5
	ai_will_do = {
		factor = 16
	}
	available = {
		has_global_flag = Russian_Civil_War
	}
	bypass = {
		
	}
	completion_reward = {
		add_tech_bonus = {
			name = industry_bonus
			uses = 1
			bonus = 0.4
			category = industry
		}
	}
}
