JAP_INF_01 = {
	name = "Army Divisions"
	for_countries = { JAP }
	can_use = { always = yes }
	division_types = { "infantry" }
	# Number reservation system will tie to another group.
	fallback_name = "Dai-%d Shidan"
	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "Dai-%d 'Gyoku' Shidan" }
		2 = { "Dai-%d 'Isamu' Shidan" }
		3 = { "Dai-%d 'Sachi' Shidan" }
		4 = { "Dai-%d 'Yodo' Shidan" }
		5 = { "Dai-%d 'Koi' Shidan" }
		6 = { "Dai-%d 'Akari' Shidan" }
		7 = { "Dai-%d 'Kuma' Shidan" }
		8 = { "Dai-%d 'Sugi' Shidan" }
		9 = { "Dai-%d 'Take' Shidan" }
		10 = { "Dai-%d 'Tetsu' Shidan" }
		11 = { "Dai-%d 'Nishiki' Shidan" }
		12 = { "Dai-%d 'Ken' Shidan" }
		13 = { "Dai-%d 'Kagami' Shidan" }
		14 = { "Dai-%d 'Teru' Shidan" }
		15 = { "Dai-%d 'Matsuri' Shidan" }
		16 = { "Dai-%d 'Kaki' Shidan" }
		17 = { "Dai-%d 'Tsuki' Shidan" }
		18 = { "Dai-%d 'Kiku' Shidan" }
		19 = { "Dai-%d 'Tora' Shidan" }
		20 = { "Dai-%d 'Asa' Shidan" }
		21 = { "Dai-%d 'Utsu' Shidan" }
		22 = { "Dai-%d 'Hara' Shidan" }
		23 = { "Dai-%d 'Asahi' Shidan" }
		24 = { "Dai-%d 'Yama' Shidan" }
		25 = { "Dai-%d 'Kuni' Shidan" }
		26 = { "Dai-%d 'Izumi' Shidan" }
		27 = { "Dai-%d 'Kiwame' Shidan" }
		28 = { "Dai-%d 'Toyo' Shidan" }
		29 = { "Dai-%d 'Rai' Shidan" }
		30 = { "Dai-%d 'Hyō' Shidan" }
		31 = { "Dai-%d 'Retsu' Shidan" }
		32 = { "Dai-%d 'Kaede' Shidan" }
		33 = { "Dai-%d 'Yumi' Shidan" }
		34 = { "Dai-%d 'Tsubaki' Shidan" }
		35 = { "Dai-%d 'Higashi' Shidan" }
		36 = { "Dai-%d 'Yuki' Shidan" }
		37 = { "Dai-%d 'Fuyu' Shidan" }
		38 = { "Dai-%d 'Numa' Shidan" }
		39 = { "Dai-%d 'Fuji' Shidan" }
		40 = { "Dai-%d 'Kujira' Shidan" }
		41 = { "Dai-%d 'Kawa' Shidan" }
		42 = { "Dai-%d 'Isao' Shidan" }
		43 = { "Dai-%d 'Homare' Shidan" }
		44 = { "Dai-%d 'Tachibana' Shidan" }
		46 = { "Dai-%d 'Sei' Shidan" }
		47 = { "Dai-%d 'Dan' Shidan" }
		48 = { "Dai-%d 'Umi' Shidan" }
		49 = { "Dai-%d 'Rō' Shidan" }
		50 = { "Dai-%d 'Hō' Shidan" }
		51 = { "Dai-%d 'Moto' Shidan" }
		52 = { "Dai-%d 'Kashiwa' Shidan" }
		53 = { "Dai-%d 'Yasushi' Shidan" }
		54 = { "Dai-%d 'Hei' Shidan" }
		55 = { "Dai-%d 'Sō' Shidan" }
		56 = { "Dai-%d 'Tatsu' Shidan" }
		57 = { "Dai-%d 'Oku' Shidan" }
		58 = { "Dai-%d 'Hiroshi' Shidan" }
		59 = { "Dai-%d 'Koromo' Shidan" }
		60 = { "Dai-%d 'Hoko' Shidan" }
		61 = { "Dai-%d 'Tobi' Shidan" }
		62 = { "Dai-%d 'Ishi' Shidan" }
		63 = { "Dai-%d 'Jin' Shidan" }
		64 = { "Dai-%d 'Kai' Shidan" }
		65 = { "Dai-%d 'Sen' Shidan" }
		66 = { "Dai-%d 'Kan' Shidan" }
		68 = { "Dai-%d 'Hinoki' Shidan" }
		69 = { "Dai-%d 'Katsu' Shidan" }
		70 = { "Dai-%d 'Yari' Shidan" }
		71 = { "Dai-%d 'Mikoto' Shidan" }
		72 = { "Dai-%d 'Den' Shidan" }
		73 = { "Dai-%d 'Ikari' Shidan" }
		77 = { "Dai-%d 'Nen' Shidan" }
		79 = { "Dai-%d 'So' Shidan" }
		81 = { "Dai-%d 'No' Shidan" }
		84 = { "Dai-%d 'Totsu' Shidan" }
		86 = { "Dai-%d 'Seki' Shidan" }
		88 = { "Dai-%d 'Kaname' Shidan" }
		89 = { "Dai-%d 'Sai' Shidan" }
		91 = { "Dai-%d 'Saki' Shidan" }
		93 = { "Dai-%d 'Ketsu' Shidan" }
		94 = { "Dai-%d 'Iretsu' Shidan" }
		96 = { "Dai-%d 'Gen' Shidan" }
		100 = { "Dai-%d 'Kyō' Shidan" }
		101 = { "Dai-%d Shidan" }
		102 = { "Dai-%d 'Batsu' Shidan" }
		103 = { "Dai-%d 'Shun' Shidan" }
		104 = { "Dai-%d 'Ōtori' Shidan" }
		105 = { "Dai-%d 'Kin' Shidan" }
		106 = { "Dai-%d Shidan" }
		107 = { "Dai-%d 'Nagi' Shidan" }
		108 = { "Dai-%d 'Yū' Shidan" }
		109 = { "Dai-%d 'Tan' Shidan" }
		110 = { "Dai-%d 'Sagi' Shidan" }
		111 = { "Dai-%d 'Ichi' Shidan" }
		112 = { "Dai-%d 'Kimi' Shidan" }
		114 = { "Dai-%d 'Shō' Shidan" }
		115 = { "Dai-%d 'Kita' Shidan" }
		116 = { "Dai-%d 'Ran' Shidan" }
		117 = { "Dai-%d 'Gu' Shidan" }
		118 = { "Dai-%d 'Megumi' Shidan" }
		119 = { "Dai-%d 'Sai' Shidan" }
		120 = { "Dai-%d 'Maishin' Shidan" }
		121 = { "Dai-%d 'Eiko' Shidan" }
		122 = { "Dai-%d 'Maizuru' Shidan" }
		123 = { "Dai-%d 'Matsukaze' Shidan" }
		124 = { "Dai-%d 'Enbō' Shidan" }
		125 = { "Dai-%d 'Eiki' Shidan" }
		127 = { "Dai-%d 'Eimai' Shidan" }
		128 = { "Dai-%d 'Eibu' Shidan" }
		129 = { "Dai-%d 'Shinbu' Shidan" }
		130 = { "Dai-%d 'Shōki' Shidan" }
		131 = { "Dai-%d 'Shūsui' Shidan" }
		132 = { "Dai-%d 'Shinki' Shidan" }
		133 = { "Dai-%d 'Shingeki' Shidan" }
		134 = { "Dai-%d 'Magatama' Shidan" }
		135 = { "Dai-%d 'Shinshin' Shidan" }
		136 = { "Dai-%d 'Fubatsu' Shidan" }
		137 = { "Dai-%d 'Fuyoku' Shidan" }
		138 = { "Dai-%d 'Fudō' Shidan" }
		139 = { "Dai-%d 'Fukutsu' Shidan" }
		140 = { "Dai-%d 'Gotō' Shidan" }
		142 = { "Dai-%d 'Gosen' Shidan" }
		143 = { "Dai-%d 'Gogo' Shidan" }
		144 = { "Dai-%d 'Gohan' Shidan" }
		145 = { "Dai-%d 'Goshu' Shidan" }
		146 = { "Dai-%d 'Gonan' Shidan" }
		147 = { "Dai-%d 'Gohoku' Shidan" }
		148 = { "Dai-%d 'Fugaku' Shidan" }
		149 = { "Dai-%d 'Fugyo' Shidan" }
		150 = { "Dai-%d 'Gocho' Shidan" }
		151 = { "Dai-%d 'Gō' Shidan" }
		152 = { "Dai-%d 'Gotaku' Shidan" }
		153 = { "Dai-%d 'Gokyo' Shidan" }
		154 = { "Dai-%d 'Goro' Shidan" }
		155 = { "Dai-%d 'Godo' Shidan" }
		156 = { "Dai-%d 'Gosai' Shidan" }
		157 = { "Dai-%d 'Gogu' Shidan" }
		158 = { "Dai-%d 'Fumetsu' Shidan" }
		160 = { "Dai-%d 'Gosen' Shidan" }
		161 = { "Dai-%d 'Shinten' Shidan" }
		201 = { "Dai-%d 'Musashi' Shidan" }
		202 = { "Dai-%d 'Aoba' Shidan" }
		205 = { "Dai-%d 'Aki' Shidan" }
		206 = { "Dai-%d 'Aso' Shidan" }
		209 = { "Dai-%d 'Kaetsu' Shidan" }
		212 = { "Dai-%d 'Kikuchi' Shidan" }
		214 = { "Dai-%d 'Tokiwa' Shidan" }
		216 = { "Dai-%d 'Hiei' Shidan" }
		221 = { "Dai-%d 'Tenryū' Shidan" }
		222 = { "Dai-%d 'Hakkou' Shidan" }
		224 = { "Dai-%d 'Ako' Shidan" }
		225 = { "Dai-%d 'Kongo' Shidan" }
		229 = { "Dai-%d 'Hokuetsu' Shidan" }
		230 = { "Dai-%d 'Sobu' Shidan" }
		231 = { "Dai-%d 'Ookuni' Shidan" }
		234 = { "Dai-%d 'Tone' Shidan" }
		303 = { "Dai-%d 'Takashi' Shidan" }
		308 = { "Dai-%d 'Iwaki' Shidan" }
		312 = { "Dai-%d 'Chitose' Shidan" }
		316 = { "Dai-%d 'Yamashiro' Shidan" }
		320 = { "Dai-%d 'Senbu' Shidan" }
		321 = { "Dai-%d 'Iso' Shidan" }
		322 = { "Dai-%d 'Bantai' Shidan" }
		344 = { "Dai-%d 'Kenzan' Shidan" }
		351 = { "Dai-%d 'Akagi' Shidan" }
		354 = { "Dai-%d 'Bukou' Shidan" }
		355 = { "Dai-%d 'Nachi' Shidan" }
	}
}
JAP_INF_02 = {
	name = "Konoe Division"
	for_countries = { JAP }
	can_use = { always = yes }
	division_types = { "infantry" }
	# Number reservation system will tie to another group.
	fallback_name = "Konoe Dai-%d Shidan"
	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = { 
		1 = { "Konoe Dai-%d Shidan" }
		2 = { "Konoe Dai-%d Shidan" }
		3 = { "Konoe Dai-%d Shidan" }
		4 = { "Konoe Dai-%d Shidan" }
		5 = { "Konoe Dai-%d Shidan" }
		6 = { "Konoe Dai-%d Shidan" }
		7 = { "Konoe Dai-%d Shidan" }
		8 = { "Konoe Dai-%d Shidan" }
		9 = { "Konoe Dai-%d Shidan" }
		10 = { "Konoe Dai-%d Shidan" }
		11 = { "Konoe Dai-%d Shidan" }
		12 = { "Konoe Dai-%d Shidan" }
		13 = { "Konoe Dai-%d Shidan" }
		14 = { "Konoe Dai-%d Shidan" }
		15 = { "Konoe Dai-%d Shidan" }
		16 = { "Konoe Dai-%d Shidan" }
		17 = { "Konoe Dai-%d Shidan" }
		18 = { "Konoe Dai-%d Shidan" }
		19 = { "Konoe Dai-%d Shidan" }
		20 = { "Konoe Dai-%d Shidan" }
		21 = { "Konoe Dai-%d Shidan" }
		22 = { "Konoe Dai-%d Shidan" }
		23 = { "Konoe Dai-%d Shidan" }
		24 = { "Konoe Dai-%d Shidan" }
		25 = { "Konoe Dai-%d Shidan" }
		26 = { "Konoe Dai-%d Shidan" }
		27 = { "Konoe Dai-%d Shidan" }
		28 = { "Konoe Dai-%d Shidan" }
		29 = { "Konoe Dai-%d Shidan" }
		30 = { "Konoe Dai-%d Shidan" }
	}
}
JAP_CAV_01 = {
	name = "Cavalry Division"
	for_countries = { JAP }
	can_use = { always = yes }
	division_types = { "cavalry" }
	# Number reservation system will tie to another group.
	fallback_name = "%d Kihei Shidan"
	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "%d Kihei Shidan" }
		2 = { "%d Kihei Shidan" }
		3 = { "%d Kihei Shidan" }
		4 = { "%d Kihei Shidan" }
		5 = { "%d Kihei Shidan" }
		6 = { "%d Kihei Shidan" }
		7 = { "%d Kihei Shidan" }
		8 = { "%d Kihei Shidan" }
		9 = { "%d Kihei Shidan" }
		10 = { "%d Kihei Shidan" }
		11 = { "%d Kihei Shidan" }
		12 = { "%d Kihei Shidan" }
		13 = { "%d Kihei Shidan" }
		14 = { "%d Kihei Shidan" }
		15 = { "%d Kihei Shidan" }
		16 = { "%d Kihei Shidan" }
		17 = { "%d Kihei Shidan" }
		18 = { "%d Kihei Shidan" }
		19 = { "%d Kihei Shidan" }
		20 = { "%d Kihei Shidan" }
	}
}
JAP_MOT_01 = {
	name = "Motorized Division"
	for_countries = { JAP }
	can_use = { always = yes }
	division_types = { "motorized" }
	# Number reservation system will tie to another group.
	fallback_name = "Jidōshaka Dai-%d Shidan"
	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered ={ 
		1 = { "Jidōshaka Dai-%d Shidan" }
		2 = { "Jidōshaka Dai-%d Shidan" }
		3 = { "Jidōshaka Dai-%d Shidan" }
		4 = { "Jidōshaka Dai-%d Shidan" }
		5 = { "Jidōshaka Dai-%d Shidan" }
		6 = { "Jidōshaka Dai-%d Shidan" }
		7 = { "Jidōshaka Dai-%d Shidan" }
		8 = { "Jidōshaka Dai-%d Shidan" }
		9 = { "Jidōshaka Dai-%d Shidan" }
		10 = { "Jidōshaka Dai-%d Shidan" }
		11 = { "Jidōshaka Dai-%d Shidan" }
		12 = { "Jidōshaka Dai-%d Shidan" }
		13 = { "Jidōshaka Dai-%d Shidan" }
		14 = { "Jidōshaka Dai-%d Shidan" }
		15 = { "Jidōshaka Dai-%d Shidan" }
		16 = { "Jidōshaka Dai-%d Shidan" }
		17 = { "Jidōshaka Dai-%d Shidan" }
		18 = { "Jidōshaka Dai-%d Shidan" }
		19 = { "Jidōshaka Dai-%d Shidan" }
		20 = { "Jidōshaka Dai-%d Shidan" }
		21 = { "Jidōshaka Dai-%d Shidan" }
		22 = { "Jidōshaka Dai-%d Shidan" }
		23 = { "Jidōshaka Dai-%d Shidan" }
		24 = { "Jidōshaka Dai-%d Shidan" }
		25 = { "Jidōshaka Dai-%d Shidan" }
		26 = { "Jidōshaka Dai-%d Shidan" }
		27 = { "Jidōshaka Dai-%d Shidan" }
		28 = { "Jidōshaka Dai-%d Shidan" }
		29 = { "Jidōshaka Dai-%d Shidan" }
		30 = { "Jidōshaka Dai-%d Shidan" }
	}
}
JAP_MEC_01 = {
	name = "Mechanized Division"
	for_countries = { JAP }
	can_use = { always = yes }
	division_types = { "mechanized" }
	# Number reservation system will tie to another group.
	fallback_name = "Kikaika Dai-%d Shidan"
	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "Kikaika Dai-%d Shidan" }
		2 = { "Kikaika Dai-%d Shidan" }
		3 = { "Kikaika Dai-%d Shidan" }
		4 = { "Kikaika Dai-%d Shidan" }
		5 = { "Kikaika Dai-%d Shidan" }
		6 = { "Kikaika Dai-%d Shidan" }
		7 = { "Kikaika Dai-%d Shidan" }
		8 = { "Kikaika Dai-%d Shidan" }
		9 = { "Kikaika Dai-%d Shidan" }
		10 = { "Kikaika Dai-%d Shidan" }
		11 = { "Kikaika Dai-%d Shidan" }
		12 = { "Kikaika Dai-%d Shidan" }
		13 = { "Kikaika Dai-%d Shidan" }
		14 = { "Kikaika Dai-%d Shidan" }
		15 = { "Kikaika Dai-%d Shidan" }
		16 = { "Kikaika Dai-%d Shidan" }
		17 = { "Kikaika Dai-%d Shidan" }
		18 = { "Kikaika Dai-%d Shidan" }
		19 = { "Kikaika Dai-%d Shidan" }
		20 = { "Kikaika Dai-%d Shidan" }
		21 = { "Kikaika Dai-%d Shidan" }
		22 = { "Kikaika Dai-%d Shidan" }
		23 = { "Kikaika Dai-%d Shidan" }
		24 = { "Kikaika Dai-%d Shidan" }
		25 = { "Kikaika Dai-%d Shidan" }
		26 = { "Kikaika Dai-%d Shidan" }
		27 = { "Kikaika Dai-%d Shidan" }
		28 = { "Kikaika Dai-%d Shidan" }
		29 = { "Kikaika Dai-%d Shidan" }
		30 = { "Kikaika Dai-%d Shidan" }
	}
}
JAP_ARM_01 = {
	name = "Armored Division"
	for_countries = { JAP }
	can_use = { always = yes }
	division_types = { "light_armor" "medium_armor" "heavy_armor" "modern_armor" }
	# Number reservation system will tie to another group.
	#link_numbering_with = { JAP_INF_01 }
	fallback_name = "Sensha Dai-%d Shidan"
	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "Sensha Dai-%d 'Taku' Shidan" }
		2 = { "Sensha Dai-%d 'Geki' Shidan" }
		3 = { "Sensha Dai-%d 'Taki' Shidan" }
		4 = { "Sensha Dai-%d 'Hagane' Shidan" }
		5 = { "Sensha Dai-%d Shidan" }
		6 = { "Sensha Dai-%d Shidan" }
		7 = { "Sensha Dai-%d Shidan" }
		8 = { "Sensha Dai-%d Shidan" }
		9 = { "Sensha Dai-%d Shidan" }
		10 = { "Sensha Dai-%d Shidan" }
		11 = { "Sensha Dai-%d Shidan" }
		12 = { "Sensha Dai-%d Shidan" }
		13 = { "Sensha Dai-%d Shidan" }
		14 = { "Sensha Dai-%d Shidan" }
		15 = { "Sensha Dai-%d Shidan" }
		16 = { "Sensha Dai-%d Shidan" }
		17 = { "Sensha Dai-%d Shidan" }
		18 = { "Sensha Dai-%d Shidan" }
		19 = { "Sensha Dai-%d Shidan" }
		20 = { "Sensha Dai-%d Shidan" }
		21 = { "Sensha Dai-%d Shidan" }
		22 = { "Sensha Dai-%d Shidan" }
		23 = { "Sensha Dai-%d Shidan" }
		24 = { "Sensha Dai-%d Shidan" }
		25 = { "Sensha Dai-%d Shidan" }
		26 = { "Sensha Dai-%d Shidan" }
		27 = { "Sensha Dai-%d Shidan" }
		28 = { "Sensha Dai-%d Shidan" }
		29 = { "Sensha Dai-%d Shidan" }
		30 = { "Sensha Dai-%d Shidan" }
	}
}
JAP_MNT_01 = { # able to use if mountaineer division template made by army reform
	name = "Mountain Division"
	for_countries = { JAP }
	can_use = { always = no }
	division_types = { "mountaineers" }
	# Number reservation system will tie to another group.
	fallback_name = "Dai-%d Sangaku Shidan"
	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "Dai-%d Sangaku Shidan" }
		2 = { "Dai-%d Sangaku Shidan" }
		3 = { "Dai-%d Sangaku Shidan" }
		4 = { "Dai-%d Sangaku Shidan" }
		5 = { "Dai-%d Sangaku Shidan" }
		6 = { "Dai-%d Sangaku Shidan" }
		7 = { "Dai-%d Sangaku Shidan" }
		8 = { "Dai-%d Sangaku Shidan" }
		9 = { "Dai-%d Sangaku Shidan" }
		10 = { "Dai-%d Sangaku Shidan" }
		11 = { "Dai-%d Sangaku Shidan" }
		12 = { "Dai-%d Sangaku Shidan" }
		13 = { "Dai-%d Sangaku Shidan" }
		14 = { "Dai-%d Sangaku Shidan" }
		15 = { "Dai-%d Sangaku Shidan" }
		16 = { "Dai-%d Sangaku Shidan" }
		17 = { "Dai-%d Sangaku Shidan" }
		18 = { "Dai-%d Sangaku Shidan" }
		19 = { "Dai-%d Sangaku Shidan" }
		20 = { "Dai-%d Sangaku Shidan" }
	}
}
JAP_PAR_01 = {
	name = "Paratrooper Division"
	for_countries = { JAP }
	can_use = { always = yes }
	division_types = { "paratrooper" }
	# Number reservation system will tie to another group.
	fallback_name = "Dai-%d Teishin Shūdan"
	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "Dai-%d Teishin Shūdan" }
		2 = { "Dai-%d Teishin Shūdan" }
		3 = { "Dai-%d Teishin Shūdan" }
		4 = { "Dai-%d Teishin Shūdan" }
		5 = { "Dai-%d Teishin Shūdan" }
		6 = { "Dai-%d Teishin Shūdan" }
		7 = { "Dai-%d Teishin Shūdan" }
		8 = { "Dai-%d Teishin Shūdan" }
		9 = { "Dai-%d Teishin Shūdan" }
		10 = { "Dai-%d Teishin Shūdan" }
		11 = { "Dai-%d Teishin Shūdan" }
		12 = { "Dai-%d Teishin Shūdan" }
		13 = { "Dai-%d Teishin Shūdan" }
		14 = { "Dai-%d Teishin Shūdan" }
		15 = { "Dai-%d Teishin Shūdan" }
		16 = { "Dai-%d Teishin Shūdan" }
		17 = { "Dai-%d Teishin Shūdan" }
		18 = { "Dai-%d Teishin Shūdan" }
		19 = { "Dai-%d Teishin Shūdan" }
		20 = { "Dai-%d Teishin Shūdan" }
	}
}
JAP_MAR_01 = {
	name = "Marine Garrison Division"
	for_countries = { JAP }
	can_use = { always = yes }
	division_types = { "marine" }
	# Number reservation system will tie to another group.
	fallback_name = "Dai-%d Tokubetsu Konkyochitai"
	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1001 = { "Sasebo Tokubetsu Rikusentai" }
		1002 = { "Maizaru Tokubetsu Rikusentai" }
		1003 = { "Yokohama Tokubetsu Rikusentai" }
		1004 = { "Kure Tokubetsu Rikusentai" }
		1005 = { "Ryōjun Tokubetsu Rikusentai" }
		1006 = { "Jyōkai Tokubetsu Rikusentai" }
		1007 = { "Chōkō Tokubetsu Rikusentai" }
		1008 = { "Kankō Tokubetsu Rikusentai" }
		1009 = { "Kōshū Tokubetsu Rikusentai" }
		1010 = { "Uraziosutoku Tokubetsu Rikusentai" }
	}
}
JAP_MAR_02 = { # able to use if Marines are restructured, Meiji government disbanded Marines in 1876
	name = "Marine Division"
	for_countries = { JAP }
	can_use = { always = no }
	division_types = { "marine" }
	fallback_name = "Dai-%d Kaihei Shidan"
	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "Dai-%d Kaihei Shidan" }
		2 = { "Dai-%d Kaihei Shidan" }
		3 = { "Dai-%d Kaihei Shidan" }
		4 = { "Dai-%d Kaihei Shidan" }
		5 = { "Dai-%d Kaihei Shidan" }
		6 = { "Dai-%d Kaihei Shidan" }
		7 = { "Dai-%d Kaihei Shidan" }
		8 = { "Dai-%d Kaihei Shidan" }
		9 = { "Dai-%d Kaihei Shidan" }
		10 = { "Dai-%d Kaihei Shidan" }
		11 = { "Dai-%d Kaihei Shidan" }
		12 = { "Dai-%d Kaihei Shidan" }
		13 = { "Dai-%d Kaihei Shidan" }
		14 = { "Dai-%d Kaihei Shidan" }
		15 = { "Dai-%d Kaihei Shidan" }
		16 = { "Dai-%d Kaihei Shidan" }
		17 = { "Dai-%d Kaihei Shidan" }
		18 = { "Dai-%d Kaihei Shidan" }
		19 = { "Dai-%d Kaihei Shidan" }
		20 = { "Dai-%d Kaihei Shidan" }
	}
}
JAP_GAR_01 = {
	name = "Kenpeitai"
	for_countries = { JAP }
	can_use = { always = yes }
	division_types = { "infantry" }
	# Number reservation system will tie to another group.
	fallback_name = "Dai-%d Kenpeitai"
	# Names with numbers (only one number per entry). 
	# It's okay to have gaps in numbering.
	ordered = {
		1 = { "Dai-%d Kenpeitai" }
		2 = { "Dai-%d Kenpeitai" }
		3 = { "Dai-%d Kenpeitai" }
		4 = { "Dai-%d Kenpeitai" }
		5 = { "Dai-%d Kenpeitai" }
		6 = { "Dai-%d Kenpeitai" }
		7 = { "Dai-%d Kenpeitai" }
		8 = { "Dai-%d Kenpeitai" }
		9 = { "Dai-%d Kenpeitai" }
		10 = { "Dai-%d Kenpeitai" }
	}
}