##### YUGOSLAVIA NAME LISTS #####
### REGULAR DESTROYER NAMES###
YUG_DD_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_DESTROYERS
	
	for_countries = { YUG }
	
	type = ship
	ship_types = { ship_hull_light destroyer }
	
	fallback_name = "%d Razarač"
	
	unique = {
		"Beograd" "Ljubljana" "Zagreb"
	}
}

### LIGHT CRUISER NAMES###
YUG_CL_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CL

	for_countries = { YUG }

	type = ship
	ship_types = { ship_hull_cruiser light_cruiser }
	
	fallback_name = "%d Laka krstarica"

}

### HEAVY CRUISER NAMES###
YUG_CA_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CA

	for_countries = { YUG }

	type = ship
	ship_types = { ship_hull_cruiser heavy_cruiser }
	
	fallback_name = "%d Teška krstarica"

}

### BATTLESHIP NAMES ###
YUG_BB_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_BB

	for_countries = { YUG }

	type = ship
	ship_types = { ship_hull_heavy battleship }
	
	fallback_name = "%d Bojni brod"		

}

### BATTLECRUISER NAMES ###
YUG_BC_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_BC

	for_countries = { YUG }

	type = ship
	ship_types = { ship_hull_heavy battle_cruiser }
	
	fallback_name = "%d Bojni krstaš"		

}

### AIRCRAFT CARRIER NAMES ###
YUG_CV_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_CARRIERS

	for_countries = { YUG }

	type = ship
	ship_types = { ship_hull_carrier carrier }
	
	fallback_name = "%d Nosač aviona"	

}

### SUBMARINES ###
YUG_SS_HISTORICAL = {
	name = NAME_THEME_HISTORICAL_SUBMARINES

	for_countries = { YUG }

	type = ship
	ship_types = { ship_hull_submarine submarine }
	
	fallback_name = "%d Podmornica"
	
}

### THEME: YUGOSLAVIAN REGIONS ###
YUG_REGIONS = {
	name = NAME_THEME_REGIONS

	for_countries = { YUG }

	type = ship

	unique = {
		"Crna Gora" "Srbija" "Slovenija" "Bosna" "Hercegovina" "Vojvodina" "Hrvatska" "Slavonija" "Makedonija" 
		"Banat" "Bačka" "Baranja" "Šumadija" "Kosovo" "Krajina" "Metohija" "Srem" "Raška" "Prekmurje"
	}
}

## THEME: YUGOSLAVIAN CITIES ###
YUG_CITIES = {
	name = NAME_THEME_CITIES

	for_countries = { YUG }

	type = ship

	unique = {
		"Sarajevo" "Skoplje" "Split" "Priština" "Podgorica" "Novi Sad" "Niš" "Banja Luka" "Rijeka" "Maribor" "Mostar"
		"Osijek" "Zadar" "Kragujevac" "Tuzla" "Subotica" "Zenica" "Prizren" "Nikšić" "Bitola" "Čačak" "Kraljevo"
	}
}

## THEME: YUGOSLAVIAN RIVERS ###
YUG_RIVERS = {
	name = NAME_THEME_RIVERS

	for_countries = { YUG }

	type = ship

	unique = {
		"Dunav" "Tisa" "Drava" "Vrbas" "Sava" "Morava" "Drina" "Kupa" "Neretva" "Mura" "Tamiš" "Timok" "Kupa" "Drin"
		"Ibar" "Begej" "Kolpa" "Savinja" "Morača"
	}
}

## THEME: YUGOSLAVIAN KINGS ###
YUG_KINGS = {
	name = NAME_THEME_KINGS

	for_countries = { YUG }

	type = ship

	unique = {
		"Petar Karađorđević" "Stefan Dušan" "Tomislav Trpimirović" "Stefan Dragutin" 
		"Stefan Dečanski" "Branimir Domagojević" "Stefan Nemanja" "Herman II. Celjski"
		"Tvrtko Kotromanic"
	}
}